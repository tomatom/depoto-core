# Depoto Core


## Usage

* add dependency `yarn add git+ssh://git@bitbucket.org:tomatom/depoto-core.git`

* add reflect-metadata `yarn add reflect-metadata`, import once in main app file `import 'reflect-metadata'`


## Contributions

* after each commit run `yarn version`

* increment version, new commits and tags are pushed automatically
