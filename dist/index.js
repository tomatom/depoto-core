"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var services_1 = require("./src/services");
var entities_1 = require("./src/services/entities");
var utils_1 = require("./src/utils");
require("reflect-metadata");
var DepotoCore = /** @class */ (function () {
    function DepotoCore(appPrefix, session, httpService, loggerType, loggerFilters) {
        if (session === void 0) { session = null; }
        if (httpService === void 0) { httpService = null; }
        if (loggerType === void 0) { loggerType = services_1.LOGGER_TYPE.CONSOLE; }
        if (loggerFilters === void 0) { loggerFilters = []; }
        this.loggerService = new services_1.LoggerService(loggerType, loggerFilters);
        this.storageService = new services_1.StorageService(this.loggerService, appPrefix);
        this.oauthService = new services_1.OAuthService(this.loggerService, this.storageService, session);
        if (httpService) {
            this.httpService = httpService;
        }
        else {
            this.httpService = new services_1.FetchService(this.loggerService, this.oauthService);
        }
        if ('addEventListener' in window) {
            this.keyEventService = new services_1.KeyEventService(this.loggerService, true);
        }
        else {
            this.keyEventService = undefined;
        }
        var config = {
            loggerService: this.loggerService,
            s: this.storageService,
            oauthService: this.oauthService,
            httpService: this.httpService,
            notificationEventObservable: new utils_1.Observable('core:notificationEventObservable'),
        };
        this.vatService = new entities_1.VatService(config);
        this.userService = new entities_1.UserService(config);
        this.addressService = new entities_1.AddressService(config);
        this.baseEventService = new entities_1.BaseEventService(config);
        this.carrierService = new entities_1.CarrierService(config);
        this.categoryService = new entities_1.CategoryService(config);
        this.checkoutService = new entities_1.CheckoutService(config);
        this.companyService = new entities_1.CompanyService(config);
        this.companyCarrierService = new entities_1.CompanyCarrierService(config);
        this.consentService = new entities_1.ConsentService(config);
        this.customerService = new entities_1.CustomerService(config);
        this.depotService = new entities_1.DepotService(config);
        this.downloadService = new entities_1.DownloadService(config);
        this.eetCertService = new entities_1.EETCertService(config);
        this.eetReceiptService = new entities_1.EETReceiptService(config);
        this.eetShopService = new entities_1.EETShopService(config);
        this.exportService = new entities_1.ExportService(config);
        this.fileService = new entities_1.FileService(config);
        this.inventoryExportService = new entities_1.InventoryExportService(config);
        this.orderExportService = new entities_1.OrderExportService(config);
        this.orderService = new entities_1.OrderService(config, this.vatService);
        this.packageService = new entities_1.PackageService(config);
        this.parameterService = new entities_1.ParameterService(config);
        this.paymentService = new entities_1.PaymentService(config);
        this.priceLevelService = new entities_1.PriceLevelService(config);
        this.producerService = new entities_1.ProducerService(config);
        this.productBundleService = new entities_1.ProductBundleService(config);
        this.productMovePackService = new entities_1.ProductMovePackService(config);
        this.productPackService = new entities_1.ProductPackService(config);
        this.productMoveService = new entities_1.ProductMoveService(config);
        this.productParameterService = new entities_1.ProductParameterService(config);
        this.productPriceLevelService = new entities_1.ProductPriceLevelService(config);
        this.productPriceService = new entities_1.ProductPriceService(config);
        this.productService = new entities_1.ProductService(config);
        this.purchaseExportService = new entities_1.PurchaseExportService(config);
        this.saleExportService = new entities_1.SaleExportService(config);
        this.soldItemService = new entities_1.SoldItemService(config);
        this.statsService = new entities_1.StatsService(config);
        this.stockExportService = new entities_1.StockExportService(config);
        this.supplierService = new entities_1.SupplierService(config);
        this.tagService = new entities_1.TagService(config);
        this.userGroupService = new entities_1.UserGroupService(config);
        this.userRoleService = new entities_1.UserRoleService(config);
        this.voucherService = new entities_1.VoucherService(config);
    }
    Object.defineProperty(DepotoCore.prototype, "services", {
        get: function () {
            return {
                baseEvent: this.baseEventService,
                keyEvent: this.keyEventService,
                logger: this.loggerService,
                storage: this.storageService,
                oauth: this.oauthService,
                http: this.httpService,
                address: this.addressService,
                carrier: this.carrierService,
                category: this.categoryService,
                checkout: this.checkoutService,
                company: this.companyService,
                companyCarrier: this.companyCarrierService,
                consent: this.consentService,
                customer: this.customerService,
                depot: this.depotService,
                download: this.downloadService,
                eetCert: this.eetCertService,
                eetReceipt: this.eetReceiptService,
                eetShop: this.eetShopService,
                export: this.exportService,
                file: this.fileService,
                inventoryExport: this.inventoryExportService,
                orderExport: this.orderExportService,
                order: this.orderService,
                pack: this.packageService,
                parameter: this.parameterService,
                payment: this.paymentService,
                priceLevel: this.priceLevelService,
                producer: this.producerService,
                productBundle: this.productBundleService,
                productMovePack: this.productMovePackService,
                productPack: this.productPackService,
                productMove: this.productMoveService,
                productParameter: this.productParameterService,
                productPriceLevel: this.productPriceLevelService,
                productPrice: this.productPriceService,
                product: this.productService,
                purchaseExport: this.purchaseExportService,
                saleExport: this.saleExportService,
                soldItem: this.soldItemService,
                stats: this.statsService,
                stockExport: this.stockExportService,
                supplier: this.supplierService,
                tag: this.tagService,
                userGroup: this.userGroupService,
                userRole: this.userRoleService,
                user: this.userService,
                vat: this.vatService,
                voucher: this.voucherService
            };
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DepotoCore.prototype, "endpoints", {
        get: function () {
            return utils_1.ENDPOINT;
        },
        enumerable: false,
        configurable: true
    });
    return DepotoCore;
}());
exports.default = DepotoCore;
//# sourceMappingURL=index.js.map