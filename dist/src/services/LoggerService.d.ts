import { LoggerEvent } from '../models';
import { Observable } from '../utils';
export declare const LOGGER_TYPE: {
    CONSOLE: string;
    OBSERVABLE: string;
};
export declare const EVENT: {
    EXCEPTION: string;
    LOGGER: string;
    LOCAL_STORAGE: string;
    OAUTH: string;
    CHECKOUT: string;
    CUSTOMER: string;
    GRAPHQL: string;
    KEY: string;
    EET: string;
    EVENTS: string;
    HTTP: string;
    MESSAGE: string;
    ORDER: string;
    PRINT: string;
    PRODUCT: string;
    STATS: string;
    USER: string;
    VAT: string;
};
export declare class LoggerService {
    logger: Observable<string> | any;
    loggerType: string;
    filters: string[] | any;
    constructor(loggerType?: string | undefined, filters?: string[]);
    log(event: LoggerEvent): void;
    err(event: LoggerEvent): void;
    getStackTrace(): string | string[];
    protected isLoggable(event: LoggerEvent): boolean;
}
