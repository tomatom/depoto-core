"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StorageService = void 0;
var LoggerService_1 = require("./LoggerService");
var StorageService = /** @class */ (function () {
    function StorageService(loggerService, appPrefix) {
        this.loggerService = loggerService;
        this.appPrefix = appPrefix;
        this.asyncStorage = window['asyncStorage']; // RN @react-native-community/async-storage[https://github.com/react-native-community/async-storage/blob/LEGACY/docs/API.md]
    }
    Object.defineProperty(StorageService.prototype, "keys", {
        get: function () {
            return {
                auth: {
                    data: { key: 'auth_data', type: 'Object' },
                    rememberMe: { key: 'auth_remember_me', type: 'boolean' },
                    isAuthenticated: { key: 'auth_is_authenticated', type: 'boolean' },
                    email: { key: 'auth_email', type: 'string' },
                    clientType: { key: 'auth_client_type', type: 'string' },
                    customServerUrl: { key: 'auth_custom_server_url', type: 'string' },
                    company: { key: 'auth_company', type: 'Object' },
                    isCompanyReloaded: { key: 'auth_is_company_reloaded', type: 'boolean' },
                    companyCarriers: { key: 'auth_company_carriers', type: 'Array' },
                },
                app: {
                    env: { key: 'app_env', type: 'string' },
                    activeChildCompany: { key: 'app_active_child_company', type: 'Object' },
                    language: { key: 'app_language', type: 'string' },
                    sideMenuState: { key: 'app_sidemenu_state', type: 'string' },
                    untranslatedMessages: { key: 'app_untranslated_messages', type: 'Array' },
                    theme: { key: 'app_theme', type: 'string' },
                    options: { key: 'app_options', type: 'Object' },
                },
                routing: {
                    lastUrl: { key: 'routing_last_url', type: 'string' },
                    queries: { key: 'routing_queries', type: 'Array' },
                    lastOpenedSubTree: { key: 'last_opened_subtree', type: 'string' },
                },
                services: {
                    notification: { key: 'services_notification', type: 'Array' },
                    userRoles: { key: 'services_user_roles', type: 'Array' },
                },
                utils: {
                    history: { key: 'utils_history_history', type: 'Array' },
                    resizerLastMaxWidth: { key: 'utils_resizer_last_max_width', type: 'number' },
                },
                views: {
                    addresses: { key: 'views_addresses', type: 'Object' },
                    barcodePrintArray: { key: 'views_barcode_print_array', type: 'Object' },
                    baseListing: { key: 'views_base_listing', type: 'Object' },
                    batchOperations: { key: 'views_batch_operations', type: 'Object' },
                    companyCarriers: { key: 'views_company_carriers', type: 'Object' },
                    consents: { key: 'views_consents', type: 'Object' },
                    customers: { key: 'views_customers', type: 'Object' },
                    dashboard: { key: 'views_dashboard', type: 'Object' },
                    depots: { key: 'views_depots', type: 'Object' },
                    eetCerts: { key: 'views_eet_certs', type: 'Object' },
                    eetReceipts: { key: 'views_eet_receipts', type: 'Object' },
                    eetShops: { key: 'views_eet_shops', type: 'Object' },
                    checkouts: { key: 'views_checkouts', type: 'Object' },
                    lastStockItemSupplier: { key: 'views_last_stock_item_supplier', type: 'number' },
                    lastStockItemDepot: { key: 'views_last_stock_item_depot', type: 'number' },
                    lastStockItemPurchasePrice: { key: 'views_last_stock_item_purchase_price', type: 'string' },
                    modal: { key: 'views_modal', type: 'Object' },
                    modalPrintExports: { key: 'views_modal_print_exports', type: 'Object' },
                    parameters: { key: 'views_parameters', type: 'Object' },
                    payments: { key: 'views_payments', type: 'Object' },
                    priceLevels: { key: 'views_price_levels', type: 'Object' },
                    printInventoryExports: { key: 'views_print_inventory_exports', type: 'Object' },
                    printOrderExports: { key: 'views_print_order_exports', type: 'Object' },
                    printPurchaseExports: { key: 'views_print_purchase_exports', type: 'Object' },
                    printReports: { key: 'views_print_reports', type: 'Object' },
                    printSaleExports: { key: 'views_print_sale_exports', type: 'Object' },
                    printStockExports: { key: 'views_print_stock_exports', type: 'Object' },
                    producers: { key: 'views_producers', type: 'Object' },
                    productMoves: { key: 'views_product_moves', type: 'Object' },
                    productMovesList: { key: 'views_product_moves_list', type: 'Object' },
                    products: { key: 'views_products', type: 'Object' },
                    stockOperationCardSupplier: { key: 'views_stock_operation_card_supplier', type: 'number' },
                    stockOperationCardSelectData: { key: 'views_stock_operation_card_select_data', type: 'Object' },
                    stockOperationCardIsLockedQuantity: { key: 'views_stock_operation_card_is_locked_quantity', type: 'boolean' },
                    stockOperationCardLockedQuantity: { key: 'views_stock_operation_card_locked_quantity', type: 'number' },
                    stockOperationList: { key: 'views_stock_operation_list', type: 'Object' },
                    soldItems: { key: 'views_sold_items', type: 'Object' },
                    suppliers: { key: 'views_suppliers', type: 'Object' },
                    orders: { key: 'views_orders', type: 'Object' },
                    tags: { key: 'views_tags', type: 'Object' },
                    users: { key: 'views_users', type: 'Object' },
                    vats: { key: 'views_vats', type: 'Object' },
                    vouchers: { key: 'views_vouchers', type: 'Object' },
                },
            };
        },
        enumerable: false,
        configurable: true
    });
    StorageService.prototype.set = function (key, value) {
        var stringifiedValue = '';
        switch (key.type) {
            case 'string':
                stringifiedValue = value;
                break;
            case 'number':
            case 'boolean':
            case 'Object':
            case 'Array':
                stringifiedValue = JSON.stringify(value);
                break;
        }
        if ('localStorage' in window) {
            window.localStorage.setItem("" + this.appPrefix + key.key, stringifiedValue);
        }
        else if ('asyncStorage' in window) {
            this.asyncStorage.setItem("" + this.appPrefix + key.key, stringifiedValue);
        }
    };
    StorageService.prototype.get = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var stringifiedValue;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!('localStorage' in window)) return [3 /*break*/, 1];
                        return [2 /*return*/, new Promise(function (resolve) {
                                return resolve(_this.getSync(key));
                            })];
                    case 1:
                        if (!('asyncStorage' in window)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.asyncStorage.getItem("" + this.appPrefix + key.key)];
                    case 2:
                        stringifiedValue = _a.sent();
                        return [2 /*return*/, this.parseStringifiedData(key, stringifiedValue)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description for usage in desktop apps only
     * @param key StorageKey
     */
    StorageService.prototype.getSync = function (key) {
        if ('localStorage' in window) {
            var stringifiedValue = window.localStorage.getItem("" + this.appPrefix + key.key);
            return this.parseStringifiedData(key, stringifiedValue);
        }
    };
    StorageService.prototype.parseStringifiedData = function (key, stringifiedValue) {
        var value;
        if (stringifiedValue && stringifiedValue.length > 0) {
            switch (key.type) {
                case 'string':
                    value = stringifiedValue;
                    break;
                case 'boolean':
                    if (!stringifiedValue || stringifiedValue == 'undefined') {
                        value = false;
                        break;
                    } // else = fallthrough && try-catch json.parse
                case 'number':
                case 'Object':
                case 'Array':
                    try {
                        value = JSON.parse(stringifiedValue);
                    }
                    catch (e) {
                        console.warn("StorageUtil Exception: " + e.message);
                    }
                    if (key.type === 'number') {
                        value = isNaN(Number(value)) ? 0 : Number(value);
                    }
                    break;
            }
        }
        return value;
    };
    StorageService.prototype.clear = function (key) {
        var _this = this;
        this.loggerService.log({
            type: LoggerService_1.EVENT.LOCAL_STORAGE,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: key,
        });
        var storage;
        if ('localStorage' in window) {
            storage = window.localStorage;
        }
        else if ('asyncStorage' in window) {
            storage = this.asyncStorage;
        }
        if (key && key['key'] && key['type']) {
            storage.removeItem("" + this.appPrefix + key['key']);
        }
        else if (key && Object.keys(key).length > 0) {
            Object.keys(key).forEach(function (k) {
                storage.removeItem("" + _this.appPrefix + key[k]['key']);
            });
        }
        else if (key === null) {
            storage.clear();
        }
    };
    return StorageService;
}());
exports.StorageService = StorageService;
//# sourceMappingURL=StorageService.js.map