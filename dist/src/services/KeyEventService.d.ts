import { Observable } from '../utils';
import { LoggerService } from './';
export declare const KEY: {
    ENTER: number;
    ESC: number;
};
export declare type GS1Result = {
    barcode?: string;
    batch?: string;
    bestBeforeDate?: string;
    expirationDate?: string;
    internalValue?: string;
    sellPrice?: string;
    rawCode: string;
    parsedCode: any;
};
export declare class KeyEventService {
    private loggerService;
    onScannedBarcode: Observable<string>;
    onScannedBox: Observable<string>;
    onScannedGS1: Observable<GS1Result>;
    onScannedPositions: Observable<string>;
    onKeystroke: Observable<string>;
    onKeyEnter: Observable<boolean>;
    onKeyEsc: Observable<boolean>;
    protected isKeyPressed: boolean;
    protected isScannerActive: boolean;
    protected timeToScan: number;
    protected scannedChars: string[];
    constructor(loggerService: LoggerService, startListeners?: boolean);
    startListeners(debug?: boolean): void;
    stopListeners(): void;
    enableBarcodeScanner(): void;
    disableBarcodeScanner(): void;
    setTimeToScan(miliseconds: number): void;
    getTimeToScan(): number;
    protected catchKeystroke(event: any): void;
    protected catchBarcodeScanner(event: any): void;
    parseGS1(code: string): void;
    isObserved(): boolean;
    testBarcodeFromString(code: string): void;
}
