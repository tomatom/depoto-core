"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FetchService = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var FetchService = /** @class */ (function () {
    function FetchService(loggerService, oauthService) {
        this.loggerService = loggerService;
        this.oauthService = oauthService;
    }
    FetchService.prototype.getAuthorizationHeaders = function () {
        var token = this.oauthService.getAccessToken();
        var headers = new Headers();
        headers.append('Authorization', "Bearer " + token);
        headers.append('Accept', 'application/json');
        headers.append('Content-type', 'application/json');
        var session = this.oauthService.getSession();
        if (session && session.clientType === 'stage') {
            headers.set('Authorization', "Basic " + btoa('stage:stage') + ", Bearer " + token);
        }
        this.loggerService.log({
            type: _1.EVENT.HTTP,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: headers,
        });
        return headers;
    };
    FetchService.prototype.get = function (endpoint) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            fetch(utils_1.getEndpoint(endpoint, _this.oauthService.getSession()), {
                method: 'get',
                headers: _this.getAuthorizationHeaders(),
            })
                .then(function (resp) { return resp.json(); })
                .then(function (data) {
                resolve(data);
            }, function (error) {
                _this.loggerService.log({
                    type: _1.EVENT.HTTP,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: error,
                });
                reject(error);
            })
                .catch(function (error) {
                if (error.status === 401) {
                    _this.oauthService.getRefreshToken().then(function (success) {
                        if (success) {
                            resolve(_this.get(endpoint));
                        }
                        else {
                            _this.oauthService.logout();
                        }
                    });
                }
                reject(error);
            });
        });
    };
    FetchService.prototype.post = function (endpoint, reqBody) {
        var _this = this;
        var rawResp, origResp;
        return new Promise(function (resolve, reject) {
            fetch(utils_1.getEndpoint(endpoint, _this.oauthService.getSession()), {
                method: 'post',
                headers: _this.getAuthorizationHeaders(),
                body: typeof reqBody === 'string' ? reqBody : JSON.stringify(reqBody),
            })
                .then(function (resp) {
                origResp = resp;
                return resp.clone();
            })
                .then(function (clonedResp) {
                rawResp = clonedResp;
                return origResp.json();
            })
                .catch(function (e) { return rawResp.text().then(function (html) { return reject({ error: e, html: html }); }); })
                .then(function (data) {
                if (origResp.status !== 200) {
                    throw { status: origResp.status };
                }
                resolve(data);
            }, function (error) {
                _this.loggerService.log({
                    type: _1.EVENT.HTTP,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: error,
                });
                reject(error);
            })
                .catch(function (error) {
                if (error.status !== 401) {
                    _this.renderErrorDetail(error);
                }
                else if (error.status === 401) {
                    _this.oauthService.getRefreshToken().then(function (success) {
                        if (success) {
                            resolve(_this.post(endpoint, reqBody));
                        }
                        else {
                            _this.oauthService.logout();
                        }
                    });
                }
                reject(error);
            });
        });
    };
    FetchService.prototype.put = function (endpoint, reqBody) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            fetch(utils_1.getEndpoint(endpoint, _this.oauthService.getSession()), {
                method: 'put',
                headers: _this.getAuthorizationHeaders(),
                body: reqBody,
            })
                .then(function (resp) { return resp.json(); })
                .then(function (data) {
                resolve(data);
            }, function (error) {
                _this.loggerService.log({
                    type: _1.EVENT.HTTP,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: error,
                });
                reject(error);
            })
                .catch(function (error) {
                if (error.status === 401) {
                    _this.oauthService.getRefreshToken().then(function (success) {
                        if (success) {
                            resolve(_this.put(endpoint, reqBody));
                        }
                        else {
                            _this.oauthService.logout();
                        }
                    });
                }
                reject(error);
            });
        });
    };
    FetchService.prototype.downloadBlob = function (uri) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            fetch(uri, {
                method: 'post',
                headers: _this.getAuthorizationHeaders(),
            })
                .then(function (resp) { return resp.blob(); })
                .then(function (data) {
                resolve(data);
            }, function (error) {
                _this.loggerService.log({
                    type: _1.EVENT.HTTP,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: error,
                });
                reject(error);
            })
                .catch(function (error) {
                if (error.status === 401) {
                    _this.oauthService.getRefreshToken().then(function (success) {
                        if (success) {
                            resolve(_this.downloadBlob(uri));
                        }
                        else {
                            _this.oauthService.logout();
                        }
                    });
                }
                reject(error);
            });
        });
    };
    FetchService.prototype.renderErrorDetail = function (error) {
        if (!!window.location && (window.location.href.includes('local') || window.location.href.includes('stage'))) {
            alert('there was an error in response');
            if (typeof document !== 'undefined') {
                var errEl = document.createElement('div');
                errEl.innerHTML = error._body;
                var targetEl = document.getElementById('err-detail');
                if (!!targetEl) {
                    targetEl.appendChild(errEl);
                }
            }
            console.warn(error);
        }
    };
    return FetchService;
}());
exports.FetchService = FetchService;
//# sourceMappingURL=FetchService.js.map