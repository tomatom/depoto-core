import { OAuthSession } from '../models';
import { LoggerService, StorageService } from './';
import { Observable } from '../utils';
export declare class OAuthService {
    private readonly loggerService;
    private readonly s;
    session: OAuthSession;
    defaultClientKeys: any;
    onLoginEmitter: Observable<string>;
    onLogoutEmitter: Observable<string>;
    constructor(loggerService: LoggerService, s: StorageService, session?: OAuthSession | any | null);
    getToken(session: OAuthSession): Promise<boolean>;
    getRefreshToken(): Promise<boolean>;
    isAuthenticated(): boolean;
    getAccessToken(): string | undefined;
    getSession(): OAuthSession;
    logout(): void;
    saveData(): void;
    clearData(): void;
    loadData(): Promise<any>;
    loadDataSync(): any;
}
