"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KeyEventService = exports.KEY = void 0;
var gs1_barcode_parser_mod_1 = require("gs1-barcode-parser-mod");
var utils_1 = require("../utils");
var utils_2 = require("../utils");
var _1 = require("./");
exports.KEY = {
    ENTER: 13,
    ESC: 27,
};
var KeyEventService = /** @class */ (function () {
    function KeyEventService(loggerService, startListeners) {
        if (startListeners === void 0) { startListeners = false; }
        this.loggerService = loggerService;
        this.onScannedBarcode = new utils_1.Observable('core:keyEventService:onScannedBarcode');
        this.onScannedBox = new utils_1.Observable('core:keyEventService:onScannedBox');
        this.onScannedGS1 = new utils_1.Observable('core:keyEventService:onScannedGS1');
        this.onScannedPositions = new utils_1.Observable('core:keyEventService:onScannedPositions');
        this.onKeystroke = new utils_1.Observable('core:keyEventService:onKeystroke');
        this.onKeyEnter = new utils_1.Observable('core:keyEventService:onKeyEnter');
        this.onKeyEsc = new utils_1.Observable('core:keyEventService:onKeyEsc');
        this.isKeyPressed = false;
        this.isScannerActive = true;
        this.timeToScan = 300;
        this.scannedChars = [];
        if (startListeners) {
            this.startListeners();
        }
    }
    KeyEventService.prototype.startListeners = function (debug) {
        var _this = this;
        if (debug === void 0) { debug = false; }
        window.onkeypress = function (event) { return _this.catchBarcodeScanner(event); };
        window.addEventListener('keydown', function (event) { return _this.catchKeystroke(event); });
        if (debug) {
            // Proxies are great for debugging integration, but are costly. Disable for performance reasons
            window.onkeypress = new Proxy(window.onkeypress, {
                apply: function (target, thisArg, args) {
                    _this.loggerService.log({
                        type: _1.EVENT.KEY,
                        origin: _this.constructor['name'],
                        trace: _this.loggerService.getStackTrace(),
                        data: {
                            message: 'KeyEventService:on key press',
                            target: target,
                            this: thisArg,
                            args: args,
                        },
                    });
                },
            });
            window.addEventListener = new Proxy(window.addEventListener, {
                apply: function (target, thisArg, args) {
                    if (args[0] === 'keydown') {
                        _this.loggerService.log({
                            type: _1.EVENT.KEY,
                            origin: _this.constructor['name'],
                            trace: _this.loggerService.getStackTrace(),
                            data: {
                                message: 'addEventListener:key down',
                                target: target,
                                this: thisArg,
                                args: args,
                            },
                        });
                    }
                },
            });
            window.removeEventListener = new Proxy(window.removeEventListener, {
                apply: function (target, thisArg, args) {
                    if (args[0] === 'keydown') {
                        _this.loggerService.log({
                            type: _1.EVENT.KEY,
                            origin: _this.constructor['name'],
                            trace: _this.loggerService.getStackTrace(),
                            data: {
                                message: 'removeEventListener:key down',
                                target: target,
                                this: thisArg,
                                args: args,
                            },
                        });
                    }
                },
            });
        }
        this.loggerService.log({
            type: _1.EVENT.KEY,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                message: 'KeyEventService:all listeners started',
                debug: debug,
            },
        });
        window['testBarcodeFromString'] = function (c) { return _this.testBarcodeFromString(c); };
    };
    KeyEventService.prototype.stopListeners = function () {
        window.onkeypress = function () { return null; };
        window.removeEventListener('keydown', this.catchKeystroke);
        this.onScannedBarcode.unregisterObservers();
        this.onScannedBox.unregisterObservers();
        this.onScannedGS1.unregisterObservers();
        this.onScannedPositions.unregisterObservers();
        this.onKeystroke.unregisterObservers();
        this.onKeyEnter.unregisterObservers();
        this.onKeyEsc.unregisterObservers();
        this.loggerService.log({
            type: _1.EVENT.KEY,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                message: 'KeyEventService:all listeners stopped',
            },
        });
    };
    KeyEventService.prototype.enableBarcodeScanner = function () {
        this.isScannerActive = true;
    };
    KeyEventService.prototype.disableBarcodeScanner = function () {
        this.isScannerActive = false;
    };
    KeyEventService.prototype.setTimeToScan = function (miliseconds) {
        this.timeToScan = isNaN(miliseconds) || miliseconds < 300 ? 300 : miliseconds;
    };
    KeyEventService.prototype.getTimeToScan = function () {
        return this.timeToScan;
    };
    KeyEventService.prototype.catchKeystroke = function (event) {
        if (this.scannedChars.length < 3) {
            // filter out scanner
            if (event.keyCode === exports.KEY.ENTER && this.onKeyEnter.isObserved()) {
                this.onKeyEnter.emit(true);
            }
            if (event.keyCode === exports.KEY.ESC && this.onKeyEsc.isObserved()) {
                this.onKeyEsc.emit(true);
            }
        }
        if (this.onKeystroke.isObserved()) {
            this.onKeystroke.emit(String.fromCharCode(event.which));
        }
    };
    KeyEventService.prototype.catchBarcodeScanner = function (event) {
        var _this = this;
        if (!this.isObserved()) {
            return;
        }
        this.scannedChars.push(String.fromCharCode(event.which));
        if (!this.isKeyPressed) {
            setTimeout(function () {
                if (_this.scannedChars.length >= 4) {
                    var code = utils_2.BarcodeUtil.translateBarcode(_this.scannedChars.join(''));
                    if (_this.scannedChars.length >= 7 && !code.includes('PR-') && _this.onScannedBarcode.isObserved()) {
                        if (!_this.isScannerActive) {
                            return;
                        }
                        _this.loggerService.log({
                            type: _1.EVENT.KEY,
                            origin: _this.constructor['name'],
                            trace: _this.loggerService.getStackTrace(),
                            data: {
                                message: 'KeyEventService:scanned',
                                type: 'code',
                            },
                        });
                        _this.onScannedBarcode.emit(code);
                    }
                    else if (!code.includes('PR-') && _this.onScannedPositions.isObserved()) {
                        var positions = code.split('-');
                        if (positions.length === 3) {
                            _this.loggerService.log({
                                type: _1.EVENT.KEY,
                                origin: _this.constructor['name'],
                                trace: _this.loggerService.getStackTrace(),
                                data: {
                                    message: 'KeyEventService:scanned',
                                    type: 'positions',
                                },
                            });
                            _this.onScannedPositions.emit(positions);
                        }
                    }
                    else if (code.includes('PR-') && _this.onScannedBox.isObserved()) {
                        _this.loggerService.log({
                            type: _1.EVENT.KEY,
                            origin: _this.constructor['name'],
                            trace: _this.loggerService.getStackTrace(),
                            data: {
                                message: 'KeyEventService:scanned',
                                type: 'box',
                            },
                        });
                        _this.onScannedBox.emit(code);
                    }
                    if (_this.scannedChars.length >= 7 && !code.includes('PR-') && _this.onScannedGS1.isObserved()) {
                        _this.parseGS1(code);
                    }
                }
                _this.isKeyPressed = false;
                _this.scannedChars = [];
            }, this.timeToScan);
        }
        this.isKeyPressed = true;
    };
    KeyEventService.prototype.parseGS1 = function (code) {
        var _a;
        if (!this.onScannedGS1.isObserved()) {
            return;
        }
        try {
            var parsedCode = gs1_barcode_parser_mod_1.parseBarcode(code);
            if (parsedCode.codeName && ((_a = parsedCode.parsedCodeItems) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                var result = { parsedCode: parsedCode, rawCode: code };
                var gtinArray = parsedCode.parsedCodeItems.filter(function (pci) { return pci.ai === '01'; });
                var batchArray = parsedCode.parsedCodeItems.filter(function (pci) { return pci.ai === '10'; });
                var bestBeforeArray = parsedCode.parsedCodeItems.filter(function (pci) { return pci.ai === '15'; });
                var expirationArray = parsedCode.parsedCodeItems.filter(function (pci) { return pci.ai === '17'; });
                var internalValueArray = parsedCode.parsedCodeItems.filter(function (pci) { return pci.ai === '99'; });
                var sellPriceArray = parsedCode.parsedCodeItems.filter(function (pci) { return pci.ai === '390'; });
                if ((gtinArray === null || gtinArray === void 0 ? void 0 : gtinArray.length) > 0) {
                    result.barcode = gtinArray[0].data;
                }
                if ((batchArray === null || batchArray === void 0 ? void 0 : batchArray.length) > 0) {
                    result.batch = batchArray[0].data;
                }
                if ((bestBeforeArray === null || bestBeforeArray === void 0 ? void 0 : bestBeforeArray.length) > 0) {
                    result.bestBeforeDate = bestBeforeArray[0].data;
                }
                if ((expirationArray === null || expirationArray === void 0 ? void 0 : expirationArray.length) > 0) {
                    result.expirationDate = expirationArray[0].data;
                }
                if ((internalValueArray === null || internalValueArray === void 0 ? void 0 : internalValueArray.length) > 0) {
                    result.internalValue = internalValueArray[0].data;
                }
                if ((sellPriceArray === null || sellPriceArray === void 0 ? void 0 : sellPriceArray.length) > 0) {
                    result.sellPrice = sellPriceArray[0].data;
                }
                this.onScannedGS1.emit(result);
            }
        }
        catch (e) {
            console.warn(e);
        }
    };
    KeyEventService.prototype.isObserved = function () {
        return (this.onScannedGS1.isObserved() ||
            this.onScannedBarcode.isObserved() ||
            this.onScannedBox.isObserved() ||
            this.onScannedPositions.isObserved() ||
            this.onKeyEnter.isObserved() ||
            this.onKeyEsc.isObserved() ||
            this.onKeystroke.isObserved());
    };
    KeyEventService.prototype.testBarcodeFromString = function (code) {
        console.warn('CORE:KeyEvent:testBarcodeFromString: ', {
            code: code,
            isScannerActive: this.isScannerActive,
            onScannedGS1: this.onScannedGS1.isObserved(),
            onScannedBarcode: this.onScannedBarcode.isObserved(),
            onScannedBox: this.onScannedBox.isObserved(),
            onScannedPositions: this.onScannedPositions.isObserved(),
            onKeyEnter: this.onKeyEnter.isObserved(),
            onKeyEsc: this.onKeyEsc.isObserved(),
            onKeystroke: this.onKeystroke.isObserved(),
        });
        if (!code) {
            code = '';
        }
        if (code.length >= 7 && !code.includes('PR-') && this.onScannedBarcode.isObserved()) {
            if (!this.isScannerActive) {
                return;
            }
            this.onScannedBarcode.emit(code);
        }
        else if (!code.includes('PR-') && this.onScannedPositions.isObserved()) {
            var positions = code.split('-');
            if (positions.length === 3) {
                this.onScannedPositions.emit(positions);
            }
        }
        else if (code.includes('PR-') && this.onScannedBox.isObserved()) {
            this.onScannedBox.emit(code);
        }
        this.parseGS1(code);
    };
    return KeyEventService;
}());
exports.KeyEventService = KeyEventService;
//# sourceMappingURL=KeyEventService.js.map