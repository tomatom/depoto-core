import { HttpService } from '../interfaces';
import { OAuthService, LoggerService } from './';
export declare class FetchService implements HttpService {
    private loggerService;
    private oauthService;
    constructor(loggerService: LoggerService, oauthService: OAuthService);
    getAuthorizationHeaders(): Headers;
    get(endpoint: string): Promise<any>;
    post(endpoint: string, reqBody: any): Promise<any>;
    put(endpoint: string, reqBody: any): Promise<unknown>;
    downloadBlob(uri: string): Promise<Blob>;
    private renderErrorDetail;
}
