"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerService = exports.EVENT = exports.LOGGER_TYPE = void 0;
var utils_1 = require("../utils");
exports.LOGGER_TYPE = {
    CONSOLE: 'console',
    OBSERVABLE: 'observable',
};
exports.EVENT = {
    EXCEPTION: 'exception',
    LOGGER: 'logger',
    LOCAL_STORAGE: 'localStorage',
    OAUTH: 'oauth',
    CHECKOUT: 'checkout',
    CUSTOMER: 'customer',
    GRAPHQL: 'graphql',
    KEY: 'key',
    EET: 'eet',
    EVENTS: 'events',
    HTTP: 'http',
    MESSAGE: 'message',
    ORDER: 'order',
    PRINT: 'print',
    PRODUCT: 'product',
    STATS: 'stats',
    USER: 'user',
    VAT: 'vat',
};
var LoggerService = /** @class */ (function () {
    function LoggerService(loggerType, filters) {
        if (loggerType === void 0) { loggerType = exports.LOGGER_TYPE.CONSOLE; }
        if (filters === void 0) { filters = []; }
        this.logger = null;
        this.loggerType = exports.LOGGER_TYPE.CONSOLE;
        this.loggerType = loggerType;
        this.filters = filters;
        if (this.loggerType === exports.LOGGER_TYPE.OBSERVABLE) {
            this.logger = new utils_1.Observable('core:loggerService:logger');
        }
        else {
            this.logger = console;
            this.loggerType = exports.LOGGER_TYPE.CONSOLE;
        }
    }
    LoggerService.prototype.log = function (event) {
        if (!this.isLoggable(event)) {
            return;
        }
        if (!!this.logger && this.loggerType === exports.LOGGER_TYPE.CONSOLE) {
            this.logger.log(event);
        }
        else if (!!this.logger && this.loggerType === exports.LOGGER_TYPE.OBSERVABLE) {
            this.logger.emit(event);
        }
    };
    LoggerService.prototype.err = function (event) {
        if (!this.isLoggable(event)) {
            return;
        }
        if (!!this.logger && this.loggerType === exports.LOGGER_TYPE.CONSOLE) {
            this.logger.warn(event);
        }
        else if (!!this.logger && this.loggerType === exports.LOGGER_TYPE.OBSERVABLE) {
            this.logger.error(event);
        }
    };
    LoggerService.prototype.getStackTrace = function () {
        if (!!Error['captureStackTrace']) {
            var obj = {};
            Error['captureStackTrace'](obj, this.getStackTrace);
            return obj.stack
                .split('\n')
                .filter(function (r) { return !(r.includes('Error') || r.includes('at LoggerService.')); })
                .map(function (r) { return r.trim(); });
            // .join('\n')
        }
        else {
            var res = '';
            try {
                var a = {};
                a['debug']();
            }
            catch (ex) {
                res = ex.stack;
            }
            return res;
            // return console.trace()
        }
    };
    LoggerService.prototype.isLoggable = function (event) {
        return !!this.filters.includes(event.type);
    };
    return LoggerService;
}());
exports.LoggerService = LoggerService;
//# sourceMappingURL=LoggerService.js.map