"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OAuthService = void 0;
var models_1 = require("../models");
var _1 = require("./");
var utils_1 = require("../utils");
var OAuthService = /** @class */ (function () {
    function OAuthService(loggerService, s, session) {
        if (session === void 0) { session = {}; }
        this.loggerService = loggerService;
        this.s = s;
        this.defaultClientKeys = utils_1.DEFAULT_CLIENT_KEYS;
        this.onLoginEmitter = new utils_1.Observable('core:oauthService:onLoginEmitter');
        this.onLogoutEmitter = new utils_1.Observable('core:oauthService:onLogoutEmitter');
        if (!!session) {
            this.session = new models_1.OAuthSession(session);
        }
        else {
            this.session = new models_1.OAuthSession();
            if (window && 'localStorage' in window) {
                this.loadDataSync();
            }
            else {
                this.loadData();
            }
        }
    }
    OAuthService.prototype.getToken = function (session) {
        var _this = this;
        this.session = new models_1.OAuthSession(session);
        // this.s.set(this.s.keys.auth.clientType, this.session.clientType)
        // this.s.set(this.s.keys.auth.customServerUrl, this.session.clientCustomUri) // todo?
        if (!session.clientId) {
            session.clientId = this.defaultClientKeys[session.clientType].clientId;
        }
        if (!session.clientSecret) {
            session.clientSecret = this.defaultClientKeys[session.clientType].clientSecret;
        }
        this.loggerService.log({
            type: _1.EVENT.OAUTH,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: session,
        });
        var reqBody = new FormData();
        reqBody.append('client_id', session.clientId);
        reqBody.append('client_secret', session.clientSecret);
        reqBody.append('grant_type', 'password');
        reqBody.append('username', session.email);
        reqBody.append('password', session.password);
        return new Promise(function (resolve) {
            fetch(utils_1.getEndpoint(utils_1.ENDPOINT.TOKEN, session), {
                method: 'post',
                // headers: {'Content-type': 'application/x-www-form-urlencoded'}, // no need to specify!
                body: reqBody,
            })
                .then(function (resp) { return resp.json(); })
                .then(function (data) {
                if (data && !!data.access_token && !data.error) {
                    _this.session.token.accessToken = data.access_token;
                    _this.session.token.refreshToken = data.refresh_token;
                    _this.session.token.tokenType = data.token_type;
                    _this.session.token.scope = data.scope;
                    _this.session.token.expiresIn = data.expires_in;
                    _this.session.token.timestamp = +new Date();
                    _this.session.isAuthenticated = true;
                    _this.saveData();
                    _this.onLoginEmitter.emit(_this.session.email);
                    _this.loggerService.log({
                        type: _1.EVENT.OAUTH,
                        origin: _this.constructor['name'],
                        trace: _this.loggerService.getStackTrace(),
                        data: data,
                    });
                    resolve(true);
                }
                else {
                    _this.session.isAuthenticated = false;
                    _this.saveData();
                    _this.onLogoutEmitter.emit(_this.session.email);
                    _this.loggerService.log({
                        type: _1.EVENT.OAUTH,
                        origin: _this.constructor['name'],
                        trace: _this.loggerService.getStackTrace(),
                        data: data,
                    });
                    resolve(false);
                }
            }, function (error) {
                _this.session.isAuthenticated = false;
                _this.saveData();
                _this.loggerService.log({
                    type: _1.EVENT.OAUTH,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: error,
                });
                _this.onLogoutEmitter.emit(_this.session.email);
                resolve(false);
            });
        });
    };
    OAuthService.prototype.getRefreshToken = function () {
        var _this = this;
        if (!this.session.isRememberMe) {
            this.logout();
            return new Promise(function (r) { return r(false); });
        }
        if (!this.session.clientId) {
            this.session.clientId = this.defaultClientKeys[this.session.clientType].clientId;
        }
        if (!this.session.clientSecret) {
            this.session.clientSecret = this.defaultClientKeys[this.session.clientType].clientSecret;
        }
        var reqBody = new FormData();
        reqBody.append('client_id', this.session.clientId);
        reqBody.append('client_secret', this.session.clientSecret);
        reqBody.append('grant_type', 'refresh_token');
        reqBody.append('refresh_token', this.session.token.refreshToken);
        return new Promise(function (resolve) {
            fetch(utils_1.getEndpoint(utils_1.ENDPOINT.REFRESH_TOKEN, _this.session), {
                method: 'post',
                body: reqBody,
            })
                .then(function (resp) { return resp.json(); })
                .then(function (data) {
                if (!data.error) {
                    _this.session.token.accessToken = data.access_token;
                    _this.session.token.refreshToken = data.refresh_token;
                    _this.session.token.tokenType = data.token_type;
                    _this.session.token.scope = data.scope;
                    _this.session.token.expiresIn = data.expires_in;
                    _this.session.token.timestamp = +new Date();
                    _this.session.isAuthenticated = true;
                    _this.onLoginEmitter.emit(_this.session.email);
                    _this.saveData();
                    resolve(true);
                }
                else {
                    _this.loggerService.log({
                        type: _1.EVENT.OAUTH,
                        origin: _this.constructor['name'],
                        trace: _this.loggerService.getStackTrace(),
                        data: data.error,
                    });
                    resolve(false);
                }
            }, function (error) {
                _this.loggerService.log({
                    type: _1.EVENT.OAUTH,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: error,
                });
                _this.onLogoutEmitter.emit(_this.session.email);
                resolve(false);
            });
        });
    };
    OAuthService.prototype.isAuthenticated = function () {
        return this.session.isAuthenticated;
    };
    OAuthService.prototype.getAccessToken = function () {
        if (!this.session || (this.session && !this.session.token)) {
            return;
        }
        return this.session.token.accessToken;
    };
    OAuthService.prototype.getSession = function () {
        if (!this.session) {
            return new models_1.OAuthSession();
        }
        return this.session;
    };
    OAuthService.prototype.logout = function () {
        this.session.isAuthenticated = false;
        this.onLogoutEmitter.emit(this.session.email);
        this.clearData();
    };
    OAuthService.prototype.saveData = function () {
        this.s.set(this.s.keys.auth.data, this.session.token);
        this.s.set(this.s.keys.auth.rememberMe, this.session.isRememberMe);
        this.s.set(this.s.keys.auth.isAuthenticated, this.session.isAuthenticated);
        this.s.set(this.s.keys.auth.email, this.session.email);
        this.s.set(this.s.keys.auth.clientType, this.session.clientType);
    };
    OAuthService.prototype.clearData = function () {
        this.s.set(this.s.keys.auth.isCompanyReloaded, false);
        this.s.clear(this.s.keys.auth);
    };
    OAuthService.prototype.loadData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = this.session;
                        return [4 /*yield*/, this.s.get(this.s.keys.auth.email)];
                    case 1:
                        _a.email = _f.sent();
                        _b = this.session;
                        return [4 /*yield*/, this.s.get(this.s.keys.auth.clientType)];
                    case 2:
                        _b.clientType = _f.sent();
                        _c = this.session;
                        return [4 /*yield*/, this.s.get(this.s.keys.auth.isAuthenticated)];
                    case 3:
                        _c.isAuthenticated = _f.sent();
                        _d = this.session;
                        return [4 /*yield*/, this.s.get(this.s.keys.auth.rememberMe)];
                    case 4:
                        _d.isRememberMe = _f.sent();
                        _e = this.session;
                        return [4 /*yield*/, this.s.get(this.s.keys.auth.data)];
                    case 5:
                        _e.token = _f.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    OAuthService.prototype.loadDataSync = function () {
        this.session.email = this.s.getSync(this.s.keys.auth.email);
        this.session.clientType = this.s.getSync(this.s.keys.auth.clientType);
        this.session.isAuthenticated = this.s.getSync(this.s.keys.auth.isAuthenticated);
        this.session.isRememberMe = this.s.getSync(this.s.keys.auth.rememberMe);
        this.session.token = this.s.getSync(this.s.keys.auth.data);
    };
    return OAuthService;
}());
exports.OAuthService = OAuthService;
//# sourceMappingURL=OAuthService.js.map