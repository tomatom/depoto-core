"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyService = void 0;
var entities_1 = require("../entities");
var CompanyService = /** @class */ (function (_super) {
    __extends(CompanyService, _super);
    function CompanyService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    // not implemented on api
    // getList(parameters: Object|any = {}, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Object|any> {
    //   const params = {
    //     page: parameters.page > 0 ? parameters.page : 1,
    //     sort: !!parameters.sort ? parameters.sort : 'id',
    //     direction: !!parameters.direction ? parameters.direction : 'DESC',
    //     filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    //   }
    //   return this.getEntityList('Company', 'companies', params, definitionDepth, definitionExcludedClasses, schema)
    // }
    //
    // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Company> {
    //   return this.getSingleEntity('Company', 'company', {id: id}, definitionDepth, definitionExcludedClasses, schema)
    // }
    //
    // create(company: Company, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Company> {
    //   const params: any = {name: company.name};
    //   return this.createUpdateEntity('Company', 'createCompany', params, definitionDepth, definitionExcludedClasses, schema)
    // }
    CompanyService.prototype.update = function (company, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: company.id,
            name: company.name,
            // ic: company.ic,
            // dic: company.dic,
            phone: company.phone,
            email: company.email,
            street: company.street,
            city: company.city,
            zip: company.zip,
            country: company.country,
            // registrationNote: company.registrationNote,
            // nextEan: company.nextEan,
            defaultVoucherValidity: company.defaultVoucherValidity,
        };
        return this.createUpdateEntity('Company', 'updateCompany', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CompanyService.prototype.updateNextEan = function (company, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: company.id,
            nextEan: company.nextEan,
        };
        return this.createUpdateEntity('Company', 'updateCompany', params, definitionDepth, definitionExcludedClasses, schema);
    };
    return CompanyService;
}(entities_1.BaseEntityService));
exports.CompanyService = CompanyService;
//# sourceMappingURL=CompanyService.js.map