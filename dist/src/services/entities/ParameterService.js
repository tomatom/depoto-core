"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParameterService = void 0;
var entities_1 = require("../entities");
var ParameterService = /** @class */ (function (_super) {
    __extends(ParameterService, _super);
    function ParameterService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    ParameterService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Parameter', 'parameters', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ParameterService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Parameter', 'parameter', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    ParameterService.prototype.create = function (parameter, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: parameter.name,
            type: parameter.type,
            enumValues: parameter.enumValues,
            unit: parameter.unit,
            text: parameter.text,
            externalId: parameter.externalId,
        };
        return this.createUpdateEntity('Parameter', 'createParameter', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ParameterService.prototype.update = function (parameter, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: parameter.id,
            name: parameter.name,
            type: parameter.type,
            enumValues: parameter.enumValues,
            unit: parameter.unit,
            text: parameter.text,
            externalId: parameter.externalId,
        };
        return this.createUpdateEntity('Parameter', 'updateParameter', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ParameterService.prototype.delete = function (parameter) {
        var params = { id: parameter.id };
        return this.deleteEntity('Parameter', 'deleteParameter', params);
    };
    return ParameterService;
}(entities_1.BaseEntityService));
exports.ParameterService = ParameterService;
//# sourceMappingURL=ParameterService.js.map