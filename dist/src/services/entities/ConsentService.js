"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConsentService = void 0;
var entities_1 = require("../entities");
var ConsentService = /** @class */ (function (_super) {
    __extends(ConsentService, _super);
    function ConsentService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    ConsentService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Consent', 'consents', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ConsentService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Consent', 'consent', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    ConsentService.prototype.create = function (consent, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = { name: consent.name };
        consent.body && consent.body.length > 0 ? (params.body = consent.body) : null;
        consent.externalId && consent.externalId.length > 0 ? (params.externalId = consent.externalId) : null;
        return this.createUpdateEntity('Consent', 'createConsent', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ConsentService.prototype.update = function (consent, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: consent.id,
            name: consent.name,
            body: consent.body,
            externalId: consent.externalId,
        };
        return this.createUpdateEntity('Consent', 'updateConsent', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ConsentService.prototype.delete = function (consent) {
        var params = { id: consent.id };
        return this.deleteEntity('Consent', 'deleteConsent', params);
    };
    ConsentService.prototype.createCustomerConsent = function (customerConsent, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            consent: customerConsent.consent.id,
            customer: customerConsent.customer.id,
        };
        return this.createUpdateEntity('Consent', 'createCustomerConsent', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ConsentService.prototype.deleteCustomerConsent = function (customerConsent) {
        var params = { id: customerConsent.id };
        return this.deleteEntity('CustomerConsent', 'deleteCustomerConsent', params);
    };
    return ConsentService;
}(entities_1.BaseEntityService));
exports.ConsentService = ConsentService;
//# sourceMappingURL=ConsentService.js.map