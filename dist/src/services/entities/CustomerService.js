"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerService = void 0;
var entities_1 = require("../entities");
var CustomerService = /** @class */ (function (_super) {
    __extends(CustomerService, _super);
    function CustomerService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    CustomerService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Customer', 'customers', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CustomerService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Customer', 'customer', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    CustomerService.prototype.create = function (customer, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            email: customer.email,
            firstName: customer.firstName,
            lastName: customer.lastName,
            companyName: customer.companyName,
            wholesale: customer.wholesale,
            priceLevel: customer.priceLevel && customer.priceLevel.id > 0 ? customer.priceLevel.id : null,
            minExpirationDays: customer.minExpirationDays || 0,
        };
        if (customer.phone && customer.phone.length > 0) {
            params.phone = customer.phone;
        }
        if (customer.note && customer.note.length > 0) {
            params.note = customer.note;
        }
        if (customer.birthday && customer.birthday.length > 0) {
            params.birthday = customer.birthday;
        }
        if (customer.users && customer.users.length > 0) {
            params.users = customer.users.map(function (d) { return d.id; });
        }
        if (customer.tags && customer.tags.length > 0) {
            params.tags = customer.tags.map(function (t) { return t.id; });
        }
        return this.createUpdateEntity('Customer', 'createCustomer', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CustomerService.prototype.update = function (customer, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: customer.id,
            email: customer.email,
            firstName: customer.firstName,
            lastName: customer.lastName,
            companyName: customer.companyName,
            wholesale: customer.wholesale,
            phone: customer.phone,
            note: customer.note,
            birthday: customer.birthday,
            priceLevel: customer.priceLevel && customer.priceLevel.id > 0 ? customer.priceLevel.id : null,
            minExpirationDays: customer.minExpirationDays || 0,
        };
        if (customer.users && customer.users.length > 0) {
            params.users = customer.users.map(function (d) { return d.id; });
        }
        if (customer.tags && customer.tags.length > 0) {
            params.tags = customer.tags.map(function (t) { return t.id; });
        }
        return this.createUpdateEntity('Customer', 'updateCustomer', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CustomerService.prototype.delete = function (customer) {
        var params = { id: customer.id };
        return this.deleteEntity('Customer', 'deleteCustomer', params);
    };
    return CustomerService;
}(entities_1.BaseEntityService));
exports.CustomerService = CustomerService;
//# sourceMappingURL=CustomerService.js.map