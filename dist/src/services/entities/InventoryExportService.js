"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryExportService = void 0;
var entities_1 = require("../entities");
var InventoryExportService = /** @class */ (function (_super) {
    __extends(InventoryExportService, _super);
    function InventoryExportService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    InventoryExportService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('InventoryExport', 'exportInventories', params, definitionDepth, definitionExcludedClasses, schema);
    };
    InventoryExportService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('InventoryExport', 'exportInventory', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    InventoryExportService.prototype.create = function (inventoryExport, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            date: inventoryExport.date,
            depots: inventoryExport.depots,
            suppliers: inventoryExport.suppliers,
            producers: inventoryExport.producers,
            approved: inventoryExport.approved,
            rejected: inventoryExport.rejected,
        };
        return this.createUpdateEntity('InventoryExport', 'createExportInventory', params, definitionDepth, definitionExcludedClasses, schema);
    };
    InventoryExportService.prototype.update = function (inventoryExport, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: inventoryExport.id,
            date: inventoryExport.date,
            depots: inventoryExport.depots,
            suppliers: inventoryExport.suppliers,
            producers: inventoryExport.producers,
            approved: inventoryExport.approved,
            rejected: inventoryExport.rejected,
        };
        return this.createUpdateEntity('InventoryExport', 'updateExportInventory', params, definitionDepth, definitionExcludedClasses, schema);
    };
    return InventoryExportService;
}(entities_1.BaseEntityService));
exports.InventoryExportService = InventoryExportService;
//# sourceMappingURL=InventoryExportService.js.map