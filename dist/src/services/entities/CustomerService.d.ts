import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Customer } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class CustomerService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Customer>;
    create(customer: Customer, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Customer>;
    update(customer: Customer, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Customer>;
    delete(customer: Customer | any): Promise<Array<any> | any>;
}
