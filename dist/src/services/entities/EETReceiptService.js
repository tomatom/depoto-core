"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EETReceiptService = void 0;
var entities_1 = require("../entities");
var EETReceiptService = /** @class */ (function (_super) {
    __extends(EETReceiptService, _super);
    function EETReceiptService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    EETReceiptService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('EETReceipt', 'eetReceipts', params, definitionDepth, definitionExcludedClasses, schema);
    };
    // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<EETReceipt> {
    //   return this.getSingleEntity('EETReceipt', 'receipt', {id: id}, definitionDepth, definitionExcludedClasses, schema)
    // }
    EETReceiptService.prototype.create = function (receipt, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            checkout: receipt.checkoutEetId,
            number: receipt.number,
            dateCreated: receipt.dateCreated,
            currency: 'CZK',
            totalPrice: receipt.totalPrice,
            // priceZeroVat: null // todo: unused mutation
            // priceStandardVat: Int
            // vatStandard: Int
            // priceFirstReducedVat: Int
            // vatFirstReduced: Int
            // priceSecondReducedVat: Int
            // vatSecondReduced: Int
            // priceForSubsequentSettlement: Int
            // priceUsedSubsequentSettlement: Int
        };
        return this.createUpdateEntity('EETReceipt', 'createEetReceipt', params, definitionDepth, definitionExcludedClasses, schema);
    };
    EETReceiptService.prototype.send = function (receipt, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: receipt.id,
        };
        return this.createUpdateEntity('EETReceipt', 'sendEetReceipt', params, definitionDepth, definitionExcludedClasses, schema);
    };
    return EETReceiptService;
}(entities_1.BaseEntityService));
exports.EETReceiptService = EETReceiptService;
//# sourceMappingURL=EETReceiptService.js.map