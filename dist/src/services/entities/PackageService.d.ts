import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Carrier, Company, Package, PackageDisposal } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class PackageService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Package>;
    create(pack: Partial<Package>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Package>;
    update(pack: Partial<Package>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Package>;
    updatePart(pack: Partial<Package>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Package>;
    send(pack: Partial<Package>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Package>;
    reset(pack: Partial<Package>): Promise<Package>;
    delete(pack: Partial<Package>): Promise<Array<any> | any>;
    getDisposals(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getDisposal(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<PackageDisposal>;
    createDisposal(company: Partial<Company>, carrier: Partial<Carrier>, packages: Array<Partial<Package>>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<PackageDisposal>;
    updateDisposalPart(disposal: Partial<PackageDisposal>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<PackageDisposal>;
    sendDisposal(disposal: Partial<PackageDisposal>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Package>;
    deleteDisposal(disposal: Partial<PackageDisposal>): Promise<Array<any> | any>;
}
