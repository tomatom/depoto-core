"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.VatService = void 0;
var entities_1 = require("../../entities");
var __1 = require("../");
var entities_2 = require("../entities");
var VatService = /** @class */ (function (_super) {
    __extends(VatService, _super);
    function VatService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        if (_this.oauthService.isAuthenticated()) {
            _this.getDefaultVat();
        }
        _this.listenForEvents();
        return _this;
    }
    VatService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Vat', 'vats', params, definitionDepth, definitionExcludedClasses, schema);
    };
    VatService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Vat', 'vat', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    VatService.prototype.getDefaultVat = function (definitionDepth, definitionExcludedClasses) {
        var _this = this;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        return this.getList({}, definitionDepth, definitionExcludedClasses).then(function (res) {
            if (!res || !res.items) {
                return null;
            }
            var v = res.items.filter(function (v) { return v.default; });
            if (v.length === 1) {
                _this.defaultVat = new entities_1.Vat(v[0]);
                _this.loggerService.log({
                    type: __1.EVENT.VAT,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: {
                        message: 'default VAT loaded',
                        defaultVat: _this.defaultVat,
                    },
                });
                return _this.defaultVat;
            }
        });
    };
    VatService.prototype.create = function (vat, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: vat.name,
            percent: vat.percent,
            default: vat.default,
        };
        return this.createUpdateEntity('Vat', 'createVat', params, definitionDepth, definitionExcludedClasses, schema);
    };
    VatService.prototype.update = function (vat, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: vat.id,
            name: vat.name,
            percent: vat.percent,
            default: vat.default,
        };
        return this.createUpdateEntity('Vat', 'updateVat', params, definitionDepth, definitionExcludedClasses, schema);
    };
    VatService.prototype.delete = function (vat) {
        var params = { id: vat.id };
        return this.deleteEntity('Vat', 'deleteVat', params);
    };
    VatService.prototype.listenForEvents = function () {
        var _this = this;
        this.oauthService.onLoginEmitter.subscribe().then(function () {
            _this.getDefaultVat();
        });
        this.oauthService.onLogoutEmitter.subscribe().then(function () {
            _this.defaultVat = undefined;
        });
        this.loggerService.log({
            type: __1.EVENT.VAT,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                message: 'VatService:listeningForEvents',
            },
        });
    };
    return VatService;
}(entities_2.BaseEntityService));
exports.VatService = VatService;
//# sourceMappingURL=VatService.js.map