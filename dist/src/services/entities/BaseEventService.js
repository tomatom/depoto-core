"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseEventService = void 0;
var entities_1 = require("../entities");
var BaseEventService = /** @class */ (function (_super) {
    __extends(BaseEventService, _super);
    function BaseEventService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    BaseEventService.prototype.subscribe = function () {
        return _super.prototype.subscribeToEvents.call(this);
    };
    BaseEventService.prototype.unsubscribe = function () {
        return _super.prototype.unsubscribeFromEvents.call(this);
    };
    return BaseEventService;
}(entities_1.BaseEntityService));
exports.BaseEventService = BaseEventService;
//# sourceMappingURL=BaseEventService.js.map