import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Parameter } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ParameterService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Parameter>;
    create(parameter: Parameter, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Parameter>;
    update(parameter: Parameter, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Parameter>;
    delete(parameter: Parameter | any): Promise<Array<any> | any>;
}
