import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Depot } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class DepotService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Depot>;
    create(depot: Depot, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Depot>;
    update(depot: Depot, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Depot>;
    delete(depot: Depot | any): Promise<Array<any> | any>;
}
