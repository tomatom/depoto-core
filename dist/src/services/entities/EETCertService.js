"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EETCertService = void 0;
var entities_1 = require("../entities");
var EETCertService = /** @class */ (function (_super) {
    __extends(EETCertService, _super);
    function EETCertService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    EETCertService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('EETCert', 'certs', params, definitionDepth, definitionExcludedClasses, schema);
    };
    EETCertService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('EETCert', 'cert', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    EETCertService.prototype.create = function (cert, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: cert.name,
            password: cert.password,
            pkcs12: cert.pkcs12,
        };
        return this.createUpdateEntity('EETCert', 'createCert', params, definitionDepth, definitionExcludedClasses, schema);
    };
    EETCertService.prototype.update = function (cert, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: cert.id,
        };
        if (cert.name && cert.name.length > 0) {
            params.name = cert.name;
        }
        if (cert.password && cert.password.length > 0) {
            params.password = cert.password;
        }
        if (cert.pkcs12 && cert.pkcs12.length > 0) {
            params.pkcs12 = cert.pkcs12;
        }
        return this.createUpdateEntity('EETCert', 'updateCert', params, definitionDepth, definitionExcludedClasses, schema);
    };
    EETCertService.prototype.delete = function (cert) {
        var params = { id: cert.id };
        return this.deleteEntity('EETCert', 'deleteCert', params);
    };
    return EETCertService;
}(entities_1.BaseEntityService));
exports.EETCertService = EETCertService;
//# sourceMappingURL=EETCertService.js.map