"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPriceService = void 0;
var entities_1 = require("../entities");
var ProductPriceService = /** @class */ (function (_super) {
    __extends(ProductPriceService, _super);
    function ProductPriceService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    // not implemented on api
    // getList(parameters: Object|any = {}, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Object|any> {
    //   const params = {
    //     page: parameters.page > 0 ? parameters.page : 1,
    //     sort: !!parameters.sort ? parameters.sort : 'id',
    //     direction: !!parameters.direction ? parameters.direction : 'DESC',
    //     filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    //   }
    //   return this.getEntityList('ProductPrice', 'productPrices', params, definitionDepth, definitionExcludedClasses, schema)
    // }
    //
    // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductPrice> {
    //   return this.getSingleEntity('ProductPrice', 'productPrice', {id: id}, definitionDepth, definitionExcludedClasses, schema)
    // }
    ProductPriceService.prototype.create = function (productPrice, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            product: productPrice.product.id,
            sellPrice: productPrice.sellPrice,
            dateFrom: productPrice.dateFrom,
            dateTo: productPrice.dateTo,
            available: productPrice.available,
            note: productPrice.note,
        };
        return this.createUpdateEntity('ProductPrice', 'createProductPrice', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductPriceService.prototype.update = function (productPrice, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: productPrice.id,
            product: productPrice.product.id,
            sellPrice: productPrice.sellPrice,
            dateFrom: productPrice.dateFrom,
            dateTo: productPrice.dateTo,
            available: productPrice.available,
            note: productPrice.note,
        };
        return this.createUpdateEntity('ProductPrice', 'updateProductPrice', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductPriceService.prototype.delete = function (productPrice) {
        var params = { id: productPrice.id };
        return this.deleteEntity('ProductPrice', 'deleteProductPrice', params);
    };
    return ProductPriceService;
}(entities_1.BaseEntityService));
exports.ProductPriceService = ProductPriceService;
//# sourceMappingURL=ProductPriceService.js.map