import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class DownloadService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    downloadDocumentAndShowInNewWindow(uri: string): void;
}
