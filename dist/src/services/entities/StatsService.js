"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatsService = void 0;
var entities_1 = require("../entities");
var StatsService = /** @class */ (function (_super) {
    __extends(StatsService, _super);
    function StatsService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    StatsService.prototype.getStatsBestsellers = function (from, to, checkoutId, results, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            from: from,
            to: to,
        };
        if (checkoutId && checkoutId > 0) {
            params.checkout = checkoutId;
        }
        if (results && results > 0) {
            params.results = results;
        }
        return this.getEntityList('StatsBestseller', 'statsBestsellers', params, definitionDepth, definitionExcludedClasses, schema);
    };
    StatsService.prototype.getStatsCarrierUsage = function (from, to, checkoutId, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            from: from,
            to: to,
        };
        if (checkoutId && checkoutId > 0) {
            params.checkout = checkoutId;
        }
        return this.getEntityList('StatsCarrierUsage', 'statsCarrierUsage', params, definitionDepth, definitionExcludedClasses, schema);
    };
    StatsService.prototype.getStatsPaymentSale = function (from, to, checkoutId, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            from: from,
            to: to,
        };
        if (checkoutId && checkoutId > 0) {
            params.checkout = checkoutId;
        }
        return this.getEntityList('StatsPaymentSale', 'statsPaymentSales', params, definitionDepth, definitionExcludedClasses, schema);
    };
    return StatsService;
}(entities_1.BaseEntityService));
exports.StatsService = StatsService;
//# sourceMappingURL=StatsService.js.map