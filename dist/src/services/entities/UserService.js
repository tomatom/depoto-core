"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
var entities_1 = require("../../entities");
var __1 = require("../");
var entities_2 = require("../entities");
var utils_1 = require("../../utils");
var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(config) {
        var _this = _super.call(this, config) || this;
        _this.currentUserEmitter = new utils_1.Observable('core:userService:currentUserEmitter');
        _this.roles = [];
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        _this.listenForEvents();
        return _this;
    }
    UserService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('User', 'users', params, definitionDepth, definitionExcludedClasses, schema);
    };
    UserService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('User', 'user', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    UserService.prototype.getMyProfile = function (definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('User', 'userMyProfile', {}, definitionDepth, definitionExcludedClasses, schema);
    };
    UserService.prototype.create = function (user, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            email: user.email,
            plainPassword: user.plainPassword,
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            groups: user.groups.map(function (g) { return g.id; }),
            pin: user.pin,
            enabled: true,
        };
        if (user.checkouts && user.checkouts.length > 0) {
            params.checkouts = user.checkouts.map(function (u) { return u.id; });
        }
        return this.createUpdateEntity('User', 'createUser', params, definitionDepth, definitionExcludedClasses, schema);
    };
    UserService.prototype.update = function (user, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: user.id,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            groups: user.groups.map(function (g) { return g.id; }),
            checkouts: user.checkouts.map(function (u) { return u.id; }),
            pin: user.pin,
        };
        if (user.plainPassword && user.plainPassword.length > 0) {
            params.plainPassword = user.plainPassword;
        }
        return this.createUpdateEntity('User', 'updateUser', params, definitionDepth, definitionExcludedClasses, schema);
    };
    UserService.prototype.delete = function (user) {
        var params = { id: user.id };
        return this.deleteEntity('User', 'deleteUser', params);
    };
    UserService.prototype.setCurrentUser = function (userEmail) {
        return __awaiter(this, void 0, void 0, function () {
            var user, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!!!userEmail) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.getMyProfile(2, [], {
                                id: null,
                                email: null,
                                firstName: null,
                                lastName: null,
                                name: null,
                                phone: null,
                                groups: {
                                    id: null,
                                    name: null,
                                    roles: null,
                                },
                                roles: null,
                                pin: null,
                                company: {
                                    id: null,
                                    name: null,
                                    ic: null,
                                    dic: null,
                                    email: null,
                                    phone: null,
                                    street: null,
                                    city: null,
                                    zip: null,
                                    country: null,
                                    registrationNote: null,
                                    nextEan: null,
                                    billLogo: null,
                                    parent: {
                                        id: null,
                                        name: null,
                                        ic: null,
                                        dic: null,
                                        parent: {
                                            id: null,
                                            name: null,
                                        },
                                        carrierRelations: {
                                            id: null,
                                        },
                                    },
                                    children: {
                                        id: null,
                                        name: null,
                                        ic: null,
                                        dic: null,
                                        children: {
                                            id: null,
                                            name: null,
                                        },
                                        carrierRelations: {
                                            id: null,
                                        },
                                    },
                                    carrierRelations: {
                                        id: null,
                                        enable: null,
                                        options: null,
                                        carrier: {
                                            id: null,
                                            name: null,
                                        },
                                        checkout: {
                                            id: null,
                                            name: null,
                                            depots: {
                                                id: null,
                                            },
                                        },
                                        externalId: null,
                                    },
                                },
                                enabled: null,
                                checkouts: {
                                    id: null,
                                    name: null,
                                    payments: {
                                        id: null,
                                        name: null,
                                        type: {
                                            id: null,
                                            name: null,
                                        },
                                        eetEnable: null,
                                    },
                                },
                            })];
                    case 1:
                        _a = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _a = null;
                        _b.label = 3;
                    case 3:
                        user = _a;
                        if (!user) {
                            this.user = undefined;
                            this.roles = [];
                            this.s.set(this.s.keys.auth.company, null);
                        }
                        else {
                            this.user = new entities_1.User(user);
                            this.extractAllRoles();
                            this.s.set(this.s.keys.auth.company, user.company);
                        }
                        this.loggerService.log({
                            type: __1.EVENT.USER,
                            origin: this.constructor['name'],
                            trace: this.loggerService.getStackTrace(),
                            data: user,
                        });
                        this.currentUserEmitter.emit(this.user);
                        return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.hasRole = function (roleNeeded) {
        if (this.roles && this.roles.length > 0) {
            return this.roles.includes(roleNeeded);
        }
        else {
            return false;
        }
    };
    UserService.prototype.hasOneOfTheRoles = function (listOfRoles) {
        if (this.roles && this.roles.length > 0) {
            for (var _i = 0, listOfRoles_1 = listOfRoles; _i < listOfRoles_1.length; _i++) {
                var roleNeeded = listOfRoles_1[_i];
                if (this.roles.includes(roleNeeded)) {
                    return true;
                }
            }
        }
        return false;
    };
    UserService.prototype.listenForEvents = function () {
        var _this = this;
        this.oauthService.onLoginEmitter.subscribe().then(function (email) {
            _this.setCurrentUser(email);
        });
        this.oauthService.onLogoutEmitter.subscribe().then(function () {
            _this.setCurrentUser(undefined);
        });
        this.loggerService.log({
            type: __1.EVENT.USER,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                message: 'UserService:listeningForEvents',
            },
        });
    };
    UserService.prototype.extractAllRoles = function () {
        var _this = this;
        if (this.user && this.user.groups && this.user.groups.length > 0) {
            this.roles = [];
            this.user.groups.forEach(function (group) {
                group.roles.forEach(function (role) {
                    _this.roles.push(role);
                });
            });
            this.s.set(this.s.keys.services.userRoles, this.roles);
        }
    };
    return UserService;
}(entities_2.BaseEntityService));
exports.UserService = UserService;
//# sourceMappingURL=UserService.js.map