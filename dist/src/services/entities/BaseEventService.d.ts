import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
import { Observer } from '../../utils';
export declare class BaseEventService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    subscribe(): Observer;
    unsubscribe(): void;
}
