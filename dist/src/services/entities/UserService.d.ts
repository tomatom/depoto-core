import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { User } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
import { Observable } from '../../utils';
export declare class UserService extends BaseEntityService {
    user?: User;
    currentUserEmitter: Observable<User>;
    roles: string[];
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<User>;
    getMyProfile(definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<User>;
    create(user: User, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<User>;
    update(user: User, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<User>;
    delete(user: User | any): Promise<Array<any> | any>;
    setCurrentUser(userEmail?: string): Promise<void>;
    hasRole(roleNeeded: string): boolean;
    hasOneOfTheRoles(listOfRoles: string[]): boolean;
    protected listenForEvents(): void;
    protected extractAllRoles(): void;
}
