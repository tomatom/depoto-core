"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryService = void 0;
var entities_1 = require("../entities");
var CategoryService = /** @class */ (function (_super) {
    __extends(CategoryService, _super);
    function CategoryService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    CategoryService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'position',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Category', 'categories', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CategoryService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Category', 'category', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    CategoryService.prototype.create = function (category, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: category.name,
            text: category.text,
            parent: category.parent ? category.parent.id : null,
            externalId: category.externalId,
            position: category.position,
            tags: category.tags,
        };
        return this.createUpdateEntity('Category', 'createCategory', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CategoryService.prototype.update = function (category, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: category.id,
            name: category.name,
            text: category.text,
            parent: category.parent ? category.parent.id : null,
            externalId: category.externalId,
            position: category.position,
            tags: category.tags,
        };
        return this.createUpdateEntity('Category', 'updateCategory', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CategoryService.prototype.delete = function (category) {
        var params = { id: category.id };
        return this.deleteEntity('Category', 'deleteCategory', params);
    };
    return CategoryService;
}(entities_1.BaseEntityService));
exports.CategoryService = CategoryService;
//# sourceMappingURL=CategoryService.js.map