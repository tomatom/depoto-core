import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Export } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ExportService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getTypes(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    create(exportObject: Export, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Export>;
}
