import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Tag } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class TagService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Tag>;
    create(tag: Tag, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Tag>;
    update(tag: Tag, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Tag>;
    delete(tag: Tag | any): Promise<Array<any> | any>;
}
