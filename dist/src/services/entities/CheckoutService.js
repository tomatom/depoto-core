"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutService = void 0;
var entities_1 = require("../entities");
var CheckoutService = /** @class */ (function (_super) {
    __extends(CheckoutService, _super);
    function CheckoutService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    // if depth > 3, exclude OrderItem class!
    CheckoutService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Checkout', 'checkouts', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CheckoutService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Checkout', 'checkout', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    CheckoutService.prototype.create = function (checkout, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: checkout.name,
            amount: checkout.amount,
            nextBillNumber: checkout.nextBillNumber,
            nextReservationNumber: checkout.nextReservationNumber,
            billFooter: checkout.billFooter,
            eetId: Number(checkout.eetId),
            eetEnable: checkout.eetEnable,
            eetPlayground: checkout.eetPlayground,
            eetVerificationMode: checkout.eetVerificationMode,
            negativeReservation: checkout.negativeReservation,
            returnsDepot: !!checkout.returnsDepot && checkout.returnsDepot.id > 0 ? checkout.returnsDepot.id : null,
            eventUrl: checkout.eventUrl,
            eventTypes: checkout.eventTypes && checkout.eventTypes.length > 0 ? checkout.eventTypes : [],
        };
        if (checkout.depots && checkout.depots.length > 0) {
            params.depots = checkout.depots.map(function (d) { return d.id; });
        }
        if (checkout.payments && checkout.payments.length > 0) {
            params.payments = checkout.payments.map(function (p) { return p.id; });
        }
        if (checkout.users && checkout.users.length > 0) {
            params.users = checkout.users.map(function (u) { return u.id; });
        }
        return this.createUpdateEntity('Checkout', 'createCheckout', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CheckoutService.prototype.update = function (checkout, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: checkout.id,
            name: checkout.name,
            amount: checkout.amount,
            billFooter: checkout.billFooter,
            eetEnable: checkout.eetEnable,
            eetPlayground: checkout.eetPlayground,
            eetVerificationMode: checkout.eetVerificationMode,
            negativeReservation: checkout.negativeReservation,
            returnsDepot: !!checkout.returnsDepot && checkout.returnsDepot.id > 0 ? checkout.returnsDepot.id : null,
            eventUrl: checkout.eventUrl,
            eventTypes: checkout.eventTypes && checkout.eventTypes.length > 0 ? checkout.eventTypes : [],
            depots: checkout.depots && checkout.depots.length > 0 ? checkout.depots.map(function (d) { return d.id; }) : [],
            payments: checkout.payments && checkout.payments.length > 0 ? checkout.payments.map(function (p) { return p.id; }) : [],
            users: checkout.users && checkout.users.length > 0 ? checkout.users.map(function (u) { return u.id; }) : [],
        };
        return this.createUpdateEntity('Checkout', 'updateCheckout', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CheckoutService.prototype.setNextBillNumber = function (checkout, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: checkout.id,
            nextBillNumber: checkout.nextBillNumber,
        };
        return this.createUpdateEntity('Checkout', 'updateCheckout', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CheckoutService.prototype.setNextReservationNumber = function (checkout, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: checkout.id,
            nextReservationNumber: checkout.nextReservationNumber,
        };
        return this.createUpdateEntity('Checkout', 'updateCheckout', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CheckoutService.prototype.getEventTypes = function () {
        return this.getEntityList('string', 'eventTypes', {});
    };
    CheckoutService.prototype.delete = function (checkout) {
        var params = { id: checkout.id };
        return this.deleteEntity('Checkout', 'deleteCheckout', params);
    };
    return CheckoutService;
}(entities_1.BaseEntityService));
exports.CheckoutService = CheckoutService;
//# sourceMappingURL=CheckoutService.js.map