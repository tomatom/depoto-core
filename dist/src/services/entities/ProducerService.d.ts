import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Producer } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProducerService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Producer>;
    create(producer: Producer, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Producer>;
    update(producer: Producer, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Producer>;
    delete(producer: Producer | any): Promise<Array<any> | any>;
}
