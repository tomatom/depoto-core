"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductBundleService = void 0;
var entities_1 = require("../entities");
var ProductBundleService = /** @class */ (function (_super) {
    __extends(ProductBundleService, _super);
    function ProductBundleService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    // not implemented on api
    // getList(parameters: Object|any = {}, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Object|any> {
    //   const params = {
    //     page: parameters.page > 0 ? parameters.page : 1,
    //     sort: !!parameters.sort ? parameters.sort : 'id',
    //     direction: !!parameters.direction ? parameters.direction : 'DESC',
    //     filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    //   }
    //   return this.getEntityList('ProductBundle', 'productBundles', params, definitionDepth, definitionExcludedClasses, schema)
    // }
    //
    // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductBundle> {
    //   return this.getSingleEntity('ProductBundle', 'productBundle', {id: id}, definitionDepth, definitionExcludedClasses, schema)
    // }
    ProductBundleService.prototype.create = function (productBundle, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            parent: (_a = productBundle.parent) === null || _a === void 0 ? void 0 : _a.id,
            child: (_b = productBundle.child) === null || _b === void 0 ? void 0 : _b.id,
            quantity: productBundle.quantity,
        };
        return this.createUpdateEntity('ProductBundle', 'createProductBundle', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductBundleService.prototype.update = function (productBundle, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: productBundle.id,
            parent: (_a = productBundle.parent) === null || _a === void 0 ? void 0 : _a.id,
            child: (_b = productBundle.child) === null || _b === void 0 ? void 0 : _b.id,
            quantity: productBundle.quantity,
        };
        return this.createUpdateEntity('ProductBundle', 'updateProductBundle', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductBundleService.prototype.delete = function (productBundle) {
        var params = { id: productBundle.id };
        return this.deleteEntity('ProductBundle', 'deleteProductBundle', params);
    };
    return ProductBundleService;
}(entities_1.BaseEntityService));
exports.ProductBundleService = ProductBundleService;
//# sourceMappingURL=ProductBundleService.js.map