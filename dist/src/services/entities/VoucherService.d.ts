import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Voucher } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class VoucherService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Voucher>;
    create(voucher: Voucher, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Voucher>;
    update(voucher: Voucher, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Voucher>;
    delete(voucher: Voucher | any): Promise<Array<any> | any>;
}
