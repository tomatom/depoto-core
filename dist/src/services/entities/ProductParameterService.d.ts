import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { ProductParameter } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProductParameterService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductParameter>;
    create(productParameter: ProductParameter, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductParameter>;
    update(productParameter: ProductParameter, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductParameter>;
    delete(productParameter: ProductParameter | any): Promise<Array<any> | any>;
}
