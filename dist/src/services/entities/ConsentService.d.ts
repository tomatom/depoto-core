import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Consent, CustomerConsent } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ConsentService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Consent>;
    create(consent: Consent, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Consent>;
    update(consent: Consent, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Consent>;
    delete(consent: Consent | any): Promise<Array<any> | any>;
    createCustomerConsent(customerConsent: CustomerConsent | any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<CustomerConsent>;
    deleteCustomerConsent(customerConsent: CustomerConsent | any): Promise<Array<any> | any>;
}
