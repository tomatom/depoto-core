import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Product, ProductDepot } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProductService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Product>;
    create(product: Product, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Product>;
    update(product: Product, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Product>;
    updateProductPart(product: Product | any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Product>;
    updateProductDepotPart(productDepot: Partial<ProductDepot>, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Product>;
    updateProductDepotPositions(productDepot: ProductDepot, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Product>;
    updateProductDepotInventoryQuantity(productDepot: ProductDepot, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Product>;
    getNextEan(): Promise<string>;
    getProductPurchasePrice(productId: number, depotId: number, quantity: number): Promise<string>;
    getProductSellItems(productId: number, checkoutId?: number, depotIds?: number[], definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    delete(product: Product | any): Promise<Array<any> | any>;
}
