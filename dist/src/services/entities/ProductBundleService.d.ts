import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { ProductBundle } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProductBundleService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    create(productBundle: ProductBundle, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductBundle>;
    update(productBundle: ProductBundle, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductBundle>;
    delete(productBundle: ProductBundle | any): Promise<Array<any> | any>;
}
