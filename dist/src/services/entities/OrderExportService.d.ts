import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { OrderExport } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class OrderExportService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    create(orderExport: OrderExport, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<OrderExport>;
}
