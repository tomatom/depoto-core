"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.VoucherService = void 0;
var entities_1 = require("../entities");
var VoucherService = /** @class */ (function (_super) {
    __extends(VoucherService, _super);
    function VoucherService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    VoucherService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Voucher', 'vouchers', params, definitionDepth, definitionExcludedClasses, schema);
    };
    VoucherService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Voucher', 'voucher', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    VoucherService.prototype.create = function (voucher, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: voucher.name,
            code: voucher.code,
            discountType: voucher.discountType,
            discountPercent: voucher.discountPercent,
            discountValue: voucher.discountValue,
            maxUse: voucher.maxUse,
            validFrom: voucher.validFrom,
            validTo: voucher.validTo,
            enabled: voucher.enabled,
            isPayment: voucher.isPayment,
            externalId: voucher.externalId,
        };
        return this.createUpdateEntity('Voucher', 'createVoucher', params, definitionDepth, definitionExcludedClasses, schema);
    };
    VoucherService.prototype.update = function (voucher, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: voucher.id,
            name: voucher.name,
            code: voucher.code,
            discountType: voucher.discountType,
            discountPercent: voucher.discountPercent,
            discountValue: voucher.discountValue,
            maxUse: voucher.maxUse,
            validFrom: voucher.validFrom,
            validTo: voucher.validTo,
            enabled: voucher.enabled,
            isPayment: voucher.isPayment,
            externalId: voucher.externalId,
        };
        return this.createUpdateEntity('Voucher', 'updateVoucher', params, definitionDepth, definitionExcludedClasses, schema);
    };
    VoucherService.prototype.delete = function (voucher) {
        var params = { id: voucher.id };
        return this.deleteEntity('Voucher', 'deleteVoucher', params);
    };
    return VoucherService;
}(entities_1.BaseEntityService));
exports.VoucherService = VoucherService;
//# sourceMappingURL=VoucherService.js.map