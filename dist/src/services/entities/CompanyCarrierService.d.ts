import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { CompanyCarrier } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class CompanyCarrierService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<CompanyCarrier>;
    create(companyCarrier: CompanyCarrier, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<CompanyCarrier>;
    update(companyCarrier: CompanyCarrier, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<CompanyCarrier>;
    delete(companyCarrier: CompanyCarrier | any): Promise<Array<any> | any>;
}
