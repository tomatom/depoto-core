"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderService = void 0;
var utils_1 = require("../../utils");
var entities_1 = require("../entities");
var OrderService = /** @class */ (function (_super) {
    __extends(OrderService, _super);
    function OrderService(config, vatService) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        _this.vatService = vatService;
        return _this;
    }
    OrderService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Order', 'orders', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.getProcessStates = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('ProcessStatus', 'processStatuses', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.getOrderItems = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('OrderItem', 'orderItems', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Order', 'order', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.create = function (order, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b, _c, _d, _e, _f;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            checkout: (_a = order.checkout) === null || _a === void 0 ? void 0 : _a.id,
            status: 'reservation',
            note: order.note,
            privateNote: order.privateNote,
            dateCreated: utils_1.DateUtil.getDateStringFromUTCString(new Date().toUTCString()),
            items: this.prepareOrderItems(order),
            paymentItems: this.preparePaymentItems(order),
            customer: order.customer && order.customer.id > 0 ? order.customer.id : null,
            priority: order.priority,
        };
        if (order.carrier && order.carrier.id && order.carrier.id.length > 0) {
            params.carrier = order.carrier.id;
        }
        if (order.invoiceAddress && order.invoiceAddress.id > 0) {
            params.invoiceAddress = order.invoiceAddress.id;
        }
        if (order.shippingAddress && order.shippingAddress.id > 0) {
            params.shippingAddress = order.shippingAddress.id;
        }
        if (order.reservationNumber && order.reservationNumber > 0) {
            params.reservationNumber = order.reservationNumber;
        }
        if (order.group && order.group.id > 0) {
            params.group = order.group.id;
        }
        if (order.groupPosition && order.groupPosition > 0) {
            params.groupPosition = order.groupPosition;
        }
        if (order.tags && order.tags.length > 0) {
            params.tags = order.tags.map(function (t) { return t.id; });
        }
        if (order.currency && ((_c = (_b = order.currency) === null || _b === void 0 ? void 0 : _b.id) === null || _c === void 0 ? void 0 : _c.length) > 0) {
            params.currency = order.currency.id;
        }
        if (((_d = order.dateExpedition) === null || _d === void 0 ? void 0 : _d.length) > 0) {
            params.dateExpedition = order.dateExpedition;
        }
        if (((_e = order.dateDue) === null || _e === void 0 ? void 0 : _e.length) > 0) {
            params.dateDue = order.dateDue;
        }
        if (((_f = order.dateTax) === null || _f === void 0 ? void 0 : _f.length) > 0) {
            params.dateTax = order.dateTax;
        }
        return this.createUpdateEntity('Order', 'createOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.update = function (order, keepReservation, dontSendPayments, updateProcessStatus, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b, _c, _d, _e;
        if (keepReservation === void 0) { keepReservation = false; }
        if (dontSendPayments === void 0) { dontSendPayments = false; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
            status: keepReservation ? 'reservation' : 'bill',
            note: order.note,
            privateNote: order.privateNote,
            items: this.prepareOrderItems(order),
            boxes: order.boxes && order.boxes.length > 0 ? order.boxes : [],
            // reservationNumber: order.reservationNumber,
            dateCreated: utils_1.DateUtil.getDateStringFromUTCString(order.dateCreated),
            customer: order.customer && order.customer.id > 0 ? order.customer.id : null,
            priority: order.priority,
            dateExpedition: order.dateExpedition,
            dateDue: order.dateDue,
            dateTax: order.dateTax,
            shippingAddress: ((_a = order.shippingAddress) === null || _a === void 0 ? void 0 : _a.id) || null,
            invoiceAddress: ((_b = order.invoiceAddress) === null || _b === void 0 ? void 0 : _b.id) || null,
            group: ((_c = order.group) === null || _c === void 0 ? void 0 : _c.id) || null,
            groupPosition: order.groupPosition || null,
        };
        if (!dontSendPayments) {
            params.paymentItems = this.preparePaymentItems(order);
        }
        if (updateProcessStatus && updateProcessStatus.length > 0) {
            params.processStatusRelation = { status: updateProcessStatus };
        }
        if (order.carrier && order.carrier.id && order.carrier.id.length > 0) {
            params.carrier = order.carrier.id;
        }
        if (order.tags && order.tags.length > 0) {
            params.tags = order.tags.map(function (t) { return t.id; });
        }
        if (order.currency && ((_e = (_d = order.currency) === null || _d === void 0 ? void 0 : _d.id) === null || _e === void 0 ? void 0 : _e.length) > 0) {
            params.currency = order.currency.id;
        }
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateOrderPart = function (order, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b, _c;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
        };
        if (!!((_a = order.status) === null || _a === void 0 ? void 0 : _a.id)) {
            params.status = order.status.id;
        }
        if (!!order.billNumber) {
            params.billNumber = order.billNumber;
        }
        if (!!order.reservationNumber) {
            params.reservationNumber = order.reservationNumber;
        }
        if (!!order.dateCreated) {
            params.dateCreated = utils_1.DateUtil.getDateStringFromUTCString(order.dateCreated);
        }
        if (!!order.note) {
            params.note = order.note;
        }
        if (!!order.privateNote) {
            params.privateNote = order.privateNote;
        }
        if (order.boxes && order.boxes.length > 0) {
            params.boxes = order.boxes;
        }
        if (order.items && order.items.length > 0) {
            params.items = this.prepareOrderItems(order);
        }
        if (order.paymentItems && order.paymentItems.length > 0) {
            params.paymentItems = this.preparePaymentItems(order);
        }
        if (order.customer && order.customer.id > 0) {
            params.customer = order.customer.id;
        }
        if (order.priority && order.priority > 0) {
            params.priority = order.priority;
        }
        if (order.carrier && order.carrier.id && order.carrier.id.length > 0) {
            params.carrier = order.carrier.id;
        }
        if (order.invoiceAddress && order.invoiceAddress.id > 0) {
            params.invoiceAddress = order.invoiceAddress.id;
        }
        if (order.shippingAddress && order.shippingAddress.id > 0) {
            params.shippingAddress = order.shippingAddress.id;
        }
        if (order.group && order.group.id > 0) {
            params.group = order.group.id;
        }
        if (order.groupPosition && order.groupPosition > 0) {
            params.groupPosition = order.groupPosition;
        }
        if (order.tags && order.tags.length > 0) {
            params.tags = order.tags.map(function (t) { return t.id; });
        }
        if (order.currency && ((_c = (_b = order.currency) === null || _b === void 0 ? void 0 : _b.id) === null || _c === void 0 ? void 0 : _c.length) > 0) {
            params.currency = order.currency.id;
        }
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateOrderNotes = function (order, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
        };
        if (order.note && order.note.length > 0) {
            params.note = order.note;
        }
        if (order.privateNote && order.privateNote.length > 0) {
            params.privateNote = order.privateNote;
        }
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateOrderCarrier = function (order, definitionDepth, definitionExcludedClasses, schema) {
        var _a;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
            carrier: (_a = order.carrier) === null || _a === void 0 ? void 0 : _a.id,
        };
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    // TODO: moves? updateClearanceItems {id, picked: num, packed: num, packageId: optional}
    OrderService.prototype.updateOrderPickedPacked = function (order, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
            items: order.items.map(function (i) { return ({
                id: i.id,
                picked: i.picked,
                packed: i.packed,
            }); }),
        };
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateOrderItemPart = function (orderItemPart, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: orderItemPart.id,
            name: orderItemPart.name,
            type: orderItemPart.type,
            ean: orderItemPart.ean,
            code: orderItemPart.code,
            quantity: orderItemPart.quantity,
            note: orderItemPart.note,
            price: orderItemPart.price,
            sale: orderItemPart.sale,
            vat: (_a = orderItemPart.vat) === null || _a === void 0 ? void 0 : _a.id,
            product: (_b = orderItemPart.product) === null || _b === void 0 ? void 0 : _b.id,
            serial: orderItemPart.serial,
            expirationDate: orderItemPart.expirationDate,
            batch: orderItemPart.batch,
            isForSubsequentSettlement: orderItemPart.isForSubsequentSettlement,
            picked: orderItemPart.picked,
            packed: orderItemPart.packed,
        };
        Object.keys(params).forEach(function (k, i) {
            if (params[k] === undefined) {
                delete params[k];
            }
        });
        return this.createUpdateEntity('OrderItem', 'updateOrderItem', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateOrderBoxes = function (order, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
            boxes: order.boxes && order.boxes.length > 0 ? order.boxes : [],
        };
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateProcessStatus = function (order, processStatus, note, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
            processStatusRelation: { status: processStatus },
        };
        if (note && note.length > 0) {
            params.processStatusRelation.note = note;
        }
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateOrderGroupAndPosition = function (order, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: order.id,
            group: !!order.group && order.group.id ? order.group.id : null,
            groupPosition: order.groupPosition,
        };
        return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateClearanceItem = function (clearanceItem, orderItem, type, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: clearanceItem.id,
            orderItem: orderItem.id,
        };
        params[type] = clearanceItem[type];
        return this.createUpdateEntity('ClearanceItem', 'updateClearanceItem', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateClearanceItemFromOrder = function (clearanceItem, order, type, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: clearanceItem.id,
            order: order.id,
        };
        params[type] = clearanceItem[type];
        return this.createUpdateEntity('ClearanceItem', 'updateClearanceItem', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateClearanceItemFromOrderGroup = function (clearanceItem, orderGroup, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: clearanceItem.id,
            orderGroup: orderGroup.id,
            picked: clearanceItem.picked,
            packed: clearanceItem.packed,
        };
        return this.createUpdateEntity('ClearanceItem', 'updateClearanceItem', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateClearanceItemLocations = function (clearanceItem, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: clearanceItem.id,
            locations: clearanceItem.locations || [],
        };
        if (clearanceItem.orderItem) {
            params.orderItem = clearanceItem.orderItem;
        }
        if (clearanceItem.order) {
            params.order = clearanceItem.order;
        }
        return this.createUpdateEntity('ClearanceItem', 'updateClearanceItem', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.deleteOrderItem = function (orderItem) {
        var params = {
            id: orderItem.id,
        };
        return this.deleteEntity('OrderItem', 'deleteOrderItem', params);
    };
    OrderService.prototype.deleteReservation = function (order) {
        var params = { id: order.id };
        return this.deleteEntity('Order', 'deleteReservation', params);
    };
    // delete(order: Order|any): Promise<Array<any>|any> {
    //   const params: any = {id: order.id}
    //   return this.deleteEntity('Order', 'deleteOrder', params)
    // }
    OrderService.prototype.getOrderGroups = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('OrderGroup', 'orderGroups', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.getOrderGroupById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('OrderGroup', 'orderGroup', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.createOrderGroup = function (orderGroup, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = { name: orderGroup.name };
        if (!!orderGroup.user && orderGroup.user.id > 0) {
            params.user = orderGroup.user.id;
        }
        if (!isNaN(orderGroup.userPosition)) {
            params.userPosition = orderGroup.userPosition;
        }
        return this.createUpdateEntity('OrderGroup', 'createOrderGroup', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.updateOrderGroup = function (orderGroup, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: orderGroup.id,
            name: orderGroup.name,
            user: !!orderGroup.user && orderGroup.user.id > 0 ? orderGroup.user.id : null,
            userPosition: !isNaN(orderGroup.userPosition) ? orderGroup.userPosition : 0,
        };
        return this.createUpdateEntity('OrderGroup', 'updateOrderGroup', params, definitionDepth, definitionExcludedClasses, schema);
    };
    OrderService.prototype.deleteOrderGroup = function (orderGroup) {
        var params = { id: orderGroup.id };
        return this.deleteEntity('OrderGroup', 'deleteOrderGroup', params);
    };
    OrderService.prototype.preparePaymentItems = function (order) {
        var _a;
        var paymentItems = [];
        (_a = order.paymentItems) === null || _a === void 0 ? void 0 : _a.forEach(function (pi) {
            var _a;
            var p = {
                payment: Number((_a = pi.payment) === null || _a === void 0 ? void 0 : _a.id),
                amount: pi.amount,
                dateCreated: utils_1.DateUtil.getDateStringFromUTCString(pi.dateCreated),
                isPaid: pi.isPaid,
            };
            if (pi.checkout && pi.checkout.id > 0) {
                p.checkout = pi.checkout.id;
            }
            if (pi.dateCancelled) {
                p.dateCancelled = utils_1.DateUtil.getDateStringFromUTCString(pi.dateCancelled);
            }
            paymentItems.push(p);
        });
        return paymentItems;
    };
    OrderService.prototype.prepareOrderItems = function (order) {
        var _this = this;
        var _a;
        var orderItems = [];
        (_a = order.items) === null || _a === void 0 ? void 0 : _a.forEach(function (orderItem) {
            var oi = {
                name: orderItem.product ? orderItem.product.fullName : orderItem.name,
                quantity: orderItem.quantity,
                type: orderItem.type ? orderItem.type : 'product',
                sale: Number(orderItem.sale).toFixed(2),
                price: Number(orderItem.price) > 0 ? Number(orderItem.price).toFixed(3) : Number(orderItem.price),
                vat: orderItem.vat && orderItem.vat.id > 0 ? orderItem.vat.id : _this.vatService.defaultVat.id,
                isForSubsequentSettlement: orderItem.isForSubsequentSettlement,
            };
            if (orderItem.id && orderItem.id > 0) {
                oi.id = orderItem.id;
            }
            if (orderItem.product && orderItem.product.id > 0) {
                oi.product = orderItem.product && orderItem.product.id > 0 ? orderItem.product.id : orderItem.id;
            }
            if (orderItem.ean && orderItem.ean.length > 0) {
                oi.ean = orderItem.ean;
            }
            if (orderItem.code && orderItem.code.length > 0) {
                oi.code = orderItem.code;
            }
            if (orderItem.serial && orderItem.serial.length > 0) {
                oi.serial = orderItem.serial;
            }
            if (orderItem.note && orderItem.note.length > 0) {
                oi.note = orderItem.note;
            }
            orderItems.push(oi);
        });
        return orderItems;
    };
    return OrderService;
}(entities_1.BaseEntityService));
exports.OrderService = OrderService;
//# sourceMappingURL=OrderService.js.map