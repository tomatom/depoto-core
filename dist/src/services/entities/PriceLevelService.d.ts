import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { PriceLevel } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from './BaseEntityService';
export declare class PriceLevelService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClass?: string[]): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<PriceLevel>;
    create(priceLevel: PriceLevel, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<PriceLevel>;
    update(priceLevel: PriceLevel, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<PriceLevel>;
    delete(priceLevel: PriceLevel | any): Promise<Array<any> | any>;
}
