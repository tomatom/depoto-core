"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageService = void 0;
var entities_1 = require("../entities");
var PackageService = /** @class */ (function (_super) {
    __extends(PackageService, _super);
    function PackageService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    PackageService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Package', 'packages', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Package', 'package', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.create = function (pack, definitionDepth, definitionExcludedClasses, schema) {
        var _a;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            order: pack.order.id,
            carrier: pack.carrier.id,
            items: ((_a = pack.items) === null || _a === void 0 ? void 0 : _a.map(function (i) { return i.id; })) || [],
        };
        if (pack.weight && pack.weight > 0) {
            params.weight = Number(pack.weight);
        }
        if (pack.dimensionX && pack.dimensionX > 0) {
            params.dimensionX = Number(pack.dimensionX);
        }
        if (pack.dimensionY && pack.dimensionY > 0) {
            params.dimensionY = Number(pack.dimensionY);
        }
        if (pack.dimensionZ && pack.dimensionZ > 0) {
            params.dimensionZ = Number(pack.dimensionZ);
        }
        return this.createUpdateEntity('Package', 'createPackage', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.update = function (pack, definitionDepth, definitionExcludedClasses, schema) {
        var _a;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: pack.id,
            order: pack.order.id,
            carrier: pack.carrier.id,
            items: ((_a = pack.items) === null || _a === void 0 ? void 0 : _a.map(function (i) { return i.id; })) || [],
        };
        if (pack.weight && pack.weight > 0) {
            params.weight = Number(pack.weight);
        }
        if (pack.dimensionX && pack.dimensionX > 0) {
            params.dimensionX = Number(pack.dimensionX);
        }
        if (pack.dimensionY && pack.dimensionY > 0) {
            params.dimensionY = Number(pack.dimensionY);
        }
        if (pack.dimensionZ && pack.dimensionZ > 0) {
            params.dimensionZ = Number(pack.dimensionZ);
        }
        return this.createUpdateEntity('Package', 'updatePackage', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.updatePart = function (pack, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: pack.id,
        };
        if (((_a = pack.order) === null || _a === void 0 ? void 0 : _a.id) && pack.order.id > 0) {
            params.order = pack.order.id;
        }
        if (((_b = pack.carrier) === null || _b === void 0 ? void 0 : _b.id) && pack.carrier.id.length > 0) {
            params.carrier = pack.carrier.id;
        }
        if (pack.items && pack.items.length > 0) {
            params.items = pack.items.map(function (i) { return i.id; });
        }
        if ((pack.weight && pack.weight > 0) || pack.weight === null) {
            params.weight = pack.weight;
        }
        if ((pack.dimensionX && pack.dimensionX > 0) || pack.dimensionX === null) {
            params.dimensionX = Number(pack.dimensionX);
        }
        if ((pack.dimensionY && pack.dimensionY > 0) || pack.dimensionY === null) {
            params.dimensionY = Number(pack.dimensionY);
        }
        if ((pack.dimensionZ && pack.dimensionZ > 0) || pack.dimensionZ === null) {
            params.dimensionZ = Number(pack.dimensionZ);
        }
        return this.createUpdateEntity('Package', 'updatePackage', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.send = function (pack, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: pack.id,
        };
        return this.createUpdateEntity('Package', 'sendPackage', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.reset = function (pack) {
        var params = {
            id: pack.id,
        };
        return this.createUpdateEntity('Package', 'resetPackage', params, undefined, undefined, { errors: null });
    };
    PackageService.prototype.delete = function (pack) {
        var params = { id: pack.id };
        return this.deleteEntity('Package', 'deletePackage', params);
    };
    PackageService.prototype.getDisposals = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('PackageDisposal', 'disposals', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.getDisposal = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('PackageDisposal', 'disposal', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.createDisposal = function (company, carrier, packages, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            company: company.id,
            carrier: carrier.id,
            packages: (packages === null || packages === void 0 ? void 0 : packages.map(function (i) { return i.id; })) || [],
        };
        return this.createUpdateEntity('PackageDisposal', 'createDisposal', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.updateDisposalPart = function (disposal, definitionDepth, definitionExcludedClasses, schema) {
        var _a;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: disposal.id,
        };
        if (disposal.carrier && ((_a = disposal.carrier.id) === null || _a === void 0 ? void 0 : _a.length) > 0) {
            params.carrier = disposal.carrier.id;
        }
        if (disposal.company && disposal.company.id > 0) {
            params.company = disposal.company.id;
        }
        if (disposal.packages && disposal.packages.length > 0) {
            params.packages = disposal.packages.map(function (p) { return p.id; });
        }
        return this.createUpdateEntity('PackageDisposal', 'updateDisposal', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.sendDisposal = function (disposal, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: disposal.id,
        };
        return this.createUpdateEntity('PackageDisposal', 'sendDisposal', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PackageService.prototype.deleteDisposal = function (disposal) {
        var params = { id: disposal.id };
        return this.deleteEntity('PackageDisposal', 'deleteDisposal', params);
    };
    return PackageService;
}(entities_1.BaseEntityService));
exports.PackageService = PackageService;
//# sourceMappingURL=PackageService.js.map