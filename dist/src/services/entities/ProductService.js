"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductService = void 0;
var entities_1 = require("../entities");
var ProductService = /** @class */ (function (_super) {
    __extends(ProductService, _super);
    function ProductService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    ProductService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Product', 'products', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Product', 'product', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.create = function (product, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: product.name,
            ean: product.ean,
            code: product.code,
            purchasePrice: product.purchasePrice,
            sellPrice: product.sellPrice,
            beforeSellPrice: product.beforeSellPrice,
            weight: product.weight,
            enabled: product.enabled,
            isBundle: product.isBundle,
            vat: product.vat.id,
            note: product.note,
            isForSubsequentSettlement: product.isForSubsequentSettlement,
            dimensionX: product.dimensionX,
            dimensionY: product.dimensionY,
            dimensionZ: product.dimensionZ,
            originCountry: product.originCountry,
            hsCode: product.hsCode,
            description: product.description,
        };
        if (product.producer && product.producer.id > 0) {
            params.producer = product.producer.id;
        }
        if (product.parent && product.parent.id > 0) {
            params.parent = product.parent.id;
        }
        if (product.ean2 && product.ean2.length > 0) {
            params.ean2 = product.ean2;
        }
        if (product.ean3 && product.ean3.length > 0) {
            params.ean3 = product.ean3;
        }
        if (product.ean4 && product.ean4.length > 0) {
            params.ean4 = product.ean4;
        }
        if (product.ean5 && product.ean5.length > 0) {
            params.ean5 = product.ean5;
        }
        if (product.isFragile !== null) {
            params.isFragile = product.isFragile;
        }
        if (product.isOversize !== null) {
            params.isOversize = product.isOversize;
        }
        if (product.supplier && product.supplier.id) {
            params.supplier = product.supplier.id;
        }
        if (product.mainCategory && product.mainCategory.id > 0) {
            params.mainCategory = product.mainCategory.id;
        }
        if (product.purchaseCurrency && product.purchaseCurrency.id) {
            params.purchaseCurrency = product.purchaseCurrency.id;
        }
        if (product.categories && product.categories.length > 0) {
            params.categories = product.categories.map(function (t) { return t.id; });
        }
        if (product.tags && product.tags.length > 0) {
            params.tags = product.tags.map(function (t) { return t.id; });
        }
        return this.createUpdateEntity('Product', 'createProduct', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.update = function (product, definitionDepth, definitionExcludedClasses, schema) {
        var _a, _b, _c, _d, _e;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: product.id,
            name: product.name,
            ean: product.ean,
            ean2: product.ean2,
            ean3: product.ean3,
            ean4: product.ean4,
            ean5: product.ean5,
            code: product.code,
            purchasePrice: product.purchasePrice,
            sellPrice: product.sellPrice,
            beforeSellPrice: product.beforeSellPrice,
            weight: product.weight,
            enabled: product.enabled,
            isBundle: product.isBundle,
            parent: product.parent && product.parent.id > 0 ? product.parent.id : null,
            vat: product.vat.id,
            note: product.note,
            isForSubsequentSettlement: product.isForSubsequentSettlement,
            dimensionX: product.dimensionX,
            dimensionY: product.dimensionY,
            dimensionZ: product.dimensionZ,
            originCountry: product.originCountry,
            hsCode: product.hsCode,
            description: product.description,
            mainCategory: product.mainCategory && product.mainCategory.id > 0 ? product.mainCategory.id : null,
            categories: ((_a = product.categories) === null || _a === void 0 ? void 0 : _a.length) > 0 ? product.categories.map(function (t) { return t.id; }) : [],
            tags: ((_b = product.tags) === null || _b === void 0 ? void 0 : _b.length) > 0 ? product.tags.map(function (t) { return t.id; }) : [],
            children: ((_c = product.children) === null || _c === void 0 ? void 0 : _c.length) > 0 ? product.children.map(function (t) { return t.id; }) : [],
            producer: product.producer && product.producer.id > 0 ? product.producer.id : null,
            supplier: product.supplier && product.supplier.id > 0 ? product.supplier.id : null,
            purchaseCurrency: ((_e = (_d = product.purchaseCurrency) === null || _d === void 0 ? void 0 : _d.id) === null || _e === void 0 ? void 0 : _e.length) > 0 ? product.purchaseCurrency.id : null,
            isFragile: !!product.isFragile,
            isOversize: !!product.isOversize,
        };
        return this.createUpdateEntity('Product', 'updateProduct', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.updateProductPart = function (product, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = { id: product.id };
        if (product.name && product.name.length > 0) {
            params.name = product.name;
        }
        if (product.ean && product.ean.length > 0) {
            params.ean = product.ean;
        }
        if (product.ean2 && product.ean2.length > 0) {
            params.ean2 = product.ean2;
        }
        if (product.ean3 && product.ean3.length > 0) {
            params.ean3 = product.ean3;
        }
        if (product.ean4 && product.ean4.length > 0) {
            params.ean4 = product.ean4;
        }
        if (product.ean5 && product.ean5.length > 0) {
            params.ean5 = product.ean5;
        }
        if (product.code && product.code.length > 0) {
            params.code = product.code;
        }
        if (product.originCountry && product.originCountry.length > 0) {
            params.originCountry = product.originCountry;
        }
        if (product.hsCode && product.hsCode.length > 0) {
            params.hsCode = product.hsCode;
        }
        if (product.dimensionX !== 0) {
            params.dimensionX = product.dimensionX;
        }
        if (product.dimensionY !== 0) {
            params.dimensionY = product.dimensionY;
        }
        if (product.dimensionZ !== 0) {
            params.dimensionZ = product.dimensionZ;
        }
        if (typeof product.purchasePrice !== 'undefined' && !!product.purchasePrice && product.purchasePrice !== 0) {
            params.purchasePrice = product.purchasePrice;
        }
        if (product.purchaseCurrency && product.purchaseCurrency.id) {
            params.purchaseCurrency = product.purchaseCurrency.id;
        }
        if (typeof product.sellPrice !== 'undefined' && !!product.sellPrice && product.sellPrice !== 0) {
            params.sellPrice = product.sellPrice;
        }
        if (typeof product.beforeSellPrice !== 'undefined' && !!product.beforeSellPrice && product.beforeSellPrice !== 0) {
            params.beforeSellPrice = product.beforeSellPrice;
        }
        if (typeof product.weight !== 'undefined' && !!product.weight && product.weight !== 0) {
            params.weight = product.weight;
        }
        if (typeof product.enabled !== 'undefined') {
            params.enabled = product.enabled;
        }
        if (product.parent && product.parent.id > 0) {
            params.parent = product.parent.id;
        }
        if (product.vat && product.vat.id > 0) {
            params.vat = product.vat.id;
        }
        if (product.note && product.note.length > 0) {
            params.note = product.note;
        }
        if (typeof product.isForSubsequentSettlement !== 'undefined') {
            params.isForSubsequentSettlement = product.isForSubsequentSettlement;
        }
        if (product.producer && product.producer.id > 0) {
            params.producer = product.producer.id;
        }
        if (product.supplier && product.supplier.id) {
            params.supplier = product.supplier.id;
        }
        if (product.children && product.children.length > 0) {
            params.children = product.children.map(function (p) { return p.id; });
        }
        if (typeof product.isFragile !== 'undefined') {
            params.isFragile = product.isFragile;
        }
        if (typeof product.isOversize !== 'undefined') {
            params.isOversize = product.isOversize;
        }
        if (product.description && product.description.length > 0) {
            params.description = product.description;
        }
        if (product.mainCategory && product.mainCategory.id > 0) {
            params.mainCategory = product.mainCategory.id;
        }
        if (product.categories && product.categories.length > 0) {
            params.categories = product.categories.map(function (t) { return t.id; });
        }
        if (product.tags && product.tags.length > 0) {
            params.tags = product.tags.map(function (t) { return t.id; });
        }
        return this.createUpdateEntity('Product', 'updateProduct', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.updateProductDepotPart = function (productDepot, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: productDepot.id,
        };
        if (productDepot.position1 !== undefined) {
            params.position1 = productDepot.position1;
        }
        if (productDepot.position2 !== undefined) {
            params.position2 = productDepot.position2;
        }
        if (productDepot.position3 !== undefined) {
            params.position3 = productDepot.position3;
        }
        if (productDepot.purchasePrice !== undefined) {
            params.purchasePrice = productDepot.purchasePrice;
        }
        if (productDepot.supplier && productDepot.supplier.id > 0) {
            params.supplier = productDepot.supplier.id;
        }
        if (productDepot.batch !== undefined) {
            params.batch = productDepot.batch;
        }
        if (productDepot.expirationDate !== undefined) {
            params.expirationDate = productDepot.expirationDate;
        }
        if (productDepot.inventoryQuantityStock !== undefined) {
            params.inventoryQuantityStock = productDepot.inventoryQuantityStock;
        }
        return this.createUpdateEntity('ProductDepot', 'updateProductDepot', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.updateProductDepotPositions = function (productDepot, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: productDepot.id,
            position1: productDepot.position1,
            position2: productDepot.position2,
            position3: productDepot.position3,
        };
        return this.createUpdateEntity('ProductDepot', 'updateProductDepot', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.updateProductDepotInventoryQuantity = function (productDepot, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: productDepot.id,
            inventoryQuantityStock: productDepot.inventoryQuantityStock,
        };
        return this.createUpdateEntity('ProductDepot', 'updateProductDepot', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.getNextEan = function () {
        var params = {};
        return this.getSingleEntity('', 'nextEan', params, undefined, []);
    };
    ProductService.prototype.getProductPurchasePrice = function (productId, depotId, quantity) {
        var params = {
            id: productId,
            depot: Number(depotId),
            quantity: Number(quantity),
        };
        return this.getEntityList('ProductPurchasePrice', 'productPurchasePrice', params, undefined, []);
    };
    ProductService.prototype.getProductSellItems = function (productId, checkoutId, depotIds, definitionDepth, definitionExcludedClasses, schema) {
        if (depotIds === void 0) { depotIds = []; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            product: productId,
        };
        if (checkoutId && checkoutId > 0) {
            params.checkout = checkoutId;
        }
        if (depotIds && depotIds.length > 0) {
            params.depots = depotIds;
        }
        return this.getEntityList('SellItem', 'productSellItems', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductService.prototype.delete = function (product) {
        var params = { id: product.id };
        return this.deleteEntity('Product', 'deleteProduct', params);
    };
    return ProductService;
}(entities_1.BaseEntityService));
exports.ProductService = ProductService;
//# sourceMappingURL=ProductService.js.map