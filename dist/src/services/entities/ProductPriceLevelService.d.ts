import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { ProductPriceLevel } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProductPriceLevelService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPriceLevel>;
    create(productPriceLevel: ProductPriceLevel, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPriceLevel>;
    update(productPriceLevel: ProductPriceLevel, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPriceLevel>;
    delete(productPriceLevel: ProductPriceLevel | any): Promise<Array<any> | any>;
}
