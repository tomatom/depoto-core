"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPackService = void 0;
var entities_1 = require("../entities");
var ProductPackService = /** @class */ (function (_super) {
    __extends(ProductPackService, _super);
    function ProductPackService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    ProductPackService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('ProductPack', 'productPacks', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductPackService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('ProductPack', 'productPack', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductPackService.prototype.create = function (productPack, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            code: productPack.code,
            ean: productPack.ean,
            externalId: productPack.externalId,
            product: productPack.product ? productPack.product.id : null,
            quantity: productPack.quantity || 0,
        };
        return this.createUpdateEntity('ProductPack', 'createProductPack', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductPackService.prototype.update = function (productPack, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: productPack.id,
            code: productPack.code,
            ean: productPack.ean,
            externalId: productPack.externalId,
            product: productPack.product ? productPack.product.id : null,
            quantity: productPack.quantity || 0,
        };
        return this.createUpdateEntity('ProductPack', 'updateProductPack', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductPackService.prototype.delete = function (productPack) {
        var params = { id: productPack.id };
        return this.deleteEntity('ProductPack', 'deleteProductPack', params);
    };
    return ProductPackService;
}(entities_1.BaseEntityService));
exports.ProductPackService = ProductPackService;
//# sourceMappingURL=ProductPackService.js.map