import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { ProductPack } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProductPackService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPack>;
    create(productPack: ProductPack, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPack>;
    update(productPack: ProductPack, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPack>;
    delete(productPack: ProductPack | any): Promise<Array<any> | any>;
}
