"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DepotService = void 0;
var entities_1 = require("../entities");
var DepotService = /** @class */ (function (_super) {
    __extends(DepotService, _super);
    function DepotService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    DepotService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Depot', 'depots', params, definitionDepth, definitionExcludedClasses, schema);
    };
    DepotService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Depot', 'depot', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    DepotService.prototype.create = function (depot, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = { name: depot.name, emailIn: depot.emailIn || '' };
        return this.createUpdateEntity('Depot', 'createDepot', params, definitionDepth, definitionExcludedClasses, schema);
    };
    DepotService.prototype.update = function (depot, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: depot.id,
            name: depot.name,
            emailIn: depot.emailIn || '',
        };
        return this.createUpdateEntity('Depot', 'updateDepot', params, definitionDepth, definitionExcludedClasses, schema);
    };
    DepotService.prototype.delete = function (depot) {
        var params = { id: depot.id };
        return this.deleteEntity('Depot', 'deleteDepot', params);
    };
    return DepotService;
}(entities_1.BaseEntityService));
exports.DepotService = DepotService;
//# sourceMappingURL=DepotService.js.map