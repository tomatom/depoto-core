import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { InventoryExport } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class InventoryExportService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<InventoryExport>;
    create(inventoryExport: InventoryExport, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<InventoryExport>;
    update(inventoryExport: InventoryExport, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<InventoryExport>;
}
