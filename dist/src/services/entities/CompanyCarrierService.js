"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyCarrierService = void 0;
var entities_1 = require("../entities");
var CompanyCarrierService = /** @class */ (function (_super) {
    __extends(CompanyCarrierService, _super);
    function CompanyCarrierService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    CompanyCarrierService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('CompanyCarrier', 'companyCarriers', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CompanyCarrierService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('CompanyCarrier', 'companyCarrier', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    CompanyCarrierService.prototype.create = function (companyCarrier, definitionDepth, definitionExcludedClasses, schema) {
        var _a;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            carrier: (_a = companyCarrier.carrier) === null || _a === void 0 ? void 0 : _a.id,
        };
        if (companyCarrier.options && companyCarrier.options.length > 0) {
            params.options = companyCarrier.options;
        }
        if (companyCarrier.checkout && companyCarrier.checkout.id > 0) {
            params.checkout = companyCarrier.checkout.id;
        }
        if (companyCarrier.externalId && companyCarrier.externalId.length > 0) {
            params.externalId = companyCarrier.externalId;
        }
        if (companyCarrier.enable !== null) {
            params.enable = companyCarrier.enable;
        }
        return this.createUpdateEntity('CompanyCarrier', 'createCompanyCarrier', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CompanyCarrierService.prototype.update = function (companyCarrier, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: companyCarrier.id,
            checkout: companyCarrier.checkout && companyCarrier.checkout.id > 0 ? companyCarrier.checkout.id : null,
        };
        if (companyCarrier.carrier && companyCarrier.carrier.id && companyCarrier.carrier.id.length > 0) {
            params.carrier = companyCarrier.carrier.id;
        }
        if (companyCarrier.options && companyCarrier.options.length > 0) {
            params.options = companyCarrier.options;
        }
        if (companyCarrier.externalId && companyCarrier.externalId.length > 0) {
            params.externalId = companyCarrier.externalId;
        }
        if (companyCarrier.enable !== null) {
            params.enable = companyCarrier.enable;
        }
        return this.createUpdateEntity('CompanyCarrier', 'updateCompanyCarrier', params, definitionDepth, definitionExcludedClasses, schema);
    };
    CompanyCarrierService.prototype.delete = function (companyCarrier) {
        var params = { id: companyCarrier.id };
        return this.deleteEntity('CompanyCarrier', 'deleteCompanyCarrier', params);
    };
    return CompanyCarrierService;
}(entities_1.BaseEntityService));
exports.CompanyCarrierService = CompanyCarrierService;
//# sourceMappingURL=CompanyCarrierService.js.map