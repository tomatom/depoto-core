"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PriceLevelService = void 0;
var BaseEntityService_1 = require("./BaseEntityService");
var PriceLevelService = /** @class */ (function (_super) {
    __extends(PriceLevelService, _super);
    function PriceLevelService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    PriceLevelService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClass) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClass === void 0) { definitionExcludedClass = []; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('PriceLevel', 'priceLevels', params, definitionDepth, definitionExcludedClass);
    };
    PriceLevelService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('PriceLevel', 'priceLevel', { id: id }, definitionDepth, definitionExcludedClasses);
    };
    PriceLevelService.prototype.create = function (priceLevel, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: priceLevel.name,
            isPercentage: priceLevel.isPercentage,
            percent: priceLevel.percent,
            currency: priceLevel.currency ? priceLevel.currency.id : null,
            externalId: priceLevel.externalId,
            withVat: priceLevel.withVat,
        };
        return this.createUpdateEntity('PriceLevel', 'createPriceLevel', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PriceLevelService.prototype.update = function (priceLevel, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: priceLevel.id,
            name: priceLevel.name,
            isPercentage: priceLevel.isPercentage,
            percent: priceLevel.percent,
            withVat: priceLevel.withVat,
            currency: priceLevel.currency ? priceLevel.currency.id : null,
            externalId: priceLevel.externalId,
        };
        return this.createUpdateEntity('PriceLevel', 'updatePriceLevel', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PriceLevelService.prototype.delete = function (priceLevel) {
        var params = { id: priceLevel.id };
        return this.deleteEntity('PriceLevel', 'deletePriceLevel', params);
    };
    return PriceLevelService;
}(BaseEntityService_1.BaseEntityService));
exports.PriceLevelService = PriceLevelService;
//# sourceMappingURL=PriceLevelService.js.map