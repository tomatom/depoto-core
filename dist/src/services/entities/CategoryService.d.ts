import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
import { Category } from '../../entities';
export declare class CategoryService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Category>;
    create(category: Category, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Category>;
    update(category: Category, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Category>;
    delete(category: Category | any): Promise<Array<any> | any>;
}
