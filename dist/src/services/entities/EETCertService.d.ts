import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { EETCert } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class EETCertService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETCert>;
    create(cert: EETCert, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETCert>;
    update(cert: EETCert, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETCert>;
    delete(cert: EETCert | any): Promise<Array<any> | any>;
}
