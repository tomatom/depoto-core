"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentService = void 0;
var entities_1 = require("../entities");
var PaymentService = /** @class */ (function (_super) {
    __extends(PaymentService, _super);
    function PaymentService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    PaymentService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Payment', 'payments', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PaymentService.prototype.getPaymentTypes = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('PaymentType', 'paymentTypes', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PaymentService.prototype.getCurrencies = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Currency', 'currencies', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PaymentService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Payment', 'payment', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    PaymentService.prototype.create = function (payment, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            name: payment.name,
            type: payment.type.id,
            eetEnable: payment.eetEnable,
        };
        return this.createUpdateEntity('Payment', 'createPayment', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PaymentService.prototype.update = function (payment, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: payment.id,
            name: payment.name,
            type: payment.type.id,
            eetEnable: payment.eetEnable,
        };
        return this.createUpdateEntity('Payment', 'updatePayment', params, definitionDepth, definitionExcludedClasses, schema);
    };
    PaymentService.prototype.delete = function (payment) {
        var params = { id: payment.id };
        return this.deleteEntity('Payment', 'deletePayment', params);
    };
    return PaymentService;
}(entities_1.BaseEntityService));
exports.PaymentService = PaymentService;
//# sourceMappingURL=PaymentService.js.map