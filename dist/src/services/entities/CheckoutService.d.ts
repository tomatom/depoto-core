import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Checkout } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class CheckoutService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Checkout>;
    create(checkout: Checkout, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Checkout>;
    update(checkout: Checkout, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Checkout>;
    setNextBillNumber(checkout: Checkout, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Checkout>;
    setNextReservationNumber(checkout: Checkout, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Checkout>;
    getEventTypes(): Promise<any>;
    delete(checkout: Checkout | any): Promise<Array<any> | any>;
}
