"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductMovePackService = void 0;
var entities_1 = require("../entities");
var ProductMovePackService = /** @class */ (function (_super) {
    __extends(ProductMovePackService, _super);
    function ProductMovePackService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    ProductMovePackService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('ProductMovePack', 'productMovePacks', params, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductMovePackService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('ProductMovePack', 'productMovePack', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    ProductMovePackService.prototype.create = function (productMovePack, definitionDepth, definitionExcludedClasses, schema) {
        var _a;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var moves = [];
        productMovePack.moves.forEach(function (move) {
            var _a, _b;
            var m = {
                product: (_a = move.product) === null || _a === void 0 ? void 0 : _a.id,
                supplier: Number(move.supplier),
                amount: Number(move.amount),
                purchasePrice: Number(move.purchasePrice),
                purchaseCurrency: ((_b = move.purchaseCurrency) === null || _b === void 0 ? void 0 : _b.length) > 0 ? move.purchaseCurrency : 'CZK',
            };
            if (move.orderItem && move.orderItem.id > 0) {
                m.orderItem = Number(move.orderItem.id);
            }
            if (move.depotFrom > 0) {
                m.depotFrom = Number(move.depotFrom);
            }
            if (move.depotTo > 0) {
                m.depotTo = Number(move.depotTo);
            }
            if (move.productDepot > 0) {
                // == depotFrom on Product detail unloading
                m.productDepot = Number(move.productDepot);
            }
            if (move.note && move.note.length > 0) {
                m.note = move.note;
            }
            if (move.batch && move.batch.length > 0) {
                m.batch = move.batch;
            }
            if (move.expirationDate && move.expirationDate.length > 0) {
                m.expirationDate = move.expirationDate;
            }
            if (move.position1 && move.position1.length > 0) {
                m.position1 = move.position1;
            }
            if (move.position2 && move.position2.length > 0) {
                m.position2 = move.position2;
            }
            if (move.position3 && move.position3.length > 0) {
                m.position3 = move.position3;
            }
            moves.push(m);
        });
        var params = {
            type: (_a = productMovePack.type) === null || _a === void 0 ? void 0 : _a.id,
            purpose: productMovePack.purpose && productMovePack.purpose.id ? productMovePack.purpose.id : 'default',
            moves: moves,
        };
        if (productMovePack.order && productMovePack.order.id > 0) {
            params.order = productMovePack.order.id;
        }
        if (productMovePack.note && productMovePack.note.length > 0) {
            params.note = productMovePack.note;
        }
        return this.createUpdateEntity('ProductMovePack', 'createProductMovePack', params, definitionDepth, definitionExcludedClasses, schema);
    };
    return ProductMovePackService;
}(entities_1.BaseEntityService));
exports.ProductMovePackService = ProductMovePackService;
//# sourceMappingURL=ProductMovePackService.js.map