"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EETShopService = void 0;
var entities_1 = require("../entities");
var EETShopService = /** @class */ (function (_super) {
    __extends(EETShopService, _super);
    function EETShopService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    EETShopService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('EETShop', 'shops', params, definitionDepth, definitionExcludedClasses, schema);
    };
    EETShopService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('EETShop', 'shop', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    EETShopService.prototype.create = function (shop, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = { name: shop.name };
        if (shop.eetId) {
            params.eetId = Number(shop.eetId);
        }
        if (shop.street && shop.street.length > 0) {
            params.street = shop.street;
        }
        if (shop.city && shop.city.length > 0) {
            params.city = shop.city;
        }
        if (shop.country && shop.country.length > 0) {
            params.country = shop.country;
        }
        if (shop.zip && shop.zip.length > 0) {
            params.zip = shop.zip;
        }
        if (shop.cert && shop.cert.id > 0) {
            params.cert = shop.cert.id;
        }
        if (shop.checkouts && shop.checkouts.length > 0) {
            params.checkouts = shop.checkouts.map(function (checkout) { return checkout.id; });
        }
        return this.createUpdateEntity('EETShop', 'createShop', params, definitionDepth, definitionExcludedClasses, schema);
    };
    EETShopService.prototype.update = function (shop, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: shop.id,
            name: shop.name,
            eetId: Number(shop.eetId),
            street: shop.street,
            city: shop.city,
            country: shop.country,
            zip: shop.zip,
        };
        if (shop.cert && shop.cert.id > 0) {
            params.cert = shop.cert.id;
        }
        if (shop.checkouts && shop.checkouts.length > 0) {
            params.checkouts = shop.checkouts.map(function (checkout) { return checkout.id; });
        }
        else {
            params.checkouts = [];
        }
        return this.createUpdateEntity('EETShop', 'updateShop', params, definitionDepth, definitionExcludedClasses, schema);
    };
    EETShopService.prototype.delete = function (shop) {
        var params = { id: shop.id };
        return this.deleteEntity('EETShop', 'deleteShop', params);
    };
    return EETShopService;
}(entities_1.BaseEntityService));
exports.EETShopService = EETShopService;
//# sourceMappingURL=EETShopService.js.map