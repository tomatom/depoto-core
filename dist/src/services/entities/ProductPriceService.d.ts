import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { ProductPrice } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProductPriceService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    create(productPrice: ProductPrice, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPrice>;
    update(productPrice: ProductPrice, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductPrice>;
    delete(productPrice: ProductPrice | any): Promise<Array<any> | any>;
}
