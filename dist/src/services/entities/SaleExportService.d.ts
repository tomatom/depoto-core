import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { SaleExport } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class SaleExportService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    create(saleExport: SaleExport, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<SaleExport>;
}
