import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { UserGroup } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class UserGroupService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<UserGroup>;
    create(userGroup: UserGroup, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<UserGroup>;
    update(userGroup: UserGroup, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<UserGroup>;
    delete(userGroup: UserGroup | any): Promise<Array<any> | any>;
}
