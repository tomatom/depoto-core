"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileService = void 0;
var entities_1 = require("../entities");
var FileService = /** @class */ (function (_super) {
    __extends(FileService, _super);
    function FileService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    FileService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('File', 'files', params, definitionDepth, definitionExcludedClasses, schema);
    };
    FileService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('File', 'file', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    FileService.prototype.create = function (file, productId, orderId, inventoryId, productMovePackId, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            text: file.text,
            originalFilename: file.originalFilename,
            mimeType: file.mimeType,
            base64Data: file.base64Data,
        };
        if (productId && productId > 0) {
            params.product = productId;
        }
        if (orderId && orderId > 0) {
            params.order = orderId;
        }
        if (inventoryId && inventoryId > 0) {
            params.inventory = inventoryId;
        }
        if (productMovePackId && productMovePackId > 0) {
            params.productMovePack = productMovePackId;
        }
        return this.createUpdateEntity('File', 'createFile', params, definitionDepth, definitionExcludedClasses, schema);
    };
    FileService.prototype.update = function (file, productId, orderId, inventoryId, productMovePackId, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: file.id,
        };
        if (productId && productId > 0) {
            params.product = productId;
        }
        if (orderId && orderId > 0) {
            params.order = orderId;
        }
        if (inventoryId && inventoryId > 0) {
            params.inventory = inventoryId;
        }
        if (productMovePackId && productMovePackId > 0) {
            params.productMovePack = productMovePackId;
        }
        return this.createUpdateEntity('File', 'updateFile', params, definitionDepth, definitionExcludedClasses, schema);
    };
    FileService.prototype.delete = function (file) {
        var params = { id: file.id };
        return this.deleteEntity('File', 'deleteFile', params);
    };
    return FileService;
}(entities_1.BaseEntityService));
exports.FileService = FileService;
//# sourceMappingURL=FileService.js.map