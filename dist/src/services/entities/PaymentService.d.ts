import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Payment } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class PaymentService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getPaymentTypes(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getCurrencies(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Payment>;
    create(payment: Payment, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Payment>;
    update(payment: Payment, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Payment>;
    delete(payment: Payment | any): Promise<Array<any> | any>;
}
