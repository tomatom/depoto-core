import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Company } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class CompanyService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    update(company: Company, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Company>;
    updateNextEan(company: Company, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Company>;
}
