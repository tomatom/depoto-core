import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { EETShop } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class EETShopService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETShop>;
    create(shop: EETShop, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETShop>;
    update(shop: EETShop, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETShop>;
    delete(shop: EETShop | any): Promise<Array<any> | any>;
}
