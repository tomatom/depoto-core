import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class StatsService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getStatsBestsellers(from: string, to: string, checkoutId?: number, results?: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getStatsCarrierUsage(from: string, to: string, checkoutId?: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getStatsPaymentSale(from: string, to: string, checkoutId?: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
}
