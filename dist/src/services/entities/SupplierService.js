"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierService = void 0;
var entities_1 = require("../entities");
var SupplierService = /** @class */ (function (_super) {
    __extends(SupplierService, _super);
    function SupplierService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    SupplierService.prototype.getList = function (parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (parameters === void 0) { parameters = {}; }
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            page: parameters.page > 0 ? parameters.page : 1,
            sort: !!parameters.sort ? parameters.sort : 'id',
            direction: !!parameters.direction ? parameters.direction : 'DESC',
            filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
        };
        return this.getEntityList('Supplier', 'suppliers', params, definitionDepth, definitionExcludedClasses, schema);
    };
    SupplierService.prototype.getById = function (id, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        return this.getSingleEntity('Supplier', 'supplier', { id: id }, definitionDepth, definitionExcludedClasses, schema);
    };
    SupplierService.prototype.create = function (supplier, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = { name: supplier.name };
        return this.createUpdateEntity('Supplier', 'createSupplier', params, definitionDepth, definitionExcludedClasses, schema);
    };
    SupplierService.prototype.update = function (supplier, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var params = {
            id: supplier.id,
            name: supplier.name,
        };
        return this.createUpdateEntity('Supplier', 'updateSupplier', params, definitionDepth, definitionExcludedClasses, schema);
    };
    SupplierService.prototype.delete = function (supplier) {
        var params = { id: supplier.id };
        return this.deleteEntity('Supplier', 'deleteSupplier', params);
    };
    return SupplierService;
}(entities_1.BaseEntityService));
exports.SupplierService = SupplierService;
//# sourceMappingURL=SupplierService.js.map