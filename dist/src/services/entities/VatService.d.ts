import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Vat } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class VatService extends BaseEntityService {
    defaultVat?: Vat;
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Vat>;
    getDefaultVat(definitionDepth?: number, definitionExcludedClasses?: string[]): Promise<Vat | null | undefined>;
    create(vat: Vat, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Vat>;
    update(vat: Vat, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Vat>;
    delete(vat: Vat | any): Promise<Array<any> | any>;
    protected listenForEvents(): void;
}
