import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Address, Customer } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class AddressService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Address>;
    create(address: Address, customer?: Customer, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Address>;
    update(address: Address, customer?: Customer, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Address>;
    delete(address: Address | any): Promise<Array<any> | any>;
}
