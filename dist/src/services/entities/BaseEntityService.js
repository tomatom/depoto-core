"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseEntityService = void 0;
// import { User, UserGroup } from '../entities'
var __1 = require("../");
var utils_1 = require("../../utils");
var models_1 = require("../../models");
var isSchemaDefined = function (definitionDepth, excludedClasses, schema) {
    return definitionDepth && (!excludedClasses || (excludedClasses && !excludedClasses.length)) && schema;
};
var BaseEntityService = /** @class */ (function () {
    function BaseEntityService(config) {
        this.paginatorDefinition = {
            current: null,
            endPage: null,
            totalCount: null,
        };
        this.loggerService = config.loggerService;
        this.s = config.s;
        this.oauthService = config.oauthService;
        this.httpService = config.httpService;
        this.onEvent = config.notificationEventObservable;
    }
    BaseEntityService.prototype.subscribeToEvents = function () {
        return this.onEvent.subscribe();
    };
    BaseEntityService.prototype.unsubscribeFromEvents = function () {
        return this.onEvent.unregisterObservers();
    };
    BaseEntityService.prototype.getEntityList = function (entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema) {
        var _this = this;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        this.onEvent.emit(new models_1.NotificationEvent({
            type: models_1.NotificationEventType.start,
            entity: entityName,
            method: method,
            operation: models_1.NotificationEventOperation.list,
        }));
        var query = this.composeQueryForList(entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema);
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                query: query,
            },
        });
        return this.httpService
            .post(utils_1.ENDPOINT.GRAPHQL_POST, { query: query })
            .then(function (response) {
            var errors = _this.getResponseErrors(response, method);
            if (errors && errors.length > 0) {
                _this.onEvent.emit(new models_1.NotificationEvent({
                    type: models_1.NotificationEventType.reject,
                    entity: entityName,
                    method: method,
                    operation: models_1.NotificationEventOperation.list,
                    response: null,
                    errors: errors,
                }));
                return Promise.reject(errors);
            }
            _this.onEvent.emit(new models_1.NotificationEvent({
                type: models_1.NotificationEventType.resolve,
                entity: entityName,
                method: method,
                operation: models_1.NotificationEventOperation.list,
                response: response,
                errors: [],
            }));
            return _this.getResponseData(response, method);
        })
            .catch(function (e) { return Promise.reject(e); });
    };
    BaseEntityService.prototype.getSingleEntity = function (entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema) {
        var _this = this;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        this.onEvent.emit(new models_1.NotificationEvent({
            type: models_1.NotificationEventType.start,
            entity: entityName,
            method: method,
            operation: models_1.NotificationEventOperation.detail,
        }));
        var query = this.composeQueryForDetail(entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema);
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                query: query,
            },
        });
        return this.httpService
            .post(utils_1.ENDPOINT.GRAPHQL_POST, { query: query })
            .then(function (response) {
            var errors = _this.getResponseErrors(response, method);
            if (errors && errors.length > 0) {
                _this.onEvent.emit(new models_1.NotificationEvent({
                    type: models_1.NotificationEventType.reject,
                    entity: entityName,
                    method: method,
                    operation: models_1.NotificationEventOperation.detail,
                    response: null,
                    errors: errors,
                }));
                return Promise.reject(errors);
            }
            _this.onEvent.emit(new models_1.NotificationEvent({
                type: models_1.NotificationEventType.resolve,
                entity: entityName,
                method: method,
                operation: models_1.NotificationEventOperation.detail,
                response: response,
                errors: [],
            }));
            return _this.getResponseData(response, method);
        })
            .catch(function (e) { return Promise.reject(e); });
    };
    BaseEntityService.prototype.createUpdateEntity = function (entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema) {
        var _this = this;
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        this.onEvent.emit(new models_1.NotificationEvent({
            type: models_1.NotificationEventType.start,
            entity: entityName,
            method: method,
            operation: method.includes('update') ? models_1.NotificationEventOperation.update : models_1.NotificationEventOperation.create,
        }));
        this.appendTestDataToWindow({ requestParams: parameters });
        var mutation = this.composeMutation(entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema);
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                query: mutation,
            },
        });
        return this.httpService
            .post(utils_1.ENDPOINT.GRAPHQL_POST, { query: mutation })
            .then(function (response) {
            var errors = _this.getResponseErrors(response, method);
            if (errors && errors.length > 0) {
                _this.appendTestDataToWindow({ responseErrors: errors });
                _this.onEvent.emit(new models_1.NotificationEvent({
                    type: models_1.NotificationEventType.reject,
                    entity: entityName,
                    method: method,
                    operation: method.includes('update')
                        ? models_1.NotificationEventOperation.update
                        : models_1.NotificationEventOperation.create,
                    response: null,
                    errors: errors,
                }));
                return Promise.reject(errors);
            }
            _this.onEvent.emit(new models_1.NotificationEvent({
                type: models_1.NotificationEventType.resolve,
                entity: entityName,
                method: method,
                operation: method.includes('update')
                    ? models_1.NotificationEventOperation.update
                    : models_1.NotificationEventOperation.create,
                response: response,
                errors: [],
            }));
            var d = _this.getResponseData(response, method);
            _this.appendTestDataToWindow({ responseData: d });
            return d;
        })
            .catch(function (e) { return Promise.reject(e); });
    };
    BaseEntityService.prototype.deleteEntity = function (entityName, method, parameters) {
        var _this = this;
        this.onEvent.emit(new models_1.NotificationEvent({
            type: models_1.NotificationEventType.start,
            entity: entityName,
            method: method,
            operation: models_1.NotificationEventOperation.delete,
        }));
        var mutation = this.composeDeleteMutation(entityName, method, parameters);
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                query: mutation,
            },
        });
        return this.httpService
            .post(utils_1.ENDPOINT.GRAPHQL_POST, { query: mutation })
            .then(function (response) {
            var errors = _this.getResponseErrors(response, method);
            _this.onEvent.emit(new models_1.NotificationEvent({
                type: errors && errors.length > 0 ? models_1.NotificationEventType.reject : models_1.NotificationEventType.resolve,
                entity: entityName,
                method: method,
                operation: models_1.NotificationEventOperation.delete,
                response: null,
                errors: errors && errors.length > 0 ? errors : [],
            }));
            return errors;
        })
            .catch(function (e) { return Promise.reject(e); });
    };
    BaseEntityService.prototype.composeQueryForList = function (entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var entitySchema;
        if (entityName === 'string') {
            entitySchema = null;
        }
        else {
            entitySchema = !!schema ? schema : utils_1.SchemaUtil.extractFrom(entityName, definitionDepth, definitionExcludedClasses);
        }
        var definition = { items: entitySchema };
        if (entityName === 'Role') {
            definition = entitySchema;
        }
        else if (!entityName.includes('Stats') &&
            entityName !== 'string' &&
            entityName !== 'SellItem' &&
            entityName !== 'ProductPurchasePrice') {
            definition.paginator = this.paginatorDefinition;
        }
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                definition: definition,
            },
        });
        return utils_1.GraphQLUtil.createQuery(definition, method, parameters, method);
    };
    BaseEntityService.prototype.composeQueryForDetail = function (entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var entitySchema = !!schema
            ? schema
            : utils_1.SchemaUtil.extractFrom(entityName, definitionDepth, definitionExcludedClasses);
        var definition = {
            data: entitySchema,
            errors: null,
        };
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                definition: definition,
            },
        });
        return utils_1.GraphQLUtil.createQuery(definition, method, parameters, method);
    };
    BaseEntityService.prototype.composeMutation = function (entityName, method, parameters, definitionDepth, definitionExcludedClasses, schema) {
        if (definitionExcludedClasses === void 0) { definitionExcludedClasses = []; }
        if (schema === void 0) { schema = null; }
        var entitySchema = isSchemaDefined(definitionDepth, definitionExcludedClasses, schema)
            ? !!schema
                ? schema
                : utils_1.SchemaUtil.extractFrom(entityName, definitionDepth, definitionExcludedClasses)
            : { id: null };
        var definition = {
            data: entitySchema,
            errors: null,
        };
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                definition: definition,
            },
        });
        var isNonStdMethod = method === 'updateClearanceItem' || method === 'resetPackage';
        return utils_1.GraphQLUtil.createMutation(parameters, isNonStdMethod ? { errors: null } : definition, method, method);
    };
    BaseEntityService.prototype.composeDeleteMutation = function (entityName, method, parameters) {
        var definition = {
            errors: null,
        };
        this.loggerService.log({
            type: __1.EVENT.GRAPHQL,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
                entityName: entityName,
                method: method,
                definition: definition,
            },
        });
        return utils_1.GraphQLUtil.createMutation(parameters, definition, method, method);
    };
    BaseEntityService.prototype.getResponseData = function (response, method) {
        if (response && response['data'] && response['data'][method] && response['data'][method]['data']) {
            return response['data'][method]['data'];
        }
        if (method === 'roles' && response && response['data'] && response['data'][method]) {
            return response['data'][method];
        }
        if (response && response['data'] && response['data'][method] && response['data'][method]['items']) {
            return {
                items: response['data'][method]['items'],
                paginator: response['data'][method]['paginator'],
            };
        }
        return null;
    };
    BaseEntityService.prototype.getResponseErrors = function (response, method) {
        if (response && response['errors'] && response['errors'].length > 0) {
            return response['errors'];
        }
        if (response &&
            response['data'] &&
            response['data'][method] &&
            response['data'][method]['errors'] &&
            response['data'][method]['errors'].length > 0) {
            return response['data'][method]['errors'];
        }
        return null;
    };
    BaseEntityService.prototype.appendTestDataToWindow = function (_a) {
        var requestParams = _a.requestParams, responseData = _a.responseData, responseErrors = _a.responseErrors;
        if (!window) {
            return;
        }
        if (!window['CYPRESS']) {
            window['CYPRESS'] = {};
        }
        if (requestParams) {
            window['CYPRESS'].mutation = requestParams;
        }
        if (responseData) {
            window['CYPRESS'].response = responseData;
        }
        if (responseErrors) {
            window['CYPRESS'].errors = responseErrors;
        }
    };
    return BaseEntityService;
}());
exports.BaseEntityService = BaseEntityService;
//# sourceMappingURL=BaseEntityService.js.map