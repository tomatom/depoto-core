import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { EETReceipt } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class EETReceiptService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    create(receipt: EETReceipt, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETReceipt>;
    send(receipt: EETReceipt, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<EETReceipt>;
}
