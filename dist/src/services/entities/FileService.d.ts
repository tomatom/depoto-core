import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { File } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class FileService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<File>;
    create(file: File, productId?: number, orderId?: number, inventoryId?: number, productMovePackId?: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<File>;
    update(file: File, productId?: number, orderId?: number, inventoryId?: number, productMovePackId?: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<File>;
    delete(file: File | any): Promise<Array<any> | any>;
}
