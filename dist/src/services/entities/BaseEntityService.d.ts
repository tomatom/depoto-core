import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { LoggerService, OAuthService, StorageService } from '../';
import { Observable, Observer } from '../../utils';
import { NotificationEvent } from '../../models';
declare type TestDataParams = {
    requestParams?: any;
    responseData?: any;
    responseErrors?: any[];
};
export declare class BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    protected onEvent: Observable<NotificationEvent>;
    protected paginatorDefinition: any;
    constructor(config: EntityServiceConfiguration);
    protected subscribeToEvents(): Observer;
    protected unsubscribeFromEvents(): void;
    protected getEntityList(entityName: string, method: string, parameters: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    protected getSingleEntity(entityName: string, method: string, parameters: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    protected createUpdateEntity(entityName: string, method: string, parameters: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    protected deleteEntity(entityName: string, method: string, parameters: any): Promise<Array<any> | any>;
    private composeQueryForList;
    private composeQueryForDetail;
    private composeMutation;
    private composeDeleteMutation;
    private getResponseData;
    private getResponseErrors;
    appendTestDataToWindow({ requestParams, responseData, responseErrors }: TestDataParams): void;
}
export {};
