"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DownloadService = void 0;
var __1 = require("../");
var entities_1 = require("../entities");
var DownloadService = /** @class */ (function (_super) {
    __extends(DownloadService, _super);
    function DownloadService(config) {
        var _this = _super.call(this, config) || this;
        _this.loggerService = config.loggerService;
        _this.s = config.s;
        _this.oauthService = config.oauthService;
        _this.httpService = config.httpService;
        return _this;
    }
    DownloadService.prototype.downloadDocumentAndShowInNewWindow = function (uri) {
        var _this = this;
        if ('open' in window) {
            var popupWin_1 = window.open('about:blank');
            this.httpService
                .downloadBlob(uri)
                .then(function (blob) {
                var mimeType = blob.type;
                var dataUrl = window.URL.createObjectURL(blob);
                _this.loggerService.log({
                    type: __1.EVENT.HTTP,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: {
                        uri: uri,
                        mime: mimeType,
                    },
                });
                switch (mimeType) {
                    default:
                    case 'application/pdf':
                        if (popupWin_1) {
                            popupWin_1.location.href = dataUrl; // beware of adblockers! (window close on blob:)
                            // popupWin.history.pushState('', report.url, report.url) // impossible
                        }
                        break;
                    case 'application/vnd.ms-excel':
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    case 'application/msexcel':
                    case 'application/x-msexcel':
                    case 'application/x-ms-excel':
                    case 'application/x-excel':
                    case 'application/x-dos_ms_excel':
                    case 'application/xls':
                    case 'application/x-xls':
                        // download as file
                        if (popupWin_1) {
                            popupWin_1.close();
                        }
                        var filename = uri.split('/')[uri.split('/').length - 1] + ".xlsx";
                        var a = document.createElement('a');
                        a.style.display = 'none';
                        document.body.appendChild(a);
                        a.href = dataUrl;
                        a.download = filename;
                        a.click();
                        break;
                }
                // release reference
                setTimeout(function () {
                    window.URL.revokeObjectURL(dataUrl);
                }, 120 * 1000);
            })
                .catch(function (err) {
                _this.loggerService.log({
                    type: __1.EVENT.HTTP,
                    origin: _this.constructor['name'],
                    trace: _this.loggerService.getStackTrace(),
                    data: {
                        uri: uri,
                        error: err,
                    },
                });
                if (popupWin_1) {
                    popupWin_1.close();
                }
                setTimeout(function () {
                    if (uri && uri.includes('bill')) {
                        alert('Došlo k chybě při stahování souboru, pokouším se otevřít záložním způsobem');
                        window.open(uri);
                    }
                    else {
                        alert('Došlo k chybě při stahování souboru');
                    }
                }, 300);
            });
        }
        else {
            console.warn('TODO: RN implementation (DownloadService:downloadDocument)'); // TODO: RN implementation
        }
    };
    return DownloadService;
}(entities_1.BaseEntityService));
exports.DownloadService = DownloadService;
//# sourceMappingURL=DownloadService.js.map