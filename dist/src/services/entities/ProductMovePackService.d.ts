import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { ProductMovePack } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class ProductMovePackService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductMovePack>;
    create(productMovePack: ProductMovePack, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<ProductMovePack>;
}
