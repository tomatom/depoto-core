import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces';
import { Supplier } from '../../entities';
import { LoggerService, StorageService, OAuthService } from '../';
import { BaseEntityService } from '../entities';
export declare class SupplierService extends BaseEntityService {
    protected loggerService: LoggerService;
    protected s: StorageService;
    protected oauthService: OAuthService;
    protected httpService: IHttpService;
    constructor(config: EntityServiceConfiguration);
    getList(parameters?: any, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<any>;
    getById(id: number, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Supplier>;
    create(supplier: Supplier, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Supplier>;
    update(supplier: Supplier, definitionDepth?: number, definitionExcludedClasses?: string[], schema?: any): Promise<Supplier>;
    delete(supplier: Supplier | any): Promise<Array<any> | any>;
}
