import { LoggerService } from './LoggerService';
interface StorageKey {
    key: string;
    type: string;
}
export declare class StorageService {
    private loggerService;
    private appPrefix;
    asyncStorage: any;
    constructor(loggerService: LoggerService, appPrefix: string);
    get keys(): any;
    set(key: StorageKey, value: any): void;
    get(key: StorageKey): Promise<any>;
    /**
     * @description for usage in desktop apps only
     * @param key StorageKey
     */
    getSync(key: StorageKey): any;
    parseStringifiedData(key: StorageKey, stringifiedValue?: string | null): Promise<any>;
    clear(key: StorageKey | null): any;
}
export {};
