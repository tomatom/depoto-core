"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListingItem = void 0;
var ListingItem = /** @class */ (function () {
    function ListingItem(data) {
        if (data === void 0) { data = {}; }
        this.icon = '';
        this.name = data.name || '';
        this.nameForSettings = data.nameForSettings || '';
        this.cssClass = data.cssClass || '';
        this.title = data.title || '';
        this.property = data.property || '';
        this.propType = data.propType || '';
        this.size = data.size || null;
        this.sortable = data.sortable;
        this.sortBy = data.sortBy || null;
        this.btnStyle = data.btnStyle || null;
        this.icon = data.icon || null;
        this.action = data.action || null;
        this.route = data.route || null;
        this.align = data.align || null;
        this.isDefault = data.isDefault || false;
    }
    ListingItem.propTypes = {
        string: 'string',
        number: 'number',
        integer: 'integer',
        price: 'price',
        'price-cents': 'price-cents',
        date: 'date',
        datetime: 'datetime',
        label: 'label',
        boolean: 'boolean',
        approved: 'approved',
        array: 'array',
        'array-vertical': 'array-vertical',
        btn: 'btn',
        'btn-right': 'btn-right',
        'btn-gen': 'btn-gen',
        'btn-resend-eet-receipt': 'btn-resend-eet-receipt',
        'modal-eet-receipt': 'modal-eet-receipt',
        'modal-address': 'modal-address',
        'order-bill-number': 'order-bill-number',
        'order-unavailables': 'order-unavailables',
        'order-reservation-actions': 'order-reservation-actions',
        'order-payment-items': 'order-payment-items',
        'reservation-number-unavailables': 'reservation-number-unavailables',
        'checkout-name': 'checkout-name',
        'customer-name': 'customer-name',
        'carrier-name': 'carrier-name',
        'process-status': 'process-status',
        'pipe-order-items-price': 'pipe-order-items-price',
    };
    ListingItem.alignTypes = {
        left: 'left',
        center: 'center',
        right: 'right',
    };
    return ListingItem;
}());
exports.ListingItem = ListingItem;
//# sourceMappingURL=ListingItem.js.map