"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Notification = void 0;
var Notification = /** @class */ (function () {
    function Notification(data) {
        if (data === void 0) { data = {}; }
        this.title = data.title || '';
        this.content = data.content || '';
        this.date = data.date || Date.now() + '';
    }
    return Notification;
}());
exports.Notification = Notification;
//# sourceMappingURL=Notification.js.map