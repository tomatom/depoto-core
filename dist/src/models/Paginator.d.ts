export declare class Paginator {
    current: number;
    endPage: number;
    totalCount: number;
    constructor(data?: any);
    get isEndPage(): boolean;
}
