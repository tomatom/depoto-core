"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerEvent = void 0;
var LoggerEvent = /** @class */ (function () {
    function LoggerEvent(data) {
        if (data === void 0) { data = {}; }
        this.type = !!data.type ? data.type : 'log';
        this.origin = !!data.origin ? data.origin : null;
        this.data = !!data.data ? data.data : null;
        this.trace = !!data.trace ? data.trace : null;
    }
    return LoggerEvent;
}());
exports.LoggerEvent = LoggerEvent;
//# sourceMappingURL=LoggerEvent.js.map