import { OAuthToken } from './OAuthToken';
export declare class OAuthSession {
    clientId: string;
    clientSecret: string;
    clientType: string;
    clientCustomUri: string;
    email: string;
    password: string;
    isAuthenticated: boolean;
    isRememberMe: boolean;
    token: OAuthToken;
    constructor(data?: any);
}
