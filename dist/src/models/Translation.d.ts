export declare class Translation {
    token: string;
    cs: string;
    en: string;
    constructor(data?: any);
}
