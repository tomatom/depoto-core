export declare class OAuthToken {
    accessToken: string;
    refreshToken: string;
    tokenType: string;
    scope: string;
    expiresIn: number;
    timestamp: number;
    constructor(data?: any);
}
