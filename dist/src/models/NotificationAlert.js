"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationAlert = void 0;
var utils_1 = require("../utils");
var NotificationAlert = /** @class */ (function () {
    function NotificationAlert(data) {
        if (data === void 0) { data = {}; }
        this.text = data.text || '';
        this.type = data.type || 'success';
        this.time = data.time || 10000;
    }
    NotificationAlert.showNotification = function (text, type, time) {
        if (type === void 0) { type = 'success'; }
        if (time === void 0) { time = 10000; }
        utils_1.Events.dispatch('notification:show', new NotificationAlert({
            text: text,
            type: type,
            time: time,
        }));
    };
    return NotificationAlert;
}());
exports.NotificationAlert = NotificationAlert;
//# sourceMappingURL=NotificationAlert.js.map