export declare class NotificationAlert {
    text: string;
    type: string;
    time: string;
    constructor(data?: any);
    static showNotification(text: string, type?: string, time?: number): void;
}
