"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Translation = void 0;
var Translation = /** @class */ (function () {
    function Translation(data) {
        if (data === void 0) { data = {}; }
        this.token = data.token || '';
        this.cs = data.cs || '';
        this.en = data.en || '';
    }
    return Translation;
}());
exports.Translation = Translation;
//# sourceMappingURL=Translation.js.map