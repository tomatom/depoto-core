export declare class NotificationEventType {
    static readonly start = "start";
    static readonly resolve = "resolve";
    static readonly reject = "reject";
}
export declare class NotificationEventOperation {
    static readonly list = "list";
    static readonly detail = "detail";
    static readonly create = "create";
    static readonly update = "update";
    static readonly delete = "delete";
}
export declare class NotificationEvent {
    type: string;
    entity: string;
    method: string;
    operation: string;
    response: any;
    errors: string[];
    date: string;
    constructor(data?: any);
}
