export declare class Notification {
    title: string;
    content: string;
    date: string;
    constructor(data?: any);
}
