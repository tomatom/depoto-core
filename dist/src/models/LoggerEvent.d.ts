export declare class LoggerEvent {
    type: string;
    origin: string;
    data: any;
    trace: string | string[];
    constructor(data?: any);
}
