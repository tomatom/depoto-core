"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Paginator = void 0;
var Paginator = /** @class */ (function () {
    function Paginator(data) {
        if (data === void 0) { data = {}; }
        this.current = !isNaN(data.current) && data.current !== 0 ? data.current : 0;
        this.endPage = !isNaN(data.endPage) && data.endPage !== 0 ? data.endPage : 0;
        this.totalCount = !isNaN(data.totalCount) && data.totalCount !== 0 ? data.totalCount : 0;
    }
    Object.defineProperty(Paginator.prototype, "isEndPage", {
        get: function () {
            return this.current === this.endPage;
        },
        enumerable: false,
        configurable: true
    });
    return Paginator;
}());
exports.Paginator = Paginator;
//# sourceMappingURL=Paginator.js.map