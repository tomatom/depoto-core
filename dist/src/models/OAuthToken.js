"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OAuthToken = void 0;
var OAuthToken = /** @class */ (function () {
    function OAuthToken(data) {
        if (data === void 0) { data = {}; }
        this.accessToken = !!data.accessToken ? data.accessToken : !!data.access_token ? data.access_token : '';
        this.refreshToken = !!data.refreshToken ? data.refreshToken : !!data.refresh_token ? data.refresh_token : '';
        this.tokenType = !!data.tokenType ? data.tokenType : !!data.token_type ? data.token_type : '';
        this.scope = !!data.scope ? data.scope : '';
        this.expiresIn = !!data.expiresIn ? data.expiresIn : !!data.expires_in ? data.expires_in : 0;
        this.timestamp = !!data.timestamp ? data.timestamp : +new Date();
    }
    return OAuthToken;
}());
exports.OAuthToken = OAuthToken;
//# sourceMappingURL=OAuthToken.js.map