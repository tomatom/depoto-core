"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationEvent = exports.NotificationEventOperation = exports.NotificationEventType = void 0;
var NotificationEventType = /** @class */ (function () {
    function NotificationEventType() {
    }
    NotificationEventType.start = 'start';
    NotificationEventType.resolve = 'resolve';
    NotificationEventType.reject = 'reject';
    return NotificationEventType;
}());
exports.NotificationEventType = NotificationEventType;
var NotificationEventOperation = /** @class */ (function () {
    function NotificationEventOperation() {
    }
    NotificationEventOperation.list = 'list';
    NotificationEventOperation.detail = 'detail';
    NotificationEventOperation.create = 'create';
    NotificationEventOperation.update = 'update';
    NotificationEventOperation.delete = 'delete';
    return NotificationEventOperation;
}());
exports.NotificationEventOperation = NotificationEventOperation;
var NotificationEvent = /** @class */ (function () {
    function NotificationEvent(data) {
        if (data === void 0) { data = {}; }
        this.type = data.type || '';
        this.entity = data.entity || '';
        this.method = data.method || '';
        this.operation = data.operation || '';
        this.response = data.response || null;
        this.errors = data.errors || [];
        this.date = data.date || Date.now() + '';
    }
    return NotificationEvent;
}());
exports.NotificationEvent = NotificationEvent;
//# sourceMappingURL=NotificationEvent.js.map