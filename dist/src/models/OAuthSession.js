"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OAuthSession = void 0;
var OAuthToken_1 = require("./OAuthToken");
var OAuthSession = /** @class */ (function () {
    function OAuthSession(data) {
        if (data === void 0) { data = {}; }
        this.clientId = !!data.clientId ? data.clientId : '';
        this.clientSecret = !!data.clientSecret ? data.clientSecret : '';
        this.clientType = !!data.clientType ? data.clientType : 'prod';
        this.clientCustomUri = !!data.clientCustomUri ? data.clientCustomUri : '';
        this.email = !!data.email ? data.email : '';
        this.password = !!data.password ? data.password : '';
        this.isAuthenticated = !!data.isAuthenticated ? data.isAuthenticated : false;
        this.isRememberMe = !!data.isRememberMe ? data.isRememberMe : false;
        this.token = !!data.token ? new OAuthToken_1.OAuthToken(data.token) : new OAuthToken_1.OAuthToken();
    }
    return OAuthSession;
}());
exports.OAuthSession = OAuthSession;
//# sourceMappingURL=OAuthSession.js.map