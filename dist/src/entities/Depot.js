"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Depot = void 0;
var Depot = /** @class */ (function () {
    function Depot(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
        this.emailIn = data.emailIn || '';
        this.unavailablesUrl = data.unavailablesUrl || '';
    }
    return Depot;
}());
exports.Depot = Depot;
//# sourceMappingURL=Depot.js.map