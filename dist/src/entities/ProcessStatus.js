"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcessStatus = exports.ProcessStatusType = void 0;
var ProcessStatusType;
(function (ProcessStatusType) {
    ProcessStatusType["RECEIVED"] = "received";
    ProcessStatusType["PICKING"] = "picking";
    ProcessStatusType["PACKING"] = "packing";
    ProcessStatusType["PACKED"] = "packed";
    ProcessStatusType["DISPATCHED"] = "dispatched";
    ProcessStatusType["DELIVERED"] = "delivered";
    ProcessStatusType["RETURNED"] = "returned";
    ProcessStatusType["PICKING_ERROR"] = "picking_error";
    ProcessStatusType["CANCELLED"] = "cancelled";
})(ProcessStatusType = exports.ProcessStatusType || (exports.ProcessStatusType = {}));
var ProcessStatus = /** @class */ (function () {
    function ProcessStatus(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || ProcessStatusType.RECEIVED;
        this.name = data.name || '';
        this.position = data.position || 0;
    }
    return ProcessStatus;
}());
exports.ProcessStatus = ProcessStatus;
//# sourceMappingURL=ProcessStatus.js.map