export declare class PackageStatus {
    id: number;
    code: string;
    text: string;
    created: string;
    constructor(data?: any);
}
