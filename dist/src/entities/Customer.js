"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Customer = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var _2 = require("./");
var Customer = /** @class */ (function () {
    function Customer(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.email = data.email || '';
        this.firstName = data.firstName || '';
        this.lastName = data.lastName || '';
        this.name = data.name || '';
        this.companyName = data.companyName || '';
        this.phone = data.phone || '';
        this.note = data.note || '';
        this.birthday = data.birthday || '';
        this.wholesale = data.wholesale || false;
        this.addresses =
            data.addresses && data.addresses.length > 0 ? data.addresses.map(function (address) { return new _1.Address(address); }) : [];
        this.consentRelations =
            data.consentRelations && data.consentRelations.length > 0
                ? data.consentRelations.map(function (c) { return new _1.CustomerConsent(c); })
                : [];
        this.users = data.users && data.users.length > 0 ? data.users.map(function (user) { return new _1.User(user); }) : [];
        this.minExpirationDays = data.minExpirationDays || 0;
        this.priceLevel = data.priceLevel ? new _2.PriceLevel(data.priceLevel) : undefined;
        this.tags = data.tags && data.tags.length ? data.tags.map(function (t) { return new _2.Tag(t); }) : [];
        this.companyName = data.companyName || '';
        this.externalId = data.externalId || '';
        this.customData = data.customData || '';
    }
    __decorate([
        utils_1.PropType('Address'),
        __metadata("design:type", Array)
    ], Customer.prototype, "addresses", void 0);
    __decorate([
        utils_1.PropType('CustomerConsent'),
        __metadata("design:type", Array)
    ], Customer.prototype, "consentRelations", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", Array)
    ], Customer.prototype, "users", void 0);
    __decorate([
        utils_1.PropType('PriceLevel'),
        __metadata("design:type", _2.PriceLevel)
    ], Customer.prototype, "priceLevel", void 0);
    __decorate([
        utils_1.PropType('Tag'),
        __metadata("design:type", Array)
    ], Customer.prototype, "tags", void 0);
    return Customer;
}());
exports.Customer = Customer;
//# sourceMappingURL=Customer.js.map