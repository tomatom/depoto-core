"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPriceLevel = void 0;
var Product_1 = require("./Product");
var utils_1 = require("../utils");
var PriceLevel_1 = require("./PriceLevel");
var ProductPriceLevel = /** @class */ (function () {
    function ProductPriceLevel(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.sellPrice = data.sellPrice || null;
        this.externalId = data.externalId || '';
        this.product = data.product ? new Product_1.Product(data.product) : new Product_1.Product();
        this.priceLevel = data.priceLevel ? new PriceLevel_1.PriceLevel(data.priceLevel) : new PriceLevel_1.PriceLevel();
    }
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", Product_1.Product)
    ], ProductPriceLevel.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('PriceLevel'),
        __metadata("design:type", PriceLevel_1.PriceLevel)
    ], ProductPriceLevel.prototype, "priceLevel", void 0);
    return ProductPriceLevel;
}());
exports.ProductPriceLevel = ProductPriceLevel;
//# sourceMappingURL=ProductPriceLevel.js.map