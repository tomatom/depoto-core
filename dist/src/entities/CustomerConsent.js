"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerConsent = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var CustomerConsent = /** @class */ (function () {
    function CustomerConsent(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.customer = data.customer ? new _1.Customer(data.customer) : undefined;
        this.consent = data.consent ? new _1.Consent(data.consent) : undefined;
        this.name = data.name || '';
        this.body = data.body || '';
        this.externalId = data.externalId || '';
        this.created = data.created || '';
        this.updated = data.updated || '';
    }
    __decorate([
        utils_1.PropType('Customer'),
        __metadata("design:type", _1.Customer)
    ], CustomerConsent.prototype, "customer", void 0);
    __decorate([
        utils_1.PropType('Consent'),
        __metadata("design:type", _1.Consent)
    ], CustomerConsent.prototype, "consent", void 0);
    return CustomerConsent;
}());
exports.CustomerConsent = CustomerConsent;
//# sourceMappingURL=CustomerConsent.js.map