import { Product } from './Product';
import { PriceLevel } from './PriceLevel';
export declare class ProductPriceLevel {
    id: number;
    sellPrice: number;
    externalId: string;
    product: Product;
    priceLevel: PriceLevel;
    constructor(data?: any);
}
