"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Checkout = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var Checkout = /** @class */ (function () {
    function Checkout(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
        this.amount = data.amount || 0;
        this.nextBillNumber = data.nextBillNumber || 1;
        this.nextReservationNumber = data.nextReservationNumber || 1;
        this.billFooter = data.billFooter || '';
        this.depots = data.depots && data.depots.length > 0 ? data.depots.map(function (depot) { return new _1.Depot(depot); }) : [];
        this.returnsDepot = data.returnsDepot && data.returnsDepot.id > 0 ? new _1.Depot(data.returnsDepot) : undefined;
        this.orders = data.orders && data.orders.length > 0 ? data.orders.map(function (order) { return new _1.Order(order); }) : [];
        this.payments = data.payments && data.payments.length > 0 ? data.payments.map(function (payment) { return new _1.Payment(payment); }) : [];
        this.currentClosing = data.currentClosing ? new _1.CheckoutClosing(data.currentClosing) : undefined;
        this.eetId = data.eetId || 0;
        this.eetEnable = data.eetEnable || false;
        this.eetPlayground = data.eetPlayground || false;
        this.eetVerificationMode = data.eetVerificationMode || false;
        this.negativeReservation = data.negativeReservation || false;
        this.eventUrl = data.eventUrl || '';
        this.eventTypes = data.eventTypes || [];
        this.users = data.users && data.users.length ? data.users.map(function (u) { return new _1.User(u); }) : [];
    }
    __decorate([
        utils_1.PropType('Depot'),
        __metadata("design:type", Array)
    ], Checkout.prototype, "depots", void 0);
    __decorate([
        utils_1.PropType('Depot'),
        __metadata("design:type", _1.Depot)
    ], Checkout.prototype, "returnsDepot", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Array)
    ], Checkout.prototype, "orders", void 0);
    __decorate([
        utils_1.PropType('Payment'),
        __metadata("design:type", Array)
    ], Checkout.prototype, "payments", void 0);
    __decorate([
        utils_1.PropType('CheckoutClosing'),
        __metadata("design:type", _1.CheckoutClosing)
    ], Checkout.prototype, "currentClosing", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", Array)
    ], Checkout.prototype, "users", void 0);
    return Checkout;
}());
exports.Checkout = Checkout;
//# sourceMappingURL=Checkout.js.map