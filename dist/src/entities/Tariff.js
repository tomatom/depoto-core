"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tariff = void 0;
var Tariff = /** @class */ (function () {
    function Tariff(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
        this.const = data.const ? data.const : data.constant ? data.constant : '';
        this.position = data.position || 0;
        this.cod = data.cod || false;
    }
    return Tariff;
}());
exports.Tariff = Tariff;
//# sourceMappingURL=Tariff.js.map