import { Company } from './';
export declare class Parameter {
    id: number;
    name: string;
    type: string;
    enumValues: string[];
    unit: string;
    text: string;
    externalId: string;
    company: Company;
    constructor(data?: any);
}
