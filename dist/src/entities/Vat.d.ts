export declare class Vat {
    id: number;
    name: string;
    percent: number;
    coefficient: number;
    default: boolean;
    constructor(data?: any);
}
