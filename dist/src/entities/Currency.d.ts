export declare class Currency {
    id: string;
    name: string;
    ratio: number;
    constructor(data?: any);
}
