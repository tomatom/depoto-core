export declare class EETReceipt {
    id: number;
    dic: string;
    checkoutEetId: number;
    shopEetId: number;
    playground: boolean;
    verificationMode: boolean;
    number: number;
    dateCreated: string;
    totalPrice: number;
    fik: string;
    bkp: string;
    pkp: string;
    constructor(data?: any);
}
