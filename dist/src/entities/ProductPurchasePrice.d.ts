import { Depot } from './';
export declare class ProductPurchasePrice {
    id: number;
    productDepot?: Depot;
    quantity: number;
    purchasePrice: number;
    constructor(data?: any);
}
