import { Address, CustomerConsent, User } from './';
import { PriceLevel, Tag } from './';
export declare class Customer {
    id: number;
    externalId: string;
    email: string;
    firstName: string;
    lastName: string;
    name: string;
    companyName: string;
    phone: string;
    note: string;
    birthday: string;
    wholesale: boolean;
    customData: string;
    addresses: Address[];
    consentRelations: CustomerConsent[];
    users: User[];
    minExpirationDays: number;
    priceLevel?: PriceLevel;
    tags: Tag[];
    constructor(data?: any);
}
