"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var Product = /** @class */ (function () {
    // expirationDate: string // todo update from picker models
    function Product(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.depots = data.depots && data.depots.length > 0 ? data.depots.map(function (pd) { return new _1.ProductDepot(pd); }) : [];
        this.name = data.name || '';
        this.fullName = data.fullName || '';
        this.ean = data.ean || '';
        this.ean2 = data.ean2 || '';
        this.ean3 = data.ean3 || '';
        this.ean4 = data.ean4 || '';
        this.ean5 = data.ean5 || '';
        this.code = data.code || '';
        this.externalId = data.externalId || '';
        this.purchaseCurrency = !!data.purchaseCurrency
            ? new _1.Currency(data.purchaseCurrency)
            : new _1.Currency({ id: 'CZK', name: 'Česká koruna', ratio: 1 });
        this.purchasePrice = data.purchasePrice || 0;
        this.sellPrice = data.sellPrice || 0;
        this.sellPriceWithoutVat = data.sellPriceWithoutVat || 0;
        this.beforeSellPrice = data.beforeSellPrice || null;
        this.actualSellPrice = data.actualSellPrice || 0;
        this.actualSellPriceWithoutVat = data.actualSellPriceWithoutVat || 0;
        this.actualBeforeSellPrice = data.actualBeforeSellPrice || 0;
        this.weight = data.weight || null;
        this.enabled = data.enabled || false;
        this.parent = data.parent || null;
        this.children = data.children && data.children.length > 0 ? data.children.map(function (child) { return new Product(child); }) : [];
        this.isBundle = data.isBundle || false;
        this.isFragile = data.isFragile || false;
        this.isOversize = data.isOversize || false;
        this.bundleParents =
            data.bundleParents && data.bundleParents.length > 0
                ? data.bundleParents.map(function (parent) { return new _1.ProductBundle(parent); })
                : [];
        this.bundleChildren =
            data.bundleChildren && data.bundleChildren.length > 0
                ? data.bundleChildren.map(function (child) { return new _1.ProductBundle(child); })
                : [];
        this.producer = data.producer ? new _1.Producer(data.producer) : undefined;
        this.vat = new _1.Vat(data.vat) || new _1.Vat();
        this.supplier = data.supplier ? new _1.Supplier(data.supplier) : undefined;
        this.isForSubsequentSettlement = data.isForSubsequentSettlement || false;
        this.mainImage = data.mainImage ? new _1.File(data.mainImage) : undefined;
        this.images = data.images && data.images.length > 0 ? data.images.map(function (img) { return new _1.File(img); }) : [];
        this.files = data.files && data.files.length > 0 ? data.files.map(function (f) { return new _1.File(f); }) : [];
        this.advancedPrices =
            data.advancedPrices && data.advancedPrices.length > 0 ? data.advancedPrices.map(function (p) { return new _1.ProductPrice(p); }) : [];
        // this.expirationDate = data.expirationDate || ''
        this.packs = data.packs && data.packs.length > 0 ? data.packs.map(function (p) { return new _1.ProductPack(p); }) : [];
        this.productParameters =
            data.productParameters && data.productParameters.length > 0
                ? data.productParameters.map(function (p) { return new _1.ProductParameter(p); })
                : [];
        this.quantityUnavailable = data.quantityUnavailable !== 0 ? Number(data.quantityUnavailable) : 0;
        this.quantityStock = data.quantityStock !== 0 ? Number(data.quantityStock) : 0;
        this.quantityReservation = data.quantityReservation !== 0 ? Number(data.quantityReservation) : 0;
        this.quantityAvailable = data.quantityAvailable !== 0 ? Number(data.quantityAvailable) : 0;
        this.note = data.note && data.note.length > 0 ? data.note : '';
        this.description = data.description || '';
        this.customData = data.customData || '';
        this.categories =
            data.categories && data.categories.length > 0 ? data.categories.map(function (category) { return new _1.Category(category); }) : [];
        this.mainCategory = data.mainCategory ? new _1.Category(data.mainCategory) : undefined;
        this.productPriceLevels =
            data.productPriceLevels && data.productPriceLevels.length > 0
                ? data.productPriceLevels.map(function (productPriceLevel) { return new _1.ProductPriceLevel(productPriceLevel); })
                : [];
        this.tags = data.tags && data.tags.length > 0 ? data.tags.map(function (tag) { return new _1.Tag(tag); }) : [];
        this.orderQuantity = data.orderQuantity || 1;
        this.sellItems =
            data.sellItems && data.sellItems.length > 0 ? data.sellItems.map(function (sellItem) { return new _1.SellItem(sellItem); }) : [];
        this.isDeletable = data.isDeletable || false;
        this.dimensionX = data.dimensionX || 0;
        this.dimensionY = data.dimensionY || 0;
        this.dimensionZ = data.dimensionZ || 0;
        this.originCountry = data.originCountry || '';
        this.hsCode = data.hsCode || '';
        this.quantities = data.quantities ? data.quantities.map(function (q) { return new _1.Quantity(q); }) : [];
    }
    __decorate([
        utils_1.PropType('ProductDepot'),
        __metadata("design:type", Array)
    ], Product.prototype, "depots", void 0);
    __decorate([
        utils_1.PropType('Currency'),
        __metadata("design:type", _1.Currency)
    ], Product.prototype, "purchaseCurrency", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", Product)
    ], Product.prototype, "parent", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", Array)
    ], Product.prototype, "children", void 0);
    __decorate([
        utils_1.PropType('ProductBundle'),
        __metadata("design:type", Array)
    ], Product.prototype, "bundleParents", void 0);
    __decorate([
        utils_1.PropType('ProductBundle'),
        __metadata("design:type", Array)
    ], Product.prototype, "bundleChildren", void 0);
    __decorate([
        utils_1.PropType('Producer'),
        __metadata("design:type", _1.Producer)
    ], Product.prototype, "producer", void 0);
    __decorate([
        utils_1.PropType('Vat'),
        __metadata("design:type", _1.Vat)
    ], Product.prototype, "vat", void 0);
    __decorate([
        utils_1.PropType('Supplier'),
        __metadata("design:type", _1.Supplier)
    ], Product.prototype, "supplier", void 0);
    __decorate([
        utils_1.PropType('File'),
        __metadata("design:type", _1.File)
    ], Product.prototype, "mainImage", void 0);
    __decorate([
        utils_1.PropType('File'),
        __metadata("design:type", Array)
    ], Product.prototype, "images", void 0);
    __decorate([
        utils_1.PropType('File'),
        __metadata("design:type", Array)
    ], Product.prototype, "files", void 0);
    __decorate([
        utils_1.PropType('ProductPrice'),
        __metadata("design:type", Array)
    ], Product.prototype, "advancedPrices", void 0);
    __decorate([
        utils_1.PropType('ProductPack'),
        __metadata("design:type", Array)
    ], Product.prototype, "packs", void 0);
    __decorate([
        utils_1.PropType('ProductParameter'),
        __metadata("design:type", Array)
    ], Product.prototype, "productParameters", void 0);
    __decorate([
        utils_1.PropType('Category'),
        __metadata("design:type", Array)
    ], Product.prototype, "categories", void 0);
    __decorate([
        utils_1.PropType('Category'),
        __metadata("design:type", _1.Category)
    ], Product.prototype, "mainCategory", void 0);
    __decorate([
        utils_1.PropType('ProductPriceLevel'),
        __metadata("design:type", Array)
    ], Product.prototype, "productPriceLevels", void 0);
    __decorate([
        utils_1.PropType('Tag'),
        __metadata("design:type", Array)
    ], Product.prototype, "tags", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], Product.prototype, "orderQuantity", void 0);
    __decorate([
        utils_1.PropType('SellItem'),
        __metadata("design:type", Array)
    ], Product.prototype, "sellItems", void 0);
    __decorate([
        utils_1.PropType('Quantity'),
        __metadata("design:type", Array)
    ], Product.prototype, "quantities", void 0);
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=Product.js.map