import { User } from './';
export declare class Message {
    content: string;
    title: string;
    author?: User;
    destination?: User;
    date: string;
    constructor(data?: any);
}
