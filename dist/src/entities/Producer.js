"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Producer = void 0;
var Producer = /** @class */ (function () {
    function Producer(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
    }
    return Producer;
}());
exports.Producer = Producer;
//# sourceMappingURL=Producer.js.map