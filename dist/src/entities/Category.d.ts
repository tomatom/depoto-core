import { Tag } from './Tag';
export declare class Category {
    id: number;
    externalId: string;
    name: string;
    text: string;
    parent?: Category;
    children: Category[];
    hasChildren: boolean;
    position: number;
    treePath: number[];
    tags: Tag[];
    constructor(data?: any);
}
