import { Company, PaymentType } from './';
export declare class Payment {
    id: number;
    name: string;
    type: PaymentType;
    company: Company;
    eetEnable: boolean;
    constructor(data?: any);
}
