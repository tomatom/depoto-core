"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Voucher = void 0;
var Product_1 = require("./Product");
var utils_1 = require("../utils");
var Voucher = /** @class */ (function () {
    function Voucher(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.product = data.product ? new Product_1.Product(data.product) : undefined;
        this.name = data.name || '';
        this.code = data.code || '';
        this.externalId = data.externalId || '';
        this.discountType = data.discountType || '';
        this.discountPercent = data.discountPercent || 0;
        this.discountValue = data.discountValue || 0;
        this.maxUse = data.maxUse || 0;
        this.validFrom = data.validFrom || '';
        this.validTo = data.validTo || '';
        this.isValid = data.isValid || false;
        this.isPayment = data.isPayment || false;
        this.enabled = data.enabled || false;
        this.used = data.user || 0;
        this.paymentItems =
            data.paymentItems && data.paymentItems.length > 0 ? data.paymentItems.map(function (item) { return new Product_1.Product(item); }) : [];
        this.orderItems =
            data.orderItems && data.orderItems.length > 0 ? data.orderItems.map(function (item) { return new Product_1.Product(item); }) : [];
    }
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", Product_1.Product)
    ], Voucher.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", Array)
    ], Voucher.prototype, "paymentItems", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", Array)
    ], Voucher.prototype, "orderItems", void 0);
    return Voucher;
}());
exports.Voucher = Voucher;
//# sourceMappingURL=Voucher.js.map