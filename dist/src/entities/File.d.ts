import { Product, Order, FileThumbnail } from '../entities';
export declare class File {
    id: number;
    text: string;
    originalFilename: string;
    main: boolean;
    mimeType: string;
    base64Data: string;
    size: number;
    url: string;
    product?: Product;
    order?: Order;
    thumbnails: FileThumbnail[];
    constructor(data?: any);
}
