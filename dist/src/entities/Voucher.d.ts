import { Product } from './Product';
export declare class Voucher {
    id: number;
    product?: Product;
    name: string;
    code: string;
    externalId: string;
    discountType: string;
    discountPercent: number;
    discountValue: number;
    maxUse: number;
    validFrom: string;
    validTo: string;
    isValid: boolean;
    isPayment: boolean;
    enabled: boolean;
    used: number;
    paymentItems: Product[];
    orderItems: Product[];
    constructor(data?: any);
}
