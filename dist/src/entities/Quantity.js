"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Quantity = void 0;
var Depot_1 = require("./Depot");
var Quantity = /** @class */ (function () {
    function Quantity(data) {
        if (data === void 0) { data = {}; }
        this.depot = data.depot ? new Depot_1.Depot(data.depot) : undefined;
        this.quantityAvailable = data.quantityAvailable || 0;
        this.quantityReservation = data.quantityReservation || 0;
        this.quantityStock = data.quantityStock || 0;
    }
    return Quantity;
}());
exports.Quantity = Quantity;
//# sourceMappingURL=Quantity.js.map