export declare class FileThumbnail {
    format: string;
    mimeType: string;
    url: string;
    constructor(data?: any);
}
