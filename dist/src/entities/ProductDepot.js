"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductDepot = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ProductDepot = /** @class */ (function () {
    function ProductDepot(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.depot = new _1.Depot(data.depot) || undefined;
        this.supplier = new _1.Supplier(data.supplier) || undefined;
        this.product = new _1.Product(data.product) || undefined;
        this.purchasePrice = data.purchasePrice !== 0 ? data.purchasePrice : 0;
        this.quantityStock = data.quantityStock !== 0 ? data.quantityStock : 0;
        this.quantityReservation = data.quantityReservation !== 0 ? data.quantityReservation : 0;
        this.quantityAvailable = data.quantityAvailable !== 0 ? data.quantityAvailable : 0;
        this.quantityUnavailable = data.quantityUnavailable !== 0 ? data.quantityUnavailable : 0;
        this.inventoryQuantityStock = data.inventoryQuantityStock !== 0 ? data.inventoryQuantityStock : 0;
        this.batch = data.batch ? data.batch : null;
        this.expirationDate = data.expirationDate ? data.expirationDate : null;
        this.position = data.position ? data.position : null;
        this.position1 = data.position1 ? data.position1 : null;
        this.position2 = data.position2 ? data.position2 : null;
        this.position3 = data.position3 ? data.position3 : null;
        this.created = data.created ? data.created : null;
        this.updated = data.updated ? data.updated : null;
    }
    __decorate([
        utils_1.PropType('Depot'),
        __metadata("design:type", _1.Depot)
    ], ProductDepot.prototype, "depot", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], ProductDepot.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('Supplier'),
        __metadata("design:type", _1.Supplier)
    ], ProductDepot.prototype, "supplier", void 0);
    return ProductDepot;
}());
exports.ProductDepot = ProductDepot;
//# sourceMappingURL=ProductDepot.js.map