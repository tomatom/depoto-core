"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductMove = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
// todo cleanup this mess, keep only props on api (need refactor of big part of client app)
var ProductMove = /** @class */ (function () {
    function ProductMove(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.uuid = data.uuid || utils_1.generateUuidV4();
        this.productDepotFrom = data.productDepotFrom ? data.productDepotFrom : null;
        this.productDepotTo = data.productDepotTo ? data.productDepotTo : null;
        this.depotFromObj =
            data.productDepotFrom && data.productDepotFrom.depot && data.productDepotFrom.depot.id > 0
                ? data.productDepotFrom.depot
                : null;
        this.productDepot = data.productDepot ? data.productDepot : null;
        this.depotFrom =
            data.productDepotFrom && data.productDepotFrom.id > 0
                ? data.productDepotFrom.id
                : data.depotFrom
                    ? data.depotFrom
                    : null;
        this.productDepotFromQuantityStock = data.productDepotFromQuantityStock > 0 ? data.productDepotFromQuantityStock : 0;
        this.depotToObj =
            data.productDepotTo && data.productDepotTo.depot && data.productDepotTo.depot.id > 0
                ? data.productDepotTo.depot
                : null;
        this.depotTo =
            data.productDepotTo && data.productDepotTo.id > 0 ? data.productDepotTo.id : data.depotTo ? data.depotTo : null;
        this.productDepotToQuantityStock = data.productDepotToQuantityStock > 0 ? data.productDepotToQuantityStock : 0;
        this.supplier = data.supplier || null;
        this.amount = data.amount || null;
        this.purchasePrice = data.purchasePrice
            ? data.purchasePrice
            : data.productDepotFrom
                ? data.productDepotFrom.purchasePrice
                : data.productDepotTo
                    ? data.productDepotTo.purchasePrice
                    : null;
        this.purchasePriceSum = data.purchasePriceSum
            ? data.purchasePriceSum
            : data.productDepotFrom
                ? data.productDepotFrom.purchasePriceSum
                : data.productDepotTo
                    ? data.productDepotTo.purchasePriceSum
                    : null;
        this.sellPrice = data.sellPrice || null;
        this.sellPriceSum = data.sellPriceSum || null;
        this.note = data.note ? decodeURIComponent(data.note) : undefined; // TODO decode here?
        this.product = new _1.Product(data.product) || null;
        this.pack = data.pack ? new _1.ProductMovePack(data.pack) : undefined;
        this.created = data.created || null;
        this.updated = data.updated || null;
        this.deleted = data.deleted || null;
        this.batch =
            data.productDepotFrom && data.productDepotFrom.id > 0
                ? data.productDepotFrom.batch
                : data.productDepotTo && data.productDepotTo.id > 0
                    ? data.productDepotTo.batch
                    : data.batch || null;
        this.expirationDate =
            data.productDepotFrom && data.productDepotFrom.id > 0
                ? data.productDepotFrom.expirationDate
                : data.productDepotTo && data.productDepotTo.id > 0
                    ? data.productDepotTo.expirationDate
                    : data.expirationDate || null;
        this.position =
            data.productDepotFrom && data.productDepotFrom.id > 0
                ? data.productDepotFrom.position
                : data.productDepotTo && data.productDepotTo.id > 0
                    ? data.productDepotTo.position
                    : data.position || null;
        this.position1 =
            data.productDepotFrom && data.productDepotFrom.id > 0
                ? data.productDepotFrom.position1
                : data.productDepotTo && data.productDepotTo.id > 0
                    ? data.productDepotTo.position1
                    : data.position1 || null;
        this.position2 =
            data.productDepotFrom && data.productDepotFrom.id > 0
                ? data.productDepotFrom.position2
                : data.productDepotTo && data.productDepotTo.id > 0
                    ? data.productDepotTo.position2
                    : data.position2 || null;
        this.position3 =
            data.productDepotFrom && data.productDepotFrom.id > 0
                ? data.productDepotFrom.position3
                : data.productDepotTo && data.productDepotTo.id > 0
                    ? data.productDepotTo.position3
                    : data.position3 || null;
        this.isEANPrintable = data.isEANPrintable || true;
        this.createdBy = data.createdBy && data.createdBy.id > 0 ? new _1.User(data.createdBy) : undefined;
        this.orderItem = data.orderItem && data.orderItem.id > 0 ? new _1.OrderItem(data.orderItem) : undefined;
        this.purchaseCurrencyDate = data.purchaseCurrencyDate ? data.purchaseCurrencyDate : null;
        this.purchaseCurrency = data.purchaseCurrency ? data.purchaseCurrency : 'CZK';
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "uuid", void 0);
    __decorate([
        utils_1.PropType('ProductDepot'),
        __metadata("design:type", _1.ProductDepot)
    ], ProductMove.prototype, "productDepotFrom", void 0);
    __decorate([
        utils_1.PropType('ProductDepot'),
        __metadata("design:type", _1.ProductDepot)
    ], ProductMove.prototype, "productDepotTo", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", _1.Depot)
    ], ProductMove.prototype, "depotFromObj", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "productDepot", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "depotFrom", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", _1.Depot)
    ], ProductMove.prototype, "depotToObj", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "depotTo", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "supplier", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "purchasePrice", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "purchasePriceSum", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "sellPrice", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], ProductMove.prototype, "sellPriceSum", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", _1.Product)
    ], ProductMove.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('ProductMovePack'),
        __metadata("design:type", _1.ProductMovePack)
    ], ProductMove.prototype, "pack", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "batch", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "expirationDate", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "position", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "position1", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "position2", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "position3", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Boolean)
    ], ProductMove.prototype, "isEANPrintable", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], ProductMove.prototype, "createdBy", void 0);
    __decorate([
        utils_1.PropType('OrderItem'),
        __metadata("design:type", _1.OrderItem)
    ], ProductMove.prototype, "orderItem", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "purchaseCurrencyDate", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMove.prototype, "purchaseCurrency", void 0);
    return ProductMove;
}());
exports.ProductMove = ProductMove;
//# sourceMappingURL=ProductMove.js.map