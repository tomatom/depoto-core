"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutClosing = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var CheckoutClosing = /** @class */ (function () {
    function CheckoutClosing(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.checkout = data.checkout ? new _1.Checkout(data.checkout) : undefined;
        this.checkoutId = data.checkoutId || null;
        this.userOpen = data.userOpen ? new _1.User(data.userOpen) : undefined;
        this.dateOpen = data.dateOpen || null;
        this.noteOpen = data.noteOpen || '';
        this.amountOpen = data.amountOpen || 0;
        this.amountRealOpen = data.amountRealOpen || 0;
        this.userClosed = data.userClosed ? new _1.User(data.userClosed) : undefined;
        this.dateClosed = data.dateClosed || null;
        this.noteClosed = data.noteClosed || '';
        this.amountClosed = data.amountClosed || 0;
        this.amountRealClosed = data.amountRealClosed || 0;
        this.orders = data.orders && data.orders.length > 0 ? data.orders.map(function (order) { return new _1.Order(order); }) : [];
        this.vatAllocations =
            data.vatAllocations && data.vatAllocations.length > 0 ? data.vatAllocations.map(function (va) { return new _1.VatAllocation(va); }) : [];
        this.billUrl = data.billUrl;
    }
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", _1.Checkout)
    ], CheckoutClosing.prototype, "checkout", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], CheckoutClosing.prototype, "checkoutId", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], CheckoutClosing.prototype, "userOpen", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], CheckoutClosing.prototype, "userClosed", void 0);
    __decorate([
        utils_1.PropType('Order'),
        __metadata("design:type", Array)
    ], CheckoutClosing.prototype, "orders", void 0);
    __decorate([
        utils_1.PropType('VatAllocation'),
        __metadata("design:type", Array)
    ], CheckoutClosing.prototype, "vatAllocations", void 0);
    return CheckoutClosing;
}());
exports.CheckoutClosing = CheckoutClosing;
//# sourceMappingURL=CheckoutClosing.js.map