"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Consent = void 0;
var Consent = /** @class */ (function () {
    function Consent(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.name = data.name || '';
        this.body = data.body || '';
        this.externalId = data.externalId || '';
        this.created = data.created || '';
        this.updated = data.updated || '';
    }
    return Consent;
}());
exports.Consent = Consent;
//# sourceMappingURL=Consent.js.map