import { Depot, Checkout, Producer, Supplier } from './';
export declare class PurchaseExport {
    id: number;
    generated: string;
    created: string;
    url: string;
    format: string;
    dateFrom: string;
    dateTo: string;
    depots: Depot[];
    checkouts: Checkout[];
    suppliers: Supplier[];
    producers: Producer[];
    constructor(data?: any);
}
