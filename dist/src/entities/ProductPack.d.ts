import { Product } from './';
export declare class ProductPack {
    id: number;
    ean: string;
    code: string;
    quantity: number;
    externalId: string;
    product?: Product;
    constructor(data?: any);
}
