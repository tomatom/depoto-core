import { Depot } from './Depot';
export declare class Quantity {
    depot?: Depot;
    quantityAvailable: number;
    quantityReservation: number;
    quantityStock: number;
    constructor(data?: any);
}
