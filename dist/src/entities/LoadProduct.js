"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadProduct = void 0;
var LoadProduct = /** @class */ (function () {
    function LoadProduct(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.depot = data.depot || null;
        this.supplier = data.supplier || null;
        this.amount = data.amount || null;
        this.purchasePrice = data.purchasePrice || null;
        this.note = data.note || null;
    }
    return LoadProduct;
}());
exports.LoadProduct = LoadProduct;
//# sourceMappingURL=LoadProduct.js.map