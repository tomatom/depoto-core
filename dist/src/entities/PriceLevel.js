"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PriceLevel = void 0;
var utils_1 = require("../utils");
var Currency_1 = require("./Currency");
var PriceLevel = /** @class */ (function () {
    function PriceLevel(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
        this.isPercentage = data.isPercentage || false;
        this.percent = data.percent || 0;
        this.withVat = data.withVat || null;
        this.currency = data.currency ? new Currency_1.Currency(data.currency) : undefined;
        this.externalId = data.externalId || '';
    }
    __decorate([
        utils_1.PropType('Currency'),
        __metadata("design:type", Currency_1.Currency)
    ], PriceLevel.prototype, "currency", void 0);
    return PriceLevel;
}());
exports.PriceLevel = PriceLevel;
//# sourceMappingURL=PriceLevel.js.map