"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageStatus = void 0;
var PackageStatus = /** @class */ (function () {
    function PackageStatus(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.code = data.code || '';
        this.text = data.text || '';
        this.created = data.created || null;
    }
    return PackageStatus;
}());
exports.PackageStatus = PackageStatus;
//# sourceMappingURL=PackageStatus.js.map