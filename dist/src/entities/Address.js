"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address = void 0;
var Address = /** @class */ (function () {
    function Address(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.companyName = data.companyName || '';
        this.firstName = data.firstName || '';
        this.lastName = data.lastName || '';
        this.street = data.street || '';
        this.city = data.city || '';
        this.zip = data.zip || '';
        this.country = data.country || '';
        this.state = data.state || '';
        this.ic = data.ic || '';
        this.dic = data.dic || '';
        this.phone = data.phone || '';
        this.email = data.email || '';
        this.branchId = data.branchId || '';
        this.externalId = data.externalId || '';
        this.isStored = data.isStored || false;
        this.isBilling = data.isBilling || false;
        this.note = data.note || '';
        this.customData = data.customData || '';
    }
    return Address;
}());
exports.Address = Address;
//# sourceMappingURL=Address.js.map