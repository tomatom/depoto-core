"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentItem = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var PaymentItem = /** @class */ (function () {
    function PaymentItem(data) {
        if (data === void 0) { data = {}; }
        var _a;
        this.id = data.id || null;
        this.uuid = data.uuid || utils_1.generateUuidV4();
        this.checkout = data.checkout ? new _1.Checkout(data.checkout) : undefined;
        this.payment = data.payment ? new _1.Payment(data.payment) : undefined;
        this.voucher = data.voucher ? new _1.Voucher(data.voucher) : undefined;
        this.dateCreated = data.dateCreated || new Date().toUTCString();
        this.dateCancelled = data.dateCancelled || null;
        this.amount = data.amount || 0;
        this.isPaid = (_a = data.isPaid) !== null && _a !== void 0 ? _a : true;
        this.datePaid = data.datePaid || null;
        this.externalId = data.externalId || '';
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], PaymentItem.prototype, "uuid", void 0);
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", _1.Checkout)
    ], PaymentItem.prototype, "checkout", void 0);
    __decorate([
        utils_1.PropType('Payment'),
        __metadata("design:type", _1.Payment)
    ], PaymentItem.prototype, "payment", void 0);
    __decorate([
        utils_1.PropType('Voucher'),
        __metadata("design:type", _1.Voucher)
    ], PaymentItem.prototype, "voucher", void 0);
    return PaymentItem;
}());
exports.PaymentItem = PaymentItem;
//# sourceMappingURL=PaymentItem.js.map