"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductMoveType = void 0;
var ProductMoveType = /** @class */ (function () {
    function ProductMoveType(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || '';
        this.name = data.name || '';
    }
    return ProductMoveType;
}());
exports.ProductMoveType = ProductMoveType;
//# sourceMappingURL=ProductMoveType.js.map