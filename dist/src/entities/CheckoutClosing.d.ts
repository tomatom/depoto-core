import { Checkout, Order, User, VatAllocation } from './';
export declare class CheckoutClosing {
    id: number;
    checkout?: Checkout;
    checkoutId: number;
    userOpen?: User;
    dateOpen: string;
    noteOpen: string;
    amountRealOpen: number;
    amountOpen: number;
    userClosed?: User;
    dateClosed: string;
    noteClosed: string;
    amountClosed: number;
    amountRealClosed: number;
    orders: Order[];
    vatAllocations: VatAllocation[];
    billUrl: string;
    constructor(data?: any);
}
