import { Consent, Customer } from './';
export declare class CustomerConsent {
    id: number;
    customer?: Customer;
    consent?: Consent;
    name: string;
    body: string;
    externalId: string;
    created: string;
    updated: string;
    constructor(data?: any);
}
