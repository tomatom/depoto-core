"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPurchasePrice = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ProductPurchasePrice = /** @class */ (function () {
    function ProductPurchasePrice(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.productDepot = data.productDepot && data.productDepot.id > 0 ? new _1.Depot(data.productDepot) : undefined; // TODO Depot -> ProductDepot after api fix
        this.quantity = data.quantity || 0;
        this.purchasePrice = data.purchasePrice || 0;
    }
    __decorate([
        utils_1.PropType('local') // todo ?
        ,
        __metadata("design:type", Number)
    ], ProductPurchasePrice.prototype, "id", void 0);
    __decorate([
        utils_1.PropType('Depot'),
        __metadata("design:type", _1.Depot
        // productDepot: ProductDepot // TODO: this is, in fact Depot on api!!!
        )
    ], ProductPurchasePrice.prototype, "productDepot", void 0);
    return ProductPurchasePrice;
}());
exports.ProductPurchasePrice = ProductPurchasePrice;
//# sourceMappingURL=ProductPurchasePrice.js.map