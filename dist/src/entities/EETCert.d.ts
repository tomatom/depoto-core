export declare class EETCert {
    id: number;
    name: string;
    password: string;
    pkcs12: string;
    publicKey: string;
    expirationDate: string;
    constructor(data?: any);
}
