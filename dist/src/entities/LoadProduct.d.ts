export declare class LoadProduct {
    id: number;
    depot: number;
    supplier: number;
    amount: number;
    purchasePrice: number;
    note: string;
    constructor(data?: any);
}
