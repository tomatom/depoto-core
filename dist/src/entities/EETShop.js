"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EETShop = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var EETShop = /** @class */ (function () {
    function EETShop(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.name = data.name || null;
        this.street = data.street || null;
        this.city = data.city || null;
        this.country = data.country || null;
        this.zip = data.zip || null;
        this.eetId = data.eetId || null;
        this.cert = data.cert ? new _1.EETCert(data.cert) : new _1.EETCert();
        this.checkouts =
            data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(function (checkout) { return new _1.Checkout(checkout); }) : [];
    }
    __decorate([
        utils_1.PropType('EETCert'),
        __metadata("design:type", _1.EETCert)
    ], EETShop.prototype, "cert", void 0);
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", Array)
    ], EETShop.prototype, "checkouts", void 0);
    return EETShop;
}());
exports.EETShop = EETShop;
//# sourceMappingURL=EETShop.js.map