"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Company = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var Company = /** @class */ (function () {
    function Company(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.name = data.name || '';
        this.ic = data.ic || '';
        this.dic = data.dic || '';
        this.email = data.email || '';
        this.phone = data.phone || '';
        this.street = data.street || '';
        this.city = data.city || '';
        this.zip = data.zip || '';
        this.country = data.country || '';
        this.registrationNote = data.registrationNote || '';
        this.nextEan = data.nextEan || 0;
        this.billLogo = data.billLogo || '';
        this.parent = data.parent && data.parent.id > 0 ? new Company(data.parent) : undefined;
        this.children = data.children && data.children.length > 0 ? data.children.map(function (c) { return new Company(c); }) : [];
        this.carrierRelations =
            data.carrierRelations && data.carrierRelations.length > 0
                ? data.carrierRelations.map(function (c) { return new _1.CompanyCarrier(c); })
                : [];
        this.defaultVoucherValidity = data.defaultVoucherValidity || 0;
    }
    __decorate([
        utils_1.PropType('Company'),
        __metadata("design:type", Company)
    ], Company.prototype, "parent", void 0);
    __decorate([
        utils_1.PropType('Company'),
        __metadata("design:type", Array)
    ], Company.prototype, "children", void 0);
    __decorate([
        utils_1.PropType('CompanyCarrier'),
        __metadata("design:type", Array)
    ], Company.prototype, "carrierRelations", void 0);
    return Company;
}());
exports.Company = Company;
//# sourceMappingURL=Company.js.map