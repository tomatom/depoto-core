"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Currency = void 0;
var Currency = /** @class */ (function () {
    function Currency(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || '';
        this.name = data.name || '';
        this.ratio = data.ratio || 1;
    }
    return Currency;
}());
exports.Currency = Currency;
//# sourceMappingURL=Currency.js.map