import { Carrier, Package } from './';
export declare class Disposal {
    id: number;
    code: string;
    carrier?: Carrier;
    packages: Package[];
    ticketUrl: string;
    sent: string;
    constructor(data?: any);
}
