export declare enum ProcessStatusType {
    RECEIVED = "received",
    PICKING = "picking",
    PACKING = "packing",
    PACKED = "packed",
    DISPATCHED = "dispatched",
    DELIVERED = "delivered",
    RETURNED = "returned",
    PICKING_ERROR = "picking_error",
    CANCELLED = "cancelled"
}
export declare class ProcessStatus {
    id: ProcessStatusType | string;
    name: string;
    position: number;
    constructor(data?: any);
}
