"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StockExport = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var StockExport = /** @class */ (function () {
    function StockExport(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.date = data.date || '';
        this.generated = data.generated || '';
        this.created = data.created || new Date().toUTCString();
        this.url = data.url || '';
        this.format = data.format || '';
        this.depots = data.depots && data.depots.length > 0 ? data.depots.map(function (depot) { return new _1.Depot(depot); }) : [];
        this.suppliers =
            data.suppliers && data.suppliers.length > 0 ? data.suppliers.map(function (supplier) { return new _1.Supplier(supplier); }) : [];
        this.producers =
            data.producers && data.producers.length > 0 ? data.producers.map(function (producer) { return new _1.Producer(producer); }) : [];
    }
    __decorate([
        utils_1.PropType('Depot'),
        __metadata("design:type", Array)
    ], StockExport.prototype, "depots", void 0);
    __decorate([
        utils_1.PropType('Supplier'),
        __metadata("design:type", Array)
    ], StockExport.prototype, "suppliers", void 0);
    __decorate([
        utils_1.PropType('Producer'),
        __metadata("design:type", Array)
    ], StockExport.prototype, "producers", void 0);
    return StockExport;
}());
exports.StockExport = StockExport;
//# sourceMappingURL=StockExport.js.map