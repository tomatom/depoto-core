import { Price, Product } from './';
export declare class StatsBestseller {
    amount: number;
    sales: Price[];
    product?: Product;
    constructor(data?: any);
}
