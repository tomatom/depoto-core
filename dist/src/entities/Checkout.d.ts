import { CheckoutClosing, Depot, Order, Payment, User } from './';
export declare class Checkout {
    id: number;
    name: string;
    amount: number;
    nextBillNumber: number;
    nextReservationNumber: number;
    billFooter: string;
    depots: Depot[];
    returnsDepot?: Depot;
    orders: Order[];
    payments: Payment[];
    currentClosing?: CheckoutClosing;
    eetId: number;
    eetEnable: boolean;
    eetPlayground: boolean;
    eetVerificationMode: boolean;
    negativeReservation: boolean;
    eventUrl: string;
    eventTypes: string[];
    users: User[];
    constructor(data?: any);
}
