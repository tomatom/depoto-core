"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentType = void 0;
var PaymentType = /** @class */ (function () {
    function PaymentType(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || '';
        this.name = data.name || '';
    }
    return PaymentType;
}());
exports.PaymentType = PaymentType;
//# sourceMappingURL=PaymentType.js.map