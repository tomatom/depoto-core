"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Disposal = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var Disposal = /** @class */ (function () {
    function Disposal(data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.code = data.code || '';
        this.carrier = data.carrier ? new _1.Carrier(data.carrier) : undefined;
        this.packages =
            data.packages && data.packages.length > 0
                ? data.packages.map(function (p) {
                    p.carrier = _this.carrier;
                    return new _1.Package(p);
                })
                : [];
        this.ticketUrl = data.ticketUrl || '';
        this.sent = data.sent || '';
    }
    __decorate([
        utils_1.PropType('Carrier'),
        __metadata("design:type", _1.Carrier)
    ], Disposal.prototype, "carrier", void 0);
    __decorate([
        utils_1.PropType('Package'),
        __metadata("design:type", Array)
    ], Disposal.prototype, "packages", void 0);
    return Disposal;
}());
exports.Disposal = Disposal;
//# sourceMappingURL=Disposal.js.map