"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Vat = void 0;
var Vat = /** @class */ (function () {
    function Vat(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
        this.percent = data.percent || 0;
        this.coefficient = data.coefficient || 0;
        this.default = data.default || false;
    }
    return Vat;
}());
exports.Vat = Vat;
//# sourceMappingURL=Vat.js.map