import { Company } from './Company';
import { Carrier } from './Carrier';
import { Package } from './Package';
import { File } from './File';
export declare class PackageDisposal {
    id: number;
    code?: string;
    company?: Company;
    carrier?: Carrier;
    packages: [Package];
    ticketUrl?: string;
    sent?: string;
    files: [File];
    constructor(data?: any);
}
