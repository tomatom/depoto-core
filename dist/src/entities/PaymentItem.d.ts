import { Checkout, Payment, Voucher } from './';
export declare class PaymentItem {
    id: number;
    uuid?: string;
    checkout?: Checkout;
    payment?: Payment;
    voucher?: Voucher;
    dateCreated: string;
    dateCancelled: string;
    amount: number;
    isPaid: boolean;
    datePaid: string;
    externalId: string;
    constructor(data?: any);
}
