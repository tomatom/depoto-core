import { Carrier, Checkout } from './';
export declare class CompanyCarrier {
    id: number;
    name: string;
    enable: boolean;
    options: string;
    carrier?: Carrier;
    checkout?: Checkout;
    externalId: string;
    constructor(data?: any);
}
