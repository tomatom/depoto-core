"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyCarrier = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var CompanyCarrier = /** @class */ (function () {
    function CompanyCarrier(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.enable = data.enable || true;
        this.options = data.options || '';
        this.carrier = data.carrier ? new _1.Carrier(data.carrier) : undefined;
        this.checkout = data.checkout ? new _1.Checkout(data.checkout) : undefined;
        this.externalId = data.externalId || '';
        this.name = this.carrier && this.carrier.name ? this.carrier.name : '';
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], CompanyCarrier.prototype, "name", void 0);
    __decorate([
        utils_1.PropType('Carrier'),
        __metadata("design:type", _1.Carrier)
    ], CompanyCarrier.prototype, "carrier", void 0);
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", _1.Checkout)
    ], CompanyCarrier.prototype, "checkout", void 0);
    return CompanyCarrier;
}());
exports.CompanyCarrier = CompanyCarrier;
//# sourceMappingURL=CompanyCarrier.js.map