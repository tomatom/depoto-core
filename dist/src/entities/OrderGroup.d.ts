import { ClearanceItem, Company, Order, User } from './';
export declare class OrderGroup {
    id: number;
    name: string;
    company?: Company;
    orders: Order[];
    user?: User;
    userPosition: number;
    clearanceItems: ClearanceItem[];
    constructor(data?: any);
}
