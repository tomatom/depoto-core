import { Product } from './';
export declare class ProductPrice {
    id: number;
    product: Product;
    sellPrice: number;
    available: number;
    dateFrom: string;
    dateTo: string;
    isActual: boolean;
    note: string;
    constructor(data?: any);
}
