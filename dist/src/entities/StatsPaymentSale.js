"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatsPaymentSale = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var StatsPaymentSale = /** @class */ (function () {
    function StatsPaymentSale(data) {
        if (data === void 0) { data = {}; }
        this.amount = data.amount ? data.amount : 0;
        this.sales = data.sales && data.sales.length > 0 ? data.sales.map(function (s) { return new _1.Price(s); }) : [];
        this.payment = data.payment ? new _1.Payment(data.payment) : undefined;
    }
    __decorate([
        utils_1.PropType('Price'),
        __metadata("design:type", Array)
    ], StatsPaymentSale.prototype, "sales", void 0);
    __decorate([
        utils_1.PropType('Payment'),
        __metadata("design:type", _1.Payment)
    ], StatsPaymentSale.prototype, "payment", void 0);
    return StatsPaymentSale;
}());
exports.StatsPaymentSale = StatsPaymentSale;
//# sourceMappingURL=StatsPaymentSale.js.map