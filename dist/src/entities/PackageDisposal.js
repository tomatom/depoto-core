"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageDisposal = void 0;
var Company_1 = require("./Company");
var Carrier_1 = require("./Carrier");
var Package_1 = require("./Package");
var File_1 = require("./File");
var PackageDisposal = /** @class */ (function () {
    function PackageDisposal(data) {
        if (data === void 0) { data = {}; }
        var _a, _b;
        this.id = data.id || undefined;
        this.code = data.code || undefined;
        this.company = data.company ? new Company_1.Company(data.company) : undefined;
        this.carrier = data.carrier ? new Carrier_1.Carrier(data.carrier) : undefined;
        this.packages = ((_a = data.packages) === null || _a === void 0 ? void 0 : _a.length) > 0 ? data.packages.map(function (p) { return new Package_1.Package(p); }) : [];
        this.ticketUrl = data.ticketUrl || undefined;
        this.sent = data.sent || undefined;
        this.files = ((_b = data.files) === null || _b === void 0 ? void 0 : _b.length) > 0 ? data.files.map(function (f) { return new File_1.File(f); }) : [];
    }
    return PackageDisposal;
}());
exports.PackageDisposal = PackageDisposal;
//# sourceMappingURL=PackageDisposal.js.map