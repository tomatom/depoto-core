"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductMovePack = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ProductMovePack = /** @class */ (function () {
    function ProductMovePack(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.externalId = data.externalId || null;
        this.dateCreated = data.dateCreated || new Date().toUTCString();
        this.dateOperation = data.dateOperation || null;
        this.url = data.url || null;
        this.type = data.type ? new _1.ProductMoveType(data.type) : undefined;
        this.purpose = new _1.ProductMovePackPurpose(data.purpose);
        this.note = data.note ? decodeURIComponent(data.note) : undefined;
        this.number = data.number || null;
        this.depotFrom = data.depotFrom && data.depotFrom.id > 0 ? new _1.Depot(data.depotFrom) : undefined;
        this.depotTo = data.depotTo && data.depotTo.id > 0 ? new _1.Depot(data.depotTo) : undefined;
        this.moves = data.moves && data.moves.length > 0 ? data.moves.map(function (operation) { return new _1.ProductMove(operation); }) : [];
        this.order = data.order ? new _1.Order(data.order) : undefined;
        this.purchasePrice = data.purchasePrice ? data.purchasePrice : 0;
        this.createdBy = data.createdBy && data.createdBy.id > 0 ? new _1.User(data.createdBy) : undefined;
        this.files = data.files && data.files.length > 0 ? data.files.map(function (f) { return new _1.File(f); }) : [];
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductMovePack.prototype, "dateOperation", void 0);
    __decorate([
        utils_1.PropType('ProductMoveType'),
        __metadata("design:type", _1.ProductMoveType)
    ], ProductMovePack.prototype, "type", void 0);
    __decorate([
        utils_1.PropType('ProductMovePackPurpose'),
        __metadata("design:type", _1.ProductMovePackPurpose)
    ], ProductMovePack.prototype, "purpose", void 0);
    __decorate([
        utils_1.PropType('Depot'),
        __metadata("design:type", _1.Depot)
    ], ProductMovePack.prototype, "depotFrom", void 0);
    __decorate([
        utils_1.PropType('Depot'),
        __metadata("design:type", _1.Depot)
    ], ProductMovePack.prototype, "depotTo", void 0);
    __decorate([
        utils_1.PropType('ProductMove'),
        __metadata("design:type", Array)
    ], ProductMovePack.prototype, "moves", void 0);
    __decorate([
        utils_1.PropType('Order'),
        __metadata("design:type", _1.Order)
    ], ProductMovePack.prototype, "order", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], ProductMovePack.prototype, "createdBy", void 0);
    __decorate([
        utils_1.PropType('File'),
        __metadata("design:type", Array)
    ], ProductMovePack.prototype, "files", void 0);
    return ProductMovePack;
}());
exports.ProductMovePack = ProductMovePack;
//# sourceMappingURL=ProductMovePack.js.map