import { ExportType, User } from './';
export declare class Export {
    id: number;
    generated: string;
    created: string;
    url: string;
    type?: ExportType;
    filter: string;
    createdBy?: User;
    note: string;
    constructor(data?: any);
}
