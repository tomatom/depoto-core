"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClearanceItem = exports.ClearanceItemLocation = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ClearanceItemLocation = /** @class */ (function () {
    function ClearanceItemLocation(data) {
        if (data === void 0) { data = {}; }
        this.uuid = data.uuid || utils_1.generateUuidV4();
        this.depot = data.depot || null;
        this.position1 = data.position1 || null;
        this.position2 = data.position2 || null;
        this.position3 = data.position3 || null;
        this.expirationDate = data.expirationDate || null;
        this.batch = data.batch || null;
        this.amount = data.amount || null;
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ClearanceItemLocation.prototype, "uuid", void 0);
    return ClearanceItemLocation;
}());
exports.ClearanceItemLocation = ClearanceItemLocation;
var ClearanceItem = /** @class */ (function () {
    function ClearanceItem(data) {
        if (data === void 0) { data = {}; }
        var _a;
        this.id = data.id || null;
        this.product = data.product && data.product.id > 0 ? new _1.Product(data.product) : undefined;
        this.amount = data.amount || 0;
        this.quantityReservation = data.quantityReservation || 0;
        this.quantityAvailable = data.quantityAvailable || 0;
        this.expirationDate = data.expirationDate || '';
        this.batch = data.batch || '';
        this.position = data.position || '';
        this.position1 = data.position1 || '';
        this.position2 = data.position2 || '';
        this.position3 = data.position3 || '';
        this.picked = data.picked || 0;
        this.packed = data.packed || 0;
        this.locked = (_a = data.locked) !== null && _a !== void 0 ? _a : false;
        this.productDepots =
            data.productDepots && data.productDepots.length ? data.productDepots.map(function (d) { return new _1.ProductDepot(d); }) : [];
        this.productMoves =
            data.productMoves && data.productMoves.length ? data.productMoves.map(function (m) { return new _1.ProductMove(m); }) : [];
        this.locations =
            data.locations && data.locations.length ? data.locations.map(function (l) { return new ClearanceItemLocation(l); }) : [];
    }
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], ClearanceItem.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('ProductMove'),
        __metadata("design:type", Array)
    ], ClearanceItem.prototype, "productMoves", void 0);
    __decorate([
        utils_1.PropType('ProductDepot'),
        __metadata("design:type", Array)
    ], ClearanceItem.prototype, "productDepots", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Array)
    ], ClearanceItem.prototype, "locations", void 0);
    return ClearanceItem;
}());
exports.ClearanceItem = ClearanceItem;
//# sourceMappingURL=ClearanceItem.js.map