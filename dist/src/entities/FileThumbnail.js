"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileThumbnail = void 0;
var FileThumbnail = /** @class */ (function () {
    function FileThumbnail(data) {
        if (data === void 0) { data = {}; }
        this.format = data.format || '';
        this.mimeType = data.mimeType || '';
        this.url = data.url || '';
    }
    return FileThumbnail;
}());
exports.FileThumbnail = FileThumbnail;
//# sourceMappingURL=FileThumbnail.js.map