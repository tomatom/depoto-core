import { Parameter, Product } from './';
export declare class ProductParameter {
    id: number;
    value: string;
    externalId: string;
    product: Product;
    parameter: Parameter;
    constructor(data?: any);
}
