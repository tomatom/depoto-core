export declare class Consent {
    id: number;
    name: string;
    body: string;
    externalId: string;
    created: string;
    updated: string;
    constructor(data?: any);
}
