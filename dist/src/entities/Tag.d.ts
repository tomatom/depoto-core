export declare class Tag {
    id: number;
    name: string;
    type: string;
    color: string;
    externalId: string;
    constructor(data?: any);
}
