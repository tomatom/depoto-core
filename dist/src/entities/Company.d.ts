import { CompanyCarrier } from './';
export declare class Company {
    id: number;
    name: string;
    ic: string;
    dic: string;
    email: string;
    phone: string;
    street: string;
    city: string;
    zip: string;
    country: string;
    registrationNote: string;
    nextEan: number;
    billLogo: string;
    parent?: Company;
    children: Company[];
    carrierRelations: CompanyCarrier[];
    defaultVoucherValidity: number;
    constructor(data?: any);
}
