"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var User = /** @class */ (function () {
    function User(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id > 0 ? data.id : data.id === 0 ? 0 : null;
        this.email = data.email || '';
        this.firstName = data.firstName || '';
        this.lastName = data.lastName || '';
        this.name = data.name || '';
        this.plainPassword = data.plainPassword || '';
        this.phone = data.phone || '';
        this.groups = data.groups || [];
        this.roles =
            !!data.roles && data.roles.length > 0
                ? data.roles
                : !!data.groups && data.groups.length > 0
                    ? data.groups.flatMap(function (g) { return g.roles; })
                    : [];
        this.pin = data.pin || '';
        this.company = data.company || null;
        this.avatar_url = data.avatar_url || ''; // 'assets/img/default-user.png'
        this.externalId = data.externalId;
        this.enabled = data.enabled || true;
        this.customers =
            data.customers && data.customers.length > 0 ? data.customers.map(function (customer) { return new _1.Customer(customer); }) : [];
        this.checkouts =
            data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(function (checkout) { return new _1.Checkout(checkout); }) : [];
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], User.prototype, "plainPassword", void 0);
    __decorate([
        utils_1.PropType('UserGroup'),
        __metadata("design:type", Array)
    ], User.prototype, "groups", void 0);
    __decorate([
        utils_1.PropType('Company'),
        __metadata("design:type", _1.Company)
    ], User.prototype, "company", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], User.prototype, "avatar_url", void 0);
    __decorate([
        utils_1.PropType('Customer'),
        __metadata("design:type", Array)
    ], User.prototype, "customers", void 0);
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", Array)
    ], User.prototype, "checkouts", void 0);
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map