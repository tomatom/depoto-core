"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Package = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var Package = /** @class */ (function () {
    function Package(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.code = data.code || '';
        this.carrier = data.carrier ? new _1.Carrier(data.carrier) : undefined;
        this.items = data.items && data.items.length > 0 ? data.items.map(function (i) { return new _1.OrderItem(i); }) : [];
        this.clearanceItems =
            data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(function (i) { return new _1.ClearanceItem(i); }) : [];
        this.statuses = data.statuses && data.statuses.length > 0 ? data.statuses.map(function (i) { return new _1.PackageStatus(i); }) : [];
        this.order = data.order && data.order.id > 0 ? new _1.Order(data.order) : undefined;
        this.disposal = data.disposal && data.disposal.id > 0 ? new _1.Disposal(data.disposal) : undefined;
        this.ticketUrl = data.ticketUrl || '';
        this.sent = data.sent || null;
        this.weight = data.weight || undefined;
        this.weightRequired = data.weightRequired || false;
        this.dimensionsRequired = data.dimensionsRequired || false;
        this.dimensionX = data.dimensionX || undefined;
        this.dimensionY = data.dimensionY || undefined;
        this.dimensionZ = data.dimensionZ || undefined;
    }
    __decorate([
        utils_1.PropType('Carrier'),
        __metadata("design:type", _1.Carrier)
    ], Package.prototype, "carrier", void 0);
    __decorate([
        utils_1.PropType('Order'),
        __metadata("design:type", _1.Order)
    ], Package.prototype, "order", void 0);
    __decorate([
        utils_1.PropType('Disposal'),
        __metadata("design:type", _1.Disposal)
    ], Package.prototype, "disposal", void 0);
    __decorate([
        utils_1.PropType('OrderItem'),
        __metadata("design:type", Array)
    ], Package.prototype, "items", void 0);
    __decorate([
        utils_1.PropType('ClearanceItem'),
        __metadata("design:type", Array)
    ], Package.prototype, "clearanceItems", void 0);
    __decorate([
        utils_1.PropType('PackageStatus'),
        __metadata("design:type", Array)
    ], Package.prototype, "statuses", void 0);
    return Package;
}());
exports.Package = Package;
//# sourceMappingURL=Package.js.map