"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderProcessStatus = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var OrderProcessStatus = /** @class */ (function () {
    function OrderProcessStatus(data) {
        if (data === void 0) { data = {}; }
        this.status = data.status && data.status.id ? data.status : null;
        this.note = data.note || '';
        this.created = data.created || '';
        this.createdBy = data.createdBy && data.createdBy.id > 0 ? new _1.User(data.createdBy) : undefined;
    }
    __decorate([
        utils_1.PropType('ProcessStatus'),
        __metadata("design:type", _1.ProcessStatus)
    ], OrderProcessStatus.prototype, "status", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], OrderProcessStatus.prototype, "createdBy", void 0);
    return OrderProcessStatus;
}());
exports.OrderProcessStatus = OrderProcessStatus;
//# sourceMappingURL=OrderProcessStatus.js.map