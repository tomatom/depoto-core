"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EETReceipt = void 0;
var EETReceipt = /** @class */ (function () {
    function EETReceipt(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.dic = data.dic || null;
        this.checkoutEetId = data.checkoutEetId || null;
        this.shopEetId = data.shopEetId || null;
        this.playground = data.playground || false;
        this.verificationMode = data.verificationMode || false;
        this.number = data.number || null;
        this.dateCreated = data.dateCreated || null;
        this.totalPrice = data.totalPrice || 0;
        this.fik = data.fik || null;
        this.bkp = data.bkp || null;
        this.pkp = data.pkp || null;
    }
    return EETReceipt;
}());
exports.EETReceipt = EETReceipt;
//# sourceMappingURL=EETReceipt.js.map