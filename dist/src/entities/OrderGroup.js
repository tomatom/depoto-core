"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderGroup = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var OrderGroup = /** @class */ (function () {
    function OrderGroup(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
        this.company = !!data.company ? new _1.Company(data.company) : undefined;
        this.orders = !!data.orders && data.orders.length > 0 ? data.orders.map(function (o) { return new _1.Order(o); }) : [];
        this.user = !!data.user ? new _1.User(data.user) : undefined;
        this.userPosition = data.userPosition || 0;
        this.clearanceItems =
            !!data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(function (c) { return new _1.ClearanceItem(c); }) : [];
    }
    __decorate([
        utils_1.PropType('Company'),
        __metadata("design:type", _1.Company)
    ], OrderGroup.prototype, "company", void 0);
    __decorate([
        utils_1.PropType('Order'),
        __metadata("design:type", Array)
    ], OrderGroup.prototype, "orders", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], OrderGroup.prototype, "user", void 0);
    __decorate([
        utils_1.PropType('ClearanceItem'),
        __metadata("design:type", Array)
    ], OrderGroup.prototype, "clearanceItems", void 0);
    return OrderGroup;
}());
exports.OrderGroup = OrderGroup;
//# sourceMappingURL=OrderGroup.js.map