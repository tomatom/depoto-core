import { Checkout, Producer, Supplier } from './';
export declare class SaleExport {
    id: number;
    generated: string;
    created: string;
    url: string;
    format: string;
    dateFrom: string;
    dateTo: string;
    checkouts: Checkout[];
    producers: Producer[];
    suppliers: Supplier[];
    constructor(data?: any);
}
