import { Product } from './Product';
import { ProductDepot } from './ProductDepot';
export declare class SellItem {
    id: string;
    product?: Product;
    quantityStock: number;
    quantityReservation: number;
    quantityAvailable: number;
    expirationDate: string;
    batch: string;
    productDepots: ProductDepot[];
    orderQuantity: number;
    constructor(data?: any);
}
