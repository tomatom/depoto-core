import { Checkout, EETCert } from './';
export declare class EETShop {
    id: number;
    name: string;
    street: string;
    city: string;
    country: string;
    zip: string;
    eetId: number;
    cert: EETCert;
    checkouts: Checkout[];
    constructor(data?: any);
}
