"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var Order = /** @class */ (function () {
    function Order(data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.status = !!data.status ? data.status : new _1.OrderStatus({ id: 0, status: 'reservation' });
        this.reservationNumber = data.reservationNumber || null;
        this.billNumber = data.billNumber || null;
        this.dateCreated = data.dateCreated || '';
        this.items = data.items && data.items.length > 0 ? data.items.map(function (item) { return new _1.OrderItem(item); }) : [];
        this.paymentItems =
            data.paymentItems && data.paymentItems.length > 0 ? data.paymentItems.map(function (item) { return new _1.PaymentItem(item); }) : [];
        this.checkout = data.checkout ? new _1.Checkout(data.checkout) : undefined;
        this.customer = data.customer ? new _1.Customer(data.customer) : undefined;
        this.currency = data.currency ? new _1.Currency(data.currency) : undefined;
        this.note = data.note || '';
        this.privateNote = data.privateNote || '';
        this.invoiceAddress = data.invoiceAddress ? new _1.Address(data.invoiceAddress) : undefined;
        this.shippingAddress = data.shippingAddress ? new _1.Address(data.shippingAddress) : undefined;
        this.relatedParent = data.relatedParent ? new Order(data.relatedParent) : undefined;
        this.rounding = data.rounding || null;
        this.eetReceipt = data.eetReceipt ? new _1.EETReceipt(data.eetReceipt) : undefined;
        this.billUrl = data.billUrl || '';
        this.vatAllocations =
            data.vatAllocations && data.vatAllocations.length
                ? data.vatAllocations
                    .map(function (v) { return new _1.VatAllocation(v); })
                    .filter(function (v) { return v.price > 0; })
                    .sort(function (a, b) {
                    if (!a.vat || (a.vat && b.vat && a.vat.percent > b.vat.percent)) {
                        return 1;
                    }
                    else if (!b.vat || (a.vat && b.vat && a.vat.percent < b.vat.percent)) {
                        return -1;
                    }
                    else {
                        return 0;
                    }
                })
                : [];
        this.processStatus = data.processStatus ? new _1.ProcessStatus(data.processStatus) : undefined;
        this.processStatusUpdated = data.processStatusUpdated ? data.processStatusUpdated : undefined;
        this.processStatusRelation = data.processStatusRelation
            ? new _1.OrderProcessStatus(data.processStatusRelation)
            : undefined;
        this.processStatusRelations =
            data.processStatusRelations && data.processStatusRelations.length > 0
                ? data.processStatusRelations.map(function (r) { return new _1.OrderProcessStatus(r); })
                : [];
        this.company = data.company && data.company.name ? new _1.Company(data.company) : undefined;
        this.carrier = data.carrier && data.carrier.name ? new _1.Carrier(data.carrier) : undefined;
        this.packages =
            data.packages && data.packages.length > 0
                ? data.packages.map(function (p) {
                    p.order = {
                        id: _this.id,
                        billNumber: _this.billNumber,
                        reservationNumber: _this.reservationNumber,
                    }; // todo get from api...
                    p.carrier = _this.carrier;
                    return new _1.Package(p);
                })
                : [];
        this.quantityUnavailable = data.quantityUnavailable !== 0 ? data.quantityUnavailable : 0;
        this.boxes = data.boxes && data.boxes.length > 0 ? data.boxes : [];
        this.isPaid = data.isPaid || false;
        this.files = data.files && data.files.length > 0 ? data.files.map(function (f) { return new _1.File(f); }) : [];
        this.vs = data.vs !== 0 ? data.vs : null;
        this.createdBy = data.createdBy && data.createdBy.id > 0 ? new _1.User(data.createdBy) : undefined;
        this.movePacks = data.movePacks && data.movePacks.length > 0 ? data.movePacks.map(function (p) { return new _1.ProductMovePack(p); }) : [];
        this.isEditable = !!data.isEditable;
        this.externalId = data.externalId || null;
        this.group = !!data.group ? new _1.OrderGroup(data.group) : undefined;
        this.groupPosition = data.groupPosition || 0;
        this.clearanceItems =
            data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(function (c) { return new _1.ClearanceItem(c); }) : [];
        this.priceAll = data.priceAll || null;
        this.isSelected = data.isSelected || false;
        this.tags = data.tags && data.tags.length > 0 ? data.tags.map(function (t) { return new _1.Tag(t); }) : [];
        this.priority = data.priority || 1;
        this.dateExpedition = data.dateExpedition || null;
        this.dateDue = data.dateDue;
        this.dateTax = data.dateTax;
    }
    __decorate([
        utils_1.PropType('OrderStatus'),
        __metadata("design:type", _1.OrderStatus)
    ], Order.prototype, "status", void 0);
    __decorate([
        utils_1.PropType('OrderItem'),
        __metadata("design:type", Array)
    ], Order.prototype, "items", void 0);
    __decorate([
        utils_1.PropType('PaymentItem'),
        __metadata("design:type", Array)
    ], Order.prototype, "paymentItems", void 0);
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", _1.Checkout)
    ], Order.prototype, "checkout", void 0);
    __decorate([
        utils_1.PropType('Customer'),
        __metadata("design:type", _1.Customer)
    ], Order.prototype, "customer", void 0);
    __decorate([
        utils_1.PropType('Currency'),
        __metadata("design:type", _1.Currency)
    ], Order.prototype, "currency", void 0);
    __decorate([
        utils_1.PropType('Address'),
        __metadata("design:type", _1.Address)
    ], Order.prototype, "invoiceAddress", void 0);
    __decorate([
        utils_1.PropType('Address'),
        __metadata("design:type", _1.Address)
    ], Order.prototype, "shippingAddress", void 0);
    __decorate([
        utils_1.PropType('Order'),
        __metadata("design:type", Order)
    ], Order.prototype, "relatedParent", void 0);
    __decorate([
        utils_1.PropType('EETReceipt'),
        __metadata("design:type", _1.EETReceipt)
    ], Order.prototype, "eetReceipt", void 0);
    __decorate([
        utils_1.PropType('VatAllocation'),
        __metadata("design:type", Array)
    ], Order.prototype, "vatAllocations", void 0);
    __decorate([
        utils_1.PropType('ProcessStatus'),
        __metadata("design:type", _1.ProcessStatus)
    ], Order.prototype, "processStatus", void 0);
    __decorate([
        utils_1.PropType('OrderProcessStatus'),
        __metadata("design:type", _1.OrderProcessStatus)
    ], Order.prototype, "processStatusRelation", void 0);
    __decorate([
        utils_1.PropType('OrderProcessStatus'),
        __metadata("design:type", Array)
    ], Order.prototype, "processStatusRelations", void 0);
    __decorate([
        utils_1.PropType('Company'),
        __metadata("design:type", _1.Company)
    ], Order.prototype, "company", void 0);
    __decorate([
        utils_1.PropType('Carrier'),
        __metadata("design:type", _1.Carrier)
    ], Order.prototype, "carrier", void 0);
    __decorate([
        utils_1.PropType('Package'),
        __metadata("design:type", Array)
    ], Order.prototype, "packages", void 0);
    __decorate([
        utils_1.PropType('File'),
        __metadata("design:type", Array)
    ], Order.prototype, "files", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], Order.prototype, "createdBy", void 0);
    __decorate([
        utils_1.PropType('ProductMovePack'),
        __metadata("design:type", Array)
    ], Order.prototype, "movePacks", void 0);
    __decorate([
        utils_1.PropType('OrderGroup'),
        __metadata("design:type", _1.OrderGroup)
    ], Order.prototype, "group", void 0);
    __decorate([
        utils_1.PropType('ClearanceItem'),
        __metadata("design:type", Array)
    ], Order.prototype, "clearanceItems", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Boolean)
    ], Order.prototype, "isSelected", void 0);
    __decorate([
        utils_1.PropType('Tag'),
        __metadata("design:type", Array)
    ], Order.prototype, "tags", void 0);
    return Order;
}());
exports.Order = Order;
//# sourceMappingURL=Order.js.map