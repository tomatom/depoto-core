"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tag = void 0;
var Tag = /** @class */ (function () {
    function Tag(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.name = data.name || '';
        this.type = data.type || '';
        this.color = data.color || '';
        this.externalId = data.externalId || '';
    }
    return Tag;
}());
exports.Tag = Tag;
//# sourceMappingURL=Tag.js.map