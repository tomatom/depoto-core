"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SellItem = void 0;
var Product_1 = require("./Product");
var ProductDepot_1 = require("./ProductDepot");
var utils_1 = require("../utils");
var SellItem = /** @class */ (function () {
    function SellItem(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.product = data.product ? new Product_1.Product(data.product) : undefined;
        this.quantityStock = data.quantityStock || 0;
        this.quantityReservation = data.quantityReservation || 0;
        this.quantityAvailable = data.quantityAvailable || 0;
        this.expirationDate = data.expirationDate || '';
        this.batch = data.batch || '';
        this.productDepots =
            data.productDepots && data.productDepots.length > 0
                ? data.productDepots.map(function (productDepot) { return new ProductDepot_1.ProductDepot(productDepot); })
                : [];
        this.orderQuantity = data.orderQuantity || 1;
    }
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", Product_1.Product)
    ], SellItem.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('ProductDepot'),
        __metadata("design:type", Array)
    ], SellItem.prototype, "productDepots", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", Number)
    ], SellItem.prototype, "orderQuantity", void 0);
    return SellItem;
}());
exports.SellItem = SellItem;
//# sourceMappingURL=SellItem.js.map