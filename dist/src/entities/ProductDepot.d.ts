import { Depot, Supplier, Product } from './';
export declare class ProductDepot {
    id: number;
    depot?: Depot;
    product?: Product;
    supplier?: Supplier;
    purchasePrice: number;
    quantityStock: number;
    quantityReservation: number;
    quantityAvailable: number;
    quantityUnavailable: number;
    inventoryQuantityStock: number;
    batch: string;
    expirationDate: string;
    position: string;
    position1: string;
    position2: string;
    position3: string;
    created: string;
    updated: string;
    constructor(data?: any);
}
