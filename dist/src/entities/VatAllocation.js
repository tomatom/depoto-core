"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VatAllocation = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var VatAllocation = /** @class */ (function () {
    function VatAllocation(data) {
        if (data === void 0) { data = {}; }
        this.vat = data.vat ? new _1.Vat(data.vat) : undefined;
        this.priceWithoutVat = data.priceWithoutVat !== 0 ? data.priceWithoutVat : 0;
        this.vatPrice = data.vatPrice !== 0 ? data.vatPrice : 0;
        this.price = data.price !== 0 ? data.price : 0;
    }
    __decorate([
        utils_1.PropType('Vat'),
        __metadata("design:type", _1.Vat)
    ], VatAllocation.prototype, "vat", void 0);
    return VatAllocation;
}());
exports.VatAllocation = VatAllocation;
//# sourceMappingURL=VatAllocation.js.map