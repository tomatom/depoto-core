import { Product } from './';
export declare class ProductBundle {
    id: number;
    uuid: string;
    parent?: Product;
    child?: Product;
    quantity: number;
    constructor(data?: any);
}
