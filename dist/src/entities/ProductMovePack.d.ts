import { Depot, File, Order, ProductMove, ProductMovePackPurpose, ProductMoveType, User } from './';
export declare class ProductMovePack {
    id: number;
    externalId: number;
    dateCreated: string;
    dateOperation: string;
    url: string;
    type?: ProductMoveType;
    purpose?: ProductMovePackPurpose;
    note?: string;
    number: number;
    depotFrom?: Depot;
    depotTo?: Depot;
    moves: ProductMove[];
    order?: Order;
    purchasePrice: number;
    createdBy?: User;
    files: File[];
    constructor(data?: any);
}
