"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderStatus = void 0;
var OrderStatus = /** @class */ (function () {
    function OrderStatus(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 'reservation';
        this.name = data.name && data.name.length > 0 ? data.name : '';
    }
    return OrderStatus;
}());
exports.OrderStatus = OrderStatus;
//# sourceMappingURL=OrderStatus.js.map