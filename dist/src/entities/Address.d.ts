export declare class Address {
    id: number;
    companyName: string;
    firstName: string;
    lastName: string;
    street: string;
    city: string;
    zip: string;
    country: string;
    state: string;
    ic: string;
    dic: string;
    phone: string;
    email: string;
    branchId: string;
    externalId: string;
    isStored: boolean;
    isBilling: boolean;
    note: string;
    customData: string;
    constructor(data?: any);
}
