import { Checkout, OrderStatus } from './';
export declare class OrderExport {
    id: number;
    generated: string;
    created: string;
    url: string;
    format: string;
    dateFrom: string;
    dateTo: string;
    checkouts: Checkout[];
    status?: OrderStatus;
    constructor(data?: any);
}
