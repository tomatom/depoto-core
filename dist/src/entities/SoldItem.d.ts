import { Product } from './';
export declare class SoldItem {
    name: string;
    type: string;
    ean: string;
    code: string;
    quantity: number;
    priceAll: number;
    priceAllWithoutVat: number;
    product?: Product;
    profit: number;
    constructor(data?: any);
}
