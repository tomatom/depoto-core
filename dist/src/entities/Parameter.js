"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Parameter = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var Parameter = /** @class */ (function () {
    function Parameter(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.name = data.name || '';
        this.type = data.type || 'text'; // todo default?
        this.enumValues = Array.isArray(data.enumValues) && data.enumValues.length > 0 ? data.enumValues : [];
        this.unit = data.unit || null;
        this.text = data.text || '';
        this.externalId = data.externalId || null;
        this.company = data.company ? new _1.Company(data.company) : new _1.Company();
    }
    __decorate([
        utils_1.PropType('Company'),
        __metadata("design:type", _1.Company)
    ], Parameter.prototype, "company", void 0);
    return Parameter;
}());
exports.Parameter = Parameter;
//# sourceMappingURL=Parameter.js.map