"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPrice = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ProductPrice = /** @class */ (function () {
    function ProductPrice(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.product = data.product && data.product.id > 0 ? new _1.Product(data.product) : new _1.Product();
        this.sellPrice = data.sellPrice || 0;
        this.available = data.available || 0;
        this.dateFrom = data.dateFrom || '';
        this.dateTo = data.dateTo || '';
        this.isActual = data.isActual || false;
        this.note = data.note || '';
    }
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], ProductPrice.prototype, "product", void 0);
    return ProductPrice;
}());
exports.ProductPrice = ProductPrice;
//# sourceMappingURL=ProductPrice.js.map