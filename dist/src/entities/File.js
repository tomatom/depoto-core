"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.File = void 0;
var entities_1 = require("../entities");
var utils_1 = require("../utils");
var File = /** @class */ (function () {
    function File(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.text = data.text || '';
        this.originalFilename = data.originalFilename || '';
        this.main = data.main || false;
        this.mimeType = data.mimeType || '';
        this.base64Data = data.base64Data || '';
        this.size = data.size || 0;
        this.url = data.url || '';
        this.product = data.product && data.product.id > 0 ? new entities_1.Product(data.product) : undefined;
        this.order = data.order && data.order.id > 0 ? new entities_1.Order(data.order) : undefined;
        this.thumbnails =
            data.thumbnails && data.thumbnails.length > 0 ? data.thumbnails.map(function (t) { return new entities_1.FileThumbnail(t); }) : [];
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], File.prototype, "base64Data", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", entities_1.Product)
    ], File.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('Order'),
        __metadata("design:type", entities_1.Order)
    ], File.prototype, "order", void 0);
    __decorate([
        utils_1.PropType('FileThumbnail'),
        __metadata("design:type", Array)
    ], File.prototype, "thumbnails", void 0);
    return File;
}());
exports.File = File;
//# sourceMappingURL=File.js.map