"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Supplier = void 0;
var Supplier = /** @class */ (function () {
    function Supplier(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
    }
    return Supplier;
}());
exports.Supplier = Supplier;
//# sourceMappingURL=Supplier.js.map