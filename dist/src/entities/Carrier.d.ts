import { Tariff } from './';
export declare class Carrier {
    id: string;
    name: string;
    color: string;
    position: number;
    tariffs: Tariff[];
    constructor(data?: any);
}
