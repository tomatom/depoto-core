"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevLog = void 0;
var DevLog = /** @class */ (function () {
    function DevLog(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.name = data.name || null;
        this.content = data.content || null;
        this.created = data.created || null;
    }
    Object.defineProperty(DevLog, "names", {
        get: function () {
            return {
                error: 'client-error',
                exception: 'client-exception',
                orderRepeated: 'client-order-repeated',
            };
        },
        enumerable: false,
        configurable: true
    });
    return DevLog;
}());
exports.DevLog = DevLog;
//# sourceMappingURL=DevLog.js.map