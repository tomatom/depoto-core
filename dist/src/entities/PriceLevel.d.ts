import { Currency } from './Currency';
export declare class PriceLevel {
    id: number;
    name: string;
    isPercentage: boolean;
    percent: number;
    withVat: boolean;
    currency?: Currency;
    externalId: string;
    constructor(data?: any);
}
