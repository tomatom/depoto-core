"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductBundle = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ProductBundle = /** @class */ (function () {
    function ProductBundle(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.uuid = data.uuid && data.uuid.length > 0 ? data.uuid : utils_1.generateUuidV4();
        this.parent = data.parent && data.parent.id > 0 ? new _1.Product(data.parent) : undefined;
        this.child = data.child && data.child.id > 0 ? new _1.Product(data.child) : undefined;
        this.quantity = data.quantity !== 0 ? data.quantity : 0;
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], ProductBundle.prototype, "uuid", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], ProductBundle.prototype, "parent", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], ProductBundle.prototype, "child", void 0);
    return ProductBundle;
}());
exports.ProductBundle = ProductBundle;
//# sourceMappingURL=ProductBundle.js.map