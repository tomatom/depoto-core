import { Depot, Producer, Supplier } from './';
export declare class StockExport {
    id: number;
    date: string;
    generated: string;
    created: string;
    url: string;
    format: string;
    depots: Depot[];
    suppliers: Supplier[];
    producers: Producer[];
    constructor(data?: any);
}
