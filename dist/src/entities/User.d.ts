import { Checkout, Company, Customer, UserGroup } from './';
export declare class User {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    name: string;
    plainPassword: string;
    phone: string;
    groups: UserGroup[];
    roles: string[];
    pin: string;
    company: Company;
    enabled: boolean;
    avatar_url: string;
    externalId?: string;
    customers: Customer[];
    checkouts: Checkout[];
    constructor(data?: any);
}
