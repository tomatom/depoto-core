import { Carrier, ClearanceItem, Disposal, Order, OrderItem, PackageStatus } from './';
export declare class Package {
    id: number;
    code: string;
    carrier?: Carrier;
    order?: Order;
    disposal?: Disposal;
    items: OrderItem[];
    clearanceItems: ClearanceItem[];
    statuses: PackageStatus[];
    ticketUrl: string;
    sent: string;
    weight?: number | null;
    weightRequired: boolean;
    dimensionsRequired: boolean;
    dimensionX?: number | null;
    dimensionY?: number | null;
    dimensionZ?: number | null;
    constructor(data?: any);
}
