"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPack = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ProductPack = /** @class */ (function () {
    function ProductPack(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.ean = data.ean || '';
        this.code = data.code || '';
        this.quantity = data.quantity || 0;
        this.externalId = data.externalId || '';
        this.product = data.product && data.product.id > 0 ? new _1.Product(data.product) : undefined;
    }
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], ProductPack.prototype, "product", void 0);
    return ProductPack;
}());
exports.ProductPack = ProductPack;
//# sourceMappingURL=ProductPack.js.map