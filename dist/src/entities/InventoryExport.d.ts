import { Depot, File, Producer, Supplier, ProductMovePack } from './';
export declare class InventoryExport {
    id: number;
    date: string;
    generated: string;
    created: string;
    url: string;
    format: string;
    files: File[];
    depots: Depot[];
    suppliers: Supplier[];
    producers: Producer[];
    approved: string;
    rejected: string;
    finished: string;
    pmpIn?: ProductMovePack;
    pmpOut?: ProductMovePack;
    constructor(data?: any);
}
