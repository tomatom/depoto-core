import { Checkout, User } from './';
export declare class ExtraOperation {
    id: number;
    checkout?: Checkout;
    amount: number;
    dateCreated: string;
    note: string;
    user?: User;
    constructor(data?: any);
}
