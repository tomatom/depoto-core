export declare class Tariff {
    id: number;
    name: string;
    const: string;
    position: number;
    cod: boolean;
    constructor(data?: any);
}
