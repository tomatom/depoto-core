import { ProcessStatus, User } from './';
export declare class OrderProcessStatus {
    status: ProcessStatus;
    note: string;
    created: string;
    createdBy?: User;
    constructor(data?: any);
}
