import { Carrier } from './';
export declare class StatsCarrierUsage {
    amount: number;
    carrier?: Carrier;
    constructor(data?: any);
}
