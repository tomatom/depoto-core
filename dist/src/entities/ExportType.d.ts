export declare class ExportType {
    id: number;
    name: string;
    format: string;
    group: string;
    constructor(data?: any);
}
