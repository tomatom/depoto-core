import { Product, ProductDepot, ProductMove } from './';
export declare class ClearanceItemLocation {
    uuid: string;
    depot: number;
    position1?: string;
    position2?: string;
    position3?: string;
    expirationDate?: string;
    batch?: string;
    amount: number;
    constructor(data?: any);
}
export declare class ClearanceItem {
    id: string;
    product?: Product;
    amount: number;
    quantityReservation: number;
    quantityAvailable: number;
    expirationDate: string;
    batch: string;
    position: string;
    position1: string;
    position2: string;
    position3: string;
    picked: number;
    packed: number;
    locked: boolean;
    productMoves: ProductMove[];
    productDepots: ProductDepot[];
    locations: ClearanceItemLocation[];
    constructor(data?: any);
}
