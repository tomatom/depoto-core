"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderItem = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var OrderItem = /** @class */ (function () {
    function OrderItem(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.uuid = data.uuid && data.uuid.length > 0 ? data.uuid : utils_1.generateUuidV4();
        this.name = data.name || '';
        this.ean = data.ean || '';
        this.code = data.code || '';
        this.quantity = data.quantity || 0;
        this.sale = data.sale || null;
        this.price = data.price || 0;
        this.priceWithoutVat = data.priceWithoutVat || 0;
        this.priceAll = data.priceAll || 0;
        this.priceAllWithoutVat = data.priceAllWithoutVat || 0;
        this.picked = data.picked || false;
        this.packed = data.packed || false;
        this.serial = data.serial || '';
        this.note = data.note || '';
        this.order = data.order ? new _1.Order(data.order) : undefined;
        this.product = data.product ? new _1.Product(data.product) : undefined;
        this.vat = data.vat ? new _1.Vat(data.vat) : undefined;
        this.isForSubsequentSettlement = data.isForSubsequentSettlement || false;
        this.type = data.type || 'product';
        this.quantityUnavailable = data.quantityUnavailable !== 0 ? data.quantityUnavailable : 0;
        this.moves = data.moves && data.moves.length > 0 ? data.moves.map(function (m) { return new _1.ProductMove(m); }) : [];
        this.clearanceItems =
            data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(function (c) { return new _1.ClearanceItem(c); }) : [];
        this.batch = data.batch;
        this.expirationDate = data.expirationDate;
        this.position = data.position;
        this.purchaseCurrency = data.purchaseCurrency;
        this.purchaseCurrencyDate = data.purchaseCurrencyDate;
    }
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], OrderItem.prototype, "uuid", void 0);
    __decorate([
        utils_1.PropType('Order'),
        __metadata("design:type", _1.Order)
    ], OrderItem.prototype, "order", void 0);
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], OrderItem.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('Vat'),
        __metadata("design:type", _1.Vat)
    ], OrderItem.prototype, "vat", void 0);
    __decorate([
        utils_1.PropType('ProductMove'),
        __metadata("design:type", Array)
    ], OrderItem.prototype, "moves", void 0);
    __decorate([
        utils_1.PropType('ClearanceItem'),
        __metadata("design:type", Array)
    ], OrderItem.prototype, "clearanceItems", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], OrderItem.prototype, "batch", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], OrderItem.prototype, "expirationDate", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], OrderItem.prototype, "position", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], OrderItem.prototype, "purchaseCurrency", void 0);
    __decorate([
        utils_1.PropType('local'),
        __metadata("design:type", String)
    ], OrderItem.prototype, "purchaseCurrencyDate", void 0);
    return OrderItem;
}());
exports.OrderItem = OrderItem;
//# sourceMappingURL=OrderItem.js.map