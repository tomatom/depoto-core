"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductMovePackPurpose = void 0;
var ProductMovePackPurpose = /** @class */ (function () {
    function ProductMovePackPurpose(data) {
        if (data === void 0) { data = {}; }
        if (!!data && typeof data === 'string') {
            this.id = data;
            this.name = data;
        }
        else if (!!data && !!data.id) {
            this.id = data.id || 'default';
            this.name = data.name || 'default';
        }
        else {
            this.id = 'default';
            this.name = 'default';
        }
    }
    return ProductMovePackPurpose;
}());
exports.ProductMovePackPurpose = ProductMovePackPurpose;
//# sourceMappingURL=ProductMovePackPurpose.js.map