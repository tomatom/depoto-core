"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Role = void 0;
var Role = /** @class */ (function () {
    function Role(data) {
        if (data === void 0) { data = {}; }
        this.name = data.name || '';
        this.role = data.role || '';
    }
    return Role;
}());
exports.Role = Role;
//# sourceMappingURL=Role.js.map