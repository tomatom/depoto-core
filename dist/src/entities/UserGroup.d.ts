import { Company } from './';
export declare class UserGroup {
    id: number;
    name: string;
    roles: string[];
    company: Company;
    constructor(data?: any);
}
