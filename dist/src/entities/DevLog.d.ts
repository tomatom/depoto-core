export declare class DevLog {
    id: number;
    name: string;
    content: string;
    created: string;
    constructor(data?: any);
    static get names(): any;
}
