"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtraOperation = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ExtraOperation = /** @class */ (function () {
    function ExtraOperation(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.checkout = data.checkout ? new _1.Checkout(data.checkout) : undefined;
        this.amount = data.amount || 0;
        this.dateCreated = data.dateCreated || 0;
        this.note = data.note || '';
        this.user = data.user ? new _1.User(data.user) : undefined;
    }
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", _1.Checkout)
    ], ExtraOperation.prototype, "checkout", void 0);
    __decorate([
        utils_1.PropType('User'),
        __metadata("design:type", _1.User)
    ], ExtraOperation.prototype, "user", void 0);
    return ExtraOperation;
}());
exports.ExtraOperation = ExtraOperation;
//# sourceMappingURL=CheckoutExtraOperation.js.map