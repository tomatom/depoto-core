"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EETCert = void 0;
var utils_1 = require("../utils");
var EETCert = /** @class */ (function () {
    function EETCert(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.name = data.name || null;
        this.password = data.password || null;
        this.pkcs12 = data.pkcs12 || null;
        this.publicKey = data.publicKey || null;
        this.expirationDate = data.expirationDate || null;
    }
    __decorate([
        utils_1.PropType('local') // in fact not local, but we never get this prop from api back (used on create)
        ,
        __metadata("design:type", String)
    ], EETCert.prototype, "password", void 0);
    __decorate([
        utils_1.PropType('local') // in fact not local, but we never get this prop from api back (used on create)
        ,
        __metadata("design:type", String)
    ], EETCert.prototype, "pkcs12", void 0);
    return EETCert;
}());
exports.EETCert = EETCert;
//# sourceMappingURL=EETCert.js.map