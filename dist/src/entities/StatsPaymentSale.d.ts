import { Payment, Price } from './';
export declare class StatsPaymentSale {
    amount: number;
    sales: Price[];
    payment?: Payment;
    constructor(data?: any);
}
