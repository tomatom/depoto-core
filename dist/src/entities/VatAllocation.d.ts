import { Vat } from './';
export declare class VatAllocation {
    vat?: Vat;
    priceWithoutVat: number;
    vatPrice: number;
    price: number;
    constructor(data?: any);
}
