"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExportType = void 0;
var ExportType = /** @class */ (function () {
    function ExportType(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.name = data.name || '';
        this.format = data.format || '';
        this.group = data.group || '';
    }
    return ExportType;
}());
exports.ExportType = ExportType;
//# sourceMappingURL=ExportType.js.map