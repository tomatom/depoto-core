"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductParameter = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var ProductParameter = /** @class */ (function () {
    function ProductParameter(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || null;
        this.value = data.value || '';
        this.externalId = data.externalId || null;
        this.product = data.product ? new _1.Product(data.product) : new _1.Product();
        this.parameter = data.parameter ? new _1.Parameter(data.parameter) : new _1.Parameter();
    }
    __decorate([
        utils_1.PropType('Product'),
        __metadata("design:type", _1.Product)
    ], ProductParameter.prototype, "product", void 0);
    __decorate([
        utils_1.PropType('Parameter'),
        __metadata("design:type", _1.Parameter)
    ], ProductParameter.prototype, "parameter", void 0);
    return ProductParameter;
}());
exports.ProductParameter = ProductParameter;
//# sourceMappingURL=ProductParameter.js.map