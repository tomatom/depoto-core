export declare class Depot {
    id: number;
    name: string;
    emailIn?: string;
    unavailablesUrl: string;
    constructor(data?: any);
}
