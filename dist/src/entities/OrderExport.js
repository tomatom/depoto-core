"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderExport = void 0;
var _1 = require("./");
var utils_1 = require("../utils");
var OrderExport = /** @class */ (function () {
    function OrderExport(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id || 0;
        this.generated = data.generated || '';
        this.created = data.created || new Date().toUTCString();
        this.url = data.url || '';
        this.format = data.format || '';
        this.dateFrom = data.dateFrom || '';
        this.dateTo = data.dateTo || '';
        this.checkouts =
            data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(function (checkout) { return new _1.Checkout(checkout); }) : [];
        this.status = data.status ? new _1.OrderStatus(data.status) : undefined;
    }
    __decorate([
        utils_1.PropType('Checkout'),
        __metadata("design:type", Array)
    ], OrderExport.prototype, "checkouts", void 0);
    __decorate([
        utils_1.PropType('OrderStatus'),
        __metadata("design:type", _1.OrderStatus)
    ], OrderExport.prototype, "status", void 0);
    return OrderExport;
}());
exports.OrderExport = OrderExport;
//# sourceMappingURL=OrderExport.js.map