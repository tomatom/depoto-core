import { HttpService as IHttpService } from './HttpService';
import { LoggerService, OAuthService, StorageService } from '../services';
import { Observable } from '../utils';
import { NotificationEvent } from '../models';
export interface EntityServiceConfiguration {
    loggerService: LoggerService;
    s: StorageService;
    oauthService: OAuthService;
    httpService: IHttpService;
    notificationEventObservable: Observable<NotificationEvent>;
}
