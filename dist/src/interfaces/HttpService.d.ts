export interface HttpService {
    get(uri: string): Promise<any>;
    post(uri: string, data: any): Promise<any>;
    put(uri: string, data: any): Promise<any>;
    downloadBlob(uri: string): Promise<Blob>;
}
