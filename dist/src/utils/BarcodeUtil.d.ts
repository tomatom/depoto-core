export declare class BarcodeUtil {
    static translateBarcode(barcode: string, skipKeyLayoutCheck?: boolean): string;
    static replaceAll(str: string, mapObj: any): string;
    static isKeyboardLayoutSupported(str: string): boolean;
}
