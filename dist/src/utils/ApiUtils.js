"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEndpoint = exports.ENDPOINT = exports.DEFAULT_CLIENT_KEYS = void 0;
exports.DEFAULT_CLIENT_KEYS = {
    prod: {
        clientId: '2_NXDodEF6OyKawRuHxCoNoyL6WcE1r5cDOxJohc7Sod6l01qP6D',
        clientSecret: 'FZMH4IrOuqQQSuJJE4I6D7VR088EB84S3I4nwmrSzjTeKXgyje',
    },
    stage: {
        clientId: '2_NXDodEF6OyKawRuHxCoNoyL6WcE1r5cDOxJohc7Sod6l01qP6D',
        clientSecret: 'FZMH4IrOuqQQSuJJE4I6D7VR088EB84S3I4nwmrSzjTeKXgyje',
    },
    dev: {
        clientId: '2_NXDodEF6OyKawRuHxCoNoyL6WcE1r5cDOxJohc7Sod6l01qP6D',
        clientSecret: 'FZMH4IrOuqQQSuJJE4I6D7VR088EB84S3I4nwmrSzjTeKXgyje',
    },
    custom: {
        clientId: '2_NXDodEF6OyKawRuHxCoNoyL6WcE1r5cDOxJohc7Sod6l01qP6D',
        clientSecret: 'FZMH4IrOuqQQSuJJE4I6D7VR088EB84S3I4nwmrSzjTeKXgyje',
    },
};
exports.ENDPOINT = {
    GRAPHQL: 'graphql',
    GRAPHQL_POST: 'graphql_post',
    TOKEN: 'token',
    REFRESH_TOKEN: 'refresh_token',
};
var getEndpoint = function (endpoint, session) {
    var server = 'https://server1.depoto.cz/';
    var route = '';
    var format = '.json';
    if (!!session.clientType) {
        switch (session.clientType) {
            case 'prod':
                server = 'https://server1.depoto.cz/';
                break;
            case 'stage':
                server = 'https://server1.depoto.cz.tomatomstage.cz/';
                break;
            case 'dev':
                server = 'https://server-dev.depoto.cz/app_dev.php/';
                break;
            case 'custom':
                server = session.clientCustomUri;
                break;
        }
    }
    switch (endpoint) {
        case exports.ENDPOINT.GRAPHQL:
            route = 'graphql?query=';
            break;
        case exports.ENDPOINT.GRAPHQL_POST:
            route = 'graphql';
            break;
        case exports.ENDPOINT.TOKEN:
        case exports.ENDPOINT.REFRESH_TOKEN:
            route = 'oauth/v2/token';
            break;
    }
    if (endpoint === 'token' || endpoint === 'refresh_token' || endpoint === 'graphql' || endpoint === 'graphql_post') {
        return "" + server + route;
    }
    else {
        return "" + server + route + format;
    }
};
exports.getEndpoint = getEndpoint;
//# sourceMappingURL=ApiUtils.js.map