export declare const SCHEMA_MAX_DEPTH = 10;
export declare class SchemaUtil {
    static extractFrom(entityType: string, depth?: number, excludedClasses?: Array<string> | any, classPath?: Array<string> | any): any;
    private static getInstance;
}
