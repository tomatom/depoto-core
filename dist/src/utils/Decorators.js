"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPropType = exports.PropType = void 0;
var DECORATOR = {
    ANNOTATION_PROPTYPE: 'annotation:proptype',
};
var PropType = function (key) {
    return Reflect.metadata(DECORATOR.ANNOTATION_PROPTYPE, key);
};
exports.PropType = PropType;
var getPropType = function (instance, key) {
    return Reflect.getMetadata(DECORATOR.ANNOTATION_PROPTYPE, instance, key);
};
exports.getPropType = getPropType;
//# sourceMappingURL=Decorators.js.map