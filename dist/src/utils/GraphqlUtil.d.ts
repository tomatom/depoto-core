export declare class GraphQLUtil {
    static prepareParams(params: any): any;
    static createMutation(data: any, dataDefinition: any, method: string, mutationName?: string): string;
    static createQuery(dataDefinition: any, method: string, parameters: any, queryName?: string): string;
    static createComposedQuery(data: any): string;
    private static processDataDefinition;
    private static flattenObject;
    private static processValue;
    private static isArray;
    private static isString;
    private static isStringDateOrBase64;
    private static isStringMap;
    private static isBlank;
    private static isNumber;
}
