"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Observer = exports.Observable = void 0;
var _1 = require("./");
var Observable = /** @class */ (function () {
    function Observable(id) {
        this.observers = [];
        this.id = id !== null && id !== void 0 ? id : _1.generateUuidV4();
    }
    Observable.prototype.getId = function () {
        return this.id;
    };
    Observable.prototype.subscribe = function () {
        return new Observer(this);
    };
    Observable.prototype.registerObserver = function (o) {
        this.observers.push(o);
    };
    Observable.prototype.unregisterObserver = function (o) {
        var _this = this;
        this.observers.forEach(function (ob, i) {
            if (ob.getId() === o.getId()) {
                o.emit({ type: 'final', data: null });
                _this.observers.splice(i, 1);
            }
        });
    };
    Observable.prototype.unregisterObservers = function () {
        this.finally();
        this.observers = [];
    };
    Observable.prototype.isObserved = function () {
        var _a;
        return ((_a = this.observers) === null || _a === void 0 ? void 0 : _a.length) > 0;
    };
    Object.defineProperty(Observable.prototype, "observersCount", {
        get: function () {
            var _a, _b;
            return (_b = (_a = this.observers) === null || _a === void 0 ? void 0 : _a.length) !== null && _b !== void 0 ? _b : 0;
        },
        enumerable: false,
        configurable: true
    });
    Observable.prototype.emit = function (data) {
        this.observers.forEach(function (o) { return o.emit({ type: 'data', data: data }); });
    };
    Observable.prototype.error = function (message) {
        this.observers.forEach(function (o) { return o.emit({ type: 'error', data: message }); });
    };
    Observable.prototype.finally = function () {
        this.observers.forEach(function (o) { return o.emit({ type: 'final', data: null }); });
    };
    return Observable;
}());
exports.Observable = Observable;
var Observer = /** @class */ (function () {
    function Observer(observable) {
        this.isSubscribed = true;
        this.id = _1.generateUuidV4();
        this.observable = observable;
        this.observable.registerObserver(this);
    }
    Observer.prototype.getId = function () {
        return this.id;
    };
    Observer.prototype.unsubscribe = function () {
        this.isSubscribed = false;
        this.observable.unregisterObserver(this);
    };
    Observer.prototype.emit = function (_a) {
        var type = _a.type, data = _a.data;
        switch (type) {
            case 'data':
                if (!this.onData || (!!this.onData && typeof this.onData !== 'function')) {
                    throw Error('no callable in .then() callback!');
                }
                this.onData(data);
                break;
            case 'error':
                if (!this.onError || (!!this.onError && typeof this.onError !== 'function')) {
                    throw Error(data);
                }
                this.onError(data);
                break;
            case 'final':
                this.isSubscribed = false;
                if (typeof this.onFinal === 'function') {
                    this.onFinal(data);
                }
                break;
        }
    };
    Observer.prototype.then = function (onData) {
        if (!onData || (!!onData && typeof onData !== 'function')) {
            throw Error('no callable in .then() callback!');
        }
        this.onData = onData;
        return this;
    };
    Observer.prototype.error = function (onError) {
        this.onError = onError;
        return this;
    };
    Observer.prototype.finally = function (onFinal) {
        this.onFinal = onFinal;
        return this;
    };
    return Observer;
}());
exports.Observer = Observer;
//# sourceMappingURL=Observable.js.map