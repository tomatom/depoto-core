export declare const PropType: (key: string) => any;
export declare const getPropType: (instance: any, key: string) => string;
