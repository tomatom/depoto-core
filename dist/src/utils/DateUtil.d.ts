export declare class DateUtil {
    static getDateStringFromUTCString(utcString: string): string;
    static formatDate(utcString: string, hoursAndMins?: boolean): string;
}
