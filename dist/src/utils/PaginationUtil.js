"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginationUtil = void 0;
var PaginationUtil = /** @class */ (function () {
    function PaginationUtil(ngComponent, paginationBtnId, collectionProp, fetchFunc, currentPageProp, endPageProp, totalItemsProp, scrollOffsetThreshold) {
        if (paginationBtnId === void 0) { paginationBtnId = 'pagination-next-btn'; }
        if (collectionProp === void 0) { collectionProp = 'items'; }
        if (fetchFunc === void 0) { fetchFunc = 'getItems'; }
        if (currentPageProp === void 0) { currentPageProp = 'currentPage'; }
        if (endPageProp === void 0) { endPageProp = 'endPage'; }
        if (totalItemsProp === void 0) { totalItemsProp = 'totalItems'; }
        if (scrollOffsetThreshold === void 0) { scrollOffsetThreshold = 5; }
        this.ngComponent = null;
        this.scrollDebounceTimeout = null;
        this.ngComponent = ngComponent;
        this.paginationBtnId = paginationBtnId;
        this.collectionProp = collectionProp;
        this.fetchFunc = fetchFunc;
        this.currentPageProp = currentPageProp;
        this.endPageProp = endPageProp;
        this.totalItemsProp = totalItemsProp;
        this.scrollOffsetThreshold = scrollOffsetThreshold;
        var t = this;
        window.onscroll = function () {
            var el = document.getElementById(t.paginationBtnId);
            if ((!t.ngComponent.loading || !t.ngComponent.reloading) && !t.scrollDebounceTimeout && t.checkVisible(el)) {
                t.scrolled();
                t.scrollDebounceTimeout = setTimeout(function () {
                    t.scrollDebounceTimeout = null;
                }, 700);
            }
        };
    }
    PaginationUtil.prototype.scrolled = function () {
        if ((!this.ngComponent[this.currentPageProp] ||
            this.ngComponent[this.currentPageProp] < this.ngComponent[this.endPageProp]) &&
            this.ngComponent[this.totalItemsProp] > this.ngComponent[this.collectionProp].length &&
            !this.ngComponent.loading) {
            if (!this.ngComponent[this.currentPageProp]) {
                this.ngComponent[this.currentPageProp] = 1;
            }
            if (typeof this.ngComponent[this.fetchFunc] === 'function') {
                this.ngComponent[this.fetchFunc](this.ngComponent[this.currentPageProp] + 1);
            }
        }
    };
    PaginationUtil.prototype.checkVisible = function (el, threshold, mode) {
        if (threshold === void 0) { threshold = this.scrollOffsetThreshold; }
        if (mode === void 0) { mode = 'visible'; }
        if (el) {
            var rect = el.getBoundingClientRect();
            var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
            var above = rect.bottom - threshold < 0;
            var below = rect.top - viewHeight + threshold >= 0;
            return mode === 'above' ? above : mode === 'below' ? below : !above && !below;
        }
        return false;
    };
    return PaginationUtil;
}());
exports.PaginationUtil = PaginationUtil;
//# sourceMappingURL=PaginationUtil.js.map