"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GraphQLUtil = void 0;
var GraphQLUtil = /** @class */ (function () {
    function GraphQLUtil() {
    }
    GraphQLUtil.prepareParams = function (params) {
        Object.keys(params).forEach(function (k, i) {
            if (params[k] === undefined) {
                delete params[k];
            }
        });
        return params;
    };
    GraphQLUtil.createMutation = function (data, dataDefinition, method, mutationName) {
        if (!method || !data) {
            return '';
        }
        if (method.includes('update')) {
            data = GraphQLUtil.prepareParams(data);
        }
        var mutation = 'mutation ' + (mutationName || method) + '{' + method;
        mutation += '(' + GraphQLUtil.flattenObject(data) + ')';
        mutation += GraphQLUtil.processDataDefinition(dataDefinition || data) + '}';
        return mutation;
    };
    GraphQLUtil.createQuery = function (dataDefinition, method, parameters, queryName) {
        if (!method || !dataDefinition) {
            return '';
        }
        var query = 'query ' + (queryName || method) + '{' + method;
        query += '(' + GraphQLUtil.flattenObject(parameters) + ')';
        query += GraphQLUtil.processDataDefinition(dataDefinition) + '}';
        return query;
    };
    GraphQLUtil.createComposedQuery = function (data) {
        var query = 'query{';
        data.forEach(function (row, i) {
            if (i > 0) {
                query += ',';
            }
            query += row.method;
            query += '(' + GraphQLUtil.flattenObject(row.parameters) + ')';
            query += GraphQLUtil.processDataDefinition(row.definition);
        });
        return query + '}';
    };
    GraphQLUtil.processDataDefinition = function (dataDefinition) {
        if (!dataDefinition) {
            return '';
        }
        var query = '';
        var keys = Object.keys(dataDefinition);
        keys.forEach(function (key, index) {
            if (!index) {
                query += '{';
            }
            query += key;
            if (dataDefinition[key] instanceof Array && dataDefinition[key].length) {
                query += GraphQLUtil.processDataDefinition(dataDefinition[key][0]);
            }
            else if (dataDefinition[key] instanceof Object) {
                query += GraphQLUtil.processDataDefinition(dataDefinition[key]);
            }
            if (index === keys.length - 1) {
                query += '}';
            }
            else {
                query += ',';
            }
        });
        return query;
    };
    GraphQLUtil.flattenObject = function (object) {
        return Object.keys(object || {})
            .reduce(function (array, key) {
            if (!GraphQLUtil.isBlank(object[key])) {
                array.push(key + ':' + GraphQLUtil.processValue(object[key]));
            }
            else {
                // fix issue with not updating null values.. so, send every key supplied to mutation | query
                array.push(key + ':' + null);
            }
            return array;
        }, [])
            .join(',');
    };
    GraphQLUtil.processValue = function (value) {
        if (GraphQLUtil.isBlank(value)) {
            return '';
        }
        // if (GraphQLUtil.isNumber(value)) { return Number(value) } // todo: debug! ('' => 0 ???)
        // const isJsonStr = str => { // todo dont use this! json must be encoded, contact BE dev in case of double encoded json returned!!
        //   let result = false
        //   let tmpObj = null
        //   if (str && str.length > 1 && (str[0] === '{' || str[0] === '[')) {
        //     try {
        //       tmpObj = JSON.parse(str)
        //       if (typeof tmpObj === 'object' || Array.isArray(tmpObj)) {
        //         result = true
        //       }
        //     } catch (e) {}
        //   }
        //   return result
        // }
        if (GraphQLUtil.isString(value)) {
            if (GraphQLUtil.isStringDateOrBase64(value)) {
                // console.warn('str eaw: ', value) // TODO: remove
                // const datastr = atob(value.substr(10))
                //dwnload
                // const link = document.createElement('a')
                // // link.setAttribute('href', base64.replace('image/jpeg', 'image/octet-stream'))
                // link.setAttribute('href', value)
                // link.setAttribute('download', 'test11111.xlsx')
                //
                // link.innerText = 'download image'
                // document.body.appendChild(link)
                // link.click()
                // return `"${value}"`
                return "\"" + value.replace('data:application/octet-stream;base64,', '') + "\"";
                // } else if (isJsonStr(value)) {
                //   return `"${value}"`
            }
            else {
                return "\"" + encodeURIComponent(value) + "\"";
            }
        }
        // if (GraphQLUtil.isString(value)) { return '"' + value.replace(/'/g, '&quot').replace(/"/g, '&quot') + '"' }
        if (GraphQLUtil.isArray(value)) {
            var arrayString_1 = '[';
            value.forEach(function (valueInArray, index) {
                arrayString_1 += GraphQLUtil.processValue(valueInArray);
                if (index !== value.length - 1) {
                    arrayString_1 += ',';
                }
            });
            arrayString_1 += ']';
            return arrayString_1;
        }
        if (GraphQLUtil.isStringMap(value)) {
            var objectString_1 = '{';
            var keys_1 = Object.keys(value);
            keys_1.forEach(function (key, index) {
                objectString_1 += key + ':' + GraphQLUtil.processValue(value[key]);
                if (index !== keys_1.length - 1) {
                    objectString_1 += ',';
                }
            });
            objectString_1 += '}';
            return objectString_1;
        }
        return value.toString();
    };
    GraphQLUtil.isArray = function (obj) {
        return Array.isArray(obj);
    };
    GraphQLUtil.isString = function (obj) {
        return typeof obj === 'string';
    };
    GraphQLUtil.isStringDateOrBase64 = function (str) {
        if (!str) {
            return false;
        }
        // try { // does not work
        //   atob(str)
        //   console.warn('is base 64!!!!!!!!!!!!!') // TODO: remove
        //   return true // is base64 string
        // } catch (e) {}
        if (str.substr(0, 5) === 'data:') {
            return true;
        }
        var date;
        try {
            date = new Date(str);
        }
        catch (e) {
            date = null;
        }
        return !!date ? date.getFullYear() === Number(str.substr(0, 4)) : false;
    };
    GraphQLUtil.isStringMap = function (obj) {
        return typeof obj === 'object' && obj !== null;
    };
    GraphQLUtil.isBlank = function (obj) {
        return obj === undefined || obj === null;
    };
    GraphQLUtil.isNumber = function (obj) {
        if (typeof obj == 'boolean' || (typeof obj == 'string' && obj[0] === '0')) {
            return false;
        }
        if (typeof obj == 'number' && obj.toFixed(0)[0] === '0') {
            return false;
        }
        return !Array.isArray(obj) && !isNaN(Number(obj));
    };
    return GraphQLUtil;
}());
exports.GraphQLUtil = GraphQLUtil;
//# sourceMappingURL=GraphqlUtil.js.map