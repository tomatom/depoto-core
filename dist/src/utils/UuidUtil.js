"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateUuidV4 = void 0;
var generateUuidV4 = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0, v = c == 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
};
exports.generateUuidV4 = generateUuidV4;
//# sourceMappingURL=UuidUtil.js.map