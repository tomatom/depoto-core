"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.styledConsoleLog = void 0;
function styledConsoleLog(args, args2, args3, args4, args5) {
    if (args === void 0) { args = null; }
    var argArray = [];
    if (arguments.length) {
        var startTagRe = /<span\s+style=(['"])([^'"]*)\1\s*>/gi;
        var endTagRe = /<\/span>/gi;
        var reResultArray = void 0;
        argArray.push(arguments[0].replace(startTagRe, '%c').replace(endTagRe, '%c'));
        while ((reResultArray = startTagRe.exec(arguments[0]))) {
            argArray.push(reResultArray[2]);
            argArray.push('');
        }
        // pass through subsequent arguments since chrome dev tools does not (yet) support console.log styling of the following form: console.log('%cBlue!', 'color: blue;', '%cRed!', 'color: red;');
        for (var j = 1; j < arguments.length; j++) {
            argArray.push(arguments[j]);
        }
    }
    console.log.apply(console, argArray);
}
exports.styledConsoleLog = styledConsoleLog;
//# sourceMappingURL=ConsoleStylingUtil.js.map