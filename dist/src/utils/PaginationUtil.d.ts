export declare class PaginationUtil {
    ngComponent: any;
    paginationBtnId: string;
    collectionProp: string;
    fetchFunc: string;
    currentPageProp: string;
    endPageProp: string;
    totalItemsProp: string;
    scrollOffsetThreshold: number;
    scrollDebounceTimeout: any;
    constructor(ngComponent: any, paginationBtnId?: string, collectionProp?: string, fetchFunc?: string, currentPageProp?: string, endPageProp?: string, totalItemsProp?: string, scrollOffsetThreshold?: number);
    scrolled(): void;
    private checkVisible;
}
