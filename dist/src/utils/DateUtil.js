"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateUtil = void 0;
var DateUtil = /** @class */ (function () {
    function DateUtil() {
    }
    DateUtil.getDateStringFromUTCString = function (utcString) {
        if (utcString && utcString.length > 0) {
            var d = new Date(utcString);
            return (d.getFullYear() +
                '-' +
                ('0' + (d.getMonth() + 1)).slice(-2) +
                '-' +
                ('0' + d.getDate()).slice(-2) +
                ' ' +
                ('0' + d.getHours()).slice(-2) +
                ':' +
                ('0' + d.getMinutes()).slice(-2) +
                ':' +
                ('0' + d.getSeconds()).slice(-2));
        }
        return '';
    };
    DateUtil.formatDate = function (utcString, hoursAndMins) {
        if (hoursAndMins === void 0) { hoursAndMins = false; }
        var d = new Date(utcString);
        if (hoursAndMins) {
            return (('0' + d.getDate()).slice(-2) +
                '.' +
                ('0' + (d.getMonth() + 1)).slice(-2) +
                '.' +
                d.getFullYear() +
                ' ' +
                ('0' + d.getHours()).slice(-2) +
                ':' +
                ('0' + d.getMinutes()).slice(-2) +
                ':' +
                ('0' + d.getSeconds()).slice(-2));
        }
        return ('0' + d.getDate()).slice(-2) + '.' + ('0' + (d.getMonth() + 1)).slice(-2) + '.' + d.getFullYear();
    };
    return DateUtil;
}());
exports.DateUtil = DateUtil;
//# sourceMappingURL=DateUtil.js.map