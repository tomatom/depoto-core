import { Customer, Product } from '../entities';
export declare class ProductPriceLevelUtil {
    static getProductPriceForCustomerPriceLevel(product: Product, customer?: Customer): number;
}
