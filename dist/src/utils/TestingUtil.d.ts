export declare const assert: (condition: boolean, message?: string | undefined) => boolean;
