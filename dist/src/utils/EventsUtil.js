"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Events = void 0;
// TODO: RN implementation
var Events = /** @class */ (function () {
    function Events() {
    }
    Events.dispatch = function (name, data) {
        var event = new CustomEvent(name, { detail: data });
        if ('dispatchEvent' in window) {
            window.dispatchEvent(event);
        }
        else {
            console.warn('TODO: RN implementation (Events:dispatch)'); // TODO: RN implementation
        }
        if (window['DEBUG'] && window['DEBUG'].events) {
            console.log('>>> events:dispatched ', name, data);
        }
    };
    Events.listen = function (name, callback) {
        if (window['DEBUG'] && window['DEBUG'].events) {
            console.log('>>> events:listening ', name, callback);
        }
        if ('addEventListener' in window) {
            window.addEventListener(name, function (e) {
                if (window['DEBUG'] && window['DEBUG'].events) {
                    console.log('>>> events:triggered ', name, e['detail']);
                }
                callback ? callback(name, e['detail']) : null;
            });
        }
        else {
            console.warn('TODO: RN implementation (Events:listen)'); // TODO: RN implementation
        }
    };
    return Events;
}());
exports.Events = Events;
//# sourceMappingURL=EventsUtil.js.map