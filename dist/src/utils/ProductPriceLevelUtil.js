"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPriceLevelUtil = void 0;
var ProductPriceLevelUtil = /** @class */ (function () {
    function ProductPriceLevelUtil() {
    }
    ProductPriceLevelUtil.getProductPriceForCustomerPriceLevel = function (product, customer) {
        var _a, _b, _c, _d;
        if (!customer || (customer && !customer.priceLevel)) {
            return product.actualSellPrice;
        }
        else if (((_a = customer.priceLevel) === null || _a === void 0 ? void 0 : _a.isPercentage) === true) {
            return product.actualSellPrice - product.actualSellPrice * (((_b = customer.priceLevel) === null || _b === void 0 ? void 0 : _b.percent) / 100);
        }
        else if (((_c = customer.priceLevel) === null || _c === void 0 ? void 0 : _c.isPercentage) === false) {
            for (var _i = 0, _e = product.productPriceLevels; _i < _e.length; _i++) {
                var productPriceLevel = _e[_i];
                if (productPriceLevel.id === ((_d = customer.priceLevel) === null || _d === void 0 ? void 0 : _d.id)) {
                    return productPriceLevel.sellPrice;
                }
            }
        }
        // if none of the productPriceLevels is the same as customer's priceLevel, then assign default sellPrice
        return product.actualSellPrice;
    };
    return ProductPriceLevelUtil;
}());
exports.ProductPriceLevelUtil = ProductPriceLevelUtil;
//# sourceMappingURL=ProductPriceLevelUtil.js.map