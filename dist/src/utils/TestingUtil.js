"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.assert = void 0;
var assert = function (condition, message) {
    if (!condition) {
        message = "Assertion failed " + message;
        if (typeof Error !== 'undefined') {
            throw new Error(message);
        }
        throw message;
    }
    return condition;
};
exports.assert = assert;
// export class TestingUtil {
//   maxLineChars: number = 42
//   stopOnError: boolean = false
//   showTestResponses: boolean = false
//   currentTest: string
//   allTests: number = 0
//   succeededTests: number = 0
//   failedTests: number = 0
//   protected testCheckoutId: number // used for Order creating
//   protected testProductAId: number // used for ProductBundle etc
//   protected testProductBId: number // used for ProductBundle etc
//   protected roles: any[]
//   constructor(private services: ServiceContainer) {}
//
//   setStopOnError(stop: boolean): void {
//     this.stopOnError = stop
//   }
//
//   setShowTestResponses(show: boolean): void {
//     this.showTestResponses = show
//   }
//
//   setCurrentTest(test: string): void {
//     this.currentTest = test
//     this.allTests++
//   }
//
//   clearTestCounts(): void {
//     this.allTests = 0
//     this.succeededTests = 0
//     this.failedTests = 0
//   }
//
//   getTestCounts(): any {
//     this.logTestStart(` ${this.allTests} tests done. success ${this.succeededTests}, fail ${this.failedTests}`)
//     return {
//       allTests: this.allTests,
//       succeededTests: this.succeededTests,
//       failedTests: this.failedTests,
//     }
//   }
//
//   testAddress(): Promise<any> {
//     this.logTestStart('Address')
//     this.setCurrentTest('Address:getList')
//     return this.services.address.getList({}).then(res => {
//       this.testResponse(res, 'Address:getList')
//       this.setCurrentTest('Address:getId')
//       return this.services.address.getById(res.items[0].id)
//     }).then(res => {
//       this.testResponse(res, 'Address:getById')
//       this.setCurrentTest('Address:create')
//       const a = new Address({firstName: 'foo', lastName: 'bar', email: 'fii@ber.bozz', country: 'CZ'})
//       return this.services.address.create(a)
//     }).then(res => {
//       if (this.testResponse(res, 'Address:create')) {
//         this.setCurrentTest('Address:update')
//         return this.services.address.update({id: res.id, lastName: 'updated', ...res}).then(res => {
//           this.testResponse(res, 'Address:update')
//           this.setCurrentTest('Address:delete')
//           return this.services.address.delete({id: res.id}).then(res => this.testResponse(res === null, 'Address:delete'))
//         }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//       }
//     })
//   }
//
//   testCarrier(): Promise<any> {
//     this.logTestStart('Carrier')
//     this.setCurrentTest('Carrier:getList')
//     return this.services.carrier.getList({}).then(res => {
//       return this.testResponse(res, 'Carrier:getList')
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testCheckout(): Promise<any> {
//     this.logTestStart('Checkout')
//     this.setCurrentTest('Checkout:getList')
//     const ch = new Checkout({name: 'CORE test checkout', nextBillNumber: 1, nextReservationNumber: 1})
//     return this.services.checkout.getList({}, 4, ['OrderItem']).then(res => {
//       this.testResponse(res, 'Checkout:getList')
//       this.setCurrentTest('Checkout:getById')
//       return this.services.checkout.getById(res.items[0].id, 4, ['OrderItem'])
//     }).then(res => {
//       this.testResponse(res, 'Checkout:getById')
//       this.setCurrentTest('Checkout:create')
//       this.testCheckoutId = res.id
//       return this.services.checkout.create(ch, 4, ['OrderItem'])
//     }).then(res => {
//       this.testResponse(res, 'Checkout:create')
//       this.setCurrentTest('Checkout:update')
//       ch.id = res.id
//       ch.name = 'updated'
//       return this.services.checkout.update(ch, 4, ['OrderItem'])
//     }).then(res => {
//       this.testResponse(res, 'Checkout:update')
//       this.setCurrentTest('Checkout:setNextBillNumber')
//       ch.nextBillNumber = 123
//       return this.services.checkout.setNextBillNumber(ch, 4, ['OrderItem'])
//     }).then(res => {
//       this.testResponse(res, 'Checkout:setNextBillNumber')
//       this.setCurrentTest('Checkout:setNextReservationNumber')
//       ch.nextReservationNumber = 1234
//       return this.services.checkout.setNextReservationNumber(ch, 4, ['OrderItem'])
//     }).then(res => {
//       this.testResponse(res, 'Checkout:setNextReservationNumber')
//       this.setCurrentTest('Checkout:delete')
//       return this.services.checkout.delete(ch)
//     }).then(res => {
//       this.testResponse(res === null, 'Checkout:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testConsent(): Promise<any> {
//     this.logTestStart('Consent')
//     this.setCurrentTest('Customer:getList')
//     let testCustomerId
//     let consentId
//     return this.services.customer.getList({}).then(res => {
//       this.testResponse(res, 'Customer:getList')
//       this.setCurrentTest('Consent:create')
//       testCustomerId = res.items[0].id
//       const c = new Consent({name: `test${this.getRndStr()}`, body: 'bar barrr'})
//       return this.services.consent.create(c)
//     }).then(res => {
//       this.testResponse(res, 'Consent:create')
//       this.setCurrentTest('Consent:update')
//       consentId = res.id
//       return this.services.consent.update({id: consentId, body: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'Consent:update')
//       this.setCurrentTest('CustomerConsent:create')
//       const cc = new CustomerConsent({customer: {id: testCustomerId}, consent: {id: consentId}})
//       return this.services.consent.createCustomerConsent(cc)
//     }).then(res => {
//       this.testResponse(res, 'CustomerConsent:create')
//       this.setCurrentTest('CustomerConsent:delete')
//       return this.services.consent.deleteCustomerConsent({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'CustomerConsent:delete')
//       this.setCurrentTest('Consent:delete')
//       return this.services.consent.delete({id: consentId})
//     }).then(res => {
//       this.testResponse(res === null, 'Consent:delete')
//       this.setCurrentTest('Consent:getList')
//       return this.services.consent.getList({})
//     }).then(res => {
//       this.testResponse(res, 'Consent:getList')
//       this.setCurrentTest('Consent:getById')
//       return this.services.consent.getById(res.items[0].id)
//     }).then(res => {
//       this.testResponse(res, 'Consent:getById')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testCustomer(): Promise<any> {
//     this.logTestStart('Customer')
//     this.setCurrentTest('Customer:getList')
//     let testCustomerId
//     return this.services.customer.getList({}).then(res => {
//       this.testResponse(res, 'Customer:getList')
//       this.setCurrentTest('Customer:getById')
//       testCustomerId = res.items[0].id
//       return this.services.customer.getById(testCustomerId)
//     }).then(res => {
//       this.testResponse(testCustomerId === res.id, 'Customer:getById')
//       this.setCurrentTest('Customer:create')
//       const c = new Customer({firstName: 'test fooo', lastName: 'bar', email: `foo${this.getRndStr()}@bar.baz`})
//       return this.services.customer.create(c)
//     }).then(res => {
//       this.testResponse(res, 'Customer:create')
//       this.setCurrentTest('Customer:update')
//       return this.services.customer.update({id: res.id, lastName: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'Customer:update')
//       this.setCurrentTest('Customer:delete')
//       return this.services.customer.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'Customer:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testDepot(): Promise<any> {
//     this.logTestStart('Depot')
//     this.setCurrentTest('Depot:getList')
//     let testDepotId
//     return this.services.depot.getList({}).then(res => {
//       this.testResponse(res, 'Depot:getList')
//       this.setCurrentTest('Depot:getById')
//       testDepotId = res.items[0].id
//       return this.services.depot.getById(testDepotId)
//     }).then(res => {
//       this.testResponse(testDepotId === res.id, 'Depot:getById')
//       this.setCurrentTest('Depot:create')
//       const d = new Depot({name: 'test fooo'})
//       return this.services.depot.create(d)
//     }).then(res => {
//       this.testResponse(res, 'Depot:create')
//       this.setCurrentTest('Depot:update')
//       return this.services.depot.update({id: res.id, name: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'Depot:update')
//       this.setCurrentTest('Depot:delete')
//       return this.services.depot.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'Depot:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testDownload(): Promise<any> {
//     this.logTestStart('Download')
//     this.setCurrentTest('Download')
//     // styledConsoleLog(`<span style="background-color: #0045b0;color: #fff;font-weight: 600;font-size: 1.05rem;">█████ TESTING Download █████</span>`)
//     // const pdfUri = 'https://server1.depoto.cz/export/purchase/147' // prod
//     // const xlsUri = 'https://server1.depoto.cz/export/purchase/146'
//     const pdfUri = 'https://server1.depoto.cz.tomatomstage.cz/export/purchase/106' // stage, ok
//     const xlsUri = 'https://server1.depoto.cz.tomatomstage.cz/export/inventory/23'
//     return new Promise(resolve => {
//       this.services.download.downloadDocumentAndShowInNewWindow(pdfUri)
//       this.services.download.downloadDocumentAndShowInNewWindow(xlsUri)
//     })
//   }
//
//   testEETCert(): Promise<any> {
//     this.logTestStart('EETCert')
//     this.setCurrentTest('EETCert:getList')
//     let testEETCertId
//     return this.services.eetCert.getList({}).then(res => {
//       this.testResponse(res, 'EETCert:getList')
//       this.setCurrentTest('EETCert:getById')
//       testEETCertId = res.items[0].id
//       return this.services.eetCert.getById(testEETCertId)
//     }).then(res => {
//       this.testResponse(testEETCertId === res.id, 'EETCert:getById')
//       //   const c = new EETCert({name: 'test fooo'}) // todo test cert valid pk12 + pass
//       //   return this.services.eetCert.create(c)
//       // }).then(res => {
//       //   this.testResponse(res, 'EETCert:create')
//       //   return this.services.eetCert.update({id: res.id, name: 'updated', ...res})
//       // }).then(res => {
//       //   this.testResponse(res, 'EETCert:update')
//       //   return this.services.eetCert.delete({id: res.id})
//       // }).then(res => {
//       //   this.testResponse(res === null, 'EETCert:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testEETReceipt(): Promise<any> {
//     this.logTestStart('EETReceipt')
//     this.setCurrentTest('EETReceipt:getList')
//     // let testEETReceiptId
//     return this.services.eetReceipt.getList({}).then(res => {
//       this.testResponse(res, 'EETReceipt:getList')
//       // testEETReceiptId = res.items[0].id
//       // return this.services.eetReceipt.getById(testEETReceiptId)
//       // }).then(res => {
//       //   this.testResponse(testEETReceiptId === res.id, 'EETReceipt:getById')
//       //   const r = new EETReceipt({name: 'test fooo'}) // todo test valid checkoutEetId
//       //   return this.services.eetReceipt.create(r)
//       // }).then(res => {
//       //   this.testResponse(res, 'EETReceipt:create')
//       //   return this.services.eetReceipt.update({id: res.id, name: 'updated', ...res})
//       // }).then(res => {
//       //   this.testResponse(res, 'EETReceipt:update')
//       //   return this.services.eetReceipt.delete({id: res.id})
//       // }).then(res => {
//       //   this.testResponse(res === null, 'EETReceipt:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testEETShop(): Promise<any> {
//     this.logTestStart('EETShop')
//     this.setCurrentTest('EETShop:getList')
//     let testEETShopId
//     return this.services.eetShop.getList({}).then(res => {
//       this.testResponse(res, 'EETShop:getList')
//       this.setCurrentTest('EETShop:getById')
//       testEETShopId = res.items[0].id
//       return this.services.eetShop.getById(testEETShopId)
//     }).then(res => {
//       this.testResponse(testEETShopId === res.id, 'EETShop:getById')
//       this.setCurrentTest('EETShop:create')
//       const s = new EETShop({name: 'test fooo', eetId: 999})
//       return this.services.eetShop.create(s)
//     }).then(res => {
//       this.testResponse(res, 'EETShop:create')
//       this.setCurrentTest('EETShop:update')
//       return this.services.eetShop.update({id: res.id, name: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'EETShop:update')
//       this.setCurrentTest('EETShop:delete')
//       return this.services.eetShop.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'EETShop:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testFile(): Promise<any> {
//     this.logTestStart('File')
//     this.setCurrentTest('File:getList')
//     let testFileId
//     return this.services.file.getList({}).then(res => {
//       this.testResponse(res, 'File:getList')
//       this.setCurrentTest('File:getById')
//       testFileId = res.items[0].id
//       return this.services.file.getById(testFileId)
//     }).then(res => {
//       this.testResponse(testFileId === res.id, 'File:getById')
//       this.setCurrentTest('File:create')
//       const base64 = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCAJZAZEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDx1VqVVoVakVa+VZ74gFPC0oWnAUhjcU5BzS4pRwaa3BluA1ehasyNsVbievRoSOWojVharaEEVkJNjvU6XPvXowkjjnFmgeKTcKpm5B70gm961uZ2LvmAVFJJnvVcy570m7NMkc7ZpoJpDSgUrFXHipY+tRqpqVKpRE2WEGRSOtNV8U8yKRzVcpNyI8GlGTSOy560+JlqbDuSRR56050FKJOOKQtVcorkewelJsHpT91KtTyjuM8sHtQIx6VLSrT5RXK7xkUyrjAEVXlT0pNDTGA05eTTFBzU8a0rDuIV4qCVeKttjFV5qicblwlZlCZapzJ7VoSjNVpENefVpXOyEygU5pNpFWWT2ppWuCdJo6ozIRS4pWXHSgVg1YtMMUmKdRipGMIppFS4ppFAEJWmMtTkUxhTQFdlqJlqywqJhVIkrEU3GKmYUwitqZEhv4UUtFdNzIcoqRRQoqQCuBm4gFOApQKdikMbikIp5ppqkhADinq+KiNJmt6crGclctCU0olPrVTeaPMrsjVMHAvJKSetTK5NZ8T1ZiJNdNOpcxlEuoamTmq8Qzir0CZ610xdznloEcZNTJEKeMDoKTNa2IHCMUjIRSq1SZBFUhMrNxULyEVYmFUpetJgh4fNSRuc1WFSK1SMvI3FO3VXjapN1VcQ/dTwahzQHoAsg0uagVqfv4piHM2Khd6HeoGakxok34NSrLgVRZ6VXpIZdMmajc5qMNS5pNXGhrCo3SpjTTWMoXNYSsVWSo2jq6VqJ1rkqUjqhMpMlRMMGrki1BItedVp2OqEiIUUUtcxoJikIp1IelKwDSKYwqQ000AQsKidasMKicVSEV3FRNU7ioWrWLsQxmKKKK05jOxZUVIopFFPArnZsAFLRRSASmmnmkNUIjIphqQikIqkSyI0lPIppFaJkskiq5BVSIVdhFdlGRhNFy3XkVoQjiqVv2q9EelejTZxzQ8ikxTyKTFamY0U9TSYoPAqkxMjmPFUpTzViZutVHaobGkJmnK1Qs1CvzU3HYvxHipc1UharI5FWmIUnNJmigCgBytzTmPFNFEnC1VxMikeoHl96SZ+arO9ZtlJExkpY35qqHqSJuaaYWLytTt1QoeKeDVCJAaetRLUi1DLQ/FNdakXpTWrKa0NIMqyLVaQVckqtLXn1onbTZWYc0lObrRXmtanShKSnUVIDDTSKeaQigCM1GwqVqjaqSEyBxULirLioXFaIlsgxRT8UVRJZApwpop4rIsKKXFGKQCUhFOxSGmhDCKQipMUhFUIjIphFTEU0imhBFVuHtVRRg1ZhNdFKVjKaNGA8CrcbVQharSNXo05nLOJcVh3pdy1WDU7dXQpGDiSs4FRO+RTS1MY0+YXKMkNVZG5qaVqqTPis5TsaRiNkemo/NV5JMmljfmslU1L5NDShersTAisqJ6tRS4710RkYyjY0NtG2q6T+9SeeMVdySXGOtQTuKZJcZqpNNnvSchpXGzydaqu/NJNJnvUBfmsJTNlAsK1TxGqaNViI1UJEyiXozUyiq0ZqyjVsmZj1FSKKapFSLikNDxwKY5oLVG7VnNmkEMkNVZTU7mq0hrzqzO2miNqQUtLivPe50oSjFLigioGNNManmkNAiM0xqkNRtVIRE1RsKmYVE9WiGR4op2KKoRItOFNFOFZtFjhRQKWkAlGKWimIbijFOxRimIaRSYp+KTFAiPFORsGlIpCKadgsWopKtRyVmKxFTxy461006tjGUDSDil31SWYetO84V1RrGLgWi/vUbyVAZh61C8w9ap1hchLI/vVK4k7A0ks2elV2JJrCdW5cYDdxzUiGo8UqnFRGepbiWUfFTpLVJWp4euiNUycS8JqUzVSElBkrT2xHsyy83vUEktRM9MZqmVW5SgK75qMtQTTawczRRJEbmrUTVTWpUbFaQqEyjc0YnqzG4rNjkqwkldMapk4Ggr07fVJZKkElU6iBUyyXqNnqEyUxpK56lU2hAkd6hY5pC2aBXBVnc6oKwopaAKWudmolFLQalgNNNNONNNCAY1RtUhpjVSJI2qNqlao2q0SyPFFOxRVEiinimCnisyhwpaQUtIYYpaKKYgooooASilooEJSEU6kNIBhFNqQ00iqTEN3EUvmN605YXboDViKwkc/dNaxUnsQ7FMyN6007j61twaNI38JrQt/D7tjKn8q6I4epIzdSKOUELn+E1ItrIf4a7i38OE4ylX4fDn+xW8cDJ7mbxEUedrYyn+E08adKf4TXpkXhwf3P0qwnhwf88/0rZZezN4lHlo0yY/wml/sub+6a9XXw5/0z/Snf8I5/wBM/wBKtYAn6yeTHTJvQ006dN6GvWj4c/6Z/pUbeG/+mf6U/qDD6yjyZrCcdjUTWcw/hNesyeGx/wA8/wBKrTeGh/c/SoeBZSxCPLDbSDqpqMxsOqmvSpvDX+x+lUp/DZGfk/SsJYOaNFXizgdpFKK6y58PMP4KzbjRpEz8prF0ZxNFOLMlTUiv71JLZSp2NRGNl6g1HNKJXLcmWSniSqy5p4pOqxqBNvoDE0xaeorOU2zRRHingU1aeBWLNEKBS0ClqWMSkpaSpGIaaacaQ0CIzTGqQ1GapCI2pjVI1RtVIhjKKX8qKokUU4UwU8VJQ4UopBSiiwC0tJRQAtFFJSsAtFFCgk4oAKTaT0FWoLZpCBitaw0d5CMrWtOhKexEpqO5ixWzyHgGtOz0eSQjKmus0zQOhKV0thoiqB8lerQy7ucdTFpbHFWPh8nGUrcsvDwAGU/Su1tNIAA+WtO20tRj5a9OnhIxOKeIbOPtdCQY/d/pWlb6KB/B+ldlbaWv92r8WlqP4a3UIRM+aTOMh0f/AGKtxaNn+Cuyj05f7tWI7AA/dp88ULlbORh0Uf3Ktx6KoH3a62OzUdqmFovoKzdVFqByQ0hf7v6Uh0hf7tdd9mWo3gUVPtUPkZyo0dc/d/Sl/sdf7tdOsKk9KlFsD2puqgUGce+ir/cFVptGH9yu4a1HoKhltB/doVRA4s4GXRVP8FVZdCU/wV6CbIE/dpPsCn+Gm5REkzzC58PjB+T9Kxr3w8vP7uvYptNQj7orKvdLU5+Wo5YyKu0eLX3hwHPyfpWHfeHmXOE/SvbLrSRk/Jmsq70VWB+Ss54SMi4YiSPDrrR5IyflNUJLV0PIr2PUNBBz8n6VzupeH+uE/SvPq4DsdcMUnued7SO1OUVv3+jSRk4U1ky2zxnkGvOqUZQ3OyE1LYiFPFNxjrThXOzVDhRRRUDCkpaQ0gENManmmmgGMNRtUjVGapEjGqNqkao2qkSxtFGKKYgFPFMFOFADhThTRS0WC46ikozQAtFJUkURc9KEmwuIiFjgVpWFg8rDirWlaa0rD5eK7TRNGGF+Su/DYNyd2c9Wuo7GZpGi52kpXW6ZowAHyfpWvpmlBQPlroLSyVccV7tHDRijy6tdtmTZaYFA+Wte2sFGOK0IrcADirMcYFdWiMNWVorUDtVmGAbhxUo2LSGdVPapdRIagzQtYBxxV9IlA6Vn2dwGAxzV0SnHpXDUq6nVCnoTbQPSkyuetQFyeppu8CsHUl0NVBF1CtPLADpVH7SFqOW8bsalykUoovliewprAH0rJe/YfxVE9+2ODStNlqKNhQA3arKAY7Vza6g2/qTV2G/LAdadpofs0bLbQKrzYqm15x1qrcX4HWhuaF7NGkjDvViMI3pXPJqa7sZFXrW8Vu+KFWkt0Q6S6GrJAhHArOu7Yc1ZFyccHNVbi7XvWsKyM5U2ZktrljxUD2SsMba0FnjL9atJGjjjFdSq6GHIctd6WuOlYl7pStn5K7u+hwOBVD7Jv421akmiWmmeZ6jogIJ2Z/CuV1fQhyVT9K9tvtNURk7a5fUdMVmI21lKlGojSNRwPEL/AEx4iflrNeNkOCK9Z1fRQQx2/pXGavpBQkha8nE4G2sT0KOKT0Zy9GanuLdo2PFV68mUXF2Z3Jpi5pKKSoKFNMNOppoQhpphpxphqkSxjVG1SGo2qkSxKKKKZI0U4UwU4UAPFLTR0paYDs0ZpuafGu40JXAkgjLn1rd0rTy7DIqvpNuCwJFdtodiHKkLXpYbDX1Zy1atizoWlD5TtrttJ08KB8tRaPZhVGRXQQqkSjoK92lTUFqeXUqOTsie2tVUDirOETmoFnyuFqCSX1OaUq6WiBUu5ea4RRgVH9pPb9aomUDpUbT4PWsnOUjRRSLslwx/i/KiIlmBNUDOD3qe3nz3rSFCUyZVFE6WwQYBzV5nRFrE0+4AXDNU1zfxKv3hQ8JJu1gVdWLNxdKp4qq97WTdX4Zjg1Rmv1UfereOA7kPErobsl9jvVd7/PG6uVvdYRM/NVO31kSybVareEjFahGtKR2X2jPenCTPU1gWt4W71dW4461g6UVsdUJtGosqA1YiuUFYfnj1o+1AfxUvZI09vY35LlexqlczAg81mG8GPvVWuL5Qp5rJ0SXUbLjSgHrVi2vCpHzVy02onecAkfWqa67GkpBfHNXHDKRk6jiekR352/eqC4vdw5Ncjba3G4ADirP24SDhqh4IPrBspdEzfK1bVnesqgGuPt5vnzmt+ykBi3E1jPDyhsaQqxluac98jMATV7T9koyK5tx5r5FbGms0MWc1HPKK1HyxlsWdUQBSBWJJaeY2AvJqxd6iHm2seBV6x2eS074xjit6dVWMp03c5nVNNRY8beTXIaxpIYMQlekSxm5lO3nNUtU06NITkVo2miFdM8L1vSdpYha5W8tmjY8V7LrumbgxC159rmnlGb5a8vF4VSV0d2HrtaM5Ckqe6hMbniq5rw5wcXZnpJ3QuaaaM00mpQ2I1NalammqRLGmo2p5qNqtEMTiikopiEpQaGFIKAHg0uaZmlzTAcOtXbGLfIOKpxgseK1tPjZSCBXTh4Xd2ZVJWR0Ol24IUYruNAt/LVciuS0TBdc8V2dhKqIMGvYptI8+d2dJA4jQEdalE4IyTWRHMz8A8VOr44radRy0RlGKiakc5PSmyS89azxMVFQXF1gdaqnRcmKVRIuyXIGRmq8l1xnNY95qSoCSwFYV7rmCQrV3U8Oc7q3Oua+VTy4qWLU4weGrzmTWHZ/vU4aqyjO6uuCUTGfvHo760qD7/wCtVZteQ/x/rXn11q5MZw3NYF3r8kbkFjWrmQqZ6pLriH+P9apXWtKVOGry7/hIWJ+/UkWsPKcbqlzLUDr9Q1MyEgNVnQpgr7m5+prlIZ9/LGtOzuccjoBWFSV1Y3oqzO9t9QjHAwKkl1aOPqwrjLe8JPBpbiVmGcmublR2qN0dNPr6LnDVRl8RNuwDXLys+epqNd2c1qkkZSg7nVjXJW/iqSPUWkHLVzERarccpUcmpka0423NmS4JRyGHSuF1nUJ7e9YA8Z4rdnuyobB7Vy+rZmkJIBq6UUjCvJI0tK1uQsAWNdZpmrFgMtXmkGY3rVtNRaEgE4reyOTmPU7XUVJGTW7Z6mhQLu4ryO31sAj561rPXgCPnqXBPcalY9ds7mNyAGHNaslwiwHaw6YFeV6d4hAYfPW7BrYkUDfmuathoy2NYVHE3TiS468Zq9dXRCpAhrDtb2NRv3c1Zt5xJJ5jGvLq4dxd0dkKt1ZnS6a6QQb5Dyar3EovJSB90dKyp7ppMIrcVNDKQnlx/ebqaxjUlB6mrhGS0K+q2iyKY41z6muF8R6VjdxzXo8rpDHtJBc1z2tRRujE4LGuuDU0czvBniut2ZjduK5+VdrEV6B4ptwpOBXD38W1ia8fG0Fe6PSw1S61KRNITSsMUwmvKsdghNNY0pNMNUkSxGppp1JirSJY2inYop2JBxUR61O1QPTaBBmlXk1HmpoBk0kruw2aFhCGYV1mj6asmM1zGm/6wV2uiuybSK9ihBKJwVZ3ZtWuilVDLVmOOSJtpqzZXg2gE1MNrvnrXQ7dDC76k9rkICepqzuAWokChcioZ5doNdFKFzGcrC3NwFHWsbU7/wApCc0l7c/PjNc7rdwzgjNerTgkjkk22VdV1ZmJ+asM3xZzlqh1BmOcGs+3jcyZJNaN6CSNU3DE5BpGvhkKWpkcLMMVmala3CzhkzWUHqU1oa7TbhwaztStjKhI61JYJMygEVe8lwPmFW7ohaHJrBKJdpzWpaIYwK0J7ZeoXmq7KUHIouMlW4K8ZqympbU2g1gXdwwOBmn225hk1DRpCXKddYXucEmtiCZXXrXEw3BiHJrW0/UQQAWrNo7KdQ6Uxq1IIBVKG+UjrU32xcdRU3NuZMnKhRVS5m2g80ya8GOtZd/dgKeaREppIW6vsEjNUZLje1Zb3ZlnODxVhSdua3SscFSXMyWWUCTimXJLR5XrWe9xi4wa0IWDr7VojJ6GYb6SJyrEgirEGquD9403U7AuN6DmoNM06R5AXBqkFzqdGvppGBJOK6iz1BlwN1cvYwiFABVo3SwtlmodiVLXQ7OHVWXGWrUttbyAN2K88TUA54binzaqIlB34xWM4J7msKjTPXbC9WUAhs1pi8SFC5IzXkOj+JwCERskda2T4kEq4V8t9a8ytQTdkdtObSudbquuKisc5k7Csq01Y3mSzc965qaaSWTzGYmkSVreUOn3WrOMXS0Kk1Mu+JEVlJ61wWqx4JrvJklvYflBNc9q2izBCzDFclSEps6KcoxWpxUhxURNXb21eOQjtVFwRXjV6bhI9CnJSQhNNNBpVFQimAFLinAUoFaJGbGYoqTbRTsK5FJVeSppDUD0MaGZqVHCioc81WvZ/LTrVUVeZM3ZHS6HcKZgCARmu80gxEAjivHdLv3Vsg11+h63Ko5Ne1HQ86Wp6OZ0U7QvPrV+1fCg5ribLWDLJgmt2HUgFX3rVWM5HSrcALyapXtygUncKofb0ZfvYrF1e+2qSGrtoWMJxbLcs4ec88Cue8QXAU4zSWt8xdjurn/Et63mYzXapXZzuNglmDHrSW7jzKxBeNirdjOzmm9iUdPAy7aY+13wRms+OZ1HNVW1MpcY5qI7lPU6O3tmA3KtWDGSOVrOstaj8oBjzV1NRikHBpuQRiRSxYP3aqTQlhjZWkbiI85FILq3z1WpUi3EwpLAE5KVPBZLjAWtj7Rbt/dqaHy2+6uabkSomJNYKUPFQW9i6uAM9a6d44yvIAqNUgVg3HFCkrDtJbGbJA0TsATgHiozJIO5rSm2OTyOarPEvao0NVKRSdpWHBx71Va3kkfBJIPWtZYVPtViO2T1FNWIk5HP/wBiyKfNjyR6VOljKy4xXSwRgcZFWVtI2GRgGrbM7Hnd7pFz5+5Qas2NpcrwwNd3/Zwb0NRvpu3kAVSaJuc/b2r8bxVkWwU/KuK02t3TsKhdZAeFp3E43My+lNvGTjmuZuLu5nucAMFzXW3NnPP95OKq/wBmNG2TF+lFxpWM6B5VjHBrM124uVj+UNXWJbps+7VW8tYpBtZQayqMuDSdzlvCGozx6zGkqMUk+Vs+ldxPbyWV9hcmNvmU+1Zum6ZAlwkgQZB4rsb63EunRy7clP5VwSTTOuMkw00+dGM1pW9ssgMbDpyKx9NmETYrSF6InDDvUVU2VE6bSUhih2kDIrK8SNFsbp7VmrqzrMy54rN1fVSASzCue7tYuyvc5vXFPmHAxmsWaF9ucVp3+pLPJhmWolZXj7EV5+MhfU7MPO2hjHOcU9RTpwBIcUIK85I62xQKcBShadjFaJEhiim5oqhFJjUTmlZqYxyM1EhjGOKx9XnwMZrVlOFNc1rkuH69a6MLG8zKs7RJ7GbjOa3bC4ZUyCa5K2l2rWtp9ycYr1UjgOv0q9cPk5rorW/Luq56CuP0tiQOK2bZ9rE0XsFrnUxXRZTzWJrl2d20Gn2s42NzWHrNxm4PNb0ZEyiaFhN8rHNYmuuWmq3pspKmm3MKyy816FNnHUMRRxWxoEXmS4I6DNKLBSOlauhQJDIxPpW8tjKJYFpmIkCsltOLzE4711MTxCMg1DEsLOSKyjoaOJhjSWCA4qzBYsoxWjfOEjG31qtbSSzSBIkLMfSm2NJEU1tIqHBNVI9OupD5jy+VFn77nj8PWuhaMRJgkSy/+Oj/ABqs9rNczfvZGY/3R2/wpJlNFS1FrAwEYe4f+8/T8BWmkkrjLDaPTpTo7NLdRwM+1DyxoMcE1nOQ0iC8lkVPlJrOM82eXwKXWL0oh2YJrm57meU5ZzWV2aKx0kFxl/mlH51eWRccMD+NcfZbjJyTWvEGOApOTVK/cTZoy3flvgNUsV7nFYF95iycMeOpplvfbWxkmmpNBZHY20yyYw20+h6VbeZ4IXY/wjNYelTxygA8VrOG8kr/AKyMjlTWsZGUlYonxMqMVPalPieM96yrzTImkaSAkjup6iqw08ZFXcmx0i6wky9aH1GNF3E1l21lgdKL20JixzRcVtTXttagdgu4Vfa4jkiJGDXJadpTGTfzW+ieTbnJpoJKxnX16I5yB0rJv9ZiiPzGn6m26ZiK43xCxMmM96zqSsgppNnbaZrMUm0gjGa7uynE2luoIIKmvGdAU7Eyec5r1rw8caft9v6Vw892zq5UkZy3GJ8ZqzeXOIQe4rOmG26YD+8abeSHycU27xH1GatqLRKrqcE1y+qaw753MauazPhVXPQVyuoSKxOTXMzS5DcaoRLwx6+tbek6i8kW3NchdYD5Fb3hwhgQPSuXEq8Deg/eNskscmpUqMCnqa8mJ3smWkkOBSBqimfjrWqRIm+iq+8UU7CuQMM01uBinseKic1myiG5bCGuR1yT98BmuovW+Q1x2rvm4rrwa1OfEPQkhPyCtfTBnFY0B/dLWvprgYr0kcTOt0kDaK0gcA1kaXJ8orQEmQaUhxLUUhCnmsXUZN0zGtND8prEvW+dj71pR6DmaGltheaublL5JrO08/uqbNIwJwa9KmcNQ2lkTHWrFrIFbPWueSV8da0rBmZetbyWhlE1ZrlQnSmWM6sehqrcfLGTU/heyudTv47W3XLMeSeijuT7VmizYttPl1FwkZCovLyHog9TU7i3s4jb2uduPnkPVv8AAe1aepTQW1sthZECCP8A1j95G7sfauVmuTeXAiiz5YOP96h6FJdTRgVpz+7JA/vVe/dW0W0Ebu5qsJFtLbLcYFZ0l2cGaY4B6D0FZSnYtRuW725+X0FZMlwXzgjFZGpa4s05hiPyg8n1pqXRK8VFyrEmqS/Ljiskt7U/VLgAcms8XAPelzDsa1kfm6V0FlGUiEhHzN09hWD4eha6nzz5acuf6V0N5crbQl2wGPCimmJoz9WdC/kqfdj/AEqisaA5zVa4uwJSWbk0iXik/eFWrEu50mlunAJ5rctrgx/K/T3rldLmDSJ05Irpr6Pbbs3Q09hb7k8sCTjzIjhx0xVOe0dyWhGJF+8n973FV7O8kgkG8fIe9bmFmRZY/vdRVp3JasZEMjjg8GrBV5SFFS3lusgNwi/OPvgd/eptLg3Sbz0pk7FiytdqYNZ2tTEZVDWxqE629u75xgVyrzGeUsTVohvqUJ0bBzXKa3Az3Ax612t1gIa5m9w1zjFYVnoaUtxdEiKBCema9Q0hglmpzwVFefaZGGjwB3rtdNkYWaA/3a447s6XsVrkj7W/1qpdNu4qWdj57Gqd0+2JnNEtENbnM6/cYmIz0rlr6Ukk7q1talLzNisC5VjmsCitNPk8mug8KS5lIz2rlbhWDcV0PhKOTzCxHasK69xm1H4jrS1OU1ASQBTlavHieiWM/LVadqkZvlqrM1bRIY3dRUG+iqsSSseKic0rGoXaudmpVv2+Q1x2pNm5PNdXqLYjNcdenN0frXfglozkxLLsRHkrV+zcDFZanCKKuWpPFd8djkZ1OmTfL1rWt33RMfSud01vlroNNG6zkPuKib0LgtS3Gf3RPtWJd9/rW2g/cN9Kxb0Y4963oImoXbDPk/hTJEfrtPWrGlqDFzWxawRuoBAr06SOCozARW7g1racMKOK0jYw4+6KljtkUYAraS0MotGfdAmM12ejW6+HvD6xsu3UL1d8rd0jPRf6msnQ7KKfU4zMAYYj5jg9CB2/E1o6vfGWWS5lILM2F+tc/NY6FG5ja7cYiaNG6n5yO/tUWgQbYjdSDj+GlmtxPhOpJrRmRIoFgXhUXmolM05ehnzSG4mLuf3a9B61yvinVssbaFuf4iO1dBrEwhs2Ef324UVxU+myu5diSSck1g22aeRTtXzKK24W+QVmwabKsmea0kgdVxSbYWRla1J8wFVLNJJpUjQFmY4AHerup2ksko2810vgrRDbIdUul56QKR37t+FOKbdhPQ0bGzj0uxW3Lc43TN2z6fhXPapfG6maQcIOFHtWp4hmk8oW0ZOWOXP9K594pPLOAacn0QJGPeTEzHmmxzNuHJpt1DN5p+Q0xI5QwyhoTYWOs8PMXnhBP8YrtfFEhg0eSReCGH864bwyGF5bggj5x/Ouv8asf7Clxk/OBWqZm0U9DuYdRtDG2N9auiXDRTNazH5l6Z7ivO9A1B7K/ViSEY813l0DKsN/b8sPvY7iqi7MJK6NsH97wenP1p5YQn5OB1qOPAgEjDtmoTJ55eBSNxUlPrV8xla5h+LdVZI1hQ8seazdPuHZMms7WHeW/JbPy8fjV3Tl/d1tHYyk9SxcyttJNc3dXR+1EVvXnEZrlp+bpjXLWehtSOk8O3BbjrzXa2YIt1+lcL4WXJ/4FXoCYWBR/s1zwWpvIy7kfvTWZrEgjtSPatO55lNc94ll2xEZqJlI5S7YPK31qlMgNFxNtYmqcl1njNYtFD/JRmHFdX4TtkKOSPbNcSLza9dh4XvVFk5zySMVhX0gzWl8Rp3yiOUqO1QBqWeXzH3VDnmvGT1PT6E5b5arTNxUhPFQSmt4mbIc0UuDRV2IHs3FQOacTUTniuVm6M/U2+Q1yUx3XLH3rptUfCn6VywOZifevSwa904cRuWgelXbU9KzlOTWhZ5zXcjlZuaecJXT6T/yDZD/ALQrmbIfIK6fTRjSWPq9ZVDWG5chGbZvpWLqC8j61u2v/Hq/0rE1DmUCuvDozqvUs2UnlxZNadrfRqoyayAMWxqvk8c16NM4Kh1i6hER1qZLpcZBrlYieOa17IFgBnrWs3ZEQV2dfpY26Y0vQzN+grndY1FTqTQK3yxcfj3rpbyRbLT1B+7DDub8BmvKDcSy3ZcklmYk/jXm1qmp3043R6HozecRJ1xTtYuFhi+Y4LGqvhklLHc3U1leOZ3EkcaEgbMn86ly90q2pR1C+Es27dx0FQi5QjqK5fV7ySJflasr+1p1/iqVOwnG53omUtwRUplXb15riLLVpnfBNbltLJIm4k89KbncXLY2tKhN9qCw/wAPVj6AV0l5dJCm4cRoNqLVHSLRtOsMScXFwAz/AOyvYf1rK1S/E8xVD+7j4HufWqvyoLXY+eYyuWbqetIAix5IFZ32hjkintdFbfLdMUkwaJwkEjElRThbW+c7RWAdaiSQrkVNFrkJIGRWqmjPkkdZo8MX22HaBw4ra8U7DpjI3eQf1rldA1JJNSt1U9XFbXja6EWlIzHGZR/I0nLQpROd+xQMeAK7fwnETZ+XIMgetcNoc4urgAdK7+yuYrO1HQGrg7ky0NG8VfI8te1UbSMNOrA4ZTT1uRNJx0Iqvp86DVDEWGemK2W5k9Ctr2hxG7eVVADnf+fWq8WnJHHgYrqdb2i0STPTisEyLjO7NbrY523czbuzXyzXOy6WpmYium1CdRGcGsH7Su8/MK5ax0U7mn4asBEcn+9XX3ACxqB/drm/D8gdRhu9b10xCgZ7VjA1dzKuDh2Ncr4lkyCK6e6Pyk1xviF8sRmsJmiOXvScGsmRuTWtfcrWTKAOazGVpTzXVeFmAs2LHr0+tcqwy1dP4eTEC1zYn+Gzeh8R0G75abupjHAxQteLE9PoSM3FRM3NOY8VCx5rpiZyHfjRTN1FaGY1jUMjcGpHNV5T8prkZuZOrt8h+lc9GOSa29Yb5DWKnCmvVwq9w4K794mhGTWnaLjFZcDjNalo4OOa60czNu04QV0tlxpI92rmLZsIK6W1bGlRD1NY1DamadsuLGQ+1YV7/rq6G1AOnSe9YF8P9I/Gu/DrQwq7k6ITa1GbRzgircI/0YGrcGCBmu6GhxVNzOjtpBjitnSIibqFWHVxRhcVc0sA3kR9Goqy0HSWpc8ZzFNHuyv8QCD8SP6ZrgNJtmklBIPJrtPFzFtPKHvKo/Q1iWaCGEyYxgcV5ktWd8dEb2nOgjEK/wAIrK8VReZcof8AZxUGi3m7UXTd2xVjXJMzIfUVoldEN6nmniQNHKYyDwawWJru/FNks2JQOR1rnDZoalwsLmKmlAtMBXpPhKxVmF3MoMUPIU/xHsK5fw/pgluUjUfMx/Ku886G1thDGMRRj/vr3pxjbUV7lTxNqZRCit++l6n0Fc4ZQqbc0msXJmvHmY9eAPQVTjYucUN3GjQjP7nPrS6gQtn+FNHEIFGoLvtce1DQM4m5fM7fWkhb5xzViezcyMfeiKykDg81nysu51fgk7tZtf8Aez+ldN8RA02lW0adWm/oa5zwTGU1q3J6DJ/Su41O1S6S3B5CuT+lbQV1YhuzuY/hTS/s9sJJBg4yanu77zbzy1PyrVrWblbGx8uPG4jFYGnB2ZpW71T00JXvanYaI+8LnqKw7ueW38ZqNxCPgitXQm2QyzE8LiqGvW7PfW94g+6+M+xramzOZ1Wtu7aRJg/dwRXIw3EhTBauqvcvpkqnqY64tCyg5Heujm0Odobqc7LCxzXLi7k8xue9berv+4auYQkueO9clZ6HRSPQPBjtJGpPrXW3g5/CuV8CL+5TNdZqBAYgdMVMNinuYt4cRtXD+IH+c12eouBG1cJr7/MawkaIwbuXrWbPJVi9fnisyd+ayGPDgvXW6Cw+zoOOlcOJMOK67Q5cQoO+K5sU7U2b4dXkb0h+alBqPORmlzXjRPUYrNUJPJp7GoyeDXREykG6iot1Fa2M7iu2agmPymnk1DOflrkZuYessdpFZWDtxWlqrZOKz2OAK9egrQR51b4hI1OeKv2hYEcGqkTYq7bsOK6UYGzbOdg+ldNbt/xLoBXLW3OK6SFsWcArCZvA6Sx508/WsO7Tdc/jW7pYzp351kTLmcn3r0qCtFHNUerJJCY7RfrVf7aUYCrGofLapWLK376uuDOWpubMd/kgGtbS7jN5Dz1YVykTfOK2rCXy5onz0YGsq8tDSkje8SjdZkH+GUf1rnNVult7bYTjjJrqNV2v5oPQgN+RrzbxleHLKDyxxXEzpuM0bVQmqh933mrqddlPkCQcgDcDXl1tIVnDA9DXpGlTDUdH8s8uq4rSntYzm+pXJjvrLcpBYDkVy94hhuCrKQM1MLqfSdUaGTIQnjPpXVaVYWOsH7RKB5MXzSe/tV/FoS7LUTwxZG3sRdyDEk4xGPRfWk1CcPIbeM5C/ePvV/Wr1YYi8a7CRtRB0UVzhl8u2dyfmbmiWmgJmVqUwFyRuqSyAODXP3dy0l8xzxmtjTXbaKxWrLNiQgIoqLUHxDgHtTLhjhapavMUiPNVcGVt3POKkRuawDeSBjzVixupJJgo5pKQrHe+EoS98HPACmutvZktrdWJAxnFc94Ot3IYkEZSm+P71rVIYlPLA/0rZNJEbuxTv75726xkkA1dhGyNVHU1haNufDGuo0O2N1dAkfIKhXZpokaE7my0XaeGfk1Jp0gurCInk4x+IrH8ZXwQ+WDx0AqTwNerPC8JIJjf9DWlOWtiJLQ7G8YLZkf7FcdLglseprotXuwqMvtXHy3YySD3Nbt2RhYh1THlkGsOOJd/frWlqFwDGSTWZDOpcDPeuWpI2gj0Dwf8kEeDitvUZTvcE5rE8K/6iMj0rTvz8zetUnoDWpkak/7s1w2usS5rtdRb92a4rWj85rCRojmrtjurLuGJY1r3I3PzWbOgyazGUMneK7DReIYyTk4rk9n7wfWuq0cYVfpXHjH+7OnDL3joVPyUE8VGh4xQTXkQPSYM3NMZuKR2qKR8LXREzkGaKg3e9FbmJMTUFwflqY1XuTxXF1OlmHqRy9UZetXb7mSqkgy1exS0ijzanxCRjirtsORVWMcVcthyK3WxibNgMsK3UP7qIelYenD5xWzGclBWEtzeK0Oy0hM6UT7VlzR/vvxrc0pQukj121muo88V7FJe6jim9WLNps11EgjXIFUrrw7cxtvZCBXa6FGGRRtFa1/bKbNiR0FWp8uhjKLep5YmlOrg81cFo4AArTuCFnZfQ0qFSQO9RWszSk2XZ4vNtYZCSC6bD+WK8p8SWdw966tnKHBHvXq5fEXk57ZWuN8WxrHeC4A+ScZ/4F3rnUUaybODisJt2SK6vwnJJBMI26GqsTKx4Aq5bgROJF4IrRJGfMbnijw2NVs1ngUecOQRUllaLpmmx2GPuDdM395v/rVteGbxmtUllAAY/KD/ADqfxBpv2qBmg6vy2O9aqK3M+a2jOVlVNQJKdOig1karp1ysDeWpOO1X3SWzuPLIKkVet79eBKARUSSe5aZ5XLDKt2wZGBz3FbWmqwUcGu6udI0y/wD3ioAx9Kp/2GIWwhyB2qFTsVzGFOCdnFZevEhDXW3FntYArWXq1hHIpBAzScLhzI4DktgV13g/RHuJFldeKl0nw0txcqQuRmvStF0qKxth8oGBVQo21YpVFbQi02BLQ7AOdma474g7ptYtk/uxZx+JrtIXD3s7A8KmP1rH1fR2v9XjkAz+7AP5mnKN9AjoZGiWjMgVV612FukemaezsQrMPyFWdM0mGziDuBwO9c34muZLu5aCJjsHBxRJcsQT5mc1r12bu7eTPy9BWn8NSRf3DE/Lt/XNZtxp7ngV1Xg7SJLS0DnhpW6+1Z0k73Km9LFjX5SssgJ6LmuR8zIzXU+JIHO9wTgrj8q5n7LIqitZXMzM1SQiI4NZtm5Mq8961dTtJGjOKpWNlN568d6xabNInqHhIYt4h/sitDUTy3OareGoCkUYbrgd6n1IEE/WtLWRP2jB1RsRmuL1Vsua63VmwhrjdTb94a55GiMa565rOmPJq/cck1RmA5qGMqrzIPrXVaUMItcso/ej610+lH5VrhxvwHXhV7xrhsU4txUZ6Um7ivKgeiwc81DMeOKexqKWuiJlIgyaKdhfWitjEsnpVW7NWTVO8PWuRbnS9jHuzmSqjn5qsz8y1XcfNXsQ2R5s92OQ8Cr1p1qko6VetV6Vt0Meps6fjeK2LcZljFY9h98fStqw5njHtWG8kdEdjurYbNKT/drOcfvQa0wMWKr6KKznB317VNe6jgludP4d5C10GooDp79eB2rmPDzOpHIxXR6hI39nycZO3tUyhcjmseUajdyJq0iZOM1c093kmGfasjUyx1iQlSOa2dCGWzjvWMm7m5W8UasdNuoH7Kw3fTvUGvKNQsXgjILOPNgbtu9Px6VhfEmYm6CZ71F4S1Q3FqbGR8TQDdEfUelQnaQ3sY9hdSeYVYEMDgg9q6jQLOfUrgR8iMcu3oKh1HR2ubsX9rGN0j7Zo17N/e+hrqrOKLT9P+zxn5sZlYHqfSt4xMZPUqatqsViPlXYiDbGvtTtC8WKR5c7cH1ri/EF4bq+YAny0OBWNNcOjjyyRiodRplOKe57Rdx2WqQbk2l8cEda5PVbC5t92FLKOhFZ3hLVLpGVWJOa76Fo7mH96lap8yM7OLPNLfWri1nKEnAPQ10Fj4himAWUfnzWzf8AhjT7sltgBPess+DljfdFIQPep5ZRehfNFln7TbTchgPxqtdW6TdGU/Wnt4euFPyuKr3mh6ps/cy4NVqTobOgwxW7Auo+orfvpIpLbbGw6VxmnWWrWoBnfcK04bpt2G4p89hci3Rb0uAo03mH7xGK1N9tbAu2N2Kz7eRT8waoNWiknRlRsGlcdr7lTxB4gwjJG4yeM1gWlwHJYtkmsLxRbahaS5IYpnrVXTLqZwF53HtWE2+prFLodtYwfbLtY16dSfQV0tzcJbxRxJwSQq/SsPRImsbMPKf3kgy3sKxzrD3eovKufLU7U+nrVqVrIhq51XiFwtorerEVh5VkzUnie7caPFIM8yjP4g1hJduV603O5PJYnvWUHGKbYlPOXjvWXql4UHWm6Xe7rhBnvWXNqaJHqemNhBj0pt82Qc1Bo8mUHfin3hJBrVvQhLU53Wmwhrjr/mQ112ufcNcdfNhzXPI1RlXAwTVCY9avXR61mzHOazYyKI5mH1rqNK6CuXt/9ePrXUaX90V5+PfunbhFqahPFMJoJpK8yB6DGsajkanvUEh5rojuYyDcKKjyfSit7GJcPSqF4etXWPFULw9a44/EdUtjIkOZagc4apnP70moXGWr2YrQ8uT1FVyMVoWbk4qgigmtCzUAitOhl1NiyODn2rd0kbrqMewrn7c4NdHoQ3X6D0ArKOs0dH2TvMfuMegFUmT95V6Nh5T/AJVTlOHzXtQ+E86RpaTL5bDIrZurjNm+Djiubs5gDVy/vVWyb6VE52BQucnqCo987cZzV3Sx5aFgPWslrhZLlznvWtYuDbn6Vzt3ZvY4fxsBPqBPpXOwRywXKTQsVdTkEVu+JJAdRfJp3hjS21O/GeII/mkb0AqUruwnojo9NmlWGC9MflmRfmU9D/8AWqzqjl7bdD/q36+q+xqrfz/6UZMbUUBETsF7Crmmq0qggYQjBU1vHsZtdTkptOaSRiqkk1b0zwm9w4eY7V9K64WNtAS45HXb3qld6ukeUQhAOwpcqW4Xb2J7DSLKwAwoyPWrU+pRQqQuOK52XU3lyFYgetZWoynYSXJ/GhytsHLfc6KfxGFJCuoqKPxK5fHmVxBkUscmrNqVLdajnZXKjvI9fzjLU+XXig42kVyCkZ61XvZSOA1VzsXKjuE1eK4GJOM094o5oz5TDJrgoLqVMYk/OtzSb+XIz+lHNcNtjbt0mt49rEn5utRyaxHHqElvIcYYjmrMV0siAOOtY+uaUt67T27Ykznim2CXc35Ira/g2yKrqR1rLtvC0Frefa+qLyq+ppnhxLuzXF43y9FXua6UXAkjJJ4AqtGtRWa2OR8UXksNqbdTiWbg47LWFa4ghz6Cuk1uz824a4PI/lWDfRjYUHFZNFJ6Gx4iAfwzG49Y2/SsGL7grqtWiVvDMUZx9yP+lc60G0cUNCZgaySTijQxm8T6ip9Vh3PUmg25F6hx3rPqUtj0zSTiIY9KlvDwai01gIhS3Zyprd7ER3Ob1x+DXHX7Zc11mt964+8++1c8jRGZdtiqErVduwSaoTgioYxbTmYV1Gmj5RXL6eP34zXU2HC15uP2R34Qu5pCaaTxSZrz4HawY1FIM5xTmNNOSOBzXRDcylsQYk/umipPNuf7w/Kiuixz3J2rNvW61oMeKy748GuKn8SOyfwmWzfOabnJprN8xpinmvaijyZMtRir9qORWdEa0LU1b2IW5pQ/6wCup8ODOo/TFcraczqPeuu8Mrm6dqyp/Gjd/CzsICfs7H1aq8/WrVuubUe7VBNG2eK9qPwnntkCOUGQf0qnrF6RZMKuTxSCMkVz+tiYwbRmuOsnzG1PYybKUtMxz3rqNNcCyP0rl9Nt5RyVrptNic2TDHOKlJlPc891lJ73X/s8ClndsACvQLWyh0fTE01OJSoa4c+vpUHh7R/sNzPrNzH+9c7bcEfrU9xDLc3BgXcS3MjHqa2hHlV+5lJ3Zlxxm7ui4z5YOF9/etJ7iOzi6gYFS3aR2MBAAGBya4vW9QklDYJC0m+UNzRvfEEdxIYkm8pgeCeh/wAKcYYryP8A0lmjfjEq8/mO9cFNMS5q5pmr3tm4VJN0efuNyKy5ncs62XSb62BK7Z4x/FHz+YrI1aRo4Tuzmuh0bW4ZUUuWhcevIq3qYtL2LM8ENwmOSOD+YrTRojqeax3JLVpWUuSK3JPDejytmOW4tSezYYf0pIPDaIS0WowsAcfMpU1PIyuZFRZSHxVHU7oKevNb39hXG/IuLcj2eqt34VnuGy97bRr7sTRysLo5+O/5AzXVaA3mRbsVFZ+ENNhYPc6g82O0aYH5muk08WlpEEtLMAD+OQ5NVGLW5LaexIkDmFH+6o7mqcVwba5bAPXqe9W7nUY1I3N5jHoB0FcVdale3t9gNtG7CqtKUtdCo+Z6JA8F3F2DVFeXJswsbc7uprCtJpbSVUduQAW9q2Zmjv7Mf3qfNoKxOHinj5xg1zviC3EMu5elWrC58u5+zSHB6CrmrWf2qzJHLKP0ovzITVmGrcaFHzxtjH6VhO67a1vFZaDSoo8/xIv/AI6a5sOcZJqZOw7FW/5kqxoozdp9aytTnIlGDVzw3Mz3qDPes1uUej2X+q/CkuiQtLZqTD17Uy8BCda2exCOb1tuDXIXbZY11Ott96uUucbjzWEi0Z9xy1U5hVu5xu4qpN0qBjtOA82ujsvu1zmn/wCsrorP7leXjnqkejhFoWqaaDxSE1xxOtjTSM3GB1pGNRSE4JFdENzGew/ZN6rRUXnv6J+VFb2MS0x4NZGoHg1rP0rH1A8GuKj8aOur8JkMfmNIp5ppPzGlj617iPHZbiq/anpWfF2q9bdqcthR3NSwOZ1rs/C4+WR/euN0wZcn0rtfDg22bH1NZ0v4hvL4DrbLm1X60suBTLE/6KlR3jlVJ9q9qOkTznuMuJ0VSCRWPevHIcYGKy9a1Roict3rPi1hW6muao0maxi7aHSWkUePuit3RrZZBt4CgZY+grmLC7EiDbyT0xXZWkTWenhHwJHG6T2HpRDUJEOoSK74QAD7qKO1Nt4I7SEu+N7ckmq1vOsl28jY2r0rC8Va+kbeSr4J9+1W5WRm43dhviC4W6mMaH5B196wbuyiaM5xRHfo3OetR3l2vl5zS0HqZraTExJ4po0iMMDxUqX0f96pEvEdwAwqboPeLVtYhUGKL2zby8xyMjDuDirVrKu0c1FfzADO7FDQJu5Vtm1FAAZ/MH+2M1pW8s7DDwxn6ZqlBPH3cVo2ksZ75paFXYeewOBAPzqjfX91HgRW6HHrmtXdHknFUZ3Uy4wTQ4oLsp2k2q3L/Myovoq1sFJlhAZiTipLEIADipJphnAFPlSQXYQWWbeOQgbtpPT61leHtKZLl7uRMiP7vue1dPEC1pEQMnbx+tBMVla5kwFiG5j6tUqKWo7nN62HhkWEcyP87n+lT6ZPLGyhulU3uRc3rTPyzNmtHKDaQAKi2o7kmt2UhCXsAORycVtaM5ubZCRyeCKNNlinh8l8HIqzpEa294YcYHUVrFJEN6WMvx7AWtIgo/5aZ/If/Xrjyrbelek+IIlnhCkZwDXHy2oVzxUVI63HF6HF6mreb0NX/Cyn7fHwetatzZxSPytafh+wiS6VttRFalNnT2IPkD6elRagODxWhBGqpgVU1MfKa0exKOI1w8tXK3J5NdR4hON1cncuRmsJbloqz9apTGrUsh5qnO3WoGT6ePmz710Vr/q657TTyPrXQ23CCvIxz949PCL3Sc9KY9PpjVyxOpkZpyjKk+lNNMyxbapwScda6YbmE9hvmr/cWir39h3f/Paz/wDAmP8A+KorfTuc9mQyfdNY2on5TWxKflNYeqHg1x4f+Ijtr/AZXUmpI+tQqakjY5r20eMy5EKuwdKzopDkCtC3ORRLYcdzZ0hchjXa6MNtko9TXH6OPk/Gux03/VRr71ND4zappA6e1OLZB7VX1E4hY+1TxnESj2qC9AaJs+lez9k83qec+JWyx+tY0PUV1OuWayH8azrXSvMmSNerMAK4aqvI6YNWOx+H2n+Yh1CZcxQ8KD/E3/1q3tXut7C2RjvflvYU+JYNL0iK3h/1cS/99N61maYGmvJJpDljWsdFYiW9yC//ANFt5Xzgd68o1m9e61J5NxxuwK9P8buU0a42HBxxXkgidrgVNT4rBHY3LAZjGaXVfltTg0+xjIjApurxO1vgDNV0M18RzXmPn7xq5pomknHJxS2+nyscla3NJsGSQEr0rJJ3NbmnZWreWuc1Bq1uQoGTWxbgAAGs7xBPFEnJya1exnHcy4U21oWku3isGXUecL+lS2Vw8hzzWaaNDoxMPWqE13i42imQsxBOarwRNJdbjnrVNslbm1HdMkOapy37FzgmnXrCOLA9KzrBGuLkIO5ouNnoGluV0uCZ85MY2j3rkPG+tbbhbFHzt+aT6+ldTNKLTSi5B228JwPcCvIb6aS4u5ZpGyzsSaKj6ChrqbFnqP70c1uJqAaPjrXGWvD1r2snODWSlqW0dbpeo7HRs9DXYW8iyutwvZCa8wtZSCoz3r0Tw4S+khm6tx+AraLIki3NJ5jleoxXP6mwSQ5FblqAwDerfzrF8SoE3MKJbCW5hS3CrL1rW0O4UzrzXF3cr/aTgmtzwxIxuBk1inqaWPRLdwy9aq6kflNS6cC0fWq+qcKa1ZCOH8SnrXIXXWus8RH71clc9awluWijITmq0xqeVuTVWRuakZoaYORXQQfcFYOljpW7HworxMY/fPWwy90mzTWNN3U0msYo3YjGmtkIWXqKRjUZkKA478V009znqbCfbB/cP50VX/c+h/Mf4UV0nNY05z8hrA1Q8Gt64Pymue1Y8GuLC/xDuxPwGapqeLmqydatQV7SPGbJ0UZ6Vfth0qmnWr9qORRLYqO50ejwnYPzrqtMHKD3rndGU+Rn8K39ObDrSw6940rP3TplI2j6VWv32wN9KejfKKo6y5W1Yg167funndTmtSm/e8ml0iZRqFvkjG8Vg61dus2Aaq2t9KsqEHkHNcE37x1RjoesawT9mQj7u7mk0YDYzVX0jUIdS0xfMxnbtdff1q3Yw+TGwVwwPStYESMjxSPNtGjzwa82RNlwVI5BxXpeqwSyswZowO2XFcTf6LqX253ghWRDzlZF/wAamad7jQ+zwQAKsTIrADFFppmpKozavn0GDVtdPv8ABLWc/wBNhqlci2pWiRVHCCnGUoeMCluI7yFT/oN0T/1yb/Cs1TdyyndazqB6xkUajZptO23g1ha07St8xJrVxIFwY2H4Vj6oSH5BqZXCO5SjhWtOwhAFUYmHoa07RgFrNF2L1rGMEkVdtbdF+bFVrU5Qe5q87COLmtUSZuq4INW/CtkPMM7D5V9uprOlJurxIE5LGus8tLCwCLwEXn3NNdxMra/OGs7iEY4hfP1xXmT253n616NqCF7C6lIyxhOK4VsbqzmUiK1tSWrSgtiHFNssVox4yKhDZHBbtvAGc7q9K02EW2nxQ55WMA/U81xWhwi51KKPtuyfoK7SOXfKB65P4VtEmWwsa7Ihj1/rWP4iy6vW0eI+fX+tYmtn71E2JLU4W4hY3B+tb3hiFhPkjtVV48yk4FbegoA/TtWCepozqtPXCdTVbVvuHmrdkBszVLV/umtW9CTg/EP3mrlbkjJ4rqPEB+YiuVueprJ7lIzpupqo+d1XJvrVU8uKgDV0tT8tbYPFZGmjpWrXh4l3qHs4de4LmlNIKG6VEC5EbdahlY7SAOvrUjn5qhk2nAY4BIrqp7nPU2IMf7v5iirv+hf88v1NFbmJYuPumud1euiufuGub1dua48H8Z1Yr4DOTrVmIkVXjPNXYdp617SPIZJExJFalr2qgoXcMYq9bHkVMioLU6/SgFsl9zWnZviRfrWXZMBbxL7Vdt3+cfWjDbmlbY6aOT5RzVTWiTaNinwNwKdfgeRzXrPY8/qeZ64j/afumqcAbzV4NdpfWcM0mSBVZdMhEgOBXHKF2dCmrDdHmlgIaNipx2rs9JnkntizYzjsKwrWxQYwK6jQIUWHbxWsYWZnKWhx/iS+kj1JYmVcEdcVSk1CWJQRbxsAfTFdB4x0xXuopQOnFRw6dDLaYK8kYNS4u41JWMddVkxzEp3e9F7rwtYv3kZJx2c0+SxCSHj7tYeuWTzEgE0WYXReXxxD5mWgudvYK/8A9erth4wt52AK3Q/4Fn+tcYdJkHrVzTdOlRu9Jc4Ox6DF4nthhi1wPwqrc+M9OjYrJLLyecxg4/SueFtJtxWHqllMZsgU3KS2Ekmd3H4x0lgf3mc/9MB/hU8Wu6bdD935RPX5oVGfxIrziCymBHy10GlWUgTLClzzK5UdhHe2p2/LD74jWke+sVB802mf9uEf4VkRxskecdaw7+Ke9v0t41JLNiq5pIVjuYPsDJ9ohtrTd/C0cIU/nUd6Q5WFQC3U5Geaz5SNKsQi8LEmPqa5yKeV5/PYsHZs5ochWOvkDIg27dx65GRWOf7Hd8Pa6fnvxjnvxVnxKWXQruRCwcRjPtkgcfnXmO1s9DUzk1sOK0PSoItJK/LZWhPoGYf1qQ22nhz/AKLBgDLHewwPXOa4HTVOehrSiid5VRQSScCsnJmljpfChVRc3XT/AJZx59+v6VsaVdGW8cj7o4FZcsI06xW3zh1XH/Az1rS8MwHlsdquJMtjYuSfKGO9YOqt13V0d0mFVfSua1wYVqVQImA0qiY81uaE6kkj0rmG/wBcfrXQ+HQcGso7lM7GyOY6z9YOFatHTx+6rP1kfK1a9CTz7X2+c1zF0wya6TxFxKa5i6HJrJlFKZhg1XQgyD61LN0NQwDMoqXsNbm7pw6YrTArP04YArRXrXgVnebPaor3QIpGp5pj04DkQN96oJ1yR9anb71VrjIbOa6ae5zzDD/3D/3x/wDWopnmN6t+dFbmBqXX3DXM6v1/Gulu/uGuZ1Y/PXLgvjOrF/AUoxVuFCegqrD1rQtxxXsI8lkkSnNX7X7y/WqsY71dtB+8Ws6mxpTWp0dq/CD0WrsL/vR9azrXr+FWoD+9FPC7l4jY6a2flak1SULb81Utm5FReIJCLXg16knZHnpambLdIH5aiO5Vn4YVyt9cyiY4Y07T7mUzctXLz6nRyaHe2k6jHIrX0u8VD94da423mfAOakF+8UwXd1Nbc5ko3Z22vgTWocckc1l6ZKGUqTUkNw09kATnjFYkVw9ndlH+6x4pt9QS0LmrARMzetc9dyjfXUX0P2y1+U8kY/HtXnus3MlvMY2BDKcGlJ2Elc1t6Y7VYtCpPSuTTU3962NMvS6g1MZJg4WOiIUJmsu6dDJggVKLk7eaw7++23BFEmgirm1b+WSOBW3ZxqYxgda5Cwu98iiuvsX2wBj6Zoi7lWsS3IVFPoBUnh2zjDPeuo3HhKrEtcyLGP4jz9Ks6rqUGnacXVgAg2oPU1TdtRWuUvE88c062y9E5b3NUYLZCVwO9ZEF+J5y7NlicmtqzmUsg9SKUdQaaNnXIw2lXSv3Cj9RXHf2fDnoK67xPII9LmY8ZYD9a477Yn96lJ2Y0tC/Y6fCOgFbug6XEtwbt1BCfdB7msXSZvPmWNOWY4FdhM8VpaBQR8g59zUX1uWUL+3W6vUQ4O3k/U102haesUGdvWuf0ZTLcbzyWNdqmIoAPQVcdiJb2KN1CC546Cuc1i1V8g100si7GYnrWBqMgJJFRMcTlm0v94SPWtzRLDy06nrUKyLurX0xwU/GoiNs1bSMrGBWXrSnY1bdvgqPpWVra/u2rSwjzHxJ/rj9a5i6PJrp/EwxMa5e66msJbllCY9abajMopZ80tiP3tZz2Khub9kMKKtg81Wsx8lTd68CprNnuQXuk2aY5pw6U160gTIh/iqC7ALfQZqZuDUFwCzjmumG5zz2IPwop+F96K2Oc0rz7lcvqx/eV1F1901y+q/6yubA/EdeM+ErQEZrRtyNtZ8KE1oW8TcV66PILadKu2Y/eLVSNcECtC1XEi1jVehvSWprWvGasQE+bVeDjNTwff8Axq8KViTetTkik1qGSa22oCadYDJUmtlRF5fzYxXpT2OCO55le6Re7ywiYj6VFY2ksc3zKRXp7z2GNm0Z9aoXenwS5eMLz0IrmSVzbm0OaQFVFUb5mEqkVtzweWxVhjFVvsqyyAEVozNbm54dk821XPpzS+JdPLW6zxDlTzimaRC1uccgVuhklgKMMgjBFXHYUvIwNCvdy+U/XpXMfEOwZZ1vY1+R/vY7Gt+8tGsrsyJkKTkVPOsWpWbQyYIcfkaT10BHlSjmt7SBhRS3ml/ZrlonHQ8H1FXbKAKgxWaVim7onY4Q/Subv+bo10zphDWI9rvuie1EgjoWNDgLTLxXYMwjgCj0rI0a1WPDHtWmo8yUL71UdEDLlqojgJI+dxj6CuI8Zah9ouxaxH93F19zXZ6pcLbadJIpy5G1P8a84mtneVnYkknJNTN3HFWF0wkS9a6vTCDPCPVhXN2Nsyvmt7Sty3tuP+mi/wA6cXYUlc2vHz7dCk2k8zKP515zvf1Nd544cyaLgd51/k1c94W0V9RvFZxiBDlj6+1TU1loVHRG94LtWtrUXsw/eycRqew9a0NQma5uFgRsqDyfU1PqrR2iCGIDzCMAf3RT/Dtg0swdhzSau7Ia2ua/h20YSByPlUfrWzeylVCg8mnwRJbxBV7VSunJk3HoOlaS0ViOtyC/lZY8A1gX07KhJrSvJS5rG1T/AFRrFspGat228/Wuh0Wcui8d65JPv11GgA7UpRGzr7TJQfSs7Wf9W1asI2x9vu1lax/q2rdGbPMfE4/fGuWuRya6rxP/AK41yt0eawluaIoTgU+wXL0yc1PpvPp1rGq7RZpSV5I27YYSpMc023HyCpcV8+37x7cVoKvSkenAUjjrW0CJFZxzUNwQOwzirL1Wu1IUHFdMTnmVt1FR596K1MDZuvuGuZ1P/W10tz901zeoj97XPgPiOrG/CR2oyRWrCPlFZ9mvIrVhXgV7J5A+JctV62X94tQW6c1ehXDiuWs9DqoouRfdNSwn5/xqOP7tLD/rK0wmwsTudFYH5VxU+oXBjjzntUGncqKm1O1eaEhR1FehPVHFHQ5i41Y+ecNxW9ot75sfJzXIzaRefbCpRsZ611Gk2bW8AzWKi7msmrC61IquCO9Z9vdKswyabr0wMoAPSsGW4ZZhzTvqQjv7edHQYNTrKUfGeDXKaXfHaATW1FcrMvXmtEyXoa9xHHdQ7WHXvXPMkun3hjkyEY/Ka1rSfawVjVm9t4ryHa4yPXuKpq4tjntathdW/nRjLp1xWVaYA5ro47We2bb99P5isnWNPa3f7RACYm5I9KlrqVoV7hhsOKo2qb5s470SyHaeam0tSXzWa1ZSNiEbIgKu2iBE8xup6VUtVMsgHYUapcmFMg47KKp6C3KWv3IkYov3VrD4z0p2oXJAJPJrMF2c1m2Oxr2+N1a2nJm8g/3xWDYT7z0NdBooeS/hAB4bNUhPQsa5A91axwDJzMDk9hg1LZzQ2FuEh4RBxj+I1NqbyC2GBt3Ng854xSaVpct1IHkUrGOgNNp30GrLcSwt576481wSWPU112mRJbRgDr6023t4raIBQBVe6uwmecAUaRC/MaU1xnjPHeqV7cLtwKx5dVXJG6mRXImPWs3IfKXm+YZrP1BQYyCKvK4C9ao6g2VPNIRkJCu7p3rotFQKUFYkRG6uj0eMkqR2xREGdEhO05rL1f8A1bVrEYSszVh+5NbkM8w8UnEprk7g5JrqvFfExrk7j7xrCW5oijcdavaYvyD1zVGf71aemL8imuXEO0Gb4dXma0IwtSCmoPlFSAV4PU9pLQVRTX4FOFNkremZyK0pFQXjKYAc4YcY9ank61WvceUSCB7V1xOaZQzRTc0VqYXN64+6a57UF/e10UwyKxr+Il8+tcmAfvHZjV7pBZJyK14l+UVSs4TxxWjsZVFe1c8cmt0yRVxUwwqtan5qu54Brkq7HXRJE+7SxcOKajU7GGBrTCsWJR0OksCADXQW6qy8jNcvpbYxit23uNpAJrvbOKxZktoSclc1Q1JAkJ2jFaysjqMVna0CISR6VS1E3Y4e8R5ZmJ9azbm1YS5roJQuSazLt134pWSFcZZRMpAzWlZiWOTrkVQhk54YVchlcHORT0C5v26GVRjg1dg82IjPIrIsL0LjditOO6jfo4qlYWqNBdr8jAP6GkktUeMo0YweoqsLjaecGrMF2Dgdqq4jmNZ8OToGmtVMkeeQOq1RtbaSIbWRgfcV6FA4yGU4PtUzxWswy9um/wBRSUFuHMchbwmGEf3m/lWFrLvLebR91a9Al0+Nmzgis6Xw7A8hclsk+lKULjUkea6hExz1rPS1cngE/hXraeFLKQ/vBx7nFaFt4a0W35MPmsOw6fmalUR86PNPD+kXVzIEijJ9TjgfWu80vTY7NRHGA7n78mP0FbTQwxR+XEkcMY/gQfz9aZG8Sv2JqlBRFzXKiadEyJvQMVYnnpVrEcC9sinXE6gDBrPupePvAfjVXSFa5Jc3XB9K57WLzggHNWrqTcCA4/OsyaDzCec1yz1N4qxjedK83fFbWkliR1qBLE7sgVf0yBkkIPas7FSZfIOKz9SciMitKc7RWPqhZkOOadmZ3KkEh3gZrtNATci8GuDtllMi8d69E8Mox2Kw9DWtOJMpG1Ou1cVnaqv7g/StC9Yh1FU9S/1B+ladSeh5R4uXE5rkLnqa7TxeP3xrjbkcmueW5qjOkGWrZ0tMRrxWZsy9b1imI04xxXFi3aB14VXkWlGBTqQ0V4aPXFFNfpThTX6VrB6kSRVl61V1DaIkUD5uSTVmaqV2cqM9a7Is5Zoo5oooroOc6R+RVG5jBOTV41VuVODivNwk+WZ6WJjzRLml2qPjkVfv7REhBDA1j6czhgMmt9Ynkh5Ga9tSueK42ZkINpq8vKCmvAQcYqRR+7A9KyqbG9LcavBqTvkUwinxqTWFGfLI3qw5omtpTcYq+8hUgjNZVg+xxnpWxgOua9WM1JHmyi4sdDqG04J4qxJOLiEjrxWRdQkcrRZ3DQnDdK0i7ESV0Ubm2mF0UwcE9apXemSNJ96ugu7mNvmA5rDudQYTcZokxRIodLdTjNXBYOq/equupGkn1UInJrPmZViSS2lX7rVGv2lDkMapHV9xwDSjUh3o5mFjWt7q5BAZq1bWdzjOK5QaiN2RVu21JiQBVqTFY7CC6ZeM1o205kHWuUtpZJADzW9pEchwea0UmUqVzVdn24BquTIOea0Y4GZeRTjZ8dKrmZXsChHOyDmq9zqoj4L4NWb21dFOBxXHa/5kTEik5sn2JsvqbSNgH9aBeInLSZPtXFDUJVOM0v22U/xVnzi5bHU3urbNoXnisyfUZZm4B/OsJrmV5MHpV63kIAyKhzGol1GduTmkd2T1pizkVHLdDPao5irF61mbjO6tG2dg+fWsaG44GMVpWcxYjNAmi7cMSOaz52UjBxVudsrWZctg81aMmXdPijaRflHWu58PxgbnwAAMCuG0c7pFPOM16FpCiOzB9ea1gQ0MuhvuQPSodUXFufpVlBumLe9R6ngxEH0qXKxolc8q8WpmZuK425j+Y16D4mgRpWri7+EK5xXPJ6miRmRR/vB9a3VVVwF6YH8qyo0O8VqIeOetedjZWhY7sHH3rjz1pKQ0orx0z1bCimv0pe9DcitIsloqTCqN2flFaEwqjdL8ua6oSOacTPx70U/bRXXzHJY6Ko3XJqWmv6141OXLK57E43iT6Vbr5wyO9dtZ2Ie24A6Vw1rciKQMa67SdYDRBAa92lUUlc8WrTaZn6pB5MpGKzlPzEVu6hG07FsVltbFZM4rSS5kRB8rIkQk9KsRRYqaGD2q0sHHSuOSsztTuVQu01dtJyo2tyKYYuMYqIgoa3o1bGNWkpLQ0pQGHFVZIVPtTYZ8DnpT3cEZBr0IzucMotFVoiuQazri23S1qSSVXYjOa0M7GdPbrGme9Y13E8rHHSt+4Uufaq/kDNCigvYw47ZlNWVtyVrTNv7Unl7e1PlDmM+K3+bpWvp1iWYHFQRgbxxXS6JBu28UJIuCuy/pmnjYMiuk06zVFHFN061O0fLWgytEn3a0SO6EUTxqijmpBsPpWPNcup5zRBdknrQ2W0jTuIFdCMVyXiDShIGIFdZBLvWoL+EMp4qWiJRPItS0xonJAqpFAx6iu91m0TniuckiVGIwKzaOOorMxRFtfkVZU7VzVholLZqC5AVcCocSUyjc3vlnGaZFMZTnNI9oZGyas2tmVNLlKuT2hO4ZrpNLhDKDise3twCDxXR6QFGBRYTZYa03L0rKv9PbOcV2Vrbq6jim3tirIQBVxM2czpNvtZVxzmu6iwloka9cVg29lscELW5DyBntV6RRCV2SxR4XNVNSBK1dZtq4qjM4kYisJyNoxOK121LuxrjdUtSrHg16rqOnmSMsFzXD63aMrsMYrCU7GsY3ORSPD9KtDgVM8GwMxFQmvJxtS+h6eFhYKWkFLXAdoZpc0lFUmS0RSjNU7pf3Zq+wzUMyZQjHWt4SMpRMfbRVryjRXR7Q5uQ1hQwyKQU4V5p6ZVkXBrT0WTbKATVKRc0+1co4rvw1Xozjr0+qPQ7GBZohgA0y70wgZC1F4WvFYKrGuxS3WaPoDxXrQ1R5U9GcStttOMdKmWIYxit7UNOKEsq1nGIqeRWc6ZrCZQlhGMiqssee1a0kfeqk0eOQM1g4tG6nczVj+QqRwaqTPJbt82SvY1rbR9KZPbrKhBFb0ptaGVSKZmeYJBkHBqJmYVMbN4G+UFk9PSrENuJBxzXTGoczp2Mt5SKaJa0bvT8DIFZkkZjbBrWM7mco2JBJTHOaaDUqjIrW5FiO3RjIOK7Tw3FwuRXNWSDeM12GhFFxSNIM7HS4VKDirdzbqV6VV0yQbRV+V/lpcxrzM53UrbAOBWXbxP5vSuiu9rZzVSONA3ap5mHMyzYR/KMirNxCCh4pluwHSpZpBso5ylJnJa9DtDYrjL5sSEV32sp5gIFcldacWkJNO5nNpmLu4qFlycmtO5tBGOorPlG04BpoyGxoCasrGAMiqobFPE5AxQIsb9taGmXQVwM1hvIzVLauyuDT0EelaPOHQCtjy1YVxegXZGATXX2swdBzWblYdhfs654FPVAtPLgCoZJcVnKdylES4bIwKoyAx/NVwDd8xqpenCmsJysjeEbsr3GpJHGQetcnrF1DMx+UZqxq82C2DXOTyncSTwK45VmdcaSRV1IqMIv1rPNTXMhdyT3qGvMqz5pHfSjZCUZoorM1FpKKKES0LTSM0tFWmQ0ReWvpRUlFXcmyFFLTRS1idAppnRqdSGnF2dxSVza0S7MMq816V4e1BJY1DGvILeTYw5rqvDupmNlBavXwuIvozy8TQ6o9XNuk8fABrF1HTGQkhas6JqQdV+auijWG5jwwHNelZSPOu4s8+mtyMgiqc0PqK7vUdFzlkGRWDd6a6E5U1lKmaxmcrNCQciowSOo4rcns2HaqUtqR1WsnA1U+5VjRH61ILPB3JwaDCV5FKk0sfuKi7iXpIq3fyjbIuPesa+tyctGQa6SSSKZdrrWTe2bRndGdye3UVcatiJUjnyGU/MpFOSTHetJoCwyMH61VltTn7tdMKphKlYWG52nNbOmaoEIBNc60LL60+ASKeHH41o6iJjDU9K0vWowBlq2l1SKRPvivMtPeQkA4/A10MGViDbv1pcyZ0xgup0VzeIejVVS7+brWBJfbWIOaauox9zg0NmvskdjBdIF+8KZdahGqn5hXL/a9yZVzWVd6iQxBc0roxnBLY3tS1QYbBzXOz6lK5OOKoz3xJPOag8/0rSLRzSTLE80j/eaqjcnrSly1Jtc9sUOaEkxNpJxViC0Mhotodzc5NbNjH8wVVyah1A5WU104gdKkTTWBziuijtgEyRzSmEdBxWbqlqBQ06Fo2GeK6SznCoBms2K3Oc9qv20WB05rJycjRRSLjTFhxSxgvgUQwk1o2tqcZxVJEtkAQhcCsrWXEcR9a37hVijJNcZ4gu9zMAeKxq6I2pbnN6tLljzWBdORkZrS1CbaxLpuVgcc4rEmfc1eXWlbQ9ClG4xjk0lFFcZ1oKSlooASkooNMTCgHikozTJYuaKTNFUSANLTQaWpNRaKTNFABmrVncGNwc1UNJmqjJxd0TKKaO70DWNpUFq77R9UDqvzfrXh9tctEw5NdPomuNGyhmr1cPilszzMRhuqPb7O7WRQCQammtIZxkAZrhtG1tJFX566mx1FWA+avUjNNHnSi4siu9GU5wtZF1pDLnC12MNwjjnBqRoYpB2puCewlNo81uNPZScpiqM1mfSvTLnS43BwBWRd6J1wuPpWcqTLVQ8/ltSO1QNEw967K50hxn5c1mz6YwPKkfhWLpGyqs5Sa3IO5Bg9xUQAPDJg10smnH0qtLphPapUGh+0TMJraJ/QU1tNQjIIrXfTpFPFRmzlH8JpSjIqMomMbVoTlc1Ks0+3buP51otayZ5BpPsb/wB2pSlE05kzO/eseSTVS8LxsDn9K6BbbC8rWbfWpdwNverjOVxynoZsd1Mw2gkClFlPM27zsZrVg00bORg1MtpIn3aOeVzPQyk0iQ8vNn6VKNLSMdMn3NaLJOD0pVgncdKrmkRZGcLQKewpTDGOign8614tMkf7wY1ettGkJ+WLH4VLUmNOKOft7eRzypVfyrXtFEYAVa3LXQJW6itW08OgY3CqVOT3Jc4o56LzX4CmrsFnIxGRXVWuhxrj5a0oNJjUD5RWqpGbqo5CGwc/wmr1vpkh/hNdZHYRr/CKm8mKMZIFWqZm6hhWml4ALCrM0cdvGelWrq7jiU4IrmNc1UKjfNSlZIFeTKHiLUFCMqtXn+rXvLEnJq1r2qlpGw1cpfXRkY815WIrpHp0KLsMvbhpHyTntVSgnJzRXlyk5O56MYqKCiiipKCiikNACGkpTTTVCAmkJpCaaTVWIY/PvRUeaKLCHg0oNRg07NFjS4/NGaZmlzSsFxaTNJmjNMQGnxTNGeDUdJTWgmb2mavJCww1dnoviMEKGf8AWvLwxHIqzbXjxEHNddHFShuctXDxme7abrKOBh63LXUQ2PmrwvS9ekiI+cn8a6zSvEgOAz4r06WKjI86phpRPWobtWHWrCvG/XFcHYa2jgfOPzratdTVgPmrrjUTOVwaOhe2hkHQVWm0uNugFQwX6nvV2K7U9xWl0ydUZU+ioc/IPyqjNogHRSK6pZkb0p+I29KXs4sfO0cNLoren6VWk0Zx0FegNBG3YVG1pGf4RUuiUqhwB0h8cpSf2Sw/5Zmu9Nkn90UfYo/7tL2JSqnAHSSf+WZqu2hM0mfL4r0cWMf90UosI8/dFL2IOqefroj4+5UiaA5PK16ALKMfwini1QdhT9iL2xwKeG84yv6Vdt/DiL1SuyECDsKcEQelUqSRLqNnNwaHEv8AAPyq7Dpca9EFa/yCkLqKfIhczKcdki/w1Otui9qc0wFQyXAHeiyQrssBUWkMqKKz5rsDvVC51AAH5qTkkOxrzXYUdRWXe6iAD81Yl9qyoCS4rmNY8QKoIV6xnWUdzWFJy2N3V9YVFPz1w2va0XLBXrK1TWXlY/McVg3Fy0h615VfGX0R6VDC21ZNe3bSMeapEk9aTNFebKTk7s74pRFopKKmxVxaM0lFFguLSUUhNOwAaaxpSajJppCAmmE0rGo2NWkQ2OyKKjzRVWJuSA04GogaUGk0aXJc0ZqPNLmlYLj80mabmkzTsK4/NGaZmjNFhXHZpM03dRmiwrkisVPBqzBeyRkcmqWaXNNXWwnZnR2GtyRkYc/nXRad4mYYDMfzrzoE9jUkc8idGNbwxE4mM6MZHslh4jjbHz/rW3aa2jAfOPzrwuDUpExya1LTXZUx+8NdkMb3OWeE7HukGqqcfPV2LUlP8VeL2niaRcZb9a17XxUONzV1QxUX1OaWGkj1uPUFP8VTpeqe9eY23ieM4+cfnWjB4hiOP3g/Ot1WT6mTotdD0JbtT3p4uV9RXDxa5Gekn61Zj1lD/wAtBVqqRyM7H7QvqKPtI9a5VdWT++Kd/ayf3xT9oLkOnNyPWmm4HrXMnVl/vj86Y2rp/fFHtA5GdMbketMa6HrXLtrCf3x+dV5dajHVx+dS6qHyM6trsetQyXqj+KuOn8QRLn94PzrOufE0S5/eD86iVeK6lqjJ9DuZdQUfxVRuNUUZ+cV59eeKhztasW98TSvnDY/GuaeMhE3hhZM9EvtcjQHLj865zU/EqjIV64a71iWUnLk1nS3Ujn7xriqY5vY7KeCS3Oj1LXpJc4cj8aw7m+klJ+Y1SLEnk0VwzrTnudkKUYjmdmPJptFFZGgUtJRQAUZpKM0ALRmkzSE07ALmkJpCaaTTSC4pNMJoJphNUkS2DGo2NKxqNjWiRDYuaKbmiqsSOBpwNRinCk0Wh+aM0w0tKwx2aTNNFBosSPzSZpooFFhDs0Zppop2AfmjNNHSgUrAPzRmm0tKwhc0A4po6UtFhkiyOOjGpVupR/FVYUtGwi9HqEq9zVmLV5V/jb86yKU01OS6hypnQRa9Mv8Ay0NWovEUw/5aGuVFOWrVea6k+yi+h2KeJph/y0/WnnxPL/f/AFrjxQaf1mZPsYdjrj4nm/v/AK0x/E0xH+s/WuTPSj1pfWZlewh2OlbxHN/z0NQS6/M38ZrAopOtN9SlSiuhqyatM2fmP51We+lY9TVMd6UVm5ye7KUUiZp5G6saYWJ6mmilrNloWlptKOtAxRS5pBRSGLRSGiiwC5pM0UhoAUmkzQaaaYhc0hNFNp2EKTTSaDTTTSE2BNMY0ppjdatIljWNMY056jNaJEMM0UlFMR//2Q=='
//       const f = new File({text: 'test fooo', originalFilename: 'foo.jpeg', mimeType: 'image/jpeg', base64Data: base64})
//       return this.services.file.create(f)
//     }).then(res => {
//       this.testResponse(res, 'File:create')
//       this.setCurrentTest('File:update')
//       return this.services.file.update({id: res.id, text: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'File:update')
//       this.setCurrentTest('File:delete')
//       return this.services.file.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'File:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testOrder(): Promise<any> {
//     this.logTestStart('Order')
//     this.setCurrentTest('Order:getList')
//     let testOrderId
//     return this.services.order.getList({}).then(res => {
//       this.testResponse(res, 'Order:getList')
//       this.setCurrentTest('Order:getProcessStates')
//       testOrderId = res.items[0].id
//       return this.services.order.getProcessStates({})
//     }).then(res => {
//       this.testResponse(res, 'Order:getProcessStates')
//       this.setCurrentTest('Order:getById')
//       return this.services.order.getById(testOrderId)
//     }).then(res => {
//       this.testResponse(testOrderId === res.id, 'Order:getById')
//       this.setCurrentTest('Order:create')
//       const o = new Order({checkout: new Checkout({id: this.testCheckoutId})})
//       return this.services.order.create(o)
//     }).then(res => {
//       this.testResponse(res, 'Order:create')
//       this.setCurrentTest('Order:update')
//       return this.services.order.update({id: res.id, externalId: '123', ...res}, true)
//     }).then(res => {
//       this.testResponse(res, 'Order:update')
//       this.setCurrentTest('Order:updateOrderNotes')
//       return this.services.order.updateOrderNotes({id: res.id, note: 'note test', privateNote: 'note test', ...res})
//     }).then(res => {
//       this.testResponse(res, 'Order:updateOrderNotes')
//       this.setCurrentTest('Order:deleteReservation')
//       return this.services.order.deleteReservation({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'Order:deleteReservation')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testPayment(): Promise<any> {
//     this.logTestStart('Payment')
//     this.setCurrentTest('Payment:getList')
//     let testPaymentId
//     return this.services.payment.getList({}).then(res => {
//       this.testResponse(res, 'Payment:getList')
//       this.setCurrentTest('Payment:getPaymentTypes')
//       testPaymentId = res.items[0].id
//       return this.services.payment.getPaymentTypes({})
//     }).then(res => {
//       this.testResponse(res, 'Payment:getPaymentTypes')
//       this.setCurrentTest('Payment:getCurrencies')
//       return this.services.payment.getCurrencies({})
//     }).then(res => {
//       this.testResponse(res, 'Payment:getCurrencies')
//       this.setCurrentTest('Payment:getById')
//       return this.services.payment.getById(testPaymentId)
//     }).then(res => {
//       this.testResponse(testPaymentId === res.id, 'Payment:getById')
//       this.setCurrentTest('Payment:create')
//       const p = new Payment({name: 'test foo', type: {id: 'cash'}, eetEnable: false})
//       return this.services.payment.create(p)
//     }).then(res => {
//       this.testResponse(res, 'Payment:create')
//       this.setCurrentTest('Payment:update')
//       return this.services.payment.update({id: res.id, name: 'updated', type: {id: 'cash'}, ...res})
//     }).then(res => {
//       this.testResponse(res, 'Payment:update')
//       this.setCurrentTest('Payment:delete')
//       return this.services.payment.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'Payment:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testProducer(): Promise<any> {
//     this.logTestStart('Producer')
//     this.setCurrentTest('Producer:getList')
//     let testProducerId
//     return this.services.producer.getList({}).then(res => {
//       this.testResponse(res, 'Producer:getList')
//       this.setCurrentTest('Producer:getById')
//       testProducerId = res.items[0].id
//       return this.services.producer.getById(testProducerId)
//     }).then(res => {
//       this.testResponse(testProducerId === res.id, 'Producer:getById')
//       this.setCurrentTest('Producer:create')
//       const p = new Producer({name: 'test foo'})
//       return this.services.producer.create(p)
//     }).then(res => {
//       this.testResponse(res, 'Producer:create')
//       this.setCurrentTest('Producer:update')
//       return this.services.producer.update({id: res.id, name: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'Producer:update')
//       this.setCurrentTest('Producer:delete')
//       return this.services.producer.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'Producer:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testProduct(): Promise<any> {
//     this.logTestStart('Product')
//     this.setCurrentTest('Product:getList')
//     return this.services.product.getList({}).then(res => {
//       this.testResponse(res, 'Product:getList')
//       this.setCurrentTest('Product:getById')
//       this.testProductAId = res.items[0].id
//       this.testProductBId = res.items[1].id
//       return this.services.product.getById(this.testProductAId)
//     }).then(res => {
//       this.testResponse(this.testProductAId === res.id, 'Product:getById')
//       this.setCurrentTest('Product:create')
//       const p = new Product({name: 'test foo', vat: {id: this.services.vat.defaultVat.id}})
//       return this.services.product.create(p)
//     }).then(res => {
//       this.testResponse(res, 'Product:create')
//       this.setCurrentTest('Product:update')
//       return this.services.product.update({id: res.id, name: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'Product:update')
//       this.setCurrentTest('Product:delete')
//       return this.services.product.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'Product:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testProductBundle(): Promise<any> {
//     this.logTestStart('ProductBundle')
//     this.setCurrentTest('ProductBundle:create')
//     const pb = new ProductBundle({
//       parent: new Product({id: this.testProductAId}),
//       child: new Product({id: this.testProductBId}),
//       quantity: 1,
//     })
//     return this.services.productBundle.create(pb).then(res => {
//       this.testResponse(res, 'ProductBundle:create')
//       this.setCurrentTest('ProductBundle:update')
//       return this.services.productBundle.update({id: res.id, quantity: 2, ...res})
//     }).then(res => {
//       this.testResponse(res, 'ProductBundle:update')
//       this.setCurrentTest('ProductBundle:delete')
//       return this.services.productBundle.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'ProductBundle:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testStats(): Promise<any> {
//     this.logTestStart('Stats')
//     this.setCurrentTest('Stats:getBestsellers')
//     return this.services.stats.getStatsBestsellers('2019-01-01', '2020-01-01', this.testCheckoutId).then(res => {
//       this.testResponse(res, 'Stats:getBestsellers')
//       this.setCurrentTest('Stats:getCarrierUsage')
//       return this.services.stats.getStatsCarrierUsage('2019-01-01', '2020-01-01', this.testCheckoutId)
//     }).then(res => {
//       this.testResponse(res, 'Stats:getCarrierUsage')
//       this.setCurrentTest('Stats:getPaymentSale')
//       return this.services.stats.getStatsPaymentSale('2019-01-01', '2020-01-01', this.testCheckoutId)
//     }).then(res => {
//       this.testResponse(res, 'Stats:getPaymentSale')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testSupplier(): Promise<any> {
//     this.logTestStart('Supplier')
//     this.setCurrentTest('Supplier:getList')
//     let testSupplierId
//     return this.services.producer.getList({}).then(res => {
//       this.testResponse(res, 'Supplier:getList')
//       this.setCurrentTest('Supplier:getById')
//       testSupplierId = res.items[0].id
//       return this.services.producer.getById(testSupplierId)
//     }).then(res => {
//       this.testResponse(testSupplierId === res.id, 'Supplier:getById')
//       this.setCurrentTest('Supplier:create')
//       const p = new Supplier({name: 'test foo'})
//       return this.services.producer.create(p)
//     }).then(res => {
//       this.testResponse(res, 'Supplier:create')
//       this.setCurrentTest('Supplier:update')
//       return this.services.producer.update({id: res.id, name: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'Supplier:update')
//       this.setCurrentTest('Supplier:delete')
//       return this.services.producer.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'Supplier:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testUserRole(): Promise<any> {
//     this.logTestStart('UserRole')
//     this.setCurrentTest('UserRole:getList')
//     return this.services.userRole.getList({}).then(res => {
//       this.testResponse(res, 'UserRole:getList')
//       this.roles = res // todo update api with std response type, res.items here
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testUser(): Promise<any> {
//     this.logTestStart('User')
//     this.setCurrentTest('User:getList')
//     let testUserId
//     return this.services.user.getList({}).then(res => {
//       this.testResponse(res, 'User:getList')
//       this.setCurrentTest('User:getById')
//       testUserId = res.items[0].id
//       return this.services.user.getById(testUserId)
//     }).then(res => {
//       this.testResponse(testUserId === res.id, 'User:getById')
//       this.setCurrentTest('User:getMyProfile')
//       return this.services.user.getMyProfile()
//     }).then(res => {
//       this.testResponse(res, 'User:getMyProfile')
//       this.setCurrentTest('User:create')
//       const u = new User({firstName: 'test foo', lastName: 'bar', email: `foo.${this.getRndStr()}@bar.baz`, plainPassword: 'mehmehmeh'})
//       return this.services.user.create(u)
//     }).then(res => {
//       this.testResponse(res, 'User:create')
//       this.setCurrentTest('User:update')
//       return this.services.user.update({id: res.id, firstName: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'User:update')
//       this.setCurrentTest('User:delete')
//       return this.services.user.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'User:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   testUserGroup(): Promise<any> {
//     this.logTestStart('UserGroup')
//     this.setCurrentTest('UserGroup:getList')
//     let testUserGroupId
//     return this.services.userGroup.getList({}).then(res => {
//       this.testResponse(res, 'UserGroup:getList')
//       this.setCurrentTest('UserGroup:getById')
//       testUserGroupId = res.items[0].id
//       return this.services.userGroup.getById(testUserGroupId)
//     }).then(res => {
//       this.testResponse(testUserGroupId === res.id, 'UserGroup:getById')
//       this.setCurrentTest('UserGroup:create')
//       const g = new UserGroup({name: 'test foo', roles: this.roles.map(r => r.role)})
//       return this.services.userGroup.create(g)
//     }).then(res => {
//       this.testResponse(res, 'UserGroup:create')
//       this.setCurrentTest('UserGroup:update')
//       return this.services.userGroup.update({id: res.id, name: 'updated', ...res})
//     }).then(res => {
//       this.testResponse(res, 'UserGroup:update')
//       this.setCurrentTest('UserGroup:delete')
//       return this.services.userGroup.delete({id: res.id})
//     }).then(res => {
//       this.testResponse(res === null, 'UserGroup:delete')
//       return res
//     }).catch(err => this.logTestResult(`${this.currentTest}:  ${err}`, false))
//   }
//
//   protected testResponse(condition: boolean|Function|any, msg: string = null): boolean {
//     if (this.showTestResponses) {
//       console.log(msg, condition, typeof condition)
//     }
//     if (typeof condition === 'boolean') {
//       const res = assert(condition, msg)
//       if (res) {
//         this.logTestResult(msg, true)
//       }
//       return res
//     } else if (typeof condition === 'object' && !Array.isArray(condition)) {
//       if (condition['items'] && condition['paginator'] && condition['items'].length > 0) {
//         this.logTestResult(msg, true)
//         return true
//       } else if (condition['items'] && !condition['paginator'] && condition['items'].length > 0 && msg && msg.includes('Stats')) {
//         this.logTestResult(msg, true)
//         return true
//       } else if (condition['id'] > 0) {
//         this.logTestResult(msg, true)
//         return true
//       }
//     } else if (typeof condition === 'object' && Array.isArray(condition.errors) && condition.errors.length > 0) { // todo, also on promise rejection
//       if (condition['errors'] && condition['errors'].length > 0) {
//         this.logTestResult(msg, false)
//         return false
//       }
//     } else if (Array.isArray(condition) && !msg.includes('UserRole')) { // todo standardize roles on api and get rid off these conditions (also on BaseEntityService)
//       if (condition.length > 0) {
//         this.logTestResult(msg, false)
//         return false
//       }
//     } else if (Array.isArray(condition) && msg.includes('UserRole')) {
//       this.logTestResult(msg, true)
//       return true
//     }
//     return false
//   }
//
//   protected logTestStart(entityName: string): void {
//     let msg = ` TESTING ${entityName} `
//     const padChars = Math.floor((this.maxLineChars - msg.length) / 2)
//     msg = msg['padStart'](padChars + msg.length, '█')
//     msg = msg['padEnd'](this.maxLineChars, '█')
//     styledConsoleLog(`<span style="background-color: #0045b0;color: #fff;font-weight: 600;font-size: 1.05rem;">${msg}</span>`)
//   }
//
//   protected logTestResult(msg: string, isSuccess?: boolean): void {
//     msg = isSuccess ? ` ✓ ${msg} ` : ` ✗ ${msg} `
//     const padChars = Math.floor((this.maxLineChars - msg.length) / 2)
//     msg = msg['padStart'](padChars + msg.length, '█')
//     msg = msg['padEnd'](this.maxLineChars, '█')
//     styledConsoleLog(`<span style="${isSuccess ? 'background-color: #228900;color: #9dff7f;font-size: 1rem;' : 'background-color: #ff000c;color: #ffa312;font-weight: 600;font-size: 1.05rem;'}">${msg}</span>`)
//     if (isSuccess) {
//       this.succeededTests++
//     }
//     if (!isSuccess) {
//       this.failedTests++
//     }
//     // console.log(this.allTests, this.succeededTests, this.failedTests)
//     if (!isSuccess && this.stopOnError) {
//       throw Error(msg);
//     }
//   }
//
//   protected getRndStr(): string {
//     if ('performance' in window) {
//       return `${window.performance.now()}`
//     } else {
//       return `${new Date().getTime()}${Math.random() * 100}`
//     }
//   }
// }
//# sourceMappingURL=TestingUtil.js.map