import { OAuthSession } from '../models';
export declare const DEFAULT_CLIENT_KEYS: {
    prod: {
        clientId: string;
        clientSecret: string;
    };
    stage: {
        clientId: string;
        clientSecret: string;
    };
    dev: {
        clientId: string;
        clientSecret: string;
    };
    custom: {
        clientId: string;
        clientSecret: string;
    };
};
export declare const ENDPOINT: {
    GRAPHQL: string;
    GRAPHQL_POST: string;
    TOKEN: string;
    REFRESH_TOKEN: string;
};
export declare const getEndpoint: (endpoint: string, session: OAuthSession) => string;
