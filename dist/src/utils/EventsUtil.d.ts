import { Fn } from '../models';
export declare class Events {
    static dispatch(name: string, data?: any): void;
    static listen(name: string, callback: Fn): void;
}
