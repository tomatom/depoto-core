"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SchemaUtil = exports.SCHEMA_MAX_DEPTH = void 0;
var ENTITIES = __importStar(require("../entities"));
var _1 = require("./");
exports.SCHEMA_MAX_DEPTH = 10;
var SchemaUtil = /** @class */ (function () {
    function SchemaUtil() {
    }
    SchemaUtil.extractFrom = function (entityType, depth, excludedClasses, classPath) {
        if (depth === void 0) { depth = exports.SCHEMA_MAX_DEPTH; }
        if (excludedClasses === void 0) { excludedClasses = []; }
        if (classPath === void 0) { classPath = []; }
        if (depth === 0) {
            return {};
        }
        if (depth === null || depth === undefined || depth > exports.SCHEMA_MAX_DEPTH) {
            depth = exports.SCHEMA_MAX_DEPTH;
        }
        if (!entityType) {
            return;
        }
        // @ts-ignore
        var instance = SchemaUtil.getInstance(entityType);
        var result = {};
        Object.keys(instance).map(function (k) {
            if (Array.isArray(instance[k]) || typeof instance[k] === 'object') {
                var kEntityType = _1.getPropType(instance, k);
                // console.warn(instance[k], k, typeof kEntityType, kEntityType)
                if (!kEntityType) {
                    result[k] = null;
                }
                else if (excludedClasses.includes(kEntityType) || kEntityType === 'local') {
                    // skip! (excludedClasses and local props)
                }
                else if (classPath.includes(kEntityType) || classPath.length >= depth) {
                    // console.warn('in classpath or depth ! ', kEntityType, classPath, depth, classPath.length)
                    var kInstance = SchemaUtil.getInstance(kEntityType);
                    // @ts-ignore
                    if (kInstance && Object.keys(kInstance).includes('id')) {
                        result[k] = { id: null };
                        if (Object.keys(kInstance).includes('name') && _1.getPropType(kInstance, 'name') !== 'local') {
                            result[k].name = null;
                        }
                        if (Object.keys(kInstance).includes('depots') && _1.getPropType(kInstance, 'depots') !== 'local') {
                            result[k].depots = { id: null };
                        }
                    } // else skip!
                }
                else {
                    result[k] = SchemaUtil.extractFrom(kEntityType, depth, excludedClasses, __spreadArray(__spreadArray([], classPath), [entityType]));
                }
            }
            else {
                if (_1.getPropType(instance, k) !== 'local') {
                    result[k] = null;
                }
            }
        });
        return result;
    };
    SchemaUtil.getInstance = function (entityType) {
        if (!entityType || typeof ENTITIES[entityType] !== 'function') {
            if (!!window.location && (window.location.href.includes('local') || window.location.href.includes('stage'))) {
                // throw new Error(`Core:SchemaUtil:getInstance ERROR: no type for ${entityType}!`)
                console.error("Core:SchemaUtil:getInstance ERROR: no type for " + entityType + "!");
                return 'NO TYPE!';
            }
            return null;
        }
        return new ENTITIES[entityType]();
    };
    return SchemaUtil;
}());
exports.SchemaUtil = SchemaUtil;
//# sourceMappingURL=SchemaUtil.js.map