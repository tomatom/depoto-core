import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Voucher } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class VoucherService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Voucher', 'vouchers', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Voucher> {
    return this.getSingleEntity('Voucher', 'voucher', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    voucher: Voucher,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Voucher> {
    const params: any = {
      name: voucher.name,
      code: voucher.code,
      discountType: voucher.discountType,
      discountPercent: voucher.discountPercent,
      discountValue: voucher.discountValue,
      maxUse: voucher.maxUse,
      validFrom: voucher.validFrom,
      validTo: voucher.validTo,
      enabled: voucher.enabled,
      isPayment: voucher.isPayment,
      externalId: voucher.externalId,
    }
    return this.createUpdateEntity(
      'Voucher',
      'createVoucher',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    voucher: Voucher,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Voucher> {
    const params: any = {
      id: voucher.id,
      name: voucher.name,
      code: voucher.code,
      discountType: voucher.discountType,
      discountPercent: voucher.discountPercent,
      discountValue: voucher.discountValue,
      maxUse: voucher.maxUse,
      validFrom: voucher.validFrom,
      validTo: voucher.validTo,
      enabled: voucher.enabled,
      isPayment: voucher.isPayment,
      externalId: voucher.externalId,
    }
    return this.createUpdateEntity(
      'Voucher',
      'updateVoucher',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(voucher: Voucher | any): Promise<Array<any> | any> {
    const params: any = { id: voucher.id }
    return this.deleteEntity('Voucher', 'deleteVoucher', params)
  }
}
