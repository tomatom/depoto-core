import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Consent, Customer, CustomerConsent } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ConsentService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Consent', 'consents', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Consent> {
    return this.getSingleEntity('Consent', 'consent', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    consent: Consent,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Consent> {
    const params: any = { name: consent.name }
    consent.body && consent.body.length > 0 ? (params.body = consent.body) : null
    consent.externalId && consent.externalId.length > 0 ? (params.externalId = consent.externalId) : null
    return this.createUpdateEntity(
      'Consent',
      'createConsent',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    consent: Consent,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Consent> {
    const params: any = {
      id: consent.id,
      name: consent.name,
      body: consent.body,
      externalId: consent.externalId,
    }
    return this.createUpdateEntity(
      'Consent',
      'updateConsent',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(consent: Consent | any): Promise<Array<any> | any> {
    const params: any = { id: consent.id }
    return this.deleteEntity('Consent', 'deleteConsent', params)
  }

  createCustomerConsent(
    customerConsent: CustomerConsent | any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<CustomerConsent> {
    const params: any = {
      consent: customerConsent.consent.id,
      customer: customerConsent.customer.id,
    }
    return this.createUpdateEntity(
      'Consent',
      'createCustomerConsent',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  deleteCustomerConsent(customerConsent: CustomerConsent | any): Promise<Array<any> | any> {
    const params: any = { id: customerConsent.id }
    return this.deleteEntity('CustomerConsent', 'deleteCustomerConsent', params)
  }
}
