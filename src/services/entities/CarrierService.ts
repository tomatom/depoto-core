import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Carrier } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class CarrierService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Carrier', 'carriers', params, definitionDepth, definitionExcludedClasses, schema)
  }

  // not implemented on api
  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Carrier> {
  //   return this.getSingleEntity('Carrier', 'carrier', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // create(carrier: Carrier, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Carrier> {
  //   const params: any = {name: carrier.name};
  //   return this.createUpdateEntity('Carrier', 'createCarrier', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // update(carrier: Carrier, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Carrier> {
  //   const params: any = {
  //     id: carrier.id,
  //     name: carrier.name,
  //   }
  //   return this.createUpdateEntity('Carrier', 'updateCarrier', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // delete(carrier: Carrier|any): Promise<Array<any>|any> {
  //   const params: any = {id: carrier.id}
  //   return this.deleteEntity('Carrier', 'deleteCarrier', params)
  // }
}
