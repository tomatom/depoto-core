import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { UserGroup } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class UserGroupService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('UserGroup', 'userGroups', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<UserGroup> {
    return this.getSingleEntity(
      'UserGroup',
      'userGroup',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    userGroup: UserGroup,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<UserGroup> {
    const params: any = {
      name: userGroup.name,
      roles: userGroup.roles,
    }
    return this.createUpdateEntity(
      'UserGroup',
      'createUserGroup',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    userGroup: UserGroup,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<UserGroup> {
    const params: any = {
      id: userGroup.id,
      name: userGroup.name,
      roles: userGroup.roles,
    }
    return this.createUpdateEntity(
      'UserGroup',
      'updateUserGroup',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(userGroup: UserGroup | any): Promise<Array<any> | any> {
    const params: any = { id: userGroup.id }
    return this.deleteEntity('UserGroup', 'deleteUserGroup', params)
  }
}
