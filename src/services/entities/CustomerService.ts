import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Customer } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class CustomerService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Customer', 'customers', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Customer> {
    return this.getSingleEntity('Customer', 'customer', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    customer: Customer,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Customer> {
    const params: any = {
      email: customer.email,
      firstName: customer.firstName,
      lastName: customer.lastName,
      companyName: customer.companyName,
      wholesale: customer.wholesale,
      priceLevel: customer.priceLevel && customer.priceLevel.id > 0 ? customer.priceLevel.id : null,
      minExpirationDays: customer.minExpirationDays || 0,
    }
    if (customer.phone && customer.phone.length > 0) {
      params.phone = customer.phone
    }
    if (customer.note && customer.note.length > 0) {
      params.note = customer.note
    }
    if (customer.birthday && customer.birthday.length > 0) {
      params.birthday = customer.birthday
    }
    if (customer.users && customer.users.length > 0) {
      params.users = customer.users.map(d => d.id)
    }
    if (customer.tags && customer.tags.length > 0) {
      params.tags = customer.tags.map(t => t.id)
    }
    return this.createUpdateEntity(
      'Customer',
      'createCustomer',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    customer: Customer,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Customer> {
    const params: any = {
      id: customer.id,
      email: customer.email,
      firstName: customer.firstName,
      lastName: customer.lastName,
      companyName: customer.companyName,
      wholesale: customer.wholesale,
      phone: customer.phone,
      note: customer.note,
      birthday: customer.birthday,
      priceLevel: customer.priceLevel && customer.priceLevel.id > 0 ? customer.priceLevel.id : null,
      minExpirationDays: customer.minExpirationDays || 0,
    }
    if (customer.users && customer.users.length > 0) {
      params.users = customer.users.map(d => d.id)
    }
    if (customer.tags && customer.tags.length > 0) {
      params.tags = customer.tags.map(t => t.id)
    }
    return this.createUpdateEntity(
      'Customer',
      'updateCustomer',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(customer: Customer | any): Promise<Array<any> | any> {
    const params: any = { id: customer.id }
    return this.deleteEntity('Customer', 'deleteCustomer', params)
  }
}
