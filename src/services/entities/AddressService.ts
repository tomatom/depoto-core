import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Address, Customer } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class AddressService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Address', 'addresses', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Address> {
    return this.getSingleEntity('Address', 'address', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    address: Address,
    customer?: Customer,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Address> {
    const params: any = {}
    address.companyName && address.companyName.length > 0 ? (params.companyName = address.companyName) : null
    address.firstName && address.firstName.length > 0
      ? (params.firstName = address.firstName)
      : customer && customer.firstName && customer.firstName.length > 0
      ? (params.firstName = customer.firstName)
      : null
    address.lastName && address.lastName.length > 0
      ? (params.lastName = address.lastName)
      : customer && customer.lastName && customer.lastName.length > 0
      ? (params.lastName = customer.lastName)
      : null
    address.street && address.street.length > 0 ? (params.street = address.street) : null
    address.city && address.city.length > 0 ? (params.city = address.city) : null
    address.zip && address.zip.length > 0 ? (params.zip = address.zip) : null
    address.country && address.country.length > 0 ? (params.country = address.country) : null
    address.state && address.state.length > 0 ? (params.state = address.state) : null
    address.ic && address.ic.length > 0 ? (params.ic = address.ic) : null
    address.dic && address.dic.length > 0 ? (params.dic = address.dic) : null
    address.phone && address.phone.length > 0
      ? (params.phone = address.phone)
      : customer && customer.phone && customer.phone.length > 0
      ? (params.phone = customer.phone)
      : null
    address.email && address.email.length > 0
      ? (params.email = address.email)
      : customer && customer.email && customer.email.length > 0
      ? (params.email = customer.email)
      : null
    address.branchId && address.branchId.length > 0 ? (params.branchId = address.branchId) : null
    address.externalId && address.externalId.length > 0 ? (params.externalId = address.externalId) : null
    address.customData && address.customData.length > 0 ? (params.customData = address.customData) : null
    customer ? (params.customer = Number(customer.id)) : null
    params.isStored = !!address.isStored
    params.isBilling = !!address.isBilling
    params.note = address.note
    return this.createUpdateEntity(
      'Address',
      'createAddress',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    address: Address,
    customer?: Customer,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Address> {
    const params: any = {
      id: address.id,
      customer: customer && customer.id > 0 ? Number(customer.id) : null,
      companyName: address.companyName && address.companyName.length > 0 ? address.companyName : null,
      firstName:
        address.firstName && address.firstName.length > 0
          ? address.firstName
          : customer?.firstName && customer?.firstName.length > 0
          ? customer?.firstName
          : null,
      lastName:
        address.lastName && address.lastName.length > 0
          ? address.lastName
          : customer?.lastName && customer?.lastName.length > 0
          ? customer?.lastName
          : null,
      street: address.street && address.street.length > 0 ? address.street : null,
      city: address.city && address.city.length > 0 ? address.city : null,
      zip: address.zip && address.zip.length > 0 ? address.zip : null,
      country: address.country && address.country.length > 0 ? address.country : null,
      state: address.state && address.state.length > 0 ? address.state : null,
      ic: address.ic && address.ic.length > 0 ? address.ic : null,
      dic: address.dic && address.dic.length > 0 ? address.dic : null,
      phone:
        address.phone && address.phone.length > 0
          ? address.phone
          : customer?.phone && customer?.phone.length > 0
          ? customer?.phone
          : null,
      email:
        address.email && address.email.length > 0
          ? address.email
          : customer?.email && customer?.email.length > 0
          ? customer?.email
          : null,
      branchId: address.branchId && address.branchId.length > 0 ? address.branchId : null,
      externalId: address.externalId && address.externalId.length > 0 ? address.externalId : null,
      customData: address.customData && address.customData.length > 0 ? address.customData : null,
      note: address.note && address.note.length > 0 ? address.note : null,
      isStored: !!address.isStored,
      isBilling: !!address.isBilling,
    }
    return this.createUpdateEntity(
      'Address',
      'updateAddress',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(address: Address | any): Promise<Array<any> | any> {
    const params: any = { id: address.id }
    return this.deleteEntity('Address', 'deleteAddress', params)
  }
}
