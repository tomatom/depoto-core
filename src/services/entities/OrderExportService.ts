import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { OrderExport } from '../../entities'
import { LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class OrderExportService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('OrderExport', 'exportOrders', params, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    orderExport: OrderExport,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<OrderExport> {
    const params: any = {
      dateFrom: orderExport.dateFrom,
      dateTo: orderExport.dateTo,
      status: orderExport.status,
      checkouts: orderExport.checkouts,
      format: orderExport.format,
    }
    return this.createUpdateEntity(
      'OrderExport',
      'createExportOrder',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }
}
