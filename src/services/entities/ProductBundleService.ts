import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { ProductBundle } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProductBundleService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  // not implemented on api
  // getList(parameters: Object|any = {}, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Object|any> {
  //   const params = {
  //     page: parameters.page > 0 ? parameters.page : 1,
  //     sort: !!parameters.sort ? parameters.sort : 'id',
  //     direction: !!parameters.direction ? parameters.direction : 'DESC',
  //     filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
  //   }
  //   return this.getEntityList('ProductBundle', 'productBundles', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductBundle> {
  //   return this.getSingleEntity('ProductBundle', 'productBundle', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }

  create(
    productBundle: ProductBundle,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductBundle> {
    const params: any = {
      parent: productBundle.parent?.id,
      child: productBundle.child?.id,
      quantity: productBundle.quantity,
    }
    return this.createUpdateEntity(
      'ProductBundle',
      'createProductBundle',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    productBundle: ProductBundle,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductBundle> {
    const params: any = {
      id: productBundle.id,
      parent: productBundle.parent?.id,
      child: productBundle.child?.id,
      quantity: productBundle.quantity,
    }
    return this.createUpdateEntity(
      'ProductBundle',
      'updateProductBundle',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(productBundle: ProductBundle | any): Promise<Array<any> | any> {
    const params: any = { id: productBundle.id }
    return this.deleteEntity('ProductBundle', 'deleteProductBundle', params)
  }
}
