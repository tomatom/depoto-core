import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'
import { Category } from '../../entities'

export class CategoryService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }
  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'position',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Category', 'categories', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Category> {
    return this.getSingleEntity('Category', 'category', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    category: Category,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Category> {
    const params: any = {
      name: category.name,
      text: category.text,
      parent: category.parent ? category.parent.id : null,
      externalId: category.externalId,
      position: category.position,
      tags: category.tags,
    }
    return this.createUpdateEntity(
      'Category',
      'createCategory',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    category: Category,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Category> {
    const params: any = {
      id: category.id,
      name: category.name,
      text: category.text,
      parent: category.parent ? category.parent.id : null,
      externalId: category.externalId,
      position: category.position,
      tags: category.tags,
    }
    return this.createUpdateEntity(
      'Category',
      'updateCategory',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(category: Category | any): Promise<Array<any> | any> {
    const params: any = { id: category.id }
    return this.deleteEntity('Category', 'deleteCategory', params)
  }
}
