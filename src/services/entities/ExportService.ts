import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Export } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ExportService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getTypes(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('ExportType', 'exportTypes', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Export', 'exports', params, definitionDepth, definitionExcludedClasses, schema)
  }

  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Export> {
  //   return this.getSingleEntity('Export', 'export', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }

  create(
    exportObject: Export,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Export> {
    const params: any = {
      type: exportObject.type?.id,
      note: exportObject.note,
      filter: exportObject.filter,
    }
    return this.createUpdateEntity('Export', 'createExport', params, definitionDepth, definitionExcludedClasses, schema)
  }

  // delete(exportObject: Export|any): Promise<Array<any>|any> {
  //   const params: any = {id: exportObject.id}
  //   return this.deleteEntity('Export', 'deleteExport', params)
  // }
}
