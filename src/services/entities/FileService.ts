import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { File } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class FileService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('File', 'files', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<File> {
    return this.getSingleEntity('File', 'file', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    file: File,
    productId?: number,
    orderId?: number,
    inventoryId?: number,
    productMovePackId?: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<File> {
    const params: any = {
      text: file.text,
      originalFilename: file.originalFilename,
      mimeType: file.mimeType,
      base64Data: file.base64Data,
    }
    if (productId && productId > 0) {
      params.product = productId
    }
    if (orderId && orderId > 0) {
      params.order = orderId
    }
    if (inventoryId && inventoryId > 0) {
      params.inventory = inventoryId
    }
    if (productMovePackId && productMovePackId > 0) {
      params.productMovePack = productMovePackId
    }
    return this.createUpdateEntity('File', 'createFile', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    file: File,
    productId?: number,
    orderId?: number,
    inventoryId?: number,
    productMovePackId?: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<File> {
    const params: any = {
      id: file.id,
    }
    if (productId && productId > 0) {
      params.product = productId
    }
    if (orderId && orderId > 0) {
      params.order = orderId
    }
    if (inventoryId && inventoryId > 0) {
      params.inventory = inventoryId
    }
    if (productMovePackId && productMovePackId > 0) {
      params.productMovePack = productMovePackId
    }
    return this.createUpdateEntity('File', 'updateFile', params, definitionDepth, definitionExcludedClasses, schema)
  }

  delete(file: File | any): Promise<Array<any> | any> {
    const params: any = { id: file.id }
    return this.deleteEntity('File', 'deleteFile', params)
  }
}
