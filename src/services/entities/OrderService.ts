import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import {
  ClearanceItem,
  Order,
  OrderGroup,
  OrderItem,
  PaymentItem,
  ProcessStatus,
  ProcessStatusType,
} from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { VatService } from './'
import { DateUtil } from '../../utils'
import { BaseEntityService } from '../entities'

export class OrderService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  protected vatService: VatService
  constructor(config: EntityServiceConfiguration, vatService: VatService) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
    this.vatService = vatService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Order', 'orders', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getProcessStates(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'ProcessStatus',
      'processStatuses',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getOrderItems(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('OrderItem', 'orderItems', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    return this.getSingleEntity('Order', 'order', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    order: Order,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      checkout: order.checkout?.id,
      status: 'reservation',
      note: order.note,
      privateNote: order.privateNote,
      dateCreated: DateUtil.getDateStringFromUTCString(new Date().toUTCString()),
      items: this.prepareOrderItems(order),
      paymentItems: this.preparePaymentItems(order),
      customer: order.customer && order.customer.id > 0 ? order.customer.id : null,
      priority: order.priority,
    }
    if (order.carrier && order.carrier.id && order.carrier.id.length > 0) {
      params.carrier = order.carrier.id
    }
    if (order.invoiceAddress && order.invoiceAddress.id > 0) {
      params.invoiceAddress = order.invoiceAddress.id
    }
    if (order.shippingAddress && order.shippingAddress.id > 0) {
      params.shippingAddress = order.shippingAddress.id
    }
    if (order.reservationNumber && order.reservationNumber > 0) {
      params.reservationNumber = order.reservationNumber
    }
    if (order.group && order.group.id > 0) {
      params.group = order.group.id
    }
    if (order.groupPosition && order.groupPosition > 0) {
      params.groupPosition = order.groupPosition
    }
    if (order.tags && order.tags.length > 0) {
      params.tags = order.tags.map(t => t.id)
    }
    if (order.currency && order.currency?.id?.length > 0) {
      params.currency = order.currency.id
    }
    if (order.dateExpedition?.length > 0) {
      params.dateExpedition = order.dateExpedition
    }
    if (order.dateDue?.length > 0) {
      params.dateDue = order.dateDue
    }
    if (order.dateTax?.length > 0) {
      params.dateTax = order.dateTax
    }
    return this.createUpdateEntity('Order', 'createOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    order: Order,
    keepReservation = false,
    dontSendPayments = false,
    updateProcessStatus?: string,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
      status: keepReservation ? 'reservation' : 'bill',
      note: order.note,
      privateNote: order.privateNote,
      items: this.prepareOrderItems(order),
      boxes: order.boxes && order.boxes.length > 0 ? order.boxes : [],
      // reservationNumber: order.reservationNumber,
      dateCreated: DateUtil.getDateStringFromUTCString(order.dateCreated),
      customer: order.customer && order.customer.id > 0 ? order.customer.id : null,
      priority: order.priority,
      dateExpedition: order.dateExpedition,
      dateDue: order.dateDue,
      dateTax: order.dateTax,
      shippingAddress: order.shippingAddress?.id || null,
      invoiceAddress: order.invoiceAddress?.id || null,
      group: order.group?.id || null,
      groupPosition: order.groupPosition || null,
    }
    if (!dontSendPayments) {
      params.paymentItems = this.preparePaymentItems(order)
    }
    if (updateProcessStatus && updateProcessStatus.length > 0) {
      params.processStatusRelation = { status: updateProcessStatus }
    }
    if (order.carrier && order.carrier.id && order.carrier.id.length > 0) {
      params.carrier = order.carrier.id
    }
    if (order.tags && order.tags.length > 0) {
      params.tags = order.tags.map(t => t.id)
    }
    if (order.currency && order.currency?.id?.length > 0) {
      params.currency = order.currency.id
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  updateOrderPart(
    order: Partial<Order>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
    }
    if (!!order.status?.id) {
      params.status = order.status.id
    }
    if (!!order.billNumber) {
      params.billNumber = order.billNumber
    }
    if (!!order.reservationNumber) {
      params.reservationNumber = order.reservationNumber
    }
    if (!!order.dateCreated) {
      params.dateCreated = DateUtil.getDateStringFromUTCString(order.dateCreated)
    }
    if (!!order.note) {
      params.note = order.note
    }
    if (!!order.privateNote) {
      params.privateNote = order.privateNote
    }
    if (order.boxes && order.boxes.length > 0) {
      params.boxes = order.boxes
    }
    if (order.items && order.items.length > 0) {
      params.items = this.prepareOrderItems(order)
    }
    if (order.paymentItems && order.paymentItems.length > 0) {
      params.paymentItems = this.preparePaymentItems(order)
    }
    if (order.customer && order.customer.id > 0) {
      params.customer = order.customer.id
    }
    if (order.priority && order.priority > 0) {
      params.priority = order.priority
    }
    if (order.carrier && order.carrier.id && order.carrier.id.length > 0) {
      params.carrier = order.carrier.id
    }
    if (order.invoiceAddress && order.invoiceAddress.id > 0) {
      params.invoiceAddress = order.invoiceAddress.id
    }
    if (order.shippingAddress && order.shippingAddress.id > 0) {
      params.shippingAddress = order.shippingAddress.id
    }
    if (order.group && order.group.id > 0) {
      params.group = order.group.id
    }
    if (order.groupPosition && order.groupPosition > 0) {
      params.groupPosition = order.groupPosition
    }
    if (order.tags && order.tags.length > 0) {
      params.tags = order.tags.map(t => t.id)
    }
    if (order.currency && order.currency?.id?.length > 0) {
      params.currency = order.currency.id
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  updateOrderNotes(
    order: Partial<Order>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
    }
    if (order.note && order.note.length > 0) {
      params.note = order.note
    }
    if (order.privateNote && order.privateNote.length > 0) {
      params.privateNote = order.privateNote
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  updateOrderCarrier(
    order: Partial<Order>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
      carrier: order.carrier?.id,
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  // TODO: moves? updateClearanceItems {id, picked: num, packed: num, packageId: optional}
  updateOrderPickedPacked(
    order: Order,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
      items: order.items.map(i => ({
        id: i.id,
        picked: i.picked,
        packed: i.packed,
      })),
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  updateOrderItemPart(
    orderItemPart: Partial<OrderItem>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<OrderItem> {
    const params: any = {
      id: orderItemPart.id,
      name: orderItemPart.name,
      type: orderItemPart.type,
      ean: orderItemPart.ean,
      code: orderItemPart.code,
      quantity: orderItemPart.quantity,
      note: orderItemPart.note,
      price: orderItemPart.price,
      sale: orderItemPart.sale,
      vat: orderItemPart.vat?.id,
      product: orderItemPart.product?.id,
      serial: orderItemPart.serial,
      expirationDate: orderItemPart.expirationDate,
      batch: orderItemPart.batch,
      isForSubsequentSettlement: orderItemPart.isForSubsequentSettlement,
      picked: orderItemPart.picked,
      packed: orderItemPart.packed,
    }
    Object.keys(params).forEach((k, i) => {
      if (params[k] === undefined) {
        delete params[k]
      }
    })
    return this.createUpdateEntity(
      'OrderItem',
      'updateOrderItem',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updateOrderBoxes(
    order: Partial<Order>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
      boxes: order.boxes && order.boxes.length > 0 ? order.boxes : [],
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  updateProcessStatus(
    order: Partial<Order>,
    processStatus:
      | 'received'
      | 'recieved'
      | 'picking'
      | 'packing'
      | 'packed'
      | 'dispatched'
      | 'delivered'
      | 'returned'
      | 'picking_error'
      | 'cancelled',
    note: string,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
      processStatusRelation: { status: processStatus },
    }
    if (note && note.length > 0) {
      params.processStatusRelation.note = note
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  updateOrderGroupAndPosition(
    order: Partial<Order>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: order.id,
      group: !!order.group && order.group.id ? order.group.id : null,
      groupPosition: order.groupPosition,
    }
    return this.createUpdateEntity('Order', 'updateOrder', params, definitionDepth, definitionExcludedClasses, schema)
  }

  updateClearanceItem(
    clearanceItem: Partial<ClearanceItem>,
    orderItem: Partial<OrderItem>,
    type: string,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: clearanceItem.id,
      orderItem: orderItem.id,
    }
    params[type] = clearanceItem[type]
    return this.createUpdateEntity(
      'ClearanceItem',
      'updateClearanceItem',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updateClearanceItemFromOrder(
    clearanceItem: Partial<ClearanceItem>,
    order: Partial<Order>,
    type: string,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: clearanceItem.id,
      order: order.id,
    }
    params[type] = clearanceItem[type]
    return this.createUpdateEntity(
      'ClearanceItem',
      'updateClearanceItem',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updateClearanceItemFromOrderGroup(
    clearanceItem: Partial<ClearanceItem>,
    orderGroup: Partial<OrderGroup>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: clearanceItem.id,
      orderGroup: orderGroup.id,
      picked: clearanceItem.picked,
      packed: clearanceItem.packed,
    }
    return this.createUpdateEntity(
      'ClearanceItem',
      'updateClearanceItem',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updateClearanceItemLocations(
    clearanceItem: ClearanceItem | any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Order> {
    const params: any = {
      id: clearanceItem.id,
      locations: clearanceItem.locations || [],
    }
    if (clearanceItem.orderItem) {
      params.orderItem = clearanceItem.orderItem
    }
    if (clearanceItem.order) {
      params.order = clearanceItem.order
    }
    return this.createUpdateEntity(
      'ClearanceItem',
      'updateClearanceItem',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  deleteOrderItem(orderItem: OrderItem): Promise<Order> {
    const params: any = {
      id: orderItem.id,
    }
    return this.deleteEntity('OrderItem', 'deleteOrderItem', params)
  }

  deleteReservation(order: Order | any): Promise<Array<any> | any> {
    const params: any = { id: order.id }
    return this.deleteEntity('Order', 'deleteReservation', params)
  }

  // delete(order: Order|any): Promise<Array<any>|any> {
  //   const params: any = {id: order.id}
  //   return this.deleteEntity('Order', 'deleteOrder', params)
  // }

  getOrderGroups(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('OrderGroup', 'orderGroups', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getOrderGroupById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<OrderGroup> {
    return this.getSingleEntity(
      'OrderGroup',
      'orderGroup',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  createOrderGroup(
    orderGroup: OrderGroup,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<OrderGroup> {
    const params: any = { name: orderGroup.name }
    if (!!orderGroup.user && orderGroup.user.id > 0) {
      params.user = orderGroup.user.id
    }
    if (!isNaN(orderGroup.userPosition)) {
      params.userPosition = orderGroup.userPosition
    }
    return this.createUpdateEntity(
      'OrderGroup',
      'createOrderGroup',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updateOrderGroup(
    orderGroup: OrderGroup,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<OrderGroup> {
    const params: any = {
      id: orderGroup.id,
      name: orderGroup.name,
      user: !!orderGroup.user && orderGroup.user.id > 0 ? orderGroup.user.id : null,
      userPosition: !isNaN(orderGroup.userPosition) ? orderGroup.userPosition : 0,
    }
    return this.createUpdateEntity(
      'OrderGroup',
      'updateOrderGroup',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  deleteOrderGroup(orderGroup: OrderGroup): Promise<Array<any> | any> {
    const params: any = { id: orderGroup.id }
    return this.deleteEntity('OrderGroup', 'deleteOrderGroup', params)
  }

  protected preparePaymentItems(order: Partial<Order>) {
    const paymentItems: any[] = []
    order.paymentItems?.forEach((pi: PaymentItem) => {
      const p: any = {
        payment: Number(pi.payment?.id),
        amount: pi.amount,
        dateCreated: DateUtil.getDateStringFromUTCString(pi.dateCreated),
        isPaid: pi.isPaid,
      }
      if (pi.checkout && pi.checkout.id > 0) {
        p.checkout = pi.checkout.id
      }
      if (pi.dateCancelled) {
        p.dateCancelled = DateUtil.getDateStringFromUTCString(pi.dateCancelled)
      }
      paymentItems.push(p)
    })
    return paymentItems
  }

  protected prepareOrderItems(order: Partial<Order>) {
    const orderItems: any[] = []
    order.items?.forEach((orderItem: OrderItem) => {
      const oi: any = {
        name: orderItem.product ? orderItem.product.fullName : orderItem.name,
        quantity: orderItem.quantity,
        type: orderItem.type ? orderItem.type : 'product',
        sale: Number(orderItem.sale).toFixed(2),
        price: Number(orderItem.price) > 0 ? Number(orderItem.price).toFixed(3) : Number(orderItem.price),
        vat: orderItem.vat && orderItem.vat.id > 0 ? orderItem.vat.id : this.vatService.defaultVat!.id,
        isForSubsequentSettlement: orderItem.isForSubsequentSettlement,
      }
      if (orderItem.id && orderItem.id > 0) {
        oi.id = orderItem.id
      }
      if (orderItem.product && orderItem.product.id > 0) {
        oi.product = orderItem.product && orderItem.product.id > 0 ? orderItem.product.id : orderItem.id
      }
      if (orderItem.ean && orderItem.ean.length > 0) {
        oi.ean = orderItem.ean
      }
      if (orderItem.code && orderItem.code.length > 0) {
        oi.code = orderItem.code
      }
      if (orderItem.serial && orderItem.serial.length > 0) {
        oi.serial = orderItem.serial
      }
      if (orderItem.note && orderItem.note.length > 0) {
        oi.note = orderItem.note
      }
      orderItems.push(oi)
    })
    return orderItems
  }
}
