import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Producer } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProducerService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Producer', 'producers', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Producer> {
    return this.getSingleEntity('Producer', 'producer', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    producer: Producer,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Producer> {
    const params: any = { name: producer.name }
    return this.createUpdateEntity(
      'Producer',
      'createProducer',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    producer: Producer,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Producer> {
    const params: any = {
      id: producer.id,
      name: producer.name,
    }
    return this.createUpdateEntity(
      'Producer',
      'updateProducer',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(producer: Producer | any): Promise<Array<any> | any> {
    const params: any = { id: producer.id }
    return this.deleteEntity('Producer', 'deleteProducer', params)
  }
}
