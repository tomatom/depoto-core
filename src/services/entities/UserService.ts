import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { User, UserGroup } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'
import { Observable } from '../../utils'

export class UserService extends BaseEntityService {
  user?: User
  currentUserEmitter: Observable<User> = new Observable('core:userService:currentUserEmitter')
  roles: string[] = []
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
    this.listenForEvents()
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('User', 'users', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<User> {
    return this.getSingleEntity('User', 'user', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  getMyProfile(definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<User> {
    return this.getSingleEntity('User', 'userMyProfile', {}, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    user: User,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<User> {
    const params: any = {
      email: user.email,
      plainPassword: user.plainPassword,
      firstName: user.firstName,
      lastName: user.lastName,
      phone: user.phone,
      groups: user.groups.map(g => g.id),
      pin: user.pin,
      enabled: true,
    }
    if (user.checkouts && user.checkouts.length > 0) {
      params.checkouts = user.checkouts.map(u => u.id)
    }
    return this.createUpdateEntity('User', 'createUser', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    user: User,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<User> {
    const params: any = {
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      phone: user.phone,
      groups: user.groups.map(g => g.id),
      checkouts: user.checkouts.map(u => u.id),
      pin: user.pin,
    }
    if (user.plainPassword && user.plainPassword.length > 0) {
      params.plainPassword = user.plainPassword
    }
    return this.createUpdateEntity('User', 'updateUser', params, definitionDepth, definitionExcludedClasses, schema)
  }

  delete(user: User | any): Promise<Array<any> | any> {
    const params: any = { id: user.id }
    return this.deleteEntity('User', 'deleteUser', params)
  }

  async setCurrentUser(userEmail?: string) {
    const user = !!userEmail
      ? await this.getMyProfile(2, [], {
          id: null,
          email: null,
          firstName: null,
          lastName: null,
          name: null,
          phone: null,
          groups: {
            id: null,
            name: null,
            roles: null,
          },
          roles: null,
          pin: null,
          company: {
            id: null,
            name: null,
            ic: null,
            dic: null,
            email: null,
            phone: null,
            street: null,
            city: null,
            zip: null,
            country: null,
            registrationNote: null,
            nextEan: null,
            billLogo: null,
            parent: {
              id: null,
              name: null,
              ic: null,
              dic: null,
              parent: {
                id: null,
                name: null,
              },
              carrierRelations: {
                id: null,
              },
            },
            children: {
              id: null,
              name: null,
              ic: null,
              dic: null,
              children: {
                id: null,
                name: null,
              },
              carrierRelations: {
                id: null,
              },
            },
            carrierRelations: {
              id: null,
              enable: null,
              options: null,
              carrier: {
                id: null,
                name: null,
              },
              checkout: {
                id: null,
                name: null,
                depots: {
                  id: null,
                },
              },
              externalId: null,
            },
          },
          enabled: null,
          checkouts: {
            id: null,
            name: null,
            payments: {
              id: null,
              name: null,
              type: {
                id: null,
                name: null,
              },
              eetEnable: null,
            },
          },
        })
      : null
    if (!user) {
      this.user = undefined
      this.roles = []
      this.s.set(this.s.keys.auth.company, null)
    } else {
      this.user = new User(user)
      this.extractAllRoles()
      this.s.set(this.s.keys.auth.company, user.company)
    }
    this.loggerService.log({
      type: EVENT.USER,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: user,
    })
    this.currentUserEmitter.emit(this.user)
  }

  hasRole(roleNeeded: string): boolean {
    if (this.roles && this.roles.length > 0) {
      return this.roles.includes(roleNeeded)
    } else {
      return false
    }
  }

  hasOneOfTheRoles(listOfRoles: string[]): boolean {
    if (this.roles && this.roles.length > 0) {
      for (const roleNeeded of listOfRoles) {
        if (this.roles.includes(roleNeeded)) {
          return true
        }
      }
    }
    return false
  }

  protected listenForEvents() {
    this.oauthService.onLoginEmitter.subscribe().then(email => {
      this.setCurrentUser(email)
    })

    this.oauthService.onLogoutEmitter.subscribe().then(() => {
      this.setCurrentUser(undefined)
    })

    this.loggerService.log({
      type: EVENT.USER,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        message: 'UserService:listeningForEvents',
      },
    })
  }

  protected extractAllRoles() {
    if (this.user && this.user.groups && this.user.groups.length > 0) {
      this.roles = []
      this.user.groups.forEach((group: UserGroup) => {
        group.roles.forEach((role: string) => {
          this.roles.push(role)
        })
      })
      this.s.set(this.s.keys.services.userRoles, this.roles)
    }
  }
}
