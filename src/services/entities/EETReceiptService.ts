import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { EETReceipt } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class EETReceiptService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('EETReceipt', 'eetReceipts', params, definitionDepth, definitionExcludedClasses, schema)
  }

  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<EETReceipt> {
  //   return this.getSingleEntity('EETReceipt', 'receipt', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }

  create(
    receipt: EETReceipt,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETReceipt> {
    const params: any = {
      checkout: receipt.checkoutEetId,
      number: receipt.number,
      dateCreated: receipt.dateCreated,
      currency: 'CZK',
      totalPrice: receipt.totalPrice,
      // priceZeroVat: null // todo: unused mutation
      // priceStandardVat: Int
      // vatStandard: Int
      // priceFirstReducedVat: Int
      // vatFirstReduced: Int
      // priceSecondReducedVat: Int
      // vatSecondReduced: Int
      // priceForSubsequentSettlement: Int
      // priceUsedSubsequentSettlement: Int
    }
    return this.createUpdateEntity(
      'EETReceipt',
      'createEetReceipt',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  send(
    receipt: EETReceipt,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETReceipt> {
    const params: any = {
      id: receipt.id,
    }
    return this.createUpdateEntity(
      'EETReceipt',
      'sendEetReceipt',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  // delete(receipt: EETReceipt|any): Promise<Array<any>|any> {
  //   const params: any = {id: receipt.id}
  //   return this.deleteEntity('EETReceipt', 'deleteEetReceipt', params)
  // }
}
