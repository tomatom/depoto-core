import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Supplier } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class SupplierService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Supplier', 'suppliers', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Supplier> {
    return this.getSingleEntity('Supplier', 'supplier', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    supplier: Supplier,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Supplier> {
    const params: any = { name: supplier.name }
    return this.createUpdateEntity(
      'Supplier',
      'createSupplier',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    supplier: Supplier,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Supplier> {
    const params: any = {
      id: supplier.id,
      name: supplier.name,
    }
    return this.createUpdateEntity(
      'Supplier',
      'updateSupplier',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(supplier: Supplier | any): Promise<Array<any> | any> {
    const params: any = { id: supplier.id }
    return this.deleteEntity('Supplier', 'deleteSupplier', params)
  }
}
