import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Vat } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class VatService extends BaseEntityService {
  public defaultVat?: Vat
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
    if (this.oauthService.isAuthenticated()) {
      this.getDefaultVat()
    }
    this.listenForEvents()
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Vat', 'vats', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Vat> {
    return this.getSingleEntity('Vat', 'vat', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  getDefaultVat(definitionDepth?: number, definitionExcludedClasses: string[] = []): Promise<Vat | null | undefined> {
    return this.getList({}, definitionDepth, definitionExcludedClasses).then(res => {
      if (!res || !res.items) {
        return null
      }
      const v = res.items.filter(v => v.default)
      if (v.length === 1) {
        this.defaultVat = new Vat(v[0])
        this.loggerService.log({
          type: EVENT.VAT,
          origin: this.constructor['name'],
          trace: this.loggerService.getStackTrace(),
          data: {
            message: 'default VAT loaded',
            defaultVat: this.defaultVat,
          },
        })
        return this.defaultVat
      }
    })
  }

  create(
    vat: Vat,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Vat> {
    const params: any = {
      name: vat.name,
      percent: vat.percent,
      default: vat.default,
    }
    return this.createUpdateEntity('Vat', 'createVat', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    vat: Vat,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Vat> {
    const params: any = {
      id: vat.id,
      name: vat.name,
      percent: vat.percent,
      default: vat.default,
    }
    return this.createUpdateEntity('Vat', 'updateVat', params, definitionDepth, definitionExcludedClasses, schema)
  }

  delete(vat: Vat | any): Promise<Array<any> | any> {
    const params: any = { id: vat.id }
    return this.deleteEntity('Vat', 'deleteVat', params)
  }

  protected listenForEvents() {
    this.oauthService.onLoginEmitter.subscribe().then(() => {
      this.getDefaultVat()
    })

    this.oauthService.onLogoutEmitter.subscribe().then(() => {
      this.defaultVat = undefined
    })

    this.loggerService.log({
      type: EVENT.VAT,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        message: 'VatService:listeningForEvents',
      },
    })
  }
}
