import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Parameter } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ParameterService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Parameter', 'parameters', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Parameter> {
    return this.getSingleEntity(
      'Parameter',
      'parameter',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    parameter: Parameter,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Parameter> {
    const params: any = {
      name: parameter.name,
      type: parameter.type,
      enumValues: parameter.enumValues,
      unit: parameter.unit,
      text: parameter.text,
      externalId: parameter.externalId,
    }
    return this.createUpdateEntity(
      'Parameter',
      'createParameter',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    parameter: Parameter,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Parameter> {
    const params: any = {
      id: parameter.id,
      name: parameter.name,
      type: parameter.type,
      enumValues: parameter.enumValues,
      unit: parameter.unit,
      text: parameter.text,
      externalId: parameter.externalId,
    }
    return this.createUpdateEntity(
      'Parameter',
      'updateParameter',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(parameter: Parameter | any): Promise<Array<any> | any> {
    const params: any = { id: parameter.id }
    return this.deleteEntity('Parameter', 'deleteParameter', params)
  }
}
