import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'
import { Observer } from '../../utils'

export class BaseEventService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  subscribe(): Observer {
    return super.subscribeToEvents()
  }

  unsubscribe(): void {
    return super.unsubscribeFromEvents()
  }
}
