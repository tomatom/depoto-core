import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { StatsBestseller, StatsCarrierUsage, StatsPaymentSale } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class StatsService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getStatsBestsellers(
    from: string,
    to: string,
    checkoutId?: number,
    results?: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params: any = {
      from: from,
      to: to,
    }
    if (checkoutId && checkoutId > 0) {
      params.checkout = checkoutId
    }
    if (results && results > 0) {
      params.results = results
    }
    return this.getEntityList(
      'StatsBestseller',
      'statsBestsellers',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getStatsCarrierUsage(
    from: string,
    to: string,
    checkoutId?: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params: any = {
      from: from,
      to: to,
    }
    if (checkoutId && checkoutId > 0) {
      params.checkout = checkoutId
    }
    return this.getEntityList(
      'StatsCarrierUsage',
      'statsCarrierUsage',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getStatsPaymentSale(
    from: string,
    to: string,
    checkoutId?: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params: any = {
      from: from,
      to: to,
    }
    if (checkoutId && checkoutId > 0) {
      params.checkout = checkoutId
    }
    return this.getEntityList(
      'StatsPaymentSale',
      'statsPaymentSales',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }
}
