import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { PriceLevel } from '../../entities'
import { LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from './BaseEntityService'

export class PriceLevelService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(parameters: any = {}, definitionDepth?: number, definitionExcludedClass: string[] = []): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('PriceLevel', 'priceLevels', params, definitionDepth, definitionExcludedClass)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<PriceLevel> {
    return this.getSingleEntity('PriceLevel', 'priceLevel', { id }, definitionDepth, definitionExcludedClasses)
  }

  create(
    priceLevel: PriceLevel,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<PriceLevel> {
    const params: any = {
      name: priceLevel.name,
      isPercentage: priceLevel.isPercentage,
      percent: priceLevel.percent,
      currency: priceLevel.currency ? priceLevel.currency.id : null,
      externalId: priceLevel.externalId,
      withVat: priceLevel.withVat,
    }
    return this.createUpdateEntity(
      'PriceLevel',
      'createPriceLevel',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    priceLevel: PriceLevel,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<PriceLevel> {
    const params: any = {
      id: priceLevel.id,
      name: priceLevel.name,
      isPercentage: priceLevel.isPercentage,
      percent: priceLevel.percent,
      withVat: priceLevel.withVat,
      currency: priceLevel.currency ? priceLevel.currency.id : null,
      externalId: priceLevel.externalId,
    }
    return this.createUpdateEntity(
      'PriceLevel',
      'updatePriceLevel',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(priceLevel: PriceLevel | any): Promise<Array<any> | any> {
    const params: any = { id: priceLevel.id }
    return this.deleteEntity('PriceLevel', 'deletePriceLevel', params)
  }
}
