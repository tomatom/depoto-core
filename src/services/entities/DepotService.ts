import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Depot } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class DepotService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Depot', 'depots', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Depot> {
    return this.getSingleEntity('Depot', 'depot', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    depot: Depot,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Depot> {
    const params: any = { name: depot.name, emailIn: depot.emailIn || '' }
    return this.createUpdateEntity('Depot', 'createDepot', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    depot: Depot,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Depot> {
    const params: any = {
      id: depot.id,
      name: depot.name,
      emailIn: depot.emailIn || '',
    }
    return this.createUpdateEntity('Depot', 'updateDepot', params, definitionDepth, definitionExcludedClasses, schema)
  }

  delete(depot: Depot | any): Promise<Array<any> | any> {
    const params: any = { id: depot.id }
    return this.deleteEntity('Depot', 'deleteDepot', params)
  }
}
