import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { ProductPrice } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProductPriceService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  // not implemented on api
  // getList(parameters: Object|any = {}, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Object|any> {
  //   const params = {
  //     page: parameters.page > 0 ? parameters.page : 1,
  //     sort: !!parameters.sort ? parameters.sort : 'id',
  //     direction: !!parameters.direction ? parameters.direction : 'DESC',
  //     filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
  //   }
  //   return this.getEntityList('ProductPrice', 'productPrices', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductPrice> {
  //   return this.getSingleEntity('ProductPrice', 'productPrice', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }

  create(
    productPrice: ProductPrice,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPrice> {
    const params: any = {
      product: productPrice.product.id,
      sellPrice: productPrice.sellPrice,
      dateFrom: productPrice.dateFrom,
      dateTo: productPrice.dateTo,
      available: productPrice.available,
      note: productPrice.note,
    }
    return this.createUpdateEntity(
      'ProductPrice',
      'createProductPrice',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    productPrice: ProductPrice,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPrice> {
    const params: any = {
      id: productPrice.id,
      product: productPrice.product.id,
      sellPrice: productPrice.sellPrice,
      dateFrom: productPrice.dateFrom,
      dateTo: productPrice.dateTo,
      available: productPrice.available,
      note: productPrice.note,
    }
    return this.createUpdateEntity(
      'ProductPrice',
      'updateProductPrice',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(productPrice: ProductPrice | any): Promise<Array<any> | any> {
    const params: any = { id: productPrice.id }
    return this.deleteEntity('ProductPrice', 'deleteProductPrice', params)
  }
}
