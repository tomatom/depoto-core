import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { CompanyCarrier } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class CompanyCarrierService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'CompanyCarrier',
      'companyCarriers',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<CompanyCarrier> {
    return this.getSingleEntity(
      'CompanyCarrier',
      'companyCarrier',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    companyCarrier: CompanyCarrier,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<CompanyCarrier> {
    const params: any = {
      carrier: companyCarrier.carrier?.id,
    }
    if (companyCarrier.options && companyCarrier.options.length > 0) {
      params.options = companyCarrier.options
    }
    if (companyCarrier.checkout && companyCarrier.checkout.id > 0) {
      params.checkout = companyCarrier.checkout.id
    }
    if (companyCarrier.externalId && companyCarrier.externalId.length > 0) {
      params.externalId = companyCarrier.externalId
    }
    if (companyCarrier.enable !== null) {
      params.enable = companyCarrier.enable
    }
    return this.createUpdateEntity(
      'CompanyCarrier',
      'createCompanyCarrier',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    companyCarrier: CompanyCarrier,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<CompanyCarrier> {
    const params: any = {
      id: companyCarrier.id,
      checkout: companyCarrier.checkout && companyCarrier.checkout.id > 0 ? companyCarrier.checkout.id : null,
    }
    if (companyCarrier.carrier && companyCarrier.carrier.id && companyCarrier.carrier.id.length > 0) {
      params.carrier = companyCarrier.carrier.id
    }
    if (companyCarrier.options && companyCarrier.options.length > 0) {
      params.options = companyCarrier.options
    }
    if (companyCarrier.externalId && companyCarrier.externalId.length > 0) {
      params.externalId = companyCarrier.externalId
    }
    if (companyCarrier.enable !== null) {
      params.enable = companyCarrier.enable
    }
    return this.createUpdateEntity(
      'CompanyCarrier',
      'updateCompanyCarrier',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(companyCarrier: CompanyCarrier | any): Promise<Array<any> | any> {
    const params: any = { id: companyCarrier.id }
    return this.deleteEntity('CompanyCarrier', 'deleteCompanyCarrier', params)
  }
}
