import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class DownloadService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  downloadDocumentAndShowInNewWindow(uri: string): void {
    if ('open' in window) {
      const popupWin = window.open('about:blank')
      this.httpService
        .downloadBlob(uri)
        .then((blob: any) => {
          const mimeType = blob.type
          const dataUrl = window.URL.createObjectURL(blob)
          this.loggerService.log({
            type: EVENT.HTTP,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
              uri: uri,
              mime: mimeType,
            },
          })
          switch (mimeType) {
            default:
            case 'application/pdf':
              if (popupWin) {
                popupWin.location.href = dataUrl // beware of adblockers! (window close on blob:)
                // popupWin.history.pushState('', report.url, report.url) // impossible
              }
              break
            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            case 'application/msexcel':
            case 'application/x-msexcel':
            case 'application/x-ms-excel':
            case 'application/x-excel':
            case 'application/x-dos_ms_excel':
            case 'application/xls':
            case 'application/x-xls':
              // download as file
              if (popupWin) {
                popupWin.close()
              }
              const filename = `${uri.split('/')[uri.split('/').length - 1]}.xlsx`
              const a = document.createElement('a')
              a.style.display = 'none'
              document.body.appendChild(a)
              a.href = dataUrl
              a.download = filename
              a.click()
              break
          }
          // release reference
          setTimeout(() => {
            window.URL.revokeObjectURL(dataUrl)
          }, 120 * 1000)
        })
        .catch(err => {
          this.loggerService.log({
            type: EVENT.HTTP,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
              uri: uri,
              error: err,
            },
          })
          if (popupWin) {
            popupWin.close()
          }
          setTimeout(() => {
            if (uri && uri.includes('bill')) {
              alert('Došlo k chybě při stahování souboru, pokouším se otevřít záložním způsobem')
              window.open(uri)
            } else {
              alert('Došlo k chybě při stahování souboru')
            }
          }, 300)
        })
    } else {
      console.warn('TODO: RN implementation (DownloadService:downloadDocument)') // TODO: RN implementation
    }
  }
}
