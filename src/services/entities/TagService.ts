import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Tag } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class TagService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Tag', 'tags', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Tag> {
    return this.getSingleEntity('Tag', 'tag', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    tag: Tag,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Tag> {
    const params: any = {
      name: tag.name,
      type: tag.type,
      color: tag.color,
      externalId: tag.externalId,
    }
    return this.createUpdateEntity('Tag', 'createTag', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    tag: Tag,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Tag> {
    const params: any = {
      id: tag.id,
      name: tag.name,
      type: tag.type,
      color: tag.color,
      externalId: tag.externalId,
    }
    return this.createUpdateEntity('Tag', 'updateTag', params, definitionDepth, definitionExcludedClasses, schema)
  }

  delete(tag: Tag | any): Promise<Array<any> | any> {
    const params: any = { id: tag.id }
    return this.deleteEntity('Tag', 'deleteTag', params)
  }
}
