import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
// import { User, UserGroup } from '../entities'
import { EVENT, LoggerService, OAuthService, StorageService } from '../'
import { ENDPOINT, GraphQLUtil, Observable, Observer, SchemaUtil } from '../../utils'
import { NotificationEvent, NotificationEventOperation, NotificationEventType } from '../../models'

const isSchemaDefined = (definitionDepth, excludedClasses, schema) =>
  definitionDepth && (!excludedClasses || (excludedClasses && !excludedClasses.length)) && schema

type TestDataParams = {
  requestParams?: any
  responseData?: any
  responseErrors?: any[]
}
export class BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  protected onEvent: Observable<NotificationEvent>
  protected paginatorDefinition: any = {
    current: null,
    endPage: null,
    totalCount: null,
  }
  constructor(config: EntityServiceConfiguration) {
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
    this.onEvent = config.notificationEventObservable
  }

  protected subscribeToEvents(): Observer {
    return this.onEvent.subscribe()
  }

  protected unsubscribeFromEvents(): void {
    return this.onEvent.unregisterObservers()
  }

  protected getEntityList(
    entityName: string,
    method: string,
    parameters: any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    this.onEvent.emit(
      new NotificationEvent({
        type: NotificationEventType.start,
        entity: entityName,
        method,
        operation: NotificationEventOperation.list,
      }),
    )
    const query = this.composeQueryForList(
      entityName,
      method,
      parameters,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        query: query,
      },
    })
    return this.httpService
      .post(ENDPOINT.GRAPHQL_POST, { query: query })
      .then(response => {
        const errors = this.getResponseErrors(response, method)
        if (errors && errors.length > 0) {
          this.onEvent.emit(
            new NotificationEvent({
              type: NotificationEventType.reject,
              entity: entityName,
              method,
              operation: NotificationEventOperation.list,
              response: null,
              errors,
            }),
          )
          return Promise.reject(errors)
        }
        this.onEvent.emit(
          new NotificationEvent({
            type: NotificationEventType.resolve,
            entity: entityName,
            method,
            operation: NotificationEventOperation.list,
            response,
            errors: [],
          }),
        )
        return this.getResponseData(response, method)
      })
      .catch(e => Promise.reject(e))
  }

  protected getSingleEntity(
    entityName: string,
    method: string,
    parameters: any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    this.onEvent.emit(
      new NotificationEvent({
        type: NotificationEventType.start,
        entity: entityName,
        method,
        operation: NotificationEventOperation.detail,
      }),
    )
    const query = this.composeQueryForDetail(
      entityName,
      method,
      parameters,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        query: query,
      },
    })
    return this.httpService
      .post(ENDPOINT.GRAPHQL_POST, { query: query })
      .then(response => {
        const errors = this.getResponseErrors(response, method)
        if (errors && errors.length > 0) {
          this.onEvent.emit(
            new NotificationEvent({
              type: NotificationEventType.reject,
              entity: entityName,
              method,
              operation: NotificationEventOperation.detail,
              response: null,
              errors,
            }),
          )
          return Promise.reject(errors)
        }
        this.onEvent.emit(
          new NotificationEvent({
            type: NotificationEventType.resolve,
            entity: entityName,
            method,
            operation: NotificationEventOperation.detail,
            response,
            errors: [],
          }),
        )
        return this.getResponseData(response, method)
      })
      .catch(e => Promise.reject(e))
  }

  protected createUpdateEntity(
    entityName: string,
    method: string,
    parameters: any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    this.onEvent.emit(
      new NotificationEvent({
        type: NotificationEventType.start,
        entity: entityName,
        method,
        operation: method.includes('update') ? NotificationEventOperation.update : NotificationEventOperation.create,
      }),
    )
    this.appendTestDataToWindow({ requestParams: parameters })
    const mutation = this.composeMutation(
      entityName,
      method,
      parameters,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        query: mutation,
      },
    })
    return this.httpService
      .post(ENDPOINT.GRAPHQL_POST, { query: mutation })
      .then(response => {
        const errors = this.getResponseErrors(response, method)
        if (errors && errors.length > 0) {
          this.appendTestDataToWindow({ responseErrors: errors })
          this.onEvent.emit(
            new NotificationEvent({
              type: NotificationEventType.reject,
              entity: entityName,
              method,
              operation: method.includes('update')
                ? NotificationEventOperation.update
                : NotificationEventOperation.create,
              response: null,
              errors,
            }),
          )
          return Promise.reject(errors)
        }
        this.onEvent.emit(
          new NotificationEvent({
            type: NotificationEventType.resolve,
            entity: entityName,
            method,
            operation: method.includes('update')
              ? NotificationEventOperation.update
              : NotificationEventOperation.create,
            response: response,
            errors: [],
          }),
        )
        const d = this.getResponseData(response, method)
        this.appendTestDataToWindow({ responseData: d })
        return d
      })
      .catch(e => Promise.reject(e))
  }

  protected deleteEntity(entityName: string, method: string, parameters: any): Promise<Array<any> | any> {
    this.onEvent.emit(
      new NotificationEvent({
        type: NotificationEventType.start,
        entity: entityName,
        method,
        operation: NotificationEventOperation.delete,
      }),
    )
    const mutation = this.composeDeleteMutation(entityName, method, parameters)
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        query: mutation,
      },
    })
    return this.httpService
      .post(ENDPOINT.GRAPHQL_POST, { query: mutation })
      .then(response => {
        const errors = this.getResponseErrors(response, method)
        this.onEvent.emit(
          new NotificationEvent({
            type: errors && errors.length > 0 ? NotificationEventType.reject : NotificationEventType.resolve,
            entity: entityName,
            method,
            operation: NotificationEventOperation.delete,
            response: null,
            errors: errors && errors.length > 0 ? errors : [],
          }),
        )
        return errors
      })
      .catch(e => Promise.reject(e))
  }

  private composeQueryForList(
    entityName: string,
    method: string,
    parameters: any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): string {
    let entitySchema
    if (entityName === 'string') {
      entitySchema = null
    } else {
      entitySchema = !!schema ? schema : SchemaUtil.extractFrom(entityName, definitionDepth, definitionExcludedClasses)
    }
    let definition: any = { items: entitySchema }
    if (entityName === 'Role') {
      definition = entitySchema
    } else if (
      !entityName.includes('Stats') &&
      entityName !== 'string' &&
      entityName !== 'SellItem' &&
      entityName !== 'ProductPurchasePrice'
    ) {
      definition.paginator = this.paginatorDefinition
    }
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        definition: definition,
      },
    })

    return GraphQLUtil.createQuery(definition, method, parameters, method)
  }

  private composeQueryForDetail(
    entityName: string,
    method: string,
    parameters: any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): string {
    const entitySchema = !!schema
      ? schema
      : SchemaUtil.extractFrom(entityName, definitionDepth, definitionExcludedClasses)
    const definition = {
      data: entitySchema,
      errors: null,
    }
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        definition: definition,
      },
    })

    return GraphQLUtil.createQuery(definition, method, parameters, method)
  }

  private composeMutation(
    entityName: string,
    method: string,
    parameters: any,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): string {
    const entitySchema = isSchemaDefined(definitionDepth, definitionExcludedClasses, schema)
      ? !!schema
        ? schema
        : SchemaUtil.extractFrom(entityName, definitionDepth, definitionExcludedClasses)
      : { id: null }
    const definition = {
      data: entitySchema,
      errors: null,
    }
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        definition: definition,
      },
    })

    const isNonStdMethod = method === 'updateClearanceItem' || method === 'resetPackage'
    return GraphQLUtil.createMutation(parameters, isNonStdMethod ? { errors: null } : definition, method, method)
  }

  private composeDeleteMutation(entityName: string, method: string, parameters: any): string {
    const definition = {
      errors: null,
    }
    this.loggerService.log({
      type: EVENT.GRAPHQL,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        entityName: entityName,
        method: method,
        definition: definition,
      },
    })

    return GraphQLUtil.createMutation(parameters, definition, method, method)
  }

  private getResponseData(response: any, method: string): any | null {
    if (response && response['data'] && response['data'][method] && response['data'][method]['data']) {
      return response['data'][method]['data']
    }
    if (method === 'roles' && response && response['data'] && response['data'][method]) {
      return response['data'][method]
    }
    if (response && response['data'] && response['data'][method] && response['data'][method]['items']) {
      return {
        items: response['data'][method]['items'],
        paginator: response['data'][method]['paginator'],
      }
    }
    return null
  }

  private getResponseErrors(response: any, method: string): Array<any> | null {
    if (response && response['errors'] && response['errors'].length > 0) {
      return response['errors']
    }
    if (
      response &&
      response['data'] &&
      response['data'][method] &&
      response['data'][method]['errors'] &&
      response['data'][method]['errors'].length > 0
    ) {
      return response['data'][method]['errors']
    }
    return null
  }

  appendTestDataToWindow({ requestParams, responseData, responseErrors }: TestDataParams) {
    if (!window) {
      return
    }
    if (!window['CYPRESS']) {
      window['CYPRESS'] = {}
    }
    if (requestParams) {
      window['CYPRESS'].mutation = requestParams
    }
    if (responseData) {
      window['CYPRESS'].response = responseData
    }
    if (responseErrors) {
      window['CYPRESS'].errors = responseErrors
    }
  }
}
