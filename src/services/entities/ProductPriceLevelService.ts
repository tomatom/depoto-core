import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { ProductPriceLevel } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProductPriceLevelService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'ProductPriceLevel',
      'productPriceLevels',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPriceLevel> {
    return this.getSingleEntity(
      'ProductPriceLevel',
      'productPriceLevel',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    productPriceLevel: ProductPriceLevel,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPriceLevel> {
    const params: any = {
      product: productPriceLevel.product.id,
      priceLevel: productPriceLevel.priceLevel.id,
      sellPrice: productPriceLevel.sellPrice,
      externalId: productPriceLevel.externalId,
    }
    return this.createUpdateEntity(
      'ProductPriceLevel',
      'createProductPriceLevel',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    productPriceLevel: ProductPriceLevel,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPriceLevel> {
    const params: any = {
      id: productPriceLevel.id,
      product: productPriceLevel.product.id,
      priceLevel: productPriceLevel.priceLevel.id,
      sellPrice: productPriceLevel.sellPrice,
      externalId: productPriceLevel.externalId,
    }
    return this.createUpdateEntity(
      'ProductPriceLevel',
      'updateProductPriceLevel',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(productPriceLevel: ProductPriceLevel | any): Promise<Array<any> | any> {
    const params: any = { id: productPriceLevel.id }
    return this.deleteEntity('ProductPriceLevel', 'deleteProductPriceLevel', params)
  }
}
