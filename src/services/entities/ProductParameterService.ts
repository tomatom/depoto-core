import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { ProductParameter } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProductParameterService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'ProductParameter',
      'productParameters',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductParameter> {
    return this.getSingleEntity(
      'ProductParameter',
      'productParameter',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    productParameter: ProductParameter,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductParameter> {
    const params: any = {
      parameter: productParameter.parameter.id,
      product: productParameter.product.id,
      value: productParameter.value,
      externalId: productParameter.externalId,
    }
    return this.createUpdateEntity(
      'ProductParameter',
      'createProductParameter',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    productParameter: ProductParameter,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductParameter> {
    const params: any = {
      id: productParameter.id,
      parameter: productParameter.parameter.id,
      product: productParameter.product.id,
      value: productParameter.value,
      externalId: productParameter.externalId,
    }
    return this.createUpdateEntity(
      'ProductParameter',
      'updateProductParameter',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(productParameter: ProductParameter | any): Promise<Array<any> | any> {
    const params: any = { id: productParameter.id }
    return this.deleteEntity('ProductParameter', 'deleteProductParameter', params)
  }
}
