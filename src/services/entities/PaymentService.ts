import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Payment, PaymentType, Currency } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class PaymentService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Payment', 'payments', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getPaymentTypes(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('PaymentType', 'paymentTypes', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getCurrencies(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Currency', 'currencies', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Payment> {
    return this.getSingleEntity('Payment', 'payment', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    payment: Payment,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Payment> {
    const params: any = {
      name: payment.name,
      type: payment.type.id,
      eetEnable: payment.eetEnable,
    }
    return this.createUpdateEntity(
      'Payment',
      'createPayment',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    payment: Payment,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Payment> {
    const params: any = {
      id: payment.id,
      name: payment.name,
      type: payment.type.id,
      eetEnable: payment.eetEnable,
    }
    return this.createUpdateEntity(
      'Payment',
      'updatePayment',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(payment: Payment | any): Promise<Array<any> | any> {
    const params: any = { id: payment.id }
    return this.deleteEntity('Payment', 'deletePayment', params)
  }
}
