import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Checkout } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class CheckoutService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  // if depth > 3, exclude OrderItem class!
  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Checkout', 'checkouts', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Checkout> {
    return this.getSingleEntity('Checkout', 'checkout', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    checkout: Checkout,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Checkout> {
    const params: any = {
      name: checkout.name,
      amount: checkout.amount,
      nextBillNumber: checkout.nextBillNumber,
      nextReservationNumber: checkout.nextReservationNumber,
      billFooter: checkout.billFooter,
      eetId: Number(checkout.eetId),
      eetEnable: checkout.eetEnable,
      eetPlayground: checkout.eetPlayground,
      eetVerificationMode: checkout.eetVerificationMode,
      negativeReservation: checkout.negativeReservation,
      returnsDepot: !!checkout.returnsDepot && checkout.returnsDepot.id > 0 ? checkout.returnsDepot.id : null,
      eventUrl: checkout.eventUrl,
      eventTypes: checkout.eventTypes && checkout.eventTypes.length > 0 ? checkout.eventTypes : [],
    }
    if (checkout.depots && checkout.depots.length > 0) {
      params.depots = checkout.depots.map(d => d.id)
    }
    if (checkout.payments && checkout.payments.length > 0) {
      params.payments = checkout.payments.map(p => p.id)
    }
    if (checkout.users && checkout.users.length > 0) {
      params.users = checkout.users.map(u => u.id)
    }
    return this.createUpdateEntity(
      'Checkout',
      'createCheckout',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    checkout: Checkout,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Checkout> {
    const params: any = {
      id: checkout.id,
      name: checkout.name,
      amount: checkout.amount,
      billFooter: checkout.billFooter,
      eetEnable: checkout.eetEnable,
      eetPlayground: checkout.eetPlayground,
      eetVerificationMode: checkout.eetVerificationMode,
      negativeReservation: checkout.negativeReservation,
      returnsDepot: !!checkout.returnsDepot && checkout.returnsDepot.id > 0 ? checkout.returnsDepot.id : null,
      eventUrl: checkout.eventUrl,
      eventTypes: checkout.eventTypes && checkout.eventTypes.length > 0 ? checkout.eventTypes : [],
      depots: checkout.depots && checkout.depots.length > 0 ? checkout.depots.map(d => d.id) : [],
      payments: checkout.payments && checkout.payments.length > 0 ? checkout.payments.map(p => p.id) : [],
      users: checkout.users && checkout.users.length > 0 ? checkout.users.map(u => u.id) : [],
    }
    return this.createUpdateEntity(
      'Checkout',
      'updateCheckout',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  setNextBillNumber(
    checkout: Checkout,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Checkout> {
    const params: any = {
      id: checkout.id,
      nextBillNumber: checkout.nextBillNumber,
    }
    return this.createUpdateEntity(
      'Checkout',
      'updateCheckout',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  setNextReservationNumber(
    checkout: Checkout,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Checkout> {
    const params: any = {
      id: checkout.id,
      nextReservationNumber: checkout.nextReservationNumber,
    }
    return this.createUpdateEntity(
      'Checkout',
      'updateCheckout',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getEventTypes(): Promise<any> {
    return this.getEntityList('string', 'eventTypes', {})
  }

  delete(checkout: Checkout | any): Promise<Array<any> | any> {
    const params: any = { id: checkout.id }
    return this.deleteEntity('Checkout', 'deleteCheckout', params)
  }
}
