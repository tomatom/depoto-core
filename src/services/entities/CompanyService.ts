import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Company } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class CompanyService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  // not implemented on api
  // getList(parameters: Object|any = {}, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Object|any> {
  //   const params = {
  //     page: parameters.page > 0 ? parameters.page : 1,
  //     sort: !!parameters.sort ? parameters.sort : 'id',
  //     direction: !!parameters.direction ? parameters.direction : 'DESC',
  //     filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
  //   }
  //   return this.getEntityList('Company', 'companies', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Company> {
  //   return this.getSingleEntity('Company', 'company', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // create(company: Company, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Company> {
  //   const params: any = {name: company.name};
  //   return this.createUpdateEntity('Company', 'createCompany', params, definitionDepth, definitionExcludedClasses, schema)
  // }

  update(
    company: Company,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Company> {
    const params: any = {
      id: company.id,
      name: company.name,
      // ic: company.ic,
      // dic: company.dic,
      phone: company.phone,
      email: company.email,
      street: company.street,
      city: company.city,
      zip: company.zip,
      country: company.country,
      // registrationNote: company.registrationNote,
      // nextEan: company.nextEan,
      defaultVoucherValidity: company.defaultVoucherValidity,
    }
    return this.createUpdateEntity(
      'Company',
      'updateCompany',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updateNextEan(
    company: Company,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Company> {
    const params: any = {
      id: company.id,
      nextEan: company.nextEan,
    }
    return this.createUpdateEntity(
      'Company',
      'updateCompany',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  // delete(company: Company|any): Promise<Array<any>|any> {
  //   const params: any = {id: company.id}
  //   return this.deleteEntity('Company', 'deleteCompany', params)
  // }
}
