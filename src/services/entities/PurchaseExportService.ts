import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { PurchaseExport } from '../../entities'
import { LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class PurchaseExportService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'PurchaseExport',
      'exportPurchases',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    purchaseExport: PurchaseExport,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<PurchaseExport> {
    const params: any = {
      dateFrom: purchaseExport.dateFrom,
      dateTo: purchaseExport.dateTo,
      depots: purchaseExport.depots,
      checkouts: purchaseExport.checkouts,
      suppliers: purchaseExport.suppliers,
      producers: purchaseExport.producers,
      format: purchaseExport.format,
    }
    return this.createUpdateEntity(
      'PurchaseExport',
      'createExportPurchase',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }
}
