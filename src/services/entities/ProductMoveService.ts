import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { ProductMove } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProductMoveService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('ProductMove', 'productMoves', params, definitionDepth, definitionExcludedClasses, schema)
  }

  // not implemented on api
  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductMove> {
  //   return this.getSingleEntity('ProductMove', 'productMove', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // create(productMove: ProductMove, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductMove> {
  //   const params: any = {name: productMove.name};
  //   return this.createUpdateEntity('ProductMove', 'createProductMove', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // update(productMove: ProductMove, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductMove> {
  //   const params: any = {
  //     id: productMove.id,
  //     name: productMove.name,
  //   }
  //   return this.createUpdateEntity('ProductMove', 'updateProductMove', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // delete(productMove: ProductMove|any): Promise<Array<any>|any> {
  //   const params: any = {id: productMove.id}
  //   return this.deleteEntity('ProductMove', 'deleteProductMove', params)
  // }
}
