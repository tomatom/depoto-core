import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { EETCert } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class EETCertService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('EETCert', 'certs', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETCert> {
    return this.getSingleEntity('EETCert', 'cert', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    cert: EETCert,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETCert> {
    const params: any = {
      name: cert.name,
      password: cert.password,
      pkcs12: cert.pkcs12,
    }
    return this.createUpdateEntity('EETCert', 'createCert', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    cert: EETCert,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETCert> {
    const params: any = {
      id: cert.id,
    }
    if (cert.name && cert.name.length > 0) {
      params.name = cert.name
    }
    if (cert.password && cert.password.length > 0) {
      params.password = cert.password
    }
    if (cert.pkcs12 && cert.pkcs12.length > 0) {
      params.pkcs12 = cert.pkcs12
    }
    return this.createUpdateEntity('EETCert', 'updateCert', params, definitionDepth, definitionExcludedClasses, schema)
  }

  delete(cert: EETCert | any): Promise<Array<any> | any> {
    const params: any = { id: cert.id }
    return this.deleteEntity('EETCert', 'deleteCert', params)
  }
}
