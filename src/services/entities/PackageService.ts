import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Carrier, Company, Package, PackageDisposal } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class PackageService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('Package', 'packages', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Package> {
    return this.getSingleEntity('Package', 'package', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    pack: Partial<Package>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Package> {
    const params: any = {
      order: pack.order!.id,
      carrier: pack.carrier!.id,
      items: pack.items?.map(i => i.id) || [],
    }
    if (pack.weight && pack.weight > 0) {
      params.weight = Number(pack.weight)
    }
    if (pack.dimensionX && pack.dimensionX > 0) {
      params.dimensionX = Number(pack.dimensionX)
    }
    if (pack.dimensionY && pack.dimensionY > 0) {
      params.dimensionY = Number(pack.dimensionY)
    }
    if (pack.dimensionZ && pack.dimensionZ > 0) {
      params.dimensionZ = Number(pack.dimensionZ)
    }
    return this.createUpdateEntity(
      'Package',
      'createPackage',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    pack: Partial<Package>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Package> {
    const params: any = {
      id: pack.id,
      order: pack.order!.id,
      carrier: pack.carrier!.id,
      items: pack.items?.map(i => i.id) || [],
    }
    if (pack.weight && pack.weight > 0) {
      params.weight = Number(pack.weight)
    }
    if (pack.dimensionX && pack.dimensionX > 0) {
      params.dimensionX = Number(pack.dimensionX)
    }
    if (pack.dimensionY && pack.dimensionY > 0) {
      params.dimensionY = Number(pack.dimensionY)
    }
    if (pack.dimensionZ && pack.dimensionZ > 0) {
      params.dimensionZ = Number(pack.dimensionZ)
    }
    return this.createUpdateEntity(
      'Package',
      'updatePackage',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updatePart(
    pack: Partial<Package>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Package> {
    const params: any = {
      id: pack.id,
    }
    if (pack.order?.id && pack.order.id > 0) {
      params.order = pack.order.id
    }
    if (pack.carrier?.id && pack.carrier.id.length > 0) {
      params.carrier = pack.carrier.id
    }
    if (pack.items && pack.items.length > 0) {
      params.items = pack.items.map(i => i.id)
    }
    if ((pack.weight && pack.weight > 0) || pack.weight === null) {
      params.weight = pack.weight
    }
    if ((pack.dimensionX && pack.dimensionX > 0) || pack.dimensionX === null) {
      params.dimensionX = Number(pack.dimensionX)
    }
    if ((pack.dimensionY && pack.dimensionY > 0) || pack.dimensionY === null) {
      params.dimensionY = Number(pack.dimensionY)
    }
    if ((pack.dimensionZ && pack.dimensionZ > 0) || pack.dimensionZ === null) {
      params.dimensionZ = Number(pack.dimensionZ)
    }
    return this.createUpdateEntity(
      'Package',
      'updatePackage',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  send(
    pack: Partial<Package>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Package> {
    const params: any = {
      id: pack.id,
    }
    return this.createUpdateEntity('Package', 'sendPackage', params, definitionDepth, definitionExcludedClasses, schema)
  }

  reset(pack: Partial<Package>): Promise<Package> {
    const params: any = {
      id: pack.id,
    }
    return this.createUpdateEntity('Package', 'resetPackage', params, undefined, undefined, { errors: null })
  }

  delete(pack: Partial<Package>): Promise<Array<any> | any> {
    const params: any = { id: pack.id }
    return this.deleteEntity('Package', 'deletePackage', params)
  }

  getDisposals(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'PackageDisposal',
      'disposals',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getDisposal(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<PackageDisposal> {
    return this.getSingleEntity(
      'PackageDisposal',
      'disposal',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  createDisposal(
    company: Partial<Company>,
    carrier: Partial<Carrier>,
    packages: Array<Partial<Package>>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<PackageDisposal> {
    const params: any = {
      company: company!.id,
      carrier: carrier!.id,
      packages: packages?.map(i => i.id) || [],
    }
    return this.createUpdateEntity(
      'PackageDisposal',
      'createDisposal',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  updateDisposalPart(
    disposal: Partial<PackageDisposal>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<PackageDisposal> {
    const params: any = {
      id: disposal!.id,
    }
    if (disposal.carrier && disposal.carrier.id?.length > 0) {
      params.carrier = disposal.carrier.id
    }
    if (disposal.company && disposal.company.id > 0) {
      params.company = disposal.company.id
    }
    if (disposal.packages && disposal.packages.length > 0) {
      params.packages = disposal.packages.map(p => p.id)
    }
    return this.createUpdateEntity(
      'PackageDisposal',
      'updateDisposal',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  sendDisposal(
    disposal: Partial<PackageDisposal>,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<Package> {
    const params: any = {
      id: disposal!.id,
    }
    return this.createUpdateEntity(
      'PackageDisposal',
      'sendDisposal',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  deleteDisposal(disposal: Partial<PackageDisposal>): Promise<Array<any> | any> {
    const params: any = { id: disposal.id }
    return this.deleteEntity('PackageDisposal', 'deleteDisposal', params)
  }
}
