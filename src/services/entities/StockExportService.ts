import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { StockExport } from '../../entities'
import { LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class StockExportService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('StockExport', 'exportStocks', params, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    stockExport: StockExport,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<StockExport> {
    const params: any = {
      date: stockExport.date,
      depots: stockExport.depots,
      suppliers: stockExport.suppliers,
      producers: stockExport.producers,
      format: stockExport.format,
    }
    return this.createUpdateEntity(
      'StockExport',
      'createExportStock',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }
}
