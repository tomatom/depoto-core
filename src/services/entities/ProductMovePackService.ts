import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { ProductMovePack, ProductMove } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProductMovePackService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'ProductMovePack',
      'productMovePacks',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductMovePack> {
    return this.getSingleEntity(
      'ProductMovePack',
      'productMovePack',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    productMovePack: ProductMovePack,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductMovePack> {
    const moves: any[] = []
    productMovePack.moves.forEach((move: ProductMove) => {
      const m: any = {
        product: move.product?.id,
        supplier: Number(move.supplier),
        amount: Number(move.amount),
        purchasePrice: Number(move.purchasePrice),
        purchaseCurrency: move.purchaseCurrency?.length > 0 ? move.purchaseCurrency : 'CZK',
      }
      if (move.orderItem && move.orderItem.id > 0) {
        m.orderItem = Number(move.orderItem.id)
      }
      if (move.depotFrom > 0) {
        m.depotFrom = Number(move.depotFrom)
      }
      if (move.depotTo > 0) {
        m.depotTo = Number(move.depotTo)
      }
      if (move.productDepot > 0) {
        // == depotFrom on Product detail unloading
        m.productDepot = Number(move.productDepot)
      }
      if (move.note && move.note.length > 0) {
        m.note = move.note
      }
      if (move.batch && move.batch.length > 0) {
        m.batch = move.batch
      }
      if (move.expirationDate && move.expirationDate.length > 0) {
        m.expirationDate = move.expirationDate
      }
      if (move.position1 && move.position1.length > 0) {
        m.position1 = move.position1
      }
      if (move.position2 && move.position2.length > 0) {
        m.position2 = move.position2
      }
      if (move.position3 && move.position3.length > 0) {
        m.position3 = move.position3
      }
      moves.push(m)
    })
    const params: any = {
      type: productMovePack.type?.id,
      purpose: productMovePack.purpose && productMovePack.purpose.id ? productMovePack.purpose.id : 'default',
      moves: moves,
    }
    if (productMovePack.order && productMovePack.order.id > 0) {
      params.order = productMovePack.order.id
    }
    if (productMovePack.note && productMovePack.note.length > 0) {
      params.note = productMovePack.note
    }
    return this.createUpdateEntity(
      'ProductMovePack',
      'createProductMovePack',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  // not implemented on api
  // update(productMovePack: ProductMovePack, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<ProductMovePack> {
  //   const params: any = {
  //     id: productMovePack.id,
  //     name: productMovePack.name,
  //   }
  //   return this.createUpdateEntity('ProductMovePack', 'updateProductMovePack', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // delete(productMovePack: ProductMovePack|any): Promise<Array<any>|any> {
  //   const params: any = {id: productMovePack.id}
  //   return this.deleteEntity('ProductMovePack', 'deleteProductMovePack', params)
  // }
}
