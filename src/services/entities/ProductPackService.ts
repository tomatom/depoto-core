import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Product, ProductPack } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class ProductPackService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('ProductPack', 'productPacks', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPack> {
    return this.getSingleEntity(
      'ProductPack',
      'productPack',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    productPack: ProductPack,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPack> {
    const params: any = {
      code: productPack.code,
      ean: productPack.ean,
      externalId: productPack.externalId,
      product: productPack.product ? productPack.product.id : null,
      quantity: productPack.quantity || 0,
    }
    return this.createUpdateEntity(
      'ProductPack',
      'createProductPack',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    productPack: ProductPack,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<ProductPack> {
    const params: any = {
      id: productPack.id,
      code: productPack.code,
      ean: productPack.ean,
      externalId: productPack.externalId,
      product: productPack.product ? productPack.product.id : null,
      quantity: productPack.quantity || 0,
    }
    return this.createUpdateEntity(
      'ProductPack',
      'updateProductPack',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  delete(productPack: ProductPack | any): Promise<Array<any> | any> {
    const params: any = { id: productPack.id }
    return this.deleteEntity('ProductPack', 'deleteProductPack', params)
  }
}
