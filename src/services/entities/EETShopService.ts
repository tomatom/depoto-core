import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { EETShop } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class EETShopService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('EETShop', 'shops', params, definitionDepth, definitionExcludedClasses, schema)
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETShop> {
    return this.getSingleEntity('EETShop', 'shop', { id: id }, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    shop: EETShop,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETShop> {
    const params: any = { name: shop.name }
    if (shop.eetId) {
      params.eetId = Number(shop.eetId)
    }
    if (shop.street && shop.street.length > 0) {
      params.street = shop.street
    }
    if (shop.city && shop.city.length > 0) {
      params.city = shop.city
    }
    if (shop.country && shop.country.length > 0) {
      params.country = shop.country
    }
    if (shop.zip && shop.zip.length > 0) {
      params.zip = shop.zip
    }
    if (shop.cert && shop.cert.id > 0) {
      params.cert = shop.cert.id
    }
    if (shop.checkouts && shop.checkouts.length > 0) {
      params.checkouts = shop.checkouts.map(checkout => checkout.id)
    }
    return this.createUpdateEntity('EETShop', 'createShop', params, definitionDepth, definitionExcludedClasses, schema)
  }

  update(
    shop: EETShop,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<EETShop> {
    const params: any = {
      id: shop.id,
      name: shop.name,
      eetId: Number(shop.eetId),
      street: shop.street,
      city: shop.city,
      country: shop.country,
      zip: shop.zip,
    }
    if (shop.cert && shop.cert.id > 0) {
      params.cert = shop.cert.id
    }
    if (shop.checkouts && shop.checkouts.length > 0) {
      params.checkouts = shop.checkouts.map(checkout => checkout.id)
    } else {
      params.checkouts = []
    }
    return this.createUpdateEntity('EETShop', 'updateShop', params, definitionDepth, definitionExcludedClasses, schema)
  }

  delete(shop: EETShop | any): Promise<Array<any> | any> {
    const params: any = { id: shop.id }
    return this.deleteEntity('EETShop', 'deleteShop', params)
  }
}
