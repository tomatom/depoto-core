import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { SaleExport } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class SaleExportService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList('SaleExport', 'exportSales', params, definitionDepth, definitionExcludedClasses, schema)
  }

  create(
    saleExport: SaleExport,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<SaleExport> {
    const params: any = {
      dateFrom: saleExport.dateFrom,
      dateTo: saleExport.dateTo,
      checkouts: saleExport.checkouts,
      suppliers: saleExport.suppliers,
      producers: saleExport.producers,
      format: saleExport.format,
    }
    return this.createUpdateEntity(
      'SaleExport',
      'createExportSale',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }
}
