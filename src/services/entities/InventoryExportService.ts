import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { InventoryExport } from '../../entities'
import { LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class InventoryExportService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    const params = {
      page: parameters.page > 0 ? parameters.page : 1,
      sort: !!parameters.sort ? parameters.sort : 'id',
      direction: !!parameters.direction ? parameters.direction : 'DESC',
      filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    }
    return this.getEntityList(
      'InventoryExport',
      'exportInventories',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  getById(
    id: number,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<InventoryExport> {
    return this.getSingleEntity(
      'InventoryExport',
      'exportInventory',
      { id: id },
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  create(
    inventoryExport: InventoryExport,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<InventoryExport> {
    const params: any = {
      date: inventoryExport.date,
      depots: inventoryExport.depots,
      suppliers: inventoryExport.suppliers,
      producers: inventoryExport.producers,
      approved: inventoryExport.approved,
      rejected: inventoryExport.rejected,
    }
    return this.createUpdateEntity(
      'InventoryExport',
      'createExportInventory',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }

  update(
    inventoryExport: InventoryExport,
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<InventoryExport> {
    const params: any = {
      id: inventoryExport.id,
      date: inventoryExport.date,
      depots: inventoryExport.depots,
      suppliers: inventoryExport.suppliers,
      producers: inventoryExport.producers,
      approved: inventoryExport.approved,
      rejected: inventoryExport.rejected,
    }
    return this.createUpdateEntity(
      'InventoryExport',
      'updateExportInventory',
      params,
      definitionDepth,
      definitionExcludedClasses,
      schema,
    )
  }
}
