import { EntityServiceConfiguration, HttpService as IHttpService } from '../../interfaces'
import { Role } from '../../entities'
import { EVENT, LoggerService, StorageService, OAuthService } from '../'
import { BaseEntityService } from '../entities'

export class UserRoleService extends BaseEntityService {
  protected loggerService: LoggerService
  protected s: StorageService
  protected oauthService: OAuthService
  protected httpService: IHttpService
  constructor(config: EntityServiceConfiguration) {
    super(config)
    this.loggerService = config.loggerService
    this.s = config.s
    this.oauthService = config.oauthService
    this.httpService = config.httpService
  }

  getList(
    parameters: any = {},
    definitionDepth?: number,
    definitionExcludedClasses: string[] = [],
    schema: any = null,
  ): Promise<any> {
    // const params = { // not on roles. also, no paginator/items on root
    //   page: parameters.page > 0 ? parameters.page : 1,
    //   sort: !!parameters.sort ? parameters.sort : 'id',
    //   direction: !!parameters.direction ? parameters.direction : 'DESC',
    //   filters: parameters.filters && Object.keys(parameters.filters).length > 0 ? parameters.filters : {},
    // }
    return this.getEntityList('Role', 'roles', {}, definitionDepth, definitionExcludedClasses)
  }

  // not implemented on api
  // getById(id: number, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Role> {
  //   return this.getSingleEntity('Role', 'role', {id: id}, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // create(role: Role, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Role> {
  //   const params: any = {
  //     name: role.name,
  //   }
  //   return this.createUpdateEntity('Role', 'createRole', params, definitionDepth, definitionExcludedClasses, schema)
  // }
  //
  // update(role: Role, definitionDepth?: number, definitionExcludedClasses: string[] = [], schema: any = null): Promise<Role> {
  //   const params: any = {
  //     id: role.id,
  //     name: role.name,
  //   }
  //   return this.createUpdateEntity('Role', 'updateRole', params, definitionDepth, definitionExcludedClasses, schema);
  // }
  //
  // delete(role: Role|any): Promise<Array<any>|any> {
  //   const params: any = {id: role.id}
  //   return this.deleteEntity('Role', 'deleteRole', params)
  // }
}
