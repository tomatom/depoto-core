import { OAuthSession } from '../models'
import { EVENT, LoggerService, StorageService } from './'
import { getEndpoint, ENDPOINT, DEFAULT_CLIENT_KEYS, Observable } from '../utils'

export class OAuthService {
  session: OAuthSession
  defaultClientKeys: any = DEFAULT_CLIENT_KEYS
  onLoginEmitter: Observable<string> = new Observable('core:oauthService:onLoginEmitter')
  onLogoutEmitter: Observable<string> = new Observable('core:oauthService:onLogoutEmitter')
  constructor(
    private readonly loggerService: LoggerService,
    private readonly s: StorageService,
    session: OAuthSession | any | null = {},
  ) {
    if (!!session) {
      this.session = new OAuthSession(session)
    } else {
      this.session = new OAuthSession()
      if (window && 'localStorage' in window) {
        this.loadDataSync()
      } else {
        this.loadData()
      }
    }
  }

  getToken(session: OAuthSession): Promise<boolean> {
    this.session = new OAuthSession(session)
    // this.s.set(this.s.keys.auth.clientType, this.session.clientType)
    // this.s.set(this.s.keys.auth.customServerUrl, this.session.clientCustomUri) // todo?
    if (!session.clientId) {
      session.clientId = this.defaultClientKeys[session.clientType].clientId
    }
    if (!session.clientSecret) {
      session.clientSecret = this.defaultClientKeys[session.clientType].clientSecret
    }
    this.loggerService.log({
      type: EVENT.OAUTH,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: session,
    })
    const reqBody: FormData | any = new FormData()
    reqBody.append('client_id', session.clientId)
    reqBody.append('client_secret', session.clientSecret)
    reqBody.append('grant_type', 'password')
    reqBody.append('username', session.email)
    reqBody.append('password', session.password)
    return new Promise(resolve => {
      fetch(getEndpoint(ENDPOINT.TOKEN, session), {
        method: 'post',
        // headers: {'Content-type': 'application/x-www-form-urlencoded'}, // no need to specify!
        body: reqBody,
      })
        .then(resp => resp.json())
        .then(
          (data: any) => {
            if (data && !!data.access_token && !data.error) {
              this.session.token.accessToken = data.access_token
              this.session.token.refreshToken = data.refresh_token
              this.session.token.tokenType = data.token_type
              this.session.token.scope = data.scope
              this.session.token.expiresIn = data.expires_in
              this.session.token.timestamp = +new Date()
              this.session.isAuthenticated = true
              this.saveData()
              this.onLoginEmitter.emit(this.session.email)
              this.loggerService.log({
                type: EVENT.OAUTH,
                origin: this.constructor['name'],
                trace: this.loggerService.getStackTrace(),
                data: data,
              })
              resolve(true)
            } else {
              this.session.isAuthenticated = false
              this.saveData()
              this.onLogoutEmitter.emit(this.session.email)
              this.loggerService.log({
                type: EVENT.OAUTH,
                origin: this.constructor['name'],
                trace: this.loggerService.getStackTrace(),
                data: data,
              })
              resolve(false)
            }
          },
          error => {
            this.session.isAuthenticated = false
            this.saveData()
            this.loggerService.log({
              type: EVENT.OAUTH,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: error,
            })
            this.onLogoutEmitter.emit(this.session.email)
            resolve(false)
          },
        )
    })
  }

  getRefreshToken(): Promise<boolean> {
    if (!this.session.isRememberMe) {
      this.logout()
      return new Promise(r => r(false))
    }
    if (!this.session.clientId) {
      this.session.clientId = this.defaultClientKeys[this.session.clientType].clientId
    }
    if (!this.session.clientSecret) {
      this.session.clientSecret = this.defaultClientKeys[this.session.clientType].clientSecret
    }
    const reqBody: FormData | any = new FormData()
    reqBody.append('client_id', this.session.clientId)
    reqBody.append('client_secret', this.session.clientSecret)
    reqBody.append('grant_type', 'refresh_token')
    reqBody.append('refresh_token', this.session.token.refreshToken)
    return new Promise(resolve => {
      fetch(getEndpoint(ENDPOINT.REFRESH_TOKEN, this.session), {
        method: 'post',
        body: reqBody,
      })
        .then(resp => resp.json())
        .then(
          (data: any) => {
            if (!data.error) {
              this.session.token.accessToken = data.access_token
              this.session.token.refreshToken = data.refresh_token
              this.session.token.tokenType = data.token_type
              this.session.token.scope = data.scope
              this.session.token.expiresIn = data.expires_in
              this.session.token.timestamp = +new Date()
              this.session.isAuthenticated = true
              this.onLoginEmitter.emit(this.session.email)
              this.saveData()
              resolve(true)
            } else {
              this.loggerService.log({
                type: EVENT.OAUTH,
                origin: this.constructor['name'],
                trace: this.loggerService.getStackTrace(),
                data: data.error,
              })
              resolve(false)
            }
          },
          error => {
            this.loggerService.log({
              type: EVENT.OAUTH,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: error,
            })
            this.onLogoutEmitter.emit(this.session.email)
            resolve(false)
          },
        )
    })
  }

  isAuthenticated(): boolean {
    return this.session.isAuthenticated
  }

  getAccessToken(): string | undefined {
    if (!this.session || (this.session && !this.session.token)) {
      return
    }
    return this.session.token.accessToken
  }

  getSession(): OAuthSession {
    if (!this.session) {
      return new OAuthSession()
    }
    return this.session
  }

  logout(): void {
    this.session.isAuthenticated = false
    this.onLogoutEmitter.emit(this.session.email)
    this.clearData()
  }

  saveData(): void {
    this.s.set(this.s.keys.auth.data, this.session.token)
    this.s.set(this.s.keys.auth.rememberMe, this.session.isRememberMe)
    this.s.set(this.s.keys.auth.isAuthenticated, this.session.isAuthenticated)
    this.s.set(this.s.keys.auth.email, this.session.email)
    this.s.set(this.s.keys.auth.clientType, this.session.clientType)
  }

  clearData(): void {
    this.s.set(this.s.keys.auth.isCompanyReloaded, false)
    this.s.clear(this.s.keys.auth)
  }

  async loadData(): Promise<any> {
    this.session.email = await this.s.get(this.s.keys.auth.email)
    this.session.clientType = await this.s.get(this.s.keys.auth.clientType)
    this.session.isAuthenticated = await this.s.get(this.s.keys.auth.isAuthenticated)
    this.session.isRememberMe = await this.s.get(this.s.keys.auth.rememberMe)
    this.session.token = await this.s.get(this.s.keys.auth.data)
  }

  loadDataSync(): any {
    this.session.email = this.s.getSync(this.s.keys.auth.email)
    this.session.clientType = this.s.getSync(this.s.keys.auth.clientType)
    this.session.isAuthenticated = this.s.getSync(this.s.keys.auth.isAuthenticated)
    this.session.isRememberMe = this.s.getSync(this.s.keys.auth.rememberMe)
    this.session.token = this.s.getSync(this.s.keys.auth.data)
  }
}
