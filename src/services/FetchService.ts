import { HttpService } from '../interfaces'
import { OAuthService, LoggerService, EVENT } from './'
import { ENDPOINT, getEndpoint } from '../utils'

export class FetchService implements HttpService {
  constructor(private loggerService: LoggerService, private oauthService: OAuthService) {}

  getAuthorizationHeaders() {
    const token = this.oauthService.getAccessToken()
    const headers = new Headers()
    headers.append('Authorization', `Bearer ${token}`)
    headers.append('Accept', 'application/json')
    headers.append('Content-type', 'application/json')
    const session = this.oauthService.getSession()
    if (session && session.clientType === 'stage') {
      headers.set('Authorization', `Basic ${btoa('stage:stage')}, Bearer ${token}`)
    }
    this.loggerService.log({
      type: EVENT.HTTP,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: headers,
    })
    return headers
  }

  get(endpoint: string): Promise<any> {
    return new Promise((resolve, reject) => {
      fetch(getEndpoint(endpoint, this.oauthService.getSession()), {
        method: 'get',
        headers: this.getAuthorizationHeaders(),
      })
        .then(resp => resp.json())
        .then(
          data => {
            resolve(data)
          },
          error => {
            this.loggerService.log({
              type: EVENT.HTTP,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: error,
            })
            reject(error)
          },
        )
        .catch(error => {
          if (error.status === 401) {
            this.oauthService.getRefreshToken().then(success => {
              if (success) {
                resolve(this.get(endpoint))
              } else {
                this.oauthService.logout()
              }
            })
          }
          reject(error)
        })
    })
  }

  post(endpoint: string, reqBody: any): Promise<any> {
    let rawResp: any, origResp: any
    return new Promise((resolve, reject) => {
      fetch(getEndpoint(endpoint, this.oauthService.getSession()), {
        method: 'post',
        headers: this.getAuthorizationHeaders(),
        body: typeof reqBody === 'string' ? reqBody : JSON.stringify(reqBody),
      })
        .then(resp => {
          origResp = resp
          return resp.clone()
        })
        .then(clonedResp => {
          rawResp = clonedResp
          return origResp.json()
        })
        .catch(e => rawResp.text().then((html: string) => reject({ error: e, html })))
        .then(
          data => {
            if (origResp.status !== 200) {
              throw { status: origResp.status }
            }
            resolve(data)
          },
          error => {
            this.loggerService.log({
              type: EVENT.HTTP,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: error,
            })
            reject(error)
          },
        )
        .catch(error => {
          if (error.status !== 401) {
            this.renderErrorDetail(error)
          } else if (error.status === 401) {
            this.oauthService.getRefreshToken().then(success => {
              if (success) {
                resolve(this.post(endpoint, reqBody))
              } else {
                this.oauthService.logout()
              }
            })
          }
          reject(error)
        })
    })
  }

  put(endpoint: string, reqBody: any) {
    return new Promise((resolve, reject) => {
      fetch(getEndpoint(endpoint, this.oauthService.getSession()), {
        method: 'put',
        headers: this.getAuthorizationHeaders(),
        body: reqBody,
      })
        .then(resp => resp.json())
        .then(
          data => {
            resolve(data)
          },
          error => {
            this.loggerService.log({
              type: EVENT.HTTP,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: error,
            })
            reject(error)
          },
        )
        .catch(error => {
          if (error.status === 401) {
            this.oauthService.getRefreshToken().then(success => {
              if (success) {
                resolve(this.put(endpoint, reqBody))
              } else {
                this.oauthService.logout()
              }
            })
          }
          reject(error)
        })
    })
  }

  downloadBlob(uri: string): Promise<Blob> {
    return new Promise((resolve, reject) => {
      fetch(uri, {
        method: 'post',
        headers: this.getAuthorizationHeaders(),
      })
        .then(resp => resp.blob())
        .then(
          data => {
            resolve(data)
          },
          error => {
            this.loggerService.log({
              type: EVENT.HTTP,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: error,
            })
            reject(error)
          },
        )
        .catch(error => {
          if (error.status === 401) {
            this.oauthService.getRefreshToken().then(success => {
              if (success) {
                resolve(this.downloadBlob(uri))
              } else {
                this.oauthService.logout()
              }
            })
          }
          reject(error)
        })
    })
  }

  private renderErrorDetail(error: Error | any) {
    if (!!window.location && (window.location.href.includes('local') || window.location.href.includes('stage'))) {
      alert('there was an error in response')
      if (typeof document !== 'undefined') {
        const errEl = document.createElement('div')
        errEl.innerHTML = error._body
        const targetEl = document.getElementById('err-detail')
        if (!!targetEl) {
          targetEl.appendChild(errEl)
        }
      }
      console.warn(error)
    }
  }
}
