import { parseBarcode } from 'gs1-barcode-parser-mod'
import { Observable } from '../utils'
import { BarcodeUtil } from '../utils'
import { EVENT, LoggerService } from './'

export const KEY = {
  ENTER: 13,
  ESC: 27,
}

export type GS1Result = {
  barcode?: string
  batch?: string
  bestBeforeDate?: string
  expirationDate?: string
  internalValue?: string
  sellPrice?: string
  rawCode: string
  parsedCode: any
}

export class KeyEventService {
  onScannedBarcode: Observable<string> = new Observable('core:keyEventService:onScannedBarcode')
  onScannedBox: Observable<string> = new Observable('core:keyEventService:onScannedBox')
  onScannedGS1: Observable<GS1Result> = new Observable('core:keyEventService:onScannedGS1')
  onScannedPositions: Observable<string> = new Observable('core:keyEventService:onScannedPositions')
  onKeystroke: Observable<string> = new Observable('core:keyEventService:onKeystroke')
  onKeyEnter: Observable<boolean> = new Observable('core:keyEventService:onKeyEnter')
  onKeyEsc: Observable<boolean> = new Observable('core:keyEventService:onKeyEsc')
  protected isKeyPressed = false
  protected isScannerActive = true
  protected timeToScan = 300
  protected scannedChars: string[] = []
  constructor(private loggerService: LoggerService, startListeners = false) {
    if (startListeners) {
      this.startListeners()
    }
  }

  startListeners(debug = false): void {
    window.onkeypress = event => this.catchBarcodeScanner(event)
    window.addEventListener('keydown', event => this.catchKeystroke(event))

    if (debug) {
      // Proxies are great for debugging integration, but are costly. Disable for performance reasons
      window.onkeypress = new Proxy(window.onkeypress, {
        apply: (target, thisArg, args) => {
          this.loggerService.log({
            type: EVENT.KEY,
            origin: this.constructor['name'],
            trace: this.loggerService.getStackTrace(),
            data: {
              message: 'KeyEventService:on key press',
              target,
              this: thisArg,
              args,
            },
          })
        },
      })
      window.addEventListener = new Proxy(window.addEventListener, {
        apply: (target, thisArg, args) => {
          if (args[0] === 'keydown') {
            this.loggerService.log({
              type: EVENT.KEY,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: {
                message: 'addEventListener:key down',
                target,
                this: thisArg,
                args,
              },
            })
          }
        },
      })
      window.removeEventListener = new Proxy(window.removeEventListener, {
        apply: (target, thisArg, args) => {
          if (args[0] === 'keydown') {
            this.loggerService.log({
              type: EVENT.KEY,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: {
                message: 'removeEventListener:key down',
                target,
                this: thisArg,
                args,
              },
            })
          }
        },
      })
    }
    this.loggerService.log({
      type: EVENT.KEY,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        message: 'KeyEventService:all listeners started',
        debug: debug,
      },
    })
    window['testBarcodeFromString'] = c => this.testBarcodeFromString(c)
  }

  stopListeners(): void {
    window.onkeypress = () => null
    window.removeEventListener('keydown', this.catchKeystroke)
    this.onScannedBarcode.unregisterObservers()
    this.onScannedBox.unregisterObservers()
    this.onScannedGS1.unregisterObservers()
    this.onScannedPositions.unregisterObservers()
    this.onKeystroke.unregisterObservers()
    this.onKeyEnter.unregisterObservers()
    this.onKeyEsc.unregisterObservers()
    this.loggerService.log({
      type: EVENT.KEY,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: {
        message: 'KeyEventService:all listeners stopped',
      },
    })
  }

  enableBarcodeScanner(): void {
    this.isScannerActive = true
  }

  disableBarcodeScanner(): void {
    this.isScannerActive = false
  }

  setTimeToScan(miliseconds: number): void {
    this.timeToScan = isNaN(miliseconds) || miliseconds < 300 ? 300 : miliseconds
  }

  getTimeToScan(): number {
    return this.timeToScan
  }

  protected catchKeystroke(event: any): void {
    if (this.scannedChars.length < 3) {
      // filter out scanner
      if (event.keyCode === KEY.ENTER && this.onKeyEnter.isObserved()) {
        this.onKeyEnter.emit(true)
      }
      if (event.keyCode === KEY.ESC && this.onKeyEsc.isObserved()) {
        this.onKeyEsc.emit(true)
      }
    }
    if (this.onKeystroke.isObserved()) {
      this.onKeystroke.emit(String.fromCharCode(event.which))
    }
  }

  protected catchBarcodeScanner(event: any): void {
    if (!this.isObserved()) {
      return
    }
    this.scannedChars.push(String.fromCharCode(event.which))
    if (!this.isKeyPressed) {
      setTimeout(() => {
        if (this.scannedChars.length >= 4) {
          const code = BarcodeUtil.translateBarcode(this.scannedChars.join(''))
          if (this.scannedChars.length >= 7 && !code.includes('PR-') && this.onScannedBarcode.isObserved()) {
            if (!this.isScannerActive) {
              return
            }
            this.loggerService.log({
              type: EVENT.KEY,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: {
                message: 'KeyEventService:scanned',
                type: 'code',
              },
            })
            this.onScannedBarcode.emit(code)
          } else if (!code.includes('PR-') && this.onScannedPositions.isObserved()) {
            const positions = code.split('-')
            if (positions.length === 3) {
              this.loggerService.log({
                type: EVENT.KEY,
                origin: this.constructor['name'],
                trace: this.loggerService.getStackTrace(),
                data: {
                  message: 'KeyEventService:scanned',
                  type: 'positions',
                },
              })
              this.onScannedPositions.emit(positions)
            }
          } else if (code.includes('PR-') && this.onScannedBox.isObserved()) {
            this.loggerService.log({
              type: EVENT.KEY,
              origin: this.constructor['name'],
              trace: this.loggerService.getStackTrace(),
              data: {
                message: 'KeyEventService:scanned',
                type: 'box',
              },
            })
            this.onScannedBox.emit(code)
          }
          if (this.scannedChars.length >= 7 && !code.includes('PR-') && this.onScannedGS1.isObserved()) {
            this.parseGS1(code)
          }
        }
        this.isKeyPressed = false
        this.scannedChars = []
      }, this.timeToScan)
    }
    this.isKeyPressed = true
  }

  parseGS1(code: string): void {
    if (!this.onScannedGS1.isObserved()) {
      return
    }
    try {
      const parsedCode = parseBarcode(code)
      if (parsedCode.codeName && parsedCode.parsedCodeItems?.length > 0) {
        const result: GS1Result = { parsedCode, rawCode: code }
        const gtinArray = parsedCode.parsedCodeItems.filter(pci => pci.ai === '01')
        const batchArray = parsedCode.parsedCodeItems.filter(pci => pci.ai === '10')
        const bestBeforeArray = parsedCode.parsedCodeItems.filter(pci => pci.ai === '15')
        const expirationArray = parsedCode.parsedCodeItems.filter(pci => pci.ai === '17')
        const internalValueArray = parsedCode.parsedCodeItems.filter(pci => pci.ai === '99')
        const sellPriceArray = parsedCode.parsedCodeItems.filter(pci => pci.ai === '390')
        if (gtinArray?.length > 0) {
          result.barcode = gtinArray[0].data
        }
        if (batchArray?.length > 0) {
          result.batch = batchArray[0].data
        }
        if (bestBeforeArray?.length > 0) {
          result.bestBeforeDate = bestBeforeArray[0].data
        }
        if (expirationArray?.length > 0) {
          result.expirationDate = expirationArray[0].data
        }
        if (internalValueArray?.length > 0) {
          result.internalValue = internalValueArray[0].data
        }
        if (sellPriceArray?.length > 0) {
          result.sellPrice = sellPriceArray[0].data
        }
        this.onScannedGS1.emit(result)
      }
    } catch (e) {
      console.warn(e)
    }
  }

  isObserved(): boolean {
    return (
      this.onScannedGS1.isObserved() ||
      this.onScannedBarcode.isObserved() ||
      this.onScannedBox.isObserved() ||
      this.onScannedPositions.isObserved() ||
      this.onKeyEnter.isObserved() ||
      this.onKeyEsc.isObserved() ||
      this.onKeystroke.isObserved()
    )
  }

  testBarcodeFromString(code: string) {
    console.warn('CORE:KeyEvent:testBarcodeFromString: ', {
      code,
      isScannerActive: this.isScannerActive,
      onScannedGS1: this.onScannedGS1.isObserved(),
      onScannedBarcode: this.onScannedBarcode.isObserved(),
      onScannedBox: this.onScannedBox.isObserved(),
      onScannedPositions: this.onScannedPositions.isObserved(),
      onKeyEnter: this.onKeyEnter.isObserved(),
      onKeyEsc: this.onKeyEsc.isObserved(),
      onKeystroke: this.onKeystroke.isObserved(),
    })
    if (!code) {
      code = ''
    }
    if (code.length >= 7 && !code.includes('PR-') && this.onScannedBarcode.isObserved()) {
      if (!this.isScannerActive) {
        return
      }
      this.onScannedBarcode.emit(code)
    } else if (!code.includes('PR-') && this.onScannedPositions.isObserved()) {
      const positions = code.split('-')
      if (positions.length === 3) {
        this.onScannedPositions.emit(positions)
      }
    } else if (code.includes('PR-') && this.onScannedBox.isObserved()) {
      this.onScannedBox.emit(code)
    }
    this.parseGS1(code)
  }
}
