import { EVENT, LoggerService } from './LoggerService'

interface StorageKey {
  key: string
  type: string
}

export class StorageService {
  asyncStorage: any = window['asyncStorage'] // RN @react-native-community/async-storage[https://github.com/react-native-community/async-storage/blob/LEGACY/docs/API.md]
  constructor(private loggerService: LoggerService, private appPrefix: string) {}

  get keys(): any {
    return {
      auth: {
        data: { key: 'auth_data', type: 'Object' },
        rememberMe: { key: 'auth_remember_me', type: 'boolean' },
        isAuthenticated: { key: 'auth_is_authenticated', type: 'boolean' },
        email: { key: 'auth_email', type: 'string' },
        clientType: { key: 'auth_client_type', type: 'string' },
        customServerUrl: { key: 'auth_custom_server_url', type: 'string' },
        company: { key: 'auth_company', type: 'Object' },
        isCompanyReloaded: { key: 'auth_is_company_reloaded', type: 'boolean' },
        companyCarriers: { key: 'auth_company_carriers', type: 'Array' },
      },
      app: {
        env: { key: 'app_env', type: 'string' },
        activeChildCompany: { key: 'app_active_child_company', type: 'Object' },
        language: { key: 'app_language', type: 'string' },
        sideMenuState: { key: 'app_sidemenu_state', type: 'string' },
        untranslatedMessages: { key: 'app_untranslated_messages', type: 'Array' },
        theme: { key: 'app_theme', type: 'string' },
        options: { key: 'app_options', type: 'Object' },
      },
      routing: {
        lastUrl: { key: 'routing_last_url', type: 'string' },
        queries: { key: 'routing_queries', type: 'Array' },
        lastOpenedSubTree: { key: 'last_opened_subtree', type: 'string' },
      },
      services: {
        notification: { key: 'services_notification', type: 'Array' },
        userRoles: { key: 'services_user_roles', type: 'Array' },
      },
      utils: {
        history: { key: 'utils_history_history', type: 'Array' },
        resizerLastMaxWidth: { key: 'utils_resizer_last_max_width', type: 'number' },
      },
      views: {
        addresses: { key: 'views_addresses', type: 'Object' },
        barcodePrintArray: { key: 'views_barcode_print_array', type: 'Object' },
        baseListing: { key: 'views_base_listing', type: 'Object' },
        batchOperations: { key: 'views_batch_operations', type: 'Object' },
        companyCarriers: { key: 'views_company_carriers', type: 'Object' },
        consents: { key: 'views_consents', type: 'Object' },
        customers: { key: 'views_customers', type: 'Object' },
        dashboard: { key: 'views_dashboard', type: 'Object' },
        depots: { key: 'views_depots', type: 'Object' },
        eetCerts: { key: 'views_eet_certs', type: 'Object' },
        eetReceipts: { key: 'views_eet_receipts', type: 'Object' },
        eetShops: { key: 'views_eet_shops', type: 'Object' },
        checkouts: { key: 'views_checkouts', type: 'Object' },
        lastStockItemSupplier: { key: 'views_last_stock_item_supplier', type: 'number' },
        lastStockItemDepot: { key: 'views_last_stock_item_depot', type: 'number' },
        lastStockItemPurchasePrice: { key: 'views_last_stock_item_purchase_price', type: 'string' },
        modal: { key: 'views_modal', type: 'Object' },
        modalPrintExports: { key: 'views_modal_print_exports', type: 'Object' },
        parameters: { key: 'views_parameters', type: 'Object' },
        payments: { key: 'views_payments', type: 'Object' },
        priceLevels: { key: 'views_price_levels', type: 'Object' },
        printInventoryExports: { key: 'views_print_inventory_exports', type: 'Object' },
        printOrderExports: { key: 'views_print_order_exports', type: 'Object' },
        printPurchaseExports: { key: 'views_print_purchase_exports', type: 'Object' },
        printReports: { key: 'views_print_reports', type: 'Object' },
        printSaleExports: { key: 'views_print_sale_exports', type: 'Object' },
        printStockExports: { key: 'views_print_stock_exports', type: 'Object' },
        producers: { key: 'views_producers', type: 'Object' },
        productMoves: { key: 'views_product_moves', type: 'Object' },
        productMovesList: { key: 'views_product_moves_list', type: 'Object' },
        products: { key: 'views_products', type: 'Object' },
        stockOperationCardSupplier: { key: 'views_stock_operation_card_supplier', type: 'number' },
        stockOperationCardSelectData: { key: 'views_stock_operation_card_select_data', type: 'Object' },
        stockOperationCardIsLockedQuantity: { key: 'views_stock_operation_card_is_locked_quantity', type: 'boolean' },
        stockOperationCardLockedQuantity: { key: 'views_stock_operation_card_locked_quantity', type: 'number' },
        stockOperationList: { key: 'views_stock_operation_list', type: 'Object' },
        soldItems: { key: 'views_sold_items', type: 'Object' },
        suppliers: { key: 'views_suppliers', type: 'Object' },
        orders: { key: 'views_orders', type: 'Object' },
        tags: { key: 'views_tags', type: 'Object' },
        users: { key: 'views_users', type: 'Object' },
        vats: { key: 'views_vats', type: 'Object' },
        vouchers: { key: 'views_vouchers', type: 'Object' },
      },
    }
  }

  set(key: StorageKey, value: any): void {
    let stringifiedValue = ''
    switch (key.type) {
      case 'string':
        stringifiedValue = value
        break
      case 'number':
      case 'boolean':
      case 'Object':
      case 'Array':
        stringifiedValue = JSON.stringify(value)
        break
    }
    if ('localStorage' in window) {
      window.localStorage.setItem(`${this.appPrefix}${key.key}`, stringifiedValue)
    } else if ('asyncStorage' in window) {
      this.asyncStorage.setItem(`${this.appPrefix}${key.key}`, stringifiedValue)
    }
  }

  async get(key: StorageKey): Promise<any> {
    if ('localStorage' in window) {
      return new Promise(resolve => {
        return resolve(this.getSync(key))
      })
    } else if ('asyncStorage' in window) {
      const stringifiedValue = await this.asyncStorage.getItem(`${this.appPrefix}${key.key}`)
      return this.parseStringifiedData(key, stringifiedValue)
    }
  }

  /**
   * @description for usage in desktop apps only
   * @param key StorageKey
   */
  getSync(key: StorageKey): any {
    if ('localStorage' in window) {
      const stringifiedValue = window.localStorage.getItem(`${this.appPrefix}${key.key}`)
      return this.parseStringifiedData(key, stringifiedValue)
    }
  }

  parseStringifiedData(key: StorageKey, stringifiedValue?: string | null): Promise<any> {
    let value: any
    if (stringifiedValue && stringifiedValue.length > 0) {
      switch (key.type) {
        case 'string':
          value = stringifiedValue
          break
        case 'boolean':
          if (!stringifiedValue || stringifiedValue == 'undefined') {
            value = false
            break
          } // else = fallthrough && try-catch json.parse
        case 'number':
        case 'Object':
        case 'Array':
          try {
            value = JSON.parse(stringifiedValue)
          } catch (e) {
            console.warn(`StorageUtil Exception: ${e.message}`)
          }
          if (key.type === 'number') {
            value = isNaN(Number(value)) ? 0 : Number(value)
          }
          break
      }
    }
    return value
  }

  clear(key: StorageKey | null): any {
    this.loggerService.log({
      type: EVENT.LOCAL_STORAGE,
      origin: this.constructor['name'],
      trace: this.loggerService.getStackTrace(),
      data: key,
    })
    let storage: any
    if ('localStorage' in window) {
      storage = window.localStorage
    } else if ('asyncStorage' in window) {
      storage = this.asyncStorage
    }
    if (key && key['key'] && key['type']) {
      storage.removeItem(`${this.appPrefix}${key['key']}`)
    } else if (key && Object.keys(key).length > 0) {
      Object.keys(key).forEach(k => {
        storage.removeItem(`${this.appPrefix}${key[k]['key']}`)
      })
    } else if (key === null) {
      storage.clear()
    }
  }
}
