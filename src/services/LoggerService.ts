import { LoggerEvent } from '../models'
import { Observable } from '../utils'

export const LOGGER_TYPE = {
  CONSOLE: 'console',
  OBSERVABLE: 'observable',
}

export const EVENT = {
  EXCEPTION: 'exception',
  LOGGER: 'logger',
  LOCAL_STORAGE: 'localStorage',
  OAUTH: 'oauth',
  CHECKOUT: 'checkout',
  CUSTOMER: 'customer',
  GRAPHQL: 'graphql',
  KEY: 'key',
  EET: 'eet',
  EVENTS: 'events',
  HTTP: 'http',
  MESSAGE: 'message',
  ORDER: 'order',
  PRINT: 'print',
  PRODUCT: 'product',
  STATS: 'stats',
  USER: 'user',
  VAT: 'vat',
}

export class LoggerService {
  logger: Observable<string> | any = null
  loggerType: string = LOGGER_TYPE.CONSOLE
  filters: string[] | any

  constructor(loggerType: string | undefined = LOGGER_TYPE.CONSOLE, filters: string[] = []) {
    this.loggerType = loggerType
    this.filters = filters
    if (this.loggerType === LOGGER_TYPE.OBSERVABLE) {
      this.logger = new Observable('core:loggerService:logger')
    } else {
      this.logger = console
      this.loggerType = LOGGER_TYPE.CONSOLE
    }
  }

  log(event: LoggerEvent): void {
    if (!this.isLoggable(event)) {
      return
    }
    if (!!this.logger && this.loggerType === LOGGER_TYPE.CONSOLE) {
      this.logger.log(event)
    } else if (!!this.logger && this.loggerType === LOGGER_TYPE.OBSERVABLE) {
      this.logger.emit(event)
    }
  }

  err(event: LoggerEvent): void {
    if (!this.isLoggable(event)) {
      return
    }
    if (!!this.logger && this.loggerType === LOGGER_TYPE.CONSOLE) {
      this.logger.warn(event)
    } else if (!!this.logger && this.loggerType === LOGGER_TYPE.OBSERVABLE) {
      this.logger.error(event)
    }
  }

  getStackTrace(): string | string[] {
    if (!!Error['captureStackTrace']) {
      const obj: any = {}
      Error['captureStackTrace'](obj, this.getStackTrace)
      return obj.stack
        .split('\n')
        .filter(r => !(r.includes('Error') || r.includes('at LoggerService.')))
        .map(r => r.trim())
      // .join('\n')
    } else {
      let res = ''
      try {
        const a = {}
        a['debug']()
      } catch (ex) {
        res = ex.stack
      }
      return res
      // return console.trace()
    }
  }

  protected isLoggable(event: LoggerEvent): boolean {
    return !!this.filters.includes(event.type)
  }
}
