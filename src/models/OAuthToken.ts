export class OAuthToken {
  accessToken: string
  refreshToken: string
  tokenType: string
  scope: string
  expiresIn: number
  timestamp: number

  constructor(data: any = {}) {
    this.accessToken = !!data.accessToken ? data.accessToken : !!data.access_token ? data.access_token : ''
    this.refreshToken = !!data.refreshToken ? data.refreshToken : !!data.refresh_token ? data.refresh_token : ''
    this.tokenType = !!data.tokenType ? data.tokenType : !!data.token_type ? data.token_type : ''
    this.scope = !!data.scope ? data.scope : ''
    this.expiresIn = !!data.expiresIn ? data.expiresIn : !!data.expires_in ? data.expires_in : 0
    this.timestamp = !!data.timestamp ? data.timestamp : +new Date()
  }
}
