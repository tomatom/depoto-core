export class NotificationEventType {
  static readonly start = 'start'
  static readonly resolve = 'resolve'
  static readonly reject = 'reject'
}

export class NotificationEventOperation {
  static readonly list = 'list'
  static readonly detail = 'detail'
  static readonly create = 'create'
  static readonly update = 'update'
  static readonly delete = 'delete'
}

export class NotificationEvent {
  type: string
  entity: string
  method: string
  operation: string
  response: any
  errors: string[]
  date: string

  constructor(data: any = {}) {
    this.type = data.type || ''
    this.entity = data.entity || ''
    this.method = data.method || ''
    this.operation = data.operation || ''
    this.response = data.response || null
    this.errors = data.errors || []
    this.date = data.date || Date.now() + ''
  }
}
