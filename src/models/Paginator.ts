export class Paginator {
  current: number
  endPage: number
  totalCount: number

  constructor(data: any = {}) {
    this.current = !isNaN(data.current) && data.current !== 0 ? data.current : 0
    this.endPage = !isNaN(data.endPage) && data.endPage !== 0 ? data.endPage : 0
    this.totalCount = !isNaN(data.totalCount) && data.totalCount !== 0 ? data.totalCount : 0
  }

  get isEndPage(): boolean {
    return this.current === this.endPage
  }
}
