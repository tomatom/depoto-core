import { Events } from '../utils'

export class NotificationAlert {
  text: string
  type: string // success | warning
  time: string

  constructor(data: any = {}) {
    this.text = data.text || ''
    this.type = data.type || 'success'
    this.time = data.time || 10000
  }

  static showNotification(text: string, type = 'success', time = 10000) {
    Events.dispatch(
      'notification:show',
      new NotificationAlert({
        text: text,
        type: type,
        time: time,
      }),
    )
  }
}
