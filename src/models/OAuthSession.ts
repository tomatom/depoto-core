import { OAuthToken } from './OAuthToken'

export class OAuthSession {
  clientId: string
  clientSecret: string
  clientType: string
  clientCustomUri: string
  email: string
  password: string
  isAuthenticated: boolean
  isRememberMe: boolean
  token: OAuthToken

  constructor(data: any = {}) {
    this.clientId = !!data.clientId ? data.clientId : ''
    this.clientSecret = !!data.clientSecret ? data.clientSecret : ''
    this.clientType = !!data.clientType ? data.clientType : 'prod'
    this.clientCustomUri = !!data.clientCustomUri ? data.clientCustomUri : ''
    this.email = !!data.email ? data.email : ''
    this.password = !!data.password ? data.password : ''
    this.isAuthenticated = !!data.isAuthenticated ? data.isAuthenticated : false
    this.isRememberMe = !!data.isRememberMe ? data.isRememberMe : false
    this.token = !!data.token ? new OAuthToken(data.token) : new OAuthToken()
  }
}
