export class LoggerEvent {
  type: string
  origin: string
  data: any
  trace: string | string[]

  constructor(data: any = {}) {
    this.type = !!data.type ? data.type : 'log'
    this.origin = !!data.origin ? data.origin : null
    this.data = !!data.data ? data.data : null
    this.trace = !!data.trace ? data.trace : null
  }
}
