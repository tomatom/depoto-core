export class Translation {
  token: string
  cs: string
  en: string
  constructor(data: any = {}) {
    this.token = data.token || ''
    this.cs = data.cs || ''
    this.en = data.en || ''
  }
}
