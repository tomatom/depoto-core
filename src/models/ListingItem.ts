export class ListingItem {
  name: string
  nameForSettings?: string
  cssClass?: string
  title?: string
  property: string
  propType: string
  size?: number
  sortable: boolean
  sortBy?: string
  btnStyle?: string
  icon?: string = ''
  action?: string
  route?: string
  align?: string
  isDefault?: boolean

  constructor(data: any = {}) {
    this.name = data.name || ''
    this.nameForSettings = data.nameForSettings || ''
    this.cssClass = data.cssClass || ''
    this.title = data.title || ''
    this.property = data.property || ''
    this.propType = data.propType || ''
    this.size = data.size || null
    this.sortable = data.sortable
    this.sortBy = data.sortBy || null
    this.btnStyle = data.btnStyle || null
    this.icon = data.icon || null
    this.action = data.action || null
    this.route = data.route || null
    this.align = data.align || null
    this.isDefault = data.isDefault || false
  }

  static propTypes = {
    string: 'string',
    number: 'number',
    integer: 'integer',
    price: 'price',
    'price-cents': 'price-cents',
    date: 'date',
    datetime: 'datetime',
    label: 'label',
    boolean: 'boolean',
    approved: 'approved',
    array: 'array',
    'array-vertical': 'array-vertical',
    btn: 'btn',
    'btn-right': 'btn-right',
    'btn-gen': 'btn-gen',
    'btn-resend-eet-receipt': 'btn-resend-eet-receipt',
    'modal-eet-receipt': 'modal-eet-receipt',
    'modal-address': 'modal-address',
    'order-bill-number': 'order-bill-number',
    'order-unavailables': 'order-unavailables',
    'order-reservation-actions': 'order-reservation-actions',
    'order-payment-items': 'order-payment-items',
    'reservation-number-unavailables': 'reservation-number-unavailables',
    'checkout-name': 'checkout-name',
    'customer-name': 'customer-name',
    'carrier-name': 'carrier-name',
    'process-status': 'process-status',
    'pipe-order-items-price': 'pipe-order-items-price',
  }

  static alignTypes = {
    left: 'left',
    center: 'center',
    right: 'right',
  }
}
