export class Notification {
  title: string
  content: string
  date: string

  constructor(data: any = {}) {
    this.title = data.title || ''
    this.content = data.content || ''
    this.date = data.date || Date.now() + ''
  }
}
