export class PaginationUtil {
  ngComponent: any = null
  paginationBtnId: string
  collectionProp: string
  fetchFunc: string
  currentPageProp: string
  endPageProp: string
  totalItemsProp: string
  scrollOffsetThreshold: number
  scrollDebounceTimeout: any = null
  constructor(
    ngComponent: any,
    paginationBtnId = 'pagination-next-btn',
    collectionProp = 'items',
    fetchFunc = 'getItems',
    currentPageProp = 'currentPage',
    endPageProp = 'endPage',
    totalItemsProp = 'totalItems',
    scrollOffsetThreshold = 5,
  ) {
    this.ngComponent = ngComponent
    this.paginationBtnId = paginationBtnId
    this.collectionProp = collectionProp
    this.fetchFunc = fetchFunc
    this.currentPageProp = currentPageProp
    this.endPageProp = endPageProp
    this.totalItemsProp = totalItemsProp
    this.scrollOffsetThreshold = scrollOffsetThreshold
    const t = this
    window.onscroll = function () {
      const el = document.getElementById(t.paginationBtnId)
      if ((!t.ngComponent.loading || !t.ngComponent.reloading) && !t.scrollDebounceTimeout && t.checkVisible(el)) {
        t.scrolled()
        t.scrollDebounceTimeout = setTimeout(() => {
          t.scrollDebounceTimeout = null
        }, 700)
      }
    }
  }

  scrolled() {
    if (
      (!this.ngComponent[this.currentPageProp] ||
        this.ngComponent[this.currentPageProp] < this.ngComponent[this.endPageProp]) &&
      this.ngComponent[this.totalItemsProp] > this.ngComponent[this.collectionProp].length &&
      !this.ngComponent.loading
    ) {
      if (!this.ngComponent[this.currentPageProp]) {
        this.ngComponent[this.currentPageProp] = 1
      }
      if (typeof this.ngComponent[this.fetchFunc] === 'function') {
        this.ngComponent[this.fetchFunc](this.ngComponent[this.currentPageProp] + 1)
      }
    }
  }

  private checkVisible(el, threshold = this.scrollOffsetThreshold, mode = 'visible'): boolean {
    if (el) {
      const rect = el.getBoundingClientRect()
      const viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight)
      const above = rect.bottom - threshold < 0
      const below = rect.top - viewHeight + threshold >= 0
      return mode === 'above' ? above : mode === 'below' ? below : !above && !below
    }
    return false
  }
}
