const DECORATOR = {
  ANNOTATION_PROPTYPE: 'annotation:proptype',
}

export const PropType = (key: string): any => {
  return Reflect.metadata(DECORATOR.ANNOTATION_PROPTYPE, key)
}

export const getPropType = (instance: any, key: string): string => {
  return Reflect.getMetadata(DECORATOR.ANNOTATION_PROPTYPE, instance, key)
}
