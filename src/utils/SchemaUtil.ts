import * as ENTITIES from '../entities'
import { getPropType } from './'

export const SCHEMA_MAX_DEPTH = 10

export class SchemaUtil {
  static extractFrom(
    entityType: string,
    depth: number = SCHEMA_MAX_DEPTH,
    excludedClasses: Array<string> | any = [],
    classPath: Array<string> | any = [],
  ): any {
    if (depth === 0) {
      return {}
    }
    if (depth === null || depth === undefined || depth > SCHEMA_MAX_DEPTH) {
      depth = SCHEMA_MAX_DEPTH
    }
    if (!entityType) {
      return
    }
    // @ts-ignore
    const instance = SchemaUtil.getInstance(entityType)
    const result: any = {}
    Object.keys(instance).map(k => {
      if (Array.isArray(instance[k]) || typeof instance[k] === 'object') {
        const kEntityType = getPropType(instance, k)
        // console.warn(instance[k], k, typeof kEntityType, kEntityType)
        if (!kEntityType) {
          result[k] = null
        } else if (excludedClasses.includes(kEntityType) || kEntityType === 'local') {
          // skip! (excludedClasses and local props)
        } else if (classPath.includes(kEntityType) || classPath.length >= depth) {
          // console.warn('in classpath or depth ! ', kEntityType, classPath, depth, classPath.length)
          const kInstance = SchemaUtil.getInstance(kEntityType)
          // @ts-ignore
          if (kInstance && Object.keys(kInstance).includes('id')) {
            result[k] = { id: null }
            if (Object.keys(kInstance).includes('name') && getPropType(kInstance, 'name') !== 'local') {
              result[k].name = null
            }
            if (Object.keys(kInstance).includes('depots') && getPropType(kInstance, 'depots') !== 'local') {
              result[k].depots = { id: null }
            }
          } // else skip!
        } else {
          result[k] = SchemaUtil.extractFrom(kEntityType, depth, excludedClasses, [...classPath, entityType])
        }
      } else {
        if (getPropType(instance, k) !== 'local') {
          result[k] = null
        }
      }
    })
    return result
  }

  private static getInstance(entityType: string): any {
    if (!entityType || typeof ENTITIES[entityType] !== 'function') {
      if (!!window.location && (window.location.href.includes('local') || window.location.href.includes('stage'))) {
        // throw new Error(`Core:SchemaUtil:getInstance ERROR: no type for ${entityType}!`)
        console.error(`Core:SchemaUtil:getInstance ERROR: no type for ${entityType}!`)
        return 'NO TYPE!'
      }
      return null
    }

    return new ENTITIES[entityType]()
  }
}
