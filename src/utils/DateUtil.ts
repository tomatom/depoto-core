export class DateUtil {
  static getDateStringFromUTCString(utcString: string): string {
    if (utcString && utcString.length > 0) {
      const d = new Date(utcString)
      return (
        d.getFullYear() +
        '-' +
        ('0' + (d.getMonth() + 1)).slice(-2) +
        '-' +
        ('0' + d.getDate()).slice(-2) +
        ' ' +
        ('0' + d.getHours()).slice(-2) +
        ':' +
        ('0' + d.getMinutes()).slice(-2) +
        ':' +
        ('0' + d.getSeconds()).slice(-2)
      )
    }
    return ''
  }

  static formatDate(utcString: string, hoursAndMins = false): string {
    const d = new Date(utcString)
    if (hoursAndMins) {
      return (
        ('0' + d.getDate()).slice(-2) +
        '.' +
        ('0' + (d.getMonth() + 1)).slice(-2) +
        '.' +
        d.getFullYear() +
        ' ' +
        ('0' + d.getHours()).slice(-2) +
        ':' +
        ('0' + d.getMinutes()).slice(-2) +
        ':' +
        ('0' + d.getSeconds()).slice(-2)
      )
    }
    return ('0' + d.getDate()).slice(-2) + '.' + ('0' + (d.getMonth() + 1)).slice(-2) + '.' + d.getFullYear()
  }
}
