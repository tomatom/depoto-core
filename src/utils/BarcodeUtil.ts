export class BarcodeUtil {
  static translateBarcode(barcode: string, skipKeyLayoutCheck = false): string {
    if (!skipKeyLayoutCheck && !(barcode && barcode.length > 0 && BarcodeUtil.isKeyboardLayoutSupported(barcode))) {
      alert('Nepodporované rozložené klávesnice, přepněte na české!')
      return ''
    }
    const keyTranslations: any = {
      // '\\+': '1',
      ě: '2',
      š: '3',
      č: '4',
      ř: '5',
      ž: '6',
      ý: '7',
      á: '8',
      í: '9',
      é: '0',
      // '\\=': '-',
      '=': '-',
    }

    let result: string = encodeURIComponent(BarcodeUtil.replaceAll(barcode, keyTranslations))
    result = result.split('%2B').join('1')
    if (result.includes('%5C')) {
      // `\` => `-`
      result = result.split('%5C').join('-')
    }
    if (result.includes('%0D')) {
      // newline
      result = result.replace('%0D', '')
    }

    // eng keylayout translate: // NOPE: it cant read some characters (like `-` or `\`), so, no ENG keyboard!
    // if (result.includes('!')) {
    //   result = result.split('!').join('1');
    // }
    // if (result.includes('%40')) { // @
    //   result = result.split('%40').join('2');
    // }
    // if (result.includes('%23')) { // #
    //   result = result.split('%23').join('3');
    // }
    // if (result.includes('%24')) { // $
    //   result = result.split('%24').join('4');
    // }
    // if (result.includes('%25')) { // %
    //   result = result.split('%25').join('5');
    // }
    // if (result.includes('%5E')) { // ^
    //   result = result.split('%5E').join('6');
    // }
    // if (result.includes('%26')) { // &
    //   result = result.split('%26').join('7');
    // }
    // if (result.includes('*')) { // *
    //   result = result.split('*').join('8');
    // }
    // if (result.includes('(')) { // (
    //   result = result.split('(').join('9');
    // }
    // if (result.includes(')')) { // )
    //   result = result.split(')').join('0');
    // }
    // if (result.includes('_')) { // _
    //   result = result.split('_').join('-');
    // }

    return decodeURIComponent(result).trim()
  }

  static replaceAll(str: string, mapObj: any): string {
    if (!str) {
      return ''
    }
    const re: any = new RegExp(Object.keys(mapObj).join('|'), 'gi')

    return str.replace(re, function (matched) {
      return mapObj[matched.toLowerCase()]
    })
  }

  static isKeyboardLayoutSupported(str: string): boolean {
    return !(
      str.includes('@') ||
      str.includes('#') ||
      str.includes('$') ||
      str.includes('%') ||
      str.includes('^') ||
      str.includes('&') ||
      str.includes('*') ||
      str.includes('(') ||
      str.includes(')') ||
      str.includes('_')
    )
  }
}
