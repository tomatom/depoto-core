export function styledConsoleLog(args: string | any = null, args2?: any, args3?: any, args4?: any, args5?: any): void {
  const argArray: string[] = []
  if (arguments.length) {
    const startTagRe = /<span\s+style=(['"])([^'"]*)\1\s*>/gi
    const endTagRe = /<\/span>/gi

    let reResultArray
    argArray.push(arguments[0].replace(startTagRe, '%c').replace(endTagRe, '%c'))
    while ((reResultArray = startTagRe.exec(arguments[0]))) {
      argArray.push(reResultArray[2])
      argArray.push('')
    }
    // pass through subsequent arguments since chrome dev tools does not (yet) support console.log styling of the following form: console.log('%cBlue!', 'color: blue;', '%cRed!', 'color: red;');
    for (let j = 1; j < arguments.length; j++) {
      argArray.push(arguments[j])
    }
  }
  console.log.apply(console, argArray)
}
