import { Fn } from '../models'

// TODO: RN implementation
export class Events {
  static dispatch(name: string, data?: any) {
    const event = new CustomEvent(name, { detail: data })
    if ('dispatchEvent' in window) {
      window.dispatchEvent(event)
    } else {
      console.warn('TODO: RN implementation (Events:dispatch)') // TODO: RN implementation
    }
    if (window['DEBUG'] && window['DEBUG'].events) {
      console.log('>>> events:dispatched ', name, data)
    }
  }

  static listen(name: string, callback: Fn) {
    if (window['DEBUG'] && window['DEBUG'].events) {
      console.log('>>> events:listening ', name, callback)
    }
    if ('addEventListener' in window) {
      window.addEventListener(name, e => {
        if (window['DEBUG'] && window['DEBUG'].events) {
          console.log('>>> events:triggered ', name, e['detail'])
        }
        callback ? callback(name, e['detail']) : null
      })
    } else {
      console.warn('TODO: RN implementation (Events:listen)') // TODO: RN implementation
    }
  }
}
