export class GraphQLUtil {
  static prepareParams(params) {
    Object.keys(params).forEach((k, i) => {
      if (params[k] === undefined) {
        delete params[k]
      }
    })
    return params
  }
  static createMutation(data: any, dataDefinition: any, method: string, mutationName?: string): string {
    if (!method || !data) {
      return ''
    }
    if (method.includes('update')) {
      data = GraphQLUtil.prepareParams(data)
    }

    let mutation: string = 'mutation ' + (mutationName || method) + '{' + method

    mutation += '(' + GraphQLUtil.flattenObject(data) + ')'

    mutation += GraphQLUtil.processDataDefinition(dataDefinition || data) + '}'

    return mutation
  }

  static createQuery(dataDefinition: any, method: string, parameters: any, queryName?: string): string {
    if (!method || !dataDefinition) {
      return ''
    }

    let query: string = 'query ' + (queryName || method) + '{' + method

    query += '(' + GraphQLUtil.flattenObject(parameters) + ')'

    query += GraphQLUtil.processDataDefinition(dataDefinition) + '}'

    return query
  }

  static createComposedQuery(data: any): string {
    let query = 'query{'
    data.forEach((row, i) => {
      if (i > 0) {
        query += ','
      }
      query += row.method
      query += '(' + GraphQLUtil.flattenObject(row.parameters) + ')'
      query += GraphQLUtil.processDataDefinition(row.definition)
    })
    return query + '}'
  }

  private static processDataDefinition(dataDefinition: any): string {
    if (!dataDefinition) {
      return ''
    }

    let query = ''

    const keys: string[] = Object.keys(dataDefinition)
    keys.forEach((key: string, index: number) => {
      if (!index) {
        query += '{'
      }

      query += key

      if (dataDefinition[key] instanceof Array && dataDefinition[key].length) {
        query += GraphQLUtil.processDataDefinition(dataDefinition[key][0])
      } else if (dataDefinition[key] instanceof Object) {
        query += GraphQLUtil.processDataDefinition(dataDefinition[key])
      }

      if (index === keys.length - 1) {
        query += '}'
      } else {
        query += ','
      }
    })

    return query
  }

  private static flattenObject(object: any): string {
    return Object.keys(object || {})
      .reduce((array: any[], key: string) => {
        if (!GraphQLUtil.isBlank(object[key])) {
          array.push(key + ':' + GraphQLUtil.processValue(object[key]))
        } else {
          // fix issue with not updating null values.. so, send every key supplied to mutation | query
          array.push(key + ':' + null)
        }

        return array
      }, [])
      .join(',')
  }

  private static processValue(value: any): string | number {
    if (GraphQLUtil.isBlank(value)) {
      return ''
    }

    // if (GraphQLUtil.isNumber(value)) { return Number(value) } // todo: debug! ('' => 0 ???)

    // const isJsonStr = str => { // todo dont use this! json must be encoded, contact BE dev in case of double encoded json returned!!
    //   let result = false
    //   let tmpObj = null
    //   if (str && str.length > 1 && (str[0] === '{' || str[0] === '[')) {
    //     try {
    //       tmpObj = JSON.parse(str)
    //       if (typeof tmpObj === 'object' || Array.isArray(tmpObj)) {
    //         result = true
    //       }
    //     } catch (e) {}
    //   }
    //   return result
    // }

    if (GraphQLUtil.isString(value)) {
      if (GraphQLUtil.isStringDateOrBase64(value)) {
        // console.warn('str eaw: ', value) // TODO: remove
        // const datastr = atob(value.substr(10))
        //dwnload
        // const link = document.createElement('a')
        // // link.setAttribute('href', base64.replace('image/jpeg', 'image/octet-stream'))
        // link.setAttribute('href', value)
        // link.setAttribute('download', 'test11111.xlsx')
        //
        // link.innerText = 'download image'
        // document.body.appendChild(link)
        // link.click()
        // return `"${value}"`
        return `"${value.replace('data:application/octet-stream;base64,', '')}"`
        // } else if (isJsonStr(value)) {
        //   return `"${value}"`
      } else {
        return `"${encodeURIComponent(value)}"`
      }
    }
    // if (GraphQLUtil.isString(value)) { return '"' + value.replace(/'/g, '&quot').replace(/"/g, '&quot') + '"' }

    if (GraphQLUtil.isArray(value)) {
      let arrayString = '['

      value.forEach((valueInArray: any, index: number) => {
        arrayString += GraphQLUtil.processValue(valueInArray)
        if (index !== value.length - 1) {
          arrayString += ','
        }
      })

      arrayString += ']'

      return arrayString
    }

    if (GraphQLUtil.isStringMap(value)) {
      let objectString = '{'

      const keys: string[] = Object.keys(value)
      keys.forEach((key: string, index: number) => {
        objectString += key + ':' + GraphQLUtil.processValue(value[key])
        if (index !== keys.length - 1) {
          objectString += ','
        }
      })

      objectString += '}'

      return objectString
    }

    return value.toString()
  }

  private static isArray(obj: any): boolean {
    return Array.isArray(obj)
  }

  private static isString(obj: any): boolean {
    return typeof obj === 'string'
  }

  private static isStringDateOrBase64(str: string): boolean {
    if (!str) {
      return false
    }
    // try { // does not work
    //   atob(str)
    //   console.warn('is base 64!!!!!!!!!!!!!') // TODO: remove
    //   return true // is base64 string
    // } catch (e) {}
    if (str.substr(0, 5) === 'data:') {
      return true
    }
    let date: any
    try {
      date = new Date(str)
    } catch (e) {
      date = null
    }
    return !!date ? date.getFullYear() === Number(str.substr(0, 4)) : false
  }

  private static isStringMap(obj: any) {
    return typeof obj === 'object' && obj !== null
  }

  private static isBlank(obj: any) {
    return obj === undefined || obj === null
  }

  private static isNumber(obj: any) {
    if (typeof obj == 'boolean' || (typeof obj == 'string' && obj[0] === '0')) {
      return false
    }
    if (typeof obj == 'number' && obj.toFixed(0)[0] === '0') {
      return false
    }
    return !Array.isArray(obj) && !isNaN(Number(obj))
  }
}
