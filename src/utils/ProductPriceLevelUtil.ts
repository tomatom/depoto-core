import { Customer, Product } from '../entities'

export class ProductPriceLevelUtil {
  static getProductPriceForCustomerPriceLevel(product: Product, customer?: Customer): number {
    if (!customer || (customer && !customer.priceLevel)) {
      return product.actualSellPrice
    } else if (customer.priceLevel?.isPercentage === true) {
      return product.actualSellPrice - product.actualSellPrice * (customer.priceLevel?.percent / 100)
    } else if (customer.priceLevel?.isPercentage === false) {
      for (const productPriceLevel of product.productPriceLevels) {
        if (productPriceLevel.id === customer.priceLevel?.id) {
          return productPriceLevel.sellPrice
        }
      }
    }
    // if none of the productPriceLevels is the same as customer's priceLevel, then assign default sellPrice
    return product.actualSellPrice
  }
}
