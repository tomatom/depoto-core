export interface EntityDefinitionArgs {
  definitionDepth: number
  definitionExcludedClasses: string[]
  definitionObject?: any
}
