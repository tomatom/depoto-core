export class Currency {
  id: string
  name: string
  ratio: number

  constructor(data: any = {}) {
    this.id = data.id || ''
    this.name = data.name || ''
    this.ratio = data.ratio || 1
  }
}
