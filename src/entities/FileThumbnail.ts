export class FileThumbnail {
  format: string
  mimeType: string
  url: string

  constructor(data: any = {}) {
    this.format = data.format || ''
    this.mimeType = data.mimeType || ''
    this.url = data.url || ''
  }
}
