import { Parameter, Product, Company } from './'
import { PropType } from '../utils'

export class ProductParameter {
  id: number
  value: string
  externalId: string
  @PropType('Product')
  product: Product
  @PropType('Parameter')
  parameter: Parameter

  constructor(data: any = {}) {
    this.id = data.id || null
    this.value = data.value || ''
    this.externalId = data.externalId || null
    this.product = data.product ? new Product(data.product) : new Product()
    this.parameter = data.parameter ? new Parameter(data.parameter) : new Parameter()
  }
}
