import { Checkout, EETCert } from './'
import { PropType } from '../utils'

export class EETShop {
  id: number
  name: string
  street: string
  city: string
  country: string
  zip: string
  eetId: number
  @PropType('EETCert')
  cert: EETCert
  @PropType('Checkout')
  checkouts: Checkout[]

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || null
    this.street = data.street || null
    this.city = data.city || null
    this.country = data.country || null
    this.zip = data.zip || null
    this.eetId = data.eetId || null
    this.cert = data.cert ? new EETCert(data.cert) : new EETCert()
    this.checkouts =
      data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(checkout => new Checkout(checkout)) : []
  }
}
