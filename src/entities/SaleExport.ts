import { Checkout, Producer, Supplier } from './'
import { PropType } from '../utils'

export class SaleExport {
  public id: number
  public generated: string
  public created: string
  public url: string
  public format: string
  public dateFrom: string
  public dateTo: string
  @PropType('Checkout')
  public checkouts: Checkout[]
  @PropType('Producer')
  public producers: Producer[]
  @PropType('Supplier')
  public suppliers: Supplier[]

  public constructor(data: any = {}) {
    this.id = data.id || 0
    this.generated = data.generated || ''
    this.created = data.created || new Date().toUTCString()
    this.url = data.url || ''
    this.format = data.format || ''
    this.dateFrom = data.dateFrom || ''
    this.dateTo = data.dateTo || ''
    this.checkouts =
      data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(checkout => new Checkout(checkout)) : []
    this.producers =
      data.producers && data.producers.length > 0 ? data.producers.map(producer => new Producer(producer)) : []
    this.suppliers =
      data.suppliers && data.suppliers.length > 0 ? data.suppliers.map(supplier => new Supplier(supplier)) : []
  }
}
