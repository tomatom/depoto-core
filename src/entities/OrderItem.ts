import { ClearanceItem, Order, Product, ProductMove, Vat } from './'
import { generateUuidV4, PropType } from '../utils'

export class OrderItem {
  id: number
  @PropType('local')
  uuid: string // local only, for order.page processes..
  name: string
  ean: string
  code: string
  quantity: number
  sale: number
  price: number
  priceWithoutVat: number
  priceAll: number
  priceAllWithoutVat: number
  picked: boolean
  packed: boolean
  serial: string
  note: string
  @PropType('Order')
  order?: Order
  @PropType('Product')
  product?: Product
  @PropType('Vat')
  vat?: Vat
  isForSubsequentSettlement: boolean
  type: string
  quantityUnavailable: number
  @PropType('ProductMove')
  moves: ProductMove[]
  @PropType('ClearanceItem')
  clearanceItems: ClearanceItem[]
  @PropType('local')
  batch: string // for batch imports from file -> ProductMove.expirationDate . local only!
  @PropType('local')
  expirationDate: string // for batch imports from file -> ProductMove.expirationDate . local only!
  @PropType('local')
  position: string
  @PropType('local')
  purchaseCurrency: string
  @PropType('local')
  purchaseCurrencyDate: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.uuid = data.uuid && data.uuid.length > 0 ? data.uuid : generateUuidV4()
    this.name = data.name || ''
    this.ean = data.ean || ''
    this.code = data.code || ''
    this.quantity = data.quantity || 0
    this.sale = data.sale || null
    this.price = data.price || 0
    this.priceWithoutVat = data.priceWithoutVat || 0
    this.priceAll = data.priceAll || 0
    this.priceAllWithoutVat = data.priceAllWithoutVat || 0
    this.picked = data.picked || false
    this.packed = data.packed || false
    this.serial = data.serial || ''
    this.note = data.note || ''
    this.order = data.order ? new Order(data.order) : undefined
    this.product = data.product ? new Product(data.product) : undefined
    this.vat = data.vat ? new Vat(data.vat) : undefined
    this.isForSubsequentSettlement = data.isForSubsequentSettlement || false
    this.type = data.type || 'product'
    this.quantityUnavailable = data.quantityUnavailable !== 0 ? data.quantityUnavailable : 0
    this.moves = data.moves && data.moves.length > 0 ? data.moves.map(m => new ProductMove(m)) : []
    this.clearanceItems =
      data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(c => new ClearanceItem(c)) : []
    this.batch = data.batch
    this.expirationDate = data.expirationDate
    this.position = data.position
    this.purchaseCurrency = data.purchaseCurrency
    this.purchaseCurrencyDate = data.purchaseCurrencyDate
  }
}
