export class PackageStatus {
  public id: number
  public code: string
  public text: string
  public created: string

  public constructor(data: any = {}) {
    this.id = data.id || 0
    this.code = data.code || ''
    this.text = data.text || ''
    this.created = data.created || null
  }
}
