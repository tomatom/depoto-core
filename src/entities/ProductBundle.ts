import { Product } from './'
import { generateUuidV4, PropType } from '../utils'

export class ProductBundle {
  id: number
  @PropType('local')
  uuid: string
  @PropType('Product')
  parent?: Product
  @PropType('Product')
  child?: Product
  quantity: number

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.uuid = data.uuid && data.uuid.length > 0 ? data.uuid : generateUuidV4()
    this.parent = data.parent && data.parent.id > 0 ? new Product(data.parent) : undefined
    this.child = data.child && data.child.id > 0 ? new Product(data.child) : undefined
    this.quantity = data.quantity !== 0 ? data.quantity : 0
  }
}
