import { Checkout, Company, Customer, UserGroup } from './'
import { PropType } from '../utils'

export class User {
  id: number
  email: string
  firstName: string
  lastName: string
  name: string
  @PropType('local')
  plainPassword: string // local helper
  phone: string
  @PropType('UserGroup')
  groups: UserGroup[]
  roles: string[]
  pin: string
  @PropType('Company')
  company: Company
  enabled: boolean
  @PropType('local')
  avatar_url: string
  externalId?: string
  @PropType('Customer')
  customers: Customer[]
  @PropType('Checkout')
  checkouts: Checkout[]

  constructor(data: any = {}) {
    this.id = data.id > 0 ? data.id : data.id === 0 ? 0 : null
    this.email = data.email || ''
    this.firstName = data.firstName || ''
    this.lastName = data.lastName || ''
    this.name = data.name || ''
    this.plainPassword = data.plainPassword || ''
    this.phone = data.phone || ''
    this.groups = data.groups || []
    this.roles =
      !!data.roles && data.roles.length > 0
        ? data.roles
        : !!data.groups && data.groups.length > 0
        ? data.groups.flatMap(g => g.roles)
        : []
    this.pin = data.pin || ''
    this.company = data.company || null
    this.avatar_url = data.avatar_url || '' // 'assets/img/default-user.png'
    this.externalId = data.externalId
    this.enabled = data.enabled || true
    this.customers =
      data.customers && data.customers.length > 0 ? data.customers.map(customer => new Customer(customer)) : []
    this.checkouts =
      data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(checkout => new Checkout(checkout)) : []
  }
}
