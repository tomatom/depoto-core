export class Tag {
  public id: number
  public name: string
  public type: string
  public color: string
  public externalId: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || ''
    this.type = data.type || ''
    this.color = data.color || ''
    this.externalId = data.externalId || ''
  }
}
