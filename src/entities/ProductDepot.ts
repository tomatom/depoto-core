import { Depot, Supplier, Product } from './'
import { PropType } from '../utils'

export class ProductDepot {
  id: number
  @PropType('Depot')
  depot?: Depot
  @PropType('Product')
  product?: Product
  @PropType('Supplier')
  supplier?: Supplier
  purchasePrice: number
  quantityStock: number
  quantityReservation: number
  quantityAvailable: number
  quantityUnavailable: number
  inventoryQuantityStock: number
  batch: string
  expirationDate: string
  position: string // combined positions for easy show in template
  position1: string
  position2: string
  position3: string
  created: string
  updated: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.depot = new Depot(data.depot) || undefined
    this.supplier = new Supplier(data.supplier) || undefined
    this.product = new Product(data.product) || undefined
    this.purchasePrice = data.purchasePrice !== 0 ? data.purchasePrice : 0
    this.quantityStock = data.quantityStock !== 0 ? data.quantityStock : 0
    this.quantityReservation = data.quantityReservation !== 0 ? data.quantityReservation : 0
    this.quantityAvailable = data.quantityAvailable !== 0 ? data.quantityAvailable : 0
    this.quantityUnavailable = data.quantityUnavailable !== 0 ? data.quantityUnavailable : 0
    this.inventoryQuantityStock = data.inventoryQuantityStock !== 0 ? data.inventoryQuantityStock : 0
    this.batch = data.batch ? data.batch : null
    this.expirationDate = data.expirationDate ? data.expirationDate : null
    this.position = data.position ? data.position : null
    this.position1 = data.position1 ? data.position1 : null
    this.position2 = data.position2 ? data.position2 : null
    this.position3 = data.position3 ? data.position3 : null
    this.created = data.created ? data.created : null
    this.updated = data.updated ? data.updated : null
  }
}
