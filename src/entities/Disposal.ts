import { Carrier, Package } from './'
import { PropType } from '../utils'

export class Disposal {
  id: number
  code: string
  @PropType('Carrier')
  carrier?: Carrier
  @PropType('Package')
  packages: Package[]
  ticketUrl: string
  sent: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.code = data.code || ''
    this.carrier = data.carrier ? new Carrier(data.carrier) : undefined
    this.packages =
      data.packages && data.packages.length > 0
        ? data.packages.map(p => {
            p.carrier = this.carrier
            return new Package(p)
          })
        : []
    this.ticketUrl = data.ticketUrl || ''
    this.sent = data.sent || ''
  }
}
