import { Company } from './Company'
import { Carrier } from './Carrier'
import { Package } from './Package'
import { File } from './File'

export class PackageDisposal {
  id: number
  code?: string
  company?: Company
  carrier?: Carrier
  packages: [Package]
  ticketUrl?: string
  sent?: string
  files: [File]

  constructor(data: any = {}) {
    this.id = data.id || undefined
    this.code = data.code || undefined
    this.company = data.company ? new Company(data.company) : undefined
    this.carrier = data.carrier ? new Carrier(data.carrier) : undefined
    this.packages = data.packages?.length > 0 ? data.packages.map(p => new Package(p)) : []
    this.ticketUrl = data.ticketUrl || undefined
    this.sent = data.sent || undefined
    this.files = data.files?.length > 0 ? data.files.map(f => new File(f)) : []
  }
}
