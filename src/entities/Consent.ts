export class Consent {
  id: number
  name: string
  body: string
  externalId: string
  created: string
  updated: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || ''
    this.body = data.body || ''
    this.externalId = data.externalId || ''
    this.created = data.created || ''
    this.updated = data.updated || ''
  }
}
