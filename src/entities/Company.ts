import { CompanyCarrier } from './'
import { PropType } from '../utils'

export class Company {
  id: number
  name: string
  ic: string
  dic: string
  email: string
  phone: string
  street: string
  city: string
  zip: string
  country: string
  registrationNote: string
  nextEan: number
  billLogo: string
  @PropType('Company')
  parent?: Company
  @PropType('Company')
  children: Company[]
  @PropType('CompanyCarrier')
  carrierRelations: CompanyCarrier[]
  defaultVoucherValidity: number

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || ''
    this.ic = data.ic || ''
    this.dic = data.dic || ''
    this.email = data.email || ''
    this.phone = data.phone || ''
    this.street = data.street || ''
    this.city = data.city || ''
    this.zip = data.zip || ''
    this.country = data.country || ''
    this.registrationNote = data.registrationNote || ''
    this.nextEan = data.nextEan || 0
    this.billLogo = data.billLogo || ''
    this.parent = data.parent && data.parent.id > 0 ? new Company(data.parent) : undefined
    this.children = data.children && data.children.length > 0 ? data.children.map(c => new Company(c)) : []
    this.carrierRelations =
      data.carrierRelations && data.carrierRelations.length > 0
        ? data.carrierRelations.map(c => new CompanyCarrier(c))
        : []
    this.defaultVoucherValidity = data.defaultVoucherValidity || 0
  }
}
