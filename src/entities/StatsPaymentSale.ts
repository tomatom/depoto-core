import { Payment, Price } from './'
import { PropType } from '../utils'

export class StatsPaymentSale {
  amount: number
  @PropType('Price')
  sales: Price[]
  @PropType('Payment')
  payment?: Payment

  constructor(data: any = {}) {
    this.amount = data.amount ? data.amount : 0
    this.sales = data.sales && data.sales.length > 0 ? data.sales.map(s => new Price(s)) : []
    this.payment = data.payment ? new Payment(data.payment) : undefined
  }
}
