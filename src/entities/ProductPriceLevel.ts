import { Product } from './Product'
import { PropType } from '../utils'
import { PriceLevel } from './PriceLevel'

export class ProductPriceLevel {
  id: number
  sellPrice: number
  externalId: string
  @PropType('Product')
  product: Product
  @PropType('PriceLevel')
  priceLevel: PriceLevel

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.sellPrice = data.sellPrice || null
    this.externalId = data.externalId || ''
    this.product = data.product ? new Product(data.product) : new Product()
    this.priceLevel = data.priceLevel ? new PriceLevel(data.priceLevel) : new PriceLevel()
  }
}
