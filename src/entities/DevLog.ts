export class DevLog {
  id: number
  name: string
  content: string
  created: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || null
    this.content = data.content || null
    this.created = data.created || null
  }

  public static get names(): any {
    return {
      error: 'client-error',
      exception: 'client-exception',
      orderRepeated: 'client-order-repeated',
    }
  }
}
