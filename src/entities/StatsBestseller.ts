import { Price, Product } from './'
import { PropType } from '../utils'

export class StatsBestseller {
  amount: number
  @PropType('Price')
  sales: Price[]
  @PropType('Product')
  product?: Product

  constructor(data: any = {}) {
    this.amount = data.amount ? data.amount : 0
    this.sales = data.sales && data.sales.length > 0 ? data.sales.map(s => new Price(s)) : []
    this.product = data.product ? new Product(data.product) : undefined
  }
}
