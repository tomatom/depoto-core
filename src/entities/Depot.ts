export class Depot {
  id: number
  name: string
  emailIn?: string
  unavailablesUrl: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
    this.emailIn = data.emailIn || ''
    this.unavailablesUrl = data.unavailablesUrl || ''
  }
}
