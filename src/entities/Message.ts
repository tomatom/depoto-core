import { User } from './'
import { PropType } from '../utils'

export class Message {
  content: string
  title: string
  @PropType('User')
  author?: User
  @PropType('User')
  destination?: User
  date: string

  constructor(data: any = {}) {
    this.content = data.content || ''
    this.title = data.title || ''
    this.author = data.author
    this.destination = data.destination
    this.date = data.date || Date.now() + ''
  }
}
