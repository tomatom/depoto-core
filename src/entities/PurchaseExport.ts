import { Depot, Checkout, Producer, Supplier } from './'
import { PropType } from '../utils'

export class PurchaseExport {
  id: number
  generated: string
  created: string
  url: string
  format: string
  dateFrom: string
  dateTo: string
  @PropType('Depot')
  depots: Depot[]
  @PropType('Checkout')
  checkouts: Checkout[]
  @PropType('Supplier')
  suppliers: Supplier[]
  @PropType('Producer')
  producers: Producer[]

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.generated = data.generated || ''
    this.created = data.created || new Date().toUTCString()
    this.url = data.url || ''
    this.format = data.format || ''
    this.dateFrom = data.dateFrom || ''
    this.dateTo = data.dateTo || ''
    this.depots = data.depots && data.depots.length > 0 ? data.depots.map(depot => new Depot(depot)) : []
    this.checkouts =
      data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(checkout => new Checkout(checkout)) : []
    this.suppliers =
      data.suppliers && data.suppliers.length > 0 ? data.suppliers.map(supplier => new Supplier(supplier)) : []
    this.producers =
      data.producers && data.producers.length > 0 ? data.producers.map(producer => new Producer(producer)) : []
  }
}
