import { Company } from './'
import { PropType } from '../utils'

export class Parameter {
  id: number
  name: string
  type: string
  enumValues: string[]
  unit: string
  text: string
  externalId: string
  @PropType('Company')
  company: Company

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || ''
    this.type = data.type || 'text' // todo default?
    this.enumValues = Array.isArray(data.enumValues) && data.enumValues.length > 0 ? data.enumValues : []
    this.unit = data.unit || null
    this.text = data.text || ''
    this.externalId = data.externalId || null
    this.company = data.company ? new Company(data.company) : new Company()
  }
}
