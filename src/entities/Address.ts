export class Address {
  id: number
  companyName: string
  firstName: string
  lastName: string
  street: string
  city: string
  zip: string
  country: string
  state: string
  ic: string
  dic: string
  phone: string
  email: string
  branchId: string
  externalId: string
  isStored: boolean
  isBilling: boolean
  note: string
  customData: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.companyName = data.companyName || ''
    this.firstName = data.firstName || ''
    this.lastName = data.lastName || ''
    this.street = data.street || ''
    this.city = data.city || ''
    this.zip = data.zip || ''
    this.country = data.country || ''
    this.state = data.state || ''
    this.ic = data.ic || ''
    this.dic = data.dic || ''
    this.phone = data.phone || ''
    this.email = data.email || ''
    this.branchId = data.branchId || ''
    this.externalId = data.externalId || ''
    this.isStored = data.isStored || false
    this.isBilling = data.isBilling || false
    this.note = data.note || ''
    this.customData = data.customData || ''
  }
}
