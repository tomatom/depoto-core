import { Carrier, ClearanceItem, Disposal, Order, OrderItem, PackageStatus } from './'
import { PropType } from '../utils'

export class Package {
  id: number
  code: string
  @PropType('Carrier')
  carrier?: Carrier
  @PropType('Order')
  order?: Order
  @PropType('Disposal')
  disposal?: Disposal // parent disposal
  @PropType('OrderItem')
  items: OrderItem[]
  @PropType('ClearanceItem')
  clearanceItems: ClearanceItem[]
  @PropType('PackageStatus')
  statuses: PackageStatus[]
  ticketUrl: string
  sent: string
  weight?: number | null
  weightRequired: boolean
  dimensionsRequired: boolean
  dimensionX?: number | null
  dimensionY?: number | null
  dimensionZ?: number | null

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.code = data.code || ''
    this.carrier = data.carrier ? new Carrier(data.carrier) : undefined
    this.items = data.items && data.items.length > 0 ? data.items.map(i => new OrderItem(i)) : []
    this.clearanceItems =
      data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(i => new ClearanceItem(i)) : []
    this.statuses = data.statuses && data.statuses.length > 0 ? data.statuses.map(i => new PackageStatus(i)) : []
    this.order = data.order && data.order.id > 0 ? new Order(data.order) : undefined
    this.disposal = data.disposal && data.disposal.id > 0 ? new Disposal(data.disposal) : undefined
    this.ticketUrl = data.ticketUrl || ''
    this.sent = data.sent || null
    this.weight = data.weight || undefined
    this.weightRequired = data.weightRequired || false
    this.dimensionsRequired = data.dimensionsRequired || false
    this.dimensionX = data.dimensionX || undefined
    this.dimensionY = data.dimensionY || undefined
    this.dimensionZ = data.dimensionZ || undefined
  }
}
