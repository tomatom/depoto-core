export class Producer {
  public id: number
  public name: string

  public constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
  }
}
