export enum ProcessStatusType {
  RECEIVED = 'received',
  PICKING = 'picking',
  PACKING = 'packing',
  PACKED = 'packed',
  DISPATCHED = 'dispatched',
  DELIVERED = 'delivered',
  RETURNED = 'returned',
  PICKING_ERROR = 'picking_error',
  CANCELLED = 'cancelled',
}

export class ProcessStatus {
  id: ProcessStatusType | string
  name: string
  position: number

  constructor(data: any = {}) {
    this.id = data.id || ProcessStatusType.RECEIVED
    this.name = data.name || ''
    this.position = data.position || 0
  }
}
