import { Company, PaymentType } from './'
import { PropType } from '../utils'

export class Payment {
  id: number
  name: string
  @PropType('PaymentType')
  type: PaymentType
  @PropType('Company')
  company: Company
  eetEnable: boolean

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || ''
    this.type = data.type ? new PaymentType(data.type) : new PaymentType()
    this.company = data.company ? new Company(data.company) : new Company()
    this.eetEnable = data.eetEnable || false
  }
}
