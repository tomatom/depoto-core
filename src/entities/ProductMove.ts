import { Depot, OrderItem, Product, ProductDepot, ProductMovePack, User } from './'
import { PropType, generateUuidV4 } from '../utils'

// todo cleanup this mess, keep only props on api (need refactor of big part of client app)
export class ProductMove {
  id: number
  @PropType('local')
  uuid: string
  @PropType('ProductDepot')
  productDepotFrom: ProductDepot
  @PropType('ProductDepot')
  productDepotTo: ProductDepot
  @PropType('local')
  depotFromObj: Depot
  @PropType('local')
  productDepot: number
  @PropType('local')
  depotFrom: number
  productDepotFromQuantityStock: number
  @PropType('local')
  depotToObj: Depot
  @PropType('local')
  depotTo: number
  productDepotToQuantityStock: number
  @PropType('local')
  supplier: number // todo?
  amount: number
  @PropType('local')
  purchasePrice: number
  @PropType('local')
  purchasePriceSum: number
  @PropType('local')
  sellPrice: number
  @PropType('local')
  sellPriceSum: number
  note?: string
  @PropType('local')
  product?: Product
  @PropType('ProductMovePack')
  pack?: ProductMovePack
  created: string
  updated: string
  deleted: string
  @PropType('local')
  batch: string
  @PropType('local')
  expirationDate: string
  @PropType('local')
  position: string
  @PropType('local')
  position1: string
  @PropType('local')
  position2: string
  @PropType('local')
  position3: string
  @PropType('local')
  isEANPrintable: boolean // helper, just local - not on API
  @PropType('User')
  createdBy?: User
  @PropType('OrderItem')
  orderItem?: OrderItem
  @PropType('local')
  purchaseCurrencyDate: string
  @PropType('local')
  purchaseCurrency: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.uuid = data.uuid || generateUuidV4()
    this.productDepotFrom = data.productDepotFrom ? data.productDepotFrom : null
    this.productDepotTo = data.productDepotTo ? data.productDepotTo : null
    this.depotFromObj =
      data.productDepotFrom && data.productDepotFrom.depot && data.productDepotFrom.depot.id > 0
        ? data.productDepotFrom.depot
        : null
    this.productDepot = data.productDepot ? data.productDepot : null
    this.depotFrom =
      data.productDepotFrom && data.productDepotFrom.id > 0
        ? data.productDepotFrom.id
        : data.depotFrom
        ? data.depotFrom
        : null
    this.productDepotFromQuantityStock = data.productDepotFromQuantityStock > 0 ? data.productDepotFromQuantityStock : 0
    this.depotToObj =
      data.productDepotTo && data.productDepotTo.depot && data.productDepotTo.depot.id > 0
        ? data.productDepotTo.depot
        : null
    this.depotTo =
      data.productDepotTo && data.productDepotTo.id > 0 ? data.productDepotTo.id : data.depotTo ? data.depotTo : null
    this.productDepotToQuantityStock = data.productDepotToQuantityStock > 0 ? data.productDepotToQuantityStock : 0
    this.supplier = data.supplier || null
    this.amount = data.amount || null
    this.purchasePrice = data.purchasePrice
      ? data.purchasePrice
      : data.productDepotFrom
      ? data.productDepotFrom.purchasePrice
      : data.productDepotTo
      ? data.productDepotTo.purchasePrice
      : null
    this.purchasePriceSum = data.purchasePriceSum
      ? data.purchasePriceSum
      : data.productDepotFrom
      ? data.productDepotFrom.purchasePriceSum
      : data.productDepotTo
      ? data.productDepotTo.purchasePriceSum
      : null
    this.sellPrice = data.sellPrice || null
    this.sellPriceSum = data.sellPriceSum || null
    this.note = data.note ? decodeURIComponent(data.note) : undefined // TODO decode here?
    this.product = new Product(data.product) || null
    this.pack = data.pack ? new ProductMovePack(data.pack) : undefined
    this.created = data.created || null
    this.updated = data.updated || null
    this.deleted = data.deleted || null
    this.batch =
      data.productDepotFrom && data.productDepotFrom.id > 0
        ? data.productDepotFrom.batch
        : data.productDepotTo && data.productDepotTo.id > 0
        ? data.productDepotTo.batch
        : data.batch || null
    this.expirationDate =
      data.productDepotFrom && data.productDepotFrom.id > 0
        ? data.productDepotFrom.expirationDate
        : data.productDepotTo && data.productDepotTo.id > 0
        ? data.productDepotTo.expirationDate
        : data.expirationDate || null
    this.position =
      data.productDepotFrom && data.productDepotFrom.id > 0
        ? data.productDepotFrom.position
        : data.productDepotTo && data.productDepotTo.id > 0
        ? data.productDepotTo.position
        : data.position || null
    this.position1 =
      data.productDepotFrom && data.productDepotFrom.id > 0
        ? data.productDepotFrom.position1
        : data.productDepotTo && data.productDepotTo.id > 0
        ? data.productDepotTo.position1
        : data.position1 || null
    this.position2 =
      data.productDepotFrom && data.productDepotFrom.id > 0
        ? data.productDepotFrom.position2
        : data.productDepotTo && data.productDepotTo.id > 0
        ? data.productDepotTo.position2
        : data.position2 || null
    this.position3 =
      data.productDepotFrom && data.productDepotFrom.id > 0
        ? data.productDepotFrom.position3
        : data.productDepotTo && data.productDepotTo.id > 0
        ? data.productDepotTo.position3
        : data.position3 || null
    this.isEANPrintable = data.isEANPrintable || true
    this.createdBy = data.createdBy && data.createdBy.id > 0 ? new User(data.createdBy) : undefined
    this.orderItem = data.orderItem && data.orderItem.id > 0 ? new OrderItem(data.orderItem) : undefined
    this.purchaseCurrencyDate = data.purchaseCurrencyDate ? data.purchaseCurrencyDate : null
    this.purchaseCurrency = data.purchaseCurrency ? data.purchaseCurrency : 'CZK'
  }
}
