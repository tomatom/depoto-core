import { Carrier } from './'
import { PropType } from '../utils'

export class StatsCarrierUsage {
  amount: number
  @PropType('Carrier')
  carrier?: Carrier

  constructor(data: any = {}) {
    this.amount = data.amount ? data.amount : 0
    this.carrier = data.carrier ? new Carrier(data.carrier) : undefined
  }
}
