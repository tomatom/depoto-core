import { Depot, ProductDepot } from './'
import { PropType } from '../utils'

export class ProductPurchasePrice {
  @PropType('local') // todo ?
  id: number
  @PropType('Depot')
  productDepot?: Depot // TODO: this is, in fact Depot on api!!!
  // productDepot: ProductDepot // TODO: this is, in fact Depot on api!!!
  quantity: number
  purchasePrice: number

  constructor(data: any = {}) {
    this.id = data.id || null
    this.productDepot = data.productDepot && data.productDepot.id > 0 ? new Depot(data.productDepot) : undefined // TODO Depot -> ProductDepot after api fix
    this.quantity = data.quantity || 0
    this.purchasePrice = data.purchasePrice || 0
  }
}
