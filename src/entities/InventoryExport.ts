import { Depot, File, Producer, Supplier, ProductMovePack } from './'
import { PropType } from '../utils'

export class InventoryExport {
  id: number
  date: string
  generated: string
  created: string
  url: string
  @PropType('local')
  format = 'excel' // not on api
  @PropType('File')
  files: File[]
  @PropType('Depot')
  depots: Depot[]
  @PropType('Supplier')
  suppliers: Supplier[]
  @PropType('Producer')
  producers: Producer[]
  approved: string
  rejected: string
  finished: string
  @PropType('ProductMovePack')
  pmpIn?: ProductMovePack
  @PropType('ProductMovePack')
  pmpOut?: ProductMovePack

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.date = data.date || ''
    this.generated = data.generated || ''
    this.created = data.created || new Date().toUTCString()
    this.url = data.url || ''
    // this.format = data.format || ''
    this.files = data.files && data.files.length > 0 ? data.files.map(f => new File(f)) : []
    this.depots = data.depots && data.depots.length > 0 ? data.depots.map(depot => new Depot(depot)) : []
    this.suppliers =
      data.suppliers && data.suppliers.length > 0 ? data.suppliers.map(supplier => new Supplier(supplier)) : []
    this.producers =
      data.producers && data.producers.length > 0 ? data.producers.map(producer => new Producer(producer)) : []
    this.approved = data.approved
    this.rejected = data.rejected
    this.finished = data.finished
    this.pmpIn = data.pmpIn ? new ProductMovePack(data.pmpIn) : undefined
    this.pmpOut = data.pmpOut ? new ProductMovePack(data.pmpOut) : undefined
  }
}
