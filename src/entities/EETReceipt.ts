export class EETReceipt {
  id: number
  dic: string
  checkoutEetId: number
  shopEetId: number
  playground: boolean
  verificationMode: boolean
  number: number
  dateCreated: string
  totalPrice: number
  fik: string
  bkp: string
  pkp: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.dic = data.dic || null
    this.checkoutEetId = data.checkoutEetId || null
    this.shopEetId = data.shopEetId || null
    this.playground = data.playground || false
    this.verificationMode = data.verificationMode || false
    this.number = data.number || null
    this.dateCreated = data.dateCreated || null
    this.totalPrice = data.totalPrice || 0
    this.fik = data.fik || null
    this.bkp = data.bkp || null
    this.pkp = data.pkp || null
  }
}
