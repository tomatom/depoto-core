import { Checkout, OrderStatus } from './'
import { PropType } from '../utils'

export class OrderExport {
  id: number
  generated: string
  created: string
  url: string
  format: string
  dateFrom: string
  dateTo: string
  @PropType('Checkout')
  checkouts: Checkout[]
  @PropType('OrderStatus')
  status?: OrderStatus

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.generated = data.generated || ''
    this.created = data.created || new Date().toUTCString()
    this.url = data.url || ''
    this.format = data.format || ''
    this.dateFrom = data.dateFrom || ''
    this.dateTo = data.dateTo || ''
    this.checkouts =
      data.checkouts && data.checkouts.length > 0 ? data.checkouts.map(checkout => new Checkout(checkout)) : []
    this.status = data.status ? new OrderStatus(data.status) : undefined
  }
}
