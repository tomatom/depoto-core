export class LoadProduct {
  id: number
  depot: number
  supplier: number
  amount: number
  purchasePrice: number
  note: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.depot = data.depot || null
    this.supplier = data.supplier || null
    this.amount = data.amount || null
    this.purchasePrice = data.purchasePrice || null
    this.note = data.note || null
  }
}
