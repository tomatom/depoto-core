import { Consent, Customer } from './'
import { PropType } from '../utils'

export class CustomerConsent {
  id: number
  @PropType('Customer')
  customer?: Customer
  @PropType('Consent')
  consent?: Consent
  name: string
  body: string
  externalId: string
  created: string
  updated: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.customer = data.customer ? new Customer(data.customer) : undefined
    this.consent = data.consent ? new Consent(data.consent) : undefined
    this.name = data.name || ''
    this.body = data.body || ''
    this.externalId = data.externalId || ''
    this.created = data.created || ''
    this.updated = data.updated || ''
  }
}
