import { Product } from './Product'
import { PropType } from '../utils'

export class Voucher {
  id: number
  @PropType('Product')
  product?: Product
  name: string
  code: string
  externalId: string
  discountType: string
  discountPercent: number
  discountValue: number
  maxUse: number
  validFrom: string
  validTo: string
  isValid: boolean
  isPayment: boolean
  enabled: boolean
  used: number
  @PropType('Product')
  paymentItems: Product[]
  @PropType('Product')
  orderItems: Product[]

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.product = data.product ? new Product(data.product) : undefined
    this.name = data.name || ''
    this.code = data.code || ''
    this.externalId = data.externalId || ''
    this.discountType = data.discountType || ''
    this.discountPercent = data.discountPercent || 0
    this.discountValue = data.discountValue || 0
    this.maxUse = data.maxUse || 0
    this.validFrom = data.validFrom || ''
    this.validTo = data.validTo || ''
    this.isValid = data.isValid || false
    this.isPayment = data.isPayment || false
    this.enabled = data.enabled || false
    this.used = data.user || 0
    this.paymentItems =
      data.paymentItems && data.paymentItems.length > 0 ? data.paymentItems.map(item => new Product(item)) : []
    this.orderItems =
      data.orderItems && data.orderItems.length > 0 ? data.orderItems.map(item => new Product(item)) : []
  }
}
