import {
  Address,
  Carrier,
  Checkout,
  ClearanceItem,
  Company,
  EETReceipt,
  File,
  Currency,
  Customer,
  OrderGroup,
  OrderItem,
  OrderProcessStatus,
  OrderStatus,
  Tag,
  Package,
  PaymentItem,
  ProcessStatus,
  ProductMovePack,
  User,
  VatAllocation,
} from './'
import { PropType } from '../utils'

export class Order {
  id: number
  @PropType('OrderStatus')
  status: OrderStatus
  reservationNumber: number
  billNumber: number
  dateCreated: string
  @PropType('OrderItem')
  items: OrderItem[]
  @PropType('PaymentItem')
  paymentItems: PaymentItem[]
  @PropType('Checkout')
  checkout?: Checkout
  @PropType('Customer')
  customer?: Customer
  @PropType('Currency')
  currency?: Currency
  note: string
  privateNote: string
  @PropType('Address')
  invoiceAddress?: Address
  @PropType('Address')
  shippingAddress?: Address
  @PropType('Order')
  relatedParent?: Order
  rounding: number
  @PropType('EETReceipt')
  eetReceipt?: EETReceipt
  billUrl: string
  @PropType('VatAllocation')
  vatAllocations: VatAllocation[]
  @PropType('ProcessStatus')
  processStatus?: ProcessStatus
  processStatusUpdated: string
  @PropType('OrderProcessStatus')
  processStatusRelation?: OrderProcessStatus
  @PropType('OrderProcessStatus')
  processStatusRelations: OrderProcessStatus[]
  @PropType('Company')
  company?: Company
  @PropType('Carrier')
  carrier?: Carrier
  @PropType('Package')
  packages: Package[]
  boxes: string[]
  isPaid: boolean
  @PropType('File')
  files: File[]
  quantityUnavailable: number
  vs: number
  @PropType('User')
  createdBy?: User
  @PropType('ProductMovePack')
  movePacks: ProductMovePack[]
  isEditable: boolean
  externalId: string
  @PropType('OrderGroup')
  group?: OrderGroup
  groupPosition: number
  @PropType('ClearanceItem')
  clearanceItems: ClearanceItem[]
  priceAll: number
  @PropType('local')
  isSelected: boolean
  @PropType('Tag')
  tags: Tag[]
  priority: number
  dateExpedition: string
  dateDue: string
  dateTax: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.status = !!data.status ? data.status : new OrderStatus({ id: 0, status: 'reservation' })
    this.reservationNumber = data.reservationNumber || null
    this.billNumber = data.billNumber || null
    this.dateCreated = data.dateCreated || ''
    this.items = data.items && data.items.length > 0 ? data.items.map(item => new OrderItem(item)) : []
    this.paymentItems =
      data.paymentItems && data.paymentItems.length > 0 ? data.paymentItems.map(item => new PaymentItem(item)) : []
    this.checkout = data.checkout ? new Checkout(data.checkout) : undefined
    this.customer = data.customer ? new Customer(data.customer) : undefined
    this.currency = data.currency ? new Currency(data.currency) : undefined
    this.note = data.note || ''
    this.privateNote = data.privateNote || ''
    this.invoiceAddress = data.invoiceAddress ? new Address(data.invoiceAddress) : undefined
    this.shippingAddress = data.shippingAddress ? new Address(data.shippingAddress) : undefined
    this.relatedParent = data.relatedParent ? new Order(data.relatedParent) : undefined
    this.rounding = data.rounding || null
    this.eetReceipt = data.eetReceipt ? new EETReceipt(data.eetReceipt) : undefined
    this.billUrl = data.billUrl || ''
    this.vatAllocations =
      data.vatAllocations && data.vatAllocations.length
        ? data.vatAllocations
            .map(v => new VatAllocation(v))
            .filter(v => v.price > 0)
            .sort((a: any, b: any) => {
              if (!a.vat || (a.vat && b.vat && a.vat.percent > b.vat.percent)) {
                return 1
              } else if (!b.vat || (a.vat && b.vat && a.vat.percent < b.vat.percent)) {
                return -1
              } else {
                return 0
              }
            })
        : []
    this.processStatus = data.processStatus ? new ProcessStatus(data.processStatus) : undefined
    this.processStatusUpdated = data.processStatusUpdated ? data.processStatusUpdated : undefined
    this.processStatusRelation = data.processStatusRelation
      ? new OrderProcessStatus(data.processStatusRelation)
      : undefined
    this.processStatusRelations =
      data.processStatusRelations && data.processStatusRelations.length > 0
        ? data.processStatusRelations.map(r => new OrderProcessStatus(r))
        : []
    this.company = data.company && data.company.name ? new Company(data.company) : undefined
    this.carrier = data.carrier && data.carrier.name ? new Carrier(data.carrier) : undefined
    this.packages =
      data.packages && data.packages.length > 0
        ? data.packages.map(p => {
            p.order = {
              id: this.id,
              billNumber: this.billNumber,
              reservationNumber: this.reservationNumber,
            } // todo get from api...
            p.carrier = this.carrier
            return new Package(p)
          })
        : []
    this.quantityUnavailable = data.quantityUnavailable !== 0 ? data.quantityUnavailable : 0
    this.boxes = data.boxes && data.boxes.length > 0 ? data.boxes : []
    this.isPaid = data.isPaid || false
    this.files = data.files && data.files.length > 0 ? data.files.map(f => new File(f)) : []
    this.vs = data.vs !== 0 ? data.vs : null
    this.createdBy = data.createdBy && data.createdBy.id > 0 ? new User(data.createdBy) : undefined
    this.movePacks = data.movePacks && data.movePacks.length > 0 ? data.movePacks.map(p => new ProductMovePack(p)) : []
    this.isEditable = !!data.isEditable
    this.externalId = data.externalId || null
    this.group = !!data.group ? new OrderGroup(data.group) : undefined
    this.groupPosition = data.groupPosition || 0
    this.clearanceItems =
      data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(c => new ClearanceItem(c)) : []
    this.priceAll = data.priceAll || null
    this.isSelected = data.isSelected || false
    this.tags = data.tags && data.tags.length > 0 ? data.tags.map(t => new Tag(t)) : []
    this.priority = data.priority || 1
    this.dateExpedition = data.dateExpedition || null
    this.dateDue = data.dateDue
    this.dateTax = data.dateTax
  }
}
