import { Product } from './'
import { PropType } from '../utils'

export class SoldItem {
  name: string
  type: string
  ean: string
  code: string
  quantity: number
  priceAll: number
  priceAllWithoutVat: number
  @PropType('Product')
  product?: Product
  profit: number

  constructor(data: any = {}) {
    this.name = data.name || ''
    this.type = data.type || ''
    this.ean = data.ean || ''
    this.code = data.code || ''
    this.quantity = data.quantity || 0
    this.priceAll = data.priceAll || 0
    this.priceAllWithoutVat = data.priceAllWithoutVat || 0
    this.product = data.product ? new Product(data.product) : undefined
    this.profit = data.profit || 0
  }
}
