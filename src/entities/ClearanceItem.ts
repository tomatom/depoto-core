import { Product, ProductDepot, ProductMove } from './'
import { generateUuidV4, PropType } from '../utils'

export class ClearanceItemLocation {
  @PropType('local')
  uuid: string
  depot: number
  position1?: string
  position2?: string
  position3?: string
  expirationDate?: string
  batch?: string
  amount: number
  constructor(data: any = {}) {
    this.uuid = data.uuid || generateUuidV4()
    this.depot = data.depot || null
    this.position1 = data.position1 || null
    this.position2 = data.position2 || null
    this.position3 = data.position3 || null
    this.expirationDate = data.expirationDate || null
    this.batch = data.batch || null
    this.amount = data.amount || null
  }
}

export class ClearanceItem {
  id: string
  @PropType('Product')
  product?: Product
  amount: number
  quantityReservation: number
  quantityAvailable: number
  expirationDate: string
  batch: string
  position: string
  position1: string
  position2: string
  position3: string
  picked: number
  packed: number
  locked: boolean
  @PropType('ProductMove')
  productMoves: ProductMove[]
  @PropType('ProductDepot')
  productDepots: ProductDepot[]
  @PropType('local')
  locations: ClearanceItemLocation[]

  constructor(data: any = {}) {
    this.id = data.id || null
    this.product = data.product && data.product.id > 0 ? new Product(data.product) : undefined
    this.amount = data.amount || 0
    this.quantityReservation = data.quantityReservation || 0
    this.quantityAvailable = data.quantityAvailable || 0
    this.expirationDate = data.expirationDate || ''
    this.batch = data.batch || ''
    this.position = data.position || ''
    this.position1 = data.position1 || ''
    this.position2 = data.position2 || ''
    this.position3 = data.position3 || ''
    this.picked = data.picked || 0
    this.packed = data.packed || 0
    this.locked = data.locked ?? false
    this.productDepots =
      data.productDepots && data.productDepots.length ? data.productDepots.map(d => new ProductDepot(d)) : []
    this.productMoves =
      data.productMoves && data.productMoves.length ? data.productMoves.map(m => new ProductMove(m)) : []
    this.locations =
      data.locations && data.locations.length ? data.locations.map(l => new ClearanceItemLocation(l)) : []
  }
}
