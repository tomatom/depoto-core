import { Product } from './'
import { PropType } from '../utils'

export class ProductPack {
  id: number
  ean: string
  code: string
  quantity: number
  externalId: string
  @PropType('Product')
  product?: Product

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.ean = data.ean || ''
    this.code = data.code || ''
    this.quantity = data.quantity || 0
    this.externalId = data.externalId || ''
    this.product = data.product && data.product.id > 0 ? new Product(data.product) : undefined
  }
}
