import { PropType } from '../utils'
import { Tag } from './Tag'

export class Category {
  id: number
  externalId: string
  name: string
  text: string
  @PropType('Category')
  parent?: Category
  @PropType('Category')
  children: Category[]
  hasChildren: boolean
  position: number
  treePath: number[]
  tags: Tag[]

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.externalId = data.externalId || ''
    this.name = data.name || ''
    this.text = data.text || ''
    this.parent = data.parent ? new Category(data.parent) : undefined
    this.children =
      data.children && data.children.length > 0 ? data.children.map(category => new Category(category)) : []
    this.hasChildren = data.hasChildren || null
    this.position = data.position || 0
    this.treePath = data.treePath || []
    this.tags = data.tags ? data.tags.map(tag => new Tag(tag)) : []
  }
}
