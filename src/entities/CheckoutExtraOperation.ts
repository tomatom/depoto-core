import { Checkout, User } from './'
import { PropType } from '../utils'

export class ExtraOperation {
  id: number
  @PropType('Checkout')
  checkout?: Checkout
  amount: number
  dateCreated: string
  note: string
  @PropType('User')
  user?: User

  constructor(data: any = {}) {
    this.id = data.id || null
    this.checkout = data.checkout ? new Checkout(data.checkout) : undefined
    this.amount = data.amount || 0
    this.dateCreated = data.dateCreated || 0
    this.note = data.note || ''
    this.user = data.user ? new User(data.user) : undefined
  }
}
