export class Vat {
  id: number
  name: string
  percent: number
  coefficient: number
  default: boolean

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
    this.percent = data.percent || 0
    this.coefficient = data.coefficient || 0
    this.default = data.default || false
  }
}
