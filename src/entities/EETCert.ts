import { PropType } from '../utils'

export class EETCert {
  id: number
  name: string
  @PropType('local') // in fact not local, but we never get this prop from api back (used on create)
  password: string
  @PropType('local') // in fact not local, but we never get this prop from api back (used on create)
  pkcs12: string
  publicKey: string
  expirationDate: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || null
    this.password = data.password || null
    this.pkcs12 = data.pkcs12 || null
    this.publicKey = data.publicKey || null
    this.expirationDate = data.expirationDate || null
  }
}
