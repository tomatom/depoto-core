import { CheckoutClosing, Depot, Order, Payment, User } from './'
import { PropType } from '../utils'

export class Checkout {
  id: number
  name: string
  amount: number
  nextBillNumber: number
  nextReservationNumber: number
  billFooter: string
  @PropType('Depot')
  depots: Depot[]
  @PropType('Depot')
  returnsDepot?: Depot
  @PropType('local')
  orders: Order[]
  @PropType('Payment')
  payments: Payment[]
  @PropType('CheckoutClosing')
  currentClosing?: CheckoutClosing
  eetId: number
  eetEnable: boolean
  eetPlayground: boolean
  eetVerificationMode: boolean
  negativeReservation: boolean
  eventUrl: string
  eventTypes: string[]
  @PropType('User')
  users: User[]

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
    this.amount = data.amount || 0
    this.nextBillNumber = data.nextBillNumber || 1
    this.nextReservationNumber = data.nextReservationNumber || 1
    this.billFooter = data.billFooter || ''
    this.depots = data.depots && data.depots.length > 0 ? data.depots.map(depot => new Depot(depot)) : []
    this.returnsDepot = data.returnsDepot && data.returnsDepot.id > 0 ? new Depot(data.returnsDepot) : undefined
    this.orders = data.orders && data.orders.length > 0 ? data.orders.map(order => new Order(order)) : []
    this.payments = data.payments && data.payments.length > 0 ? data.payments.map(payment => new Payment(payment)) : []
    this.currentClosing = data.currentClosing ? new CheckoutClosing(data.currentClosing) : undefined
    this.eetId = data.eetId || 0
    this.eetEnable = data.eetEnable || false
    this.eetPlayground = data.eetPlayground || false
    this.eetVerificationMode = data.eetVerificationMode || false
    this.negativeReservation = data.negativeReservation || false
    this.eventUrl = data.eventUrl || ''
    this.eventTypes = data.eventTypes || []
    this.users = data.users && data.users.length ? data.users.map(u => new User(u)) : []
  }
}
