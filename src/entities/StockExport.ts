import { Depot, Producer, Supplier } from './'
import { PropType } from '../utils'

export class StockExport {
  id: number
  date: string
  generated: string
  created: string
  url: string
  format: string
  @PropType('Depot')
  depots: Depot[]
  @PropType('Supplier')
  suppliers: Supplier[]
  @PropType('Producer')
  producers: Producer[]

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.date = data.date || ''
    this.generated = data.generated || ''
    this.created = data.created || new Date().toUTCString()
    this.url = data.url || ''
    this.format = data.format || ''
    this.depots = data.depots && data.depots.length > 0 ? data.depots.map(depot => new Depot(depot)) : []
    this.suppliers =
      data.suppliers && data.suppliers.length > 0 ? data.suppliers.map(supplier => new Supplier(supplier)) : []
    this.producers =
      data.producers && data.producers.length > 0 ? data.producers.map(producer => new Producer(producer)) : []
  }
}
