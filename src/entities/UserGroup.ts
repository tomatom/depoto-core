import { Company } from './'
import { PropType } from '../utils'

export class UserGroup {
  id: number
  name: string
  roles: string[]
  // @PropType('Company')
  @PropType('local')
  company: Company

  constructor(data: any = {}) {
    this.id = data.id || null
    this.name = data.name || ''
    this.roles = data.roles || []
    this.company = data.company || null
  }
}
