import { Address, CustomerConsent, User } from './'
import { PropType } from '../utils'
import { PriceLevel, Tag } from './'

export class Customer {
  id: number
  externalId: string
  email: string
  firstName: string
  lastName: string
  name: string
  companyName: string
  phone: string
  note: string
  birthday: string
  wholesale: boolean
  customData: string
  @PropType('Address')
  addresses: Address[]
  @PropType('CustomerConsent')
  consentRelations: CustomerConsent[]
  @PropType('User')
  users: User[]
  minExpirationDays: number
  @PropType('PriceLevel')
  priceLevel?: PriceLevel
  @PropType('Tag')
  tags: Tag[]

  constructor(data: any = {}) {
    this.id = data.id || null
    this.email = data.email || ''
    this.firstName = data.firstName || ''
    this.lastName = data.lastName || ''
    this.name = data.name || ''
    this.companyName = data.companyName || ''
    this.phone = data.phone || ''
    this.note = data.note || ''
    this.birthday = data.birthday || ''
    this.wholesale = data.wholesale || false
    this.addresses =
      data.addresses && data.addresses.length > 0 ? data.addresses.map(address => new Address(address)) : []
    this.consentRelations =
      data.consentRelations && data.consentRelations.length > 0
        ? data.consentRelations.map(c => new CustomerConsent(c))
        : []
    this.users = data.users && data.users.length > 0 ? data.users.map(user => new User(user)) : []
    this.minExpirationDays = data.minExpirationDays || 0
    this.priceLevel = data.priceLevel ? new PriceLevel(data.priceLevel) : undefined
    this.tags = data.tags && data.tags.length ? data.tags.map(t => new Tag(t)) : []
    this.companyName = data.companyName || ''
    this.externalId = data.externalId || ''
    this.customData = data.customData || ''
  }
}
