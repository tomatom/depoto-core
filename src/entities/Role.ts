export class Role {
  name: string
  role: string

  constructor(data: any = {}) {
    this.name = data.name || ''
    this.role = data.role || ''
  }
}
