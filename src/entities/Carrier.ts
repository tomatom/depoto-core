import { Tariff } from './'
import { PropType } from '../utils'

export class Carrier {
  id: string
  name: string
  color: string
  position: number
  @PropType('Tariff')
  tariffs: Tariff[]

  constructor(data: any = {}) {
    this.id = data.id || ''
    this.name = data.name || ''
    this.color = data.color || ''
    this.position = data.position || 0
    this.tariffs = data.tariffs && data.tariffs.length > 0 ? data.tariffs.map(t => new Tariff(t)) : []
  }
}
