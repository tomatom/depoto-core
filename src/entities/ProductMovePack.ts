import { Depot, File, Order, ProductMove, ProductMovePackPurpose, ProductMoveType, User } from './'
import { PropType } from '../utils'

export class ProductMovePack {
  id: number
  externalId: number
  dateCreated: string
  @PropType('local')
  dateOperation: string
  url: string
  @PropType('ProductMoveType')
  type?: ProductMoveType
  @PropType('ProductMovePackPurpose')
  purpose?: ProductMovePackPurpose
  note?: string
  number: number
  @PropType('Depot')
  depotFrom?: Depot
  @PropType('Depot')
  depotTo?: Depot
  @PropType('ProductMove')
  moves: ProductMove[]
  @PropType('Order')
  order?: Order
  purchasePrice: number
  @PropType('User')
  createdBy?: User
  @PropType('File')
  files: File[]

  constructor(data: any = {}) {
    this.id = data.id || null
    this.externalId = data.externalId || null
    this.dateCreated = data.dateCreated || new Date().toUTCString()
    this.dateOperation = data.dateOperation || null
    this.url = data.url || null
    this.type = data.type ? new ProductMoveType(data.type) : undefined
    this.purpose = new ProductMovePackPurpose(data.purpose)
    this.note = data.note ? decodeURIComponent(data.note) : undefined
    this.number = data.number || null
    this.depotFrom = data.depotFrom && data.depotFrom.id > 0 ? new Depot(data.depotFrom) : undefined
    this.depotTo = data.depotTo && data.depotTo.id > 0 ? new Depot(data.depotTo) : undefined
    this.moves = data.moves && data.moves.length > 0 ? data.moves.map(operation => new ProductMove(operation)) : []
    this.order = data.order ? new Order(data.order) : undefined
    this.purchasePrice = data.purchasePrice ? data.purchasePrice : 0
    this.createdBy = data.createdBy && data.createdBy.id > 0 ? new User(data.createdBy) : undefined
    this.files = data.files && data.files.length > 0 ? data.files.map(f => new File(f)) : []
  }
}
