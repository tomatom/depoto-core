import {
  Category,
  Currency,
  File,
  Producer,
  ProductBundle,
  ProductDepot,
  ProductPack,
  ProductParameter,
  ProductPrice,
  ProductPriceLevel,
  Quantity,
  SellItem,
  Supplier,
  Tag,
  Vat,
} from './'
import { PropType } from '../utils'

export class Product {
  id: number
  @PropType('ProductDepot')
  depots: ProductDepot[]
  name: string
  fullName: string
  ean: string
  ean2: string
  ean3: string
  ean4: string
  ean5: string
  code: string
  externalId: string
  @PropType('Currency')
  purchaseCurrency: Currency
  purchasePrice: number
  sellPrice: number
  sellPriceWithoutVat: number
  beforeSellPrice: number
  actualSellPrice: number
  actualSellPriceWithoutVat: number
  actualBeforeSellPrice: number
  weight: number
  enabled: boolean
  @PropType('Product')
  parent?: Product
  @PropType('Product')
  children: Product[]
  isBundle: boolean
  isFragile: boolean
  isOversize: boolean
  @PropType('ProductBundle')
  bundleParents: ProductBundle[]
  @PropType('ProductBundle')
  bundleChildren: ProductBundle[]
  @PropType('Producer')
  producer?: Producer
  @PropType('Vat')
  vat: Vat
  @PropType('Supplier')
  supplier?: Supplier
  isForSubsequentSettlement: boolean
  @PropType('File')
  mainImage?: File
  @PropType('File')
  images: File[]
  @PropType('File')
  files: File[]
  @PropType('ProductPrice')
  advancedPrices: ProductPrice[]
  @PropType('ProductPack')
  packs: ProductPack[]
  @PropType('ProductParameter')
  productParameters: ProductParameter[]
  quantityUnavailable: number
  quantityStock: number
  quantityAvailable: number
  quantityReservation: number
  note: string
  description: string
  customData: string
  @PropType('Category')
  categories: Category[]
  @PropType('Category')
  mainCategory?: Category
  @PropType('ProductPriceLevel')
  productPriceLevels: ProductPriceLevel[]
  @PropType('Tag')
  tags: Tag[]
  @PropType('local')
  orderQuantity: number
  @PropType('SellItem')
  sellItems: SellItem[]
  isDeletable: boolean
  dimensionX: number
  dimensionY: number
  dimensionZ: number
  originCountry: string
  hsCode: string
  @PropType('Quantity')
  quantities: Quantity[]

  // expirationDate: string // todo update from picker models

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.depots = data.depots && data.depots.length > 0 ? data.depots.map(pd => new ProductDepot(pd)) : []
    this.name = data.name || ''
    this.fullName = data.fullName || ''
    this.ean = data.ean || ''
    this.ean2 = data.ean2 || ''
    this.ean3 = data.ean3 || ''
    this.ean4 = data.ean4 || ''
    this.ean5 = data.ean5 || ''
    this.code = data.code || ''
    this.externalId = data.externalId || ''
    this.purchaseCurrency = !!data.purchaseCurrency
      ? new Currency(data.purchaseCurrency)
      : new Currency({ id: 'CZK', name: 'Česká koruna', ratio: 1 })
    this.purchasePrice = data.purchasePrice || 0
    this.sellPrice = data.sellPrice || 0
    this.sellPriceWithoutVat = data.sellPriceWithoutVat || 0
    this.beforeSellPrice = data.beforeSellPrice || null
    this.actualSellPrice = data.actualSellPrice || 0
    this.actualSellPriceWithoutVat = data.actualSellPriceWithoutVat || 0
    this.actualBeforeSellPrice = data.actualBeforeSellPrice || 0
    this.weight = data.weight || null
    this.enabled = data.enabled || false
    this.parent = data.parent || null
    this.children = data.children && data.children.length > 0 ? data.children.map(child => new Product(child)) : []
    this.isBundle = data.isBundle || false
    this.isFragile = data.isFragile || false
    this.isOversize = data.isOversize || false
    this.bundleParents =
      data.bundleParents && data.bundleParents.length > 0
        ? data.bundleParents.map(parent => new ProductBundle(parent))
        : []
    this.bundleChildren =
      data.bundleChildren && data.bundleChildren.length > 0
        ? data.bundleChildren.map(child => new ProductBundle(child))
        : []
    this.producer = data.producer ? new Producer(data.producer) : undefined
    this.vat = new Vat(data.vat) || new Vat()
    this.supplier = data.supplier ? new Supplier(data.supplier) : undefined
    this.isForSubsequentSettlement = data.isForSubsequentSettlement || false
    this.mainImage = data.mainImage ? new File(data.mainImage) : undefined
    this.images = data.images && data.images.length > 0 ? data.images.map(img => new File(img)) : []
    this.files = data.files && data.files.length > 0 ? data.files.map(f => new File(f)) : []
    this.advancedPrices =
      data.advancedPrices && data.advancedPrices.length > 0 ? data.advancedPrices.map(p => new ProductPrice(p)) : []
    // this.expirationDate = data.expirationDate || ''
    this.packs = data.packs && data.packs.length > 0 ? data.packs.map(p => new ProductPack(p)) : []
    this.productParameters =
      data.productParameters && data.productParameters.length > 0
        ? data.productParameters.map(p => new ProductParameter(p))
        : []
    this.quantityUnavailable = data.quantityUnavailable !== 0 ? Number(data.quantityUnavailable) : 0
    this.quantityStock = data.quantityStock !== 0 ? Number(data.quantityStock) : 0
    this.quantityReservation = data.quantityReservation !== 0 ? Number(data.quantityReservation) : 0
    this.quantityAvailable = data.quantityAvailable !== 0 ? Number(data.quantityAvailable) : 0
    this.note = data.note && data.note.length > 0 ? data.note : ''
    this.description = data.description || ''
    this.customData = data.customData || ''
    this.categories =
      data.categories && data.categories.length > 0 ? data.categories.map(category => new Category(category)) : []
    this.mainCategory = data.mainCategory ? new Category(data.mainCategory) : undefined
    this.productPriceLevels =
      data.productPriceLevels && data.productPriceLevels.length > 0
        ? data.productPriceLevels.map(productPriceLevel => new ProductPriceLevel(productPriceLevel))
        : []
    this.tags = data.tags && data.tags.length > 0 ? data.tags.map(tag => new Tag(tag)) : []
    this.orderQuantity = data.orderQuantity || 1
    this.sellItems =
      data.sellItems && data.sellItems.length > 0 ? data.sellItems.map(sellItem => new SellItem(sellItem)) : []
    this.isDeletable = data.isDeletable || false
    this.dimensionX = data.dimensionX || 0
    this.dimensionY = data.dimensionY || 0
    this.dimensionZ = data.dimensionZ || 0
    this.originCountry = data.originCountry || ''
    this.hsCode = data.hsCode || ''
    this.quantities = data.quantities ? data.quantities.map(q => new Quantity(q)) : []
  }
}
