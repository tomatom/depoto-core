import { Vat } from './'
import { PropType } from '../utils'

export class VatAllocation {
  @PropType('Vat')
  vat?: Vat
  priceWithoutVat: number
  vatPrice: number
  price: number

  constructor(data: any = {}) {
    this.vat = data.vat ? new Vat(data.vat) : undefined
    this.priceWithoutVat = data.priceWithoutVat !== 0 ? data.priceWithoutVat : 0
    this.vatPrice = data.vatPrice !== 0 ? data.vatPrice : 0
    this.price = data.price !== 0 ? data.price : 0
  }
}
