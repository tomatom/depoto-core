import { Depot } from './Depot'

export class Quantity {
  depot?: Depot
  quantityAvailable: number
  quantityReservation: number
  quantityStock: number

  constructor(data: any = {}) {
    this.depot = data.depot ? new Depot(data.depot) : undefined
    this.quantityAvailable = data.quantityAvailable || 0
    this.quantityReservation = data.quantityReservation || 0
    this.quantityStock = data.quantityStock || 0
  }
}
