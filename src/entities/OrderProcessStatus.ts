import { ProcessStatus, User } from './'
import { PropType } from '../utils'

export class OrderProcessStatus {
  @PropType('ProcessStatus')
  status: ProcessStatus
  note: string
  created: string
  @PropType('User')
  createdBy?: User

  constructor(data: any = {}) {
    this.status = data.status && data.status.id ? data.status : null
    this.note = data.note || ''
    this.created = data.created || ''
    this.createdBy = data.createdBy && data.createdBy.id > 0 ? new User(data.createdBy) : undefined
  }
}
