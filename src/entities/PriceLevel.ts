import { PropType } from '../utils'
import { Currency } from './Currency'

export class PriceLevel {
  id: number
  name: string
  isPercentage: boolean
  percent: number
  withVat: boolean
  @PropType('Currency')
  currency?: Currency
  externalId: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
    this.isPercentage = data.isPercentage || false
    this.percent = data.percent || 0
    this.withVat = data.withVat || null
    this.currency = data.currency ? new Currency(data.currency) : undefined
    this.externalId = data.externalId || ''
  }
}
