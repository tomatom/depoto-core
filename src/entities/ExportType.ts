export class ExportType {
  id: number
  name: string
  format: string
  group: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
    this.format = data.format || ''
    this.group = data.group || ''
  }
}
