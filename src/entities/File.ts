import { Product, Order, FileThumbnail } from '../entities'
import { PropType } from '../utils'

export class File {
  id: number
  text: string
  originalFilename: string
  main: boolean
  mimeType: string
  @PropType('local')
  base64Data: string
  size: number
  url: string
  @PropType('Product')
  product?: Product
  @PropType('Order')
  order?: Order
  @PropType('FileThumbnail')
  thumbnails: FileThumbnail[]

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.text = data.text || ''
    this.originalFilename = data.originalFilename || ''
    this.main = data.main || false
    this.mimeType = data.mimeType || ''
    this.base64Data = data.base64Data || ''
    this.size = data.size || 0
    this.url = data.url || ''
    this.product = data.product && data.product.id > 0 ? new Product(data.product) : undefined
    this.order = data.order && data.order.id > 0 ? new Order(data.order) : undefined
    this.thumbnails =
      data.thumbnails && data.thumbnails.length > 0 ? data.thumbnails.map(t => new FileThumbnail(t)) : []
  }
}
