import { Checkout, Payment, Voucher } from './'
import { generateUuidV4, PropType } from '../utils'

export class PaymentItem {
  id: number
  @PropType('local')
  uuid?: string
  @PropType('Checkout')
  checkout?: Checkout
  @PropType('Payment')
  payment?: Payment
  @PropType('Voucher')
  voucher?: Voucher
  dateCreated: string
  dateCancelled: string
  amount: number
  isPaid: boolean
  datePaid: string
  externalId: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.uuid = data.uuid || generateUuidV4()
    this.checkout = data.checkout ? new Checkout(data.checkout) : undefined
    this.payment = data.payment ? new Payment(data.payment) : undefined
    this.voucher = data.voucher ? new Voucher(data.voucher) : undefined
    this.dateCreated = data.dateCreated || new Date().toUTCString()
    this.dateCancelled = data.dateCancelled || null
    this.amount = data.amount || 0
    this.isPaid = data.isPaid ?? true
    this.datePaid = data.datePaid || null
    this.externalId = data.externalId || ''
  }
}
