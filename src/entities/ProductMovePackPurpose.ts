export class ProductMovePackPurpose {
  id: string // 'default' | 'internal'
  name: string

  constructor(data: any = {}) {
    if (!!data && typeof data === 'string') {
      this.id = data
      this.name = data
    } else if (!!data && !!data.id) {
      this.id = data.id || 'default'
      this.name = data.name || 'default'
    } else {
      this.id = 'default'
      this.name = 'default'
    }
  }
}
