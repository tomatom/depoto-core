import { Checkout, Order, User, VatAllocation } from './'
import { PropType } from '../utils'

export class CheckoutClosing {
  id: number
  @PropType('Checkout')
  checkout?: Checkout
  @PropType('local')
  checkoutId: number
  @PropType('User')
  userOpen?: User
  dateOpen: string
  noteOpen: string
  amountRealOpen: number
  amountOpen: number
  @PropType('User')
  userClosed?: User
  dateClosed: string
  noteClosed: string
  amountClosed: number
  amountRealClosed: number
  @PropType('Order')
  orders: Order[]
  @PropType('VatAllocation')
  vatAllocations: VatAllocation[]
  billUrl: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.checkout = data.checkout ? new Checkout(data.checkout) : undefined
    this.checkoutId = data.checkoutId || null
    this.userOpen = data.userOpen ? new User(data.userOpen) : undefined
    this.dateOpen = data.dateOpen || null
    this.noteOpen = data.noteOpen || ''
    this.amountOpen = data.amountOpen || 0
    this.amountRealOpen = data.amountRealOpen || 0
    this.userClosed = data.userClosed ? new User(data.userClosed) : undefined
    this.dateClosed = data.dateClosed || null
    this.noteClosed = data.noteClosed || ''
    this.amountClosed = data.amountClosed || 0
    this.amountRealClosed = data.amountRealClosed || 0
    this.orders = data.orders && data.orders.length > 0 ? data.orders.map(order => new Order(order)) : []
    this.vatAllocations =
      data.vatAllocations && data.vatAllocations.length > 0 ? data.vatAllocations.map(va => new VatAllocation(va)) : []
    this.billUrl = data.billUrl
  }
}
