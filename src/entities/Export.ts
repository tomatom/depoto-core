import { ExportType, User } from './'
import { PropType } from '../utils'

export class Export {
  id: number
  generated: string
  created: string
  url: string
  @PropType('ExportType')
  type?: ExportType
  filter: string
  @PropType('User')
  createdBy?: User
  note: string

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.generated = data.generated || ''
    this.created = data.created || ''
    this.url = data.url || ''
    this.type =
      data.type && typeof data.type === 'string'
        ? new ExportType({ id: data.type })
        : typeof data.type === 'object'
        ? new ExportType(data.type)
        : undefined
    this.filter = data.filter || ''
    this.createdBy = data.createdBy ? new User(data.createdBy) : undefined
    this.note = data.note || ''
  }
}
