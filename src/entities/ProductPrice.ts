import { Product } from './'
import { PropType } from '../utils'

export class ProductPrice {
  id: number
  @PropType('Product')
  product: Product
  sellPrice: number
  available: number
  dateFrom: string
  dateTo: string
  isActual: boolean
  note: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.product = data.product && data.product.id > 0 ? new Product(data.product) : new Product()
    this.sellPrice = data.sellPrice || 0
    this.available = data.available || 0
    this.dateFrom = data.dateFrom || ''
    this.dateTo = data.dateTo || ''
    this.isActual = data.isActual || false
    this.note = data.note || ''
  }
}
