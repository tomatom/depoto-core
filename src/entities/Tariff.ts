export class Tariff {
  id: number
  name: string
  const: string
  position: number
  cod: boolean

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
    this.const = data.const ? data.const : data.constant ? data.constant : ''
    this.position = data.position || 0
    this.cod = data.cod || false
  }
}
