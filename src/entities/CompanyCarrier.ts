import { Carrier, Checkout } from './'
import { PropType } from '../utils'

export class CompanyCarrier {
  id: number
  @PropType('local')
  name: string // local helper!
  enable: boolean
  options: string
  @PropType('Carrier')
  carrier?: Carrier
  @PropType('Checkout')
  checkout?: Checkout
  externalId: string

  constructor(data: any = {}) {
    this.id = data.id || null
    this.enable = data.enable || true
    this.options = data.options || ''
    this.carrier = data.carrier ? new Carrier(data.carrier) : undefined
    this.checkout = data.checkout ? new Checkout(data.checkout) : undefined
    this.externalId = data.externalId || ''

    this.name = this.carrier && this.carrier.name ? this.carrier.name : ''
  }
}
