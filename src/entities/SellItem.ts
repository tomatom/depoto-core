import { Product } from './Product'
import { ProductDepot } from './ProductDepot'
import { PropType } from '../utils'

export class SellItem {
  id: string
  @PropType('Product')
  product?: Product
  quantityStock: number
  quantityReservation: number
  quantityAvailable: number
  expirationDate: string
  batch: string
  @PropType('ProductDepot')
  productDepots: ProductDepot[]
  @PropType('local')
  orderQuantity: number

  constructor(data: any = {}) {
    this.id = data.id || null
    this.product = data.product ? new Product(data.product) : undefined
    this.quantityStock = data.quantityStock || 0
    this.quantityReservation = data.quantityReservation || 0
    this.quantityAvailable = data.quantityAvailable || 0
    this.expirationDate = data.expirationDate || ''
    this.batch = data.batch || ''
    this.productDepots =
      data.productDepots && data.productDepots.length > 0
        ? data.productDepots.map(productDepot => new ProductDepot(productDepot))
        : []
    this.orderQuantity = data.orderQuantity || 1
  }
}
