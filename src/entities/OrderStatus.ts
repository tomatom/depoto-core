export class OrderStatus {
  id: string
  name: string

  constructor(data: any = {}) {
    this.id = data.id || 'reservation'
    this.name = data.name && data.name.length > 0 ? data.name : ''
  }
}
