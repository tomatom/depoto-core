import { Currency } from './'
import { PropType } from '../utils'

export class Price {
  @PropType('Currency')
  currency?: Currency
  value: number

  constructor(data: any = {}) {
    this.currency = data.currency ? new Currency(data.currency) : undefined
    this.value = data.value ? data.value : 0
  }
}
