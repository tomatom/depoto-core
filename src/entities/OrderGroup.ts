import { ClearanceItem, Company, Order, User } from './'
import { PropType } from '../utils'

export class OrderGroup {
  id: number
  name: string
  @PropType('Company')
  company?: Company
  @PropType('Order')
  orders: Order[]
  @PropType('User')
  user?: User
  userPosition: number
  @PropType('ClearanceItem')
  clearanceItems: ClearanceItem[]

  constructor(data: any = {}) {
    this.id = data.id || 0
    this.name = data.name || ''
    this.company = !!data.company ? new Company(data.company) : undefined
    this.orders = !!data.orders && data.orders.length > 0 ? data.orders.map(o => new Order(o)) : []
    this.user = !!data.user ? new User(data.user) : undefined
    this.userPosition = data.userPosition || 0
    this.clearanceItems =
      !!data.clearanceItems && data.clearanceItems.length > 0 ? data.clearanceItems.map(c => new ClearanceItem(c)) : []
  }
}
