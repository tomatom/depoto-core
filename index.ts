import {
  EntityServiceConfiguration,
  HttpService as IHttpService,
  ServiceContainer
} from './src/interfaces'
import { OAuthSession } from './src/models'
import {
  FetchService,
  KeyEventService,
  LOGGER_TYPE,
  LoggerService,
  OAuthService,
  StorageService,
} from './src/services'
import {
  AddressService,
  BaseEventService,
  CarrierService,
  CategoryService,
  CheckoutService,
  CompanyService,
  CompanyCarrierService,
  ConsentService,
  CustomerService,
  DepotService,
  DownloadService,
  EETCertService,
  EETReceiptService,
  EETShopService,
  ExportService,
  FileService,
  InventoryExportService,
  OrderExportService,
  OrderService,
  PackageService,
  ParameterService,
  PaymentService,
  PriceLevelService,
  ProducerService,
  ProductBundleService,
  ProductMovePackService,
  ProductPackService,
  ProductParameterService,
  ProductMoveService,
  ProductPriceLevelService,
  ProductPriceService,
  ProductService,
  PurchaseExportService,
  SaleExportService,
  StatsService,
  StockExportService,
  SoldItemService,
  SupplierService,
  TagService,
  UserGroupService,
  UserRoleService,
  UserService,
  VatService,
  VoucherService,
} from './src/services/entities'
import { ENDPOINT, Observable } from './src/utils'

import 'reflect-metadata'


class DepotoCore {
  private readonly loggerService: LoggerService
  private readonly storageService: StorageService
  private readonly oauthService: OAuthService
  private readonly httpService: IHttpService
  private readonly keyEventService?: KeyEventService
  // private readonly notificationService: NotificationService // todo notification service from client, implement in other apps
  // entity services:
  private readonly addressService: AddressService
  private readonly baseEventService: BaseEventService
  private readonly carrierService: CarrierService
  private readonly categoryService: CategoryService
  private readonly checkoutService: CheckoutService
  private readonly companyService: CompanyService
  private readonly companyCarrierService: CompanyCarrierService
  private readonly consentService: ConsentService
  private readonly customerService: CustomerService
  private readonly depotService: DepotService
  private readonly downloadService: DownloadService
  private readonly eetCertService: EETCertService
  private readonly eetReceiptService: EETReceiptService
  private readonly eetShopService: EETShopService
  private readonly exportService: ExportService
  private readonly fileService: FileService
  private readonly inventoryExportService: InventoryExportService
  private readonly orderExportService: OrderExportService
  private readonly orderService: OrderService
  private readonly packageService: PackageService
  private readonly parameterService: ParameterService
  private readonly paymentService: PaymentService
  private readonly priceLevelService: PriceLevelService
  private readonly producerService: ProducerService
  private readonly productBundleService: ProductBundleService
  private readonly productMovePackService: ProductMovePackService
  private readonly productPackService: ProductPackService
  private readonly productMoveService: ProductMoveService
  private readonly productParameterService: ProductParameterService
  private readonly productPriceLevelService: ProductPriceLevelService
  private readonly productPriceService: ProductPriceService
  private readonly productService: ProductService
  private readonly purchaseExportService: PurchaseExportService
  private readonly saleExportService: SaleExportService
  private readonly soldItemService: SoldItemService
  private readonly statsService: StatsService
  private readonly stockExportService: StockExportService
  private readonly supplierService: SupplierService
  private readonly tagService: TagService
  private readonly userGroupService: UserGroupService
  private readonly userRoleService: UserRoleService
  private readonly userService: UserService
  private readonly vatService: VatService
  private readonly voucherService: VoucherService

  constructor(
    appPrefix: string,
    session: OAuthSession|null = null,
    httpService: IHttpService|null = null,
    loggerType: string = LOGGER_TYPE.CONSOLE,
    loggerFilters: string[] = []
  ) {
    this.loggerService = new LoggerService(loggerType, loggerFilters)
    this.storageService = new StorageService(this.loggerService, appPrefix)
    this.oauthService = new OAuthService(this.loggerService, this.storageService, session)
    if (httpService) {
      this.httpService = httpService
    } else {
      this.httpService = new FetchService(this.loggerService, this.oauthService)
    }
    if ('addEventListener' in window) {
      this.keyEventService = new KeyEventService(this.loggerService, true)
    } else {
      this.keyEventService = undefined
    }

    const config: EntityServiceConfiguration = {
      loggerService: this.loggerService,
      s: this.storageService,
      oauthService: this.oauthService,
      httpService: this.httpService,
      notificationEventObservable: new Observable('core:notificationEventObservable'),
    }
    this.vatService = new VatService(config)
    this.userService = new UserService(config)
    this.addressService = new AddressService(config)
    this.baseEventService = new BaseEventService(config)
    this.carrierService = new CarrierService(config)
    this.categoryService = new CategoryService(config)
    this.checkoutService = new CheckoutService(config)
    this.companyService = new CompanyService(config)
    this.companyCarrierService = new CompanyCarrierService(config)
    this.consentService = new ConsentService(config)
    this.customerService = new CustomerService(config)
    this.depotService= new DepotService(config)
    this.downloadService = new DownloadService(config)
    this.eetCertService = new EETCertService(config)
    this.eetReceiptService = new EETReceiptService(config)
    this.eetShopService = new EETShopService(config)
    this.exportService = new ExportService(config)
    this.fileService = new FileService(config)
    this.inventoryExportService = new InventoryExportService(config);
    this.orderExportService = new OrderExportService(config);
    this.orderService = new OrderService(config, this.vatService)
    this.packageService = new PackageService(config)
    this.parameterService = new ParameterService(config)
    this.paymentService = new PaymentService(config)
    this.priceLevelService = new PriceLevelService(config)
    this.producerService = new ProducerService(config)
    this.productBundleService = new ProductBundleService(config)
    this.productMovePackService = new ProductMovePackService(config)
    this.productPackService = new ProductPackService(config)
    this.productMoveService = new ProductMoveService(config)
    this.productParameterService = new ProductParameterService(config)
    this.productPriceLevelService = new ProductPriceLevelService(config)
    this.productPriceService = new ProductPriceService(config)
    this.productService = new ProductService(config)
    this.purchaseExportService = new PurchaseExportService(config);
    this.saleExportService = new SaleExportService(config);
    this.soldItemService = new SoldItemService(config)
    this.statsService = new StatsService(config)
    this.stockExportService = new StockExportService(config);
    this.supplierService = new SupplierService(config)
    this.tagService = new TagService(config)
    this.userGroupService = new UserGroupService(config)
    this.userRoleService = new UserRoleService(config)
    this.voucherService = new VoucherService(config)
  }

  get services(): ServiceContainer {
    return {
      baseEvent: this.baseEventService,
      keyEvent: this.keyEventService,
      logger: this.loggerService,
      storage: this.storageService,
      oauth: this.oauthService,
      http: this.httpService,
      address: this.addressService,
      carrier: this.carrierService,
      category: this.categoryService,
      checkout: this.checkoutService,
      company: this.companyService,
      companyCarrier: this.companyCarrierService,
      consent: this.consentService,
      customer: this.customerService,
      depot: this.depotService,
      download: this.downloadService,
      eetCert: this.eetCertService,
      eetReceipt: this.eetReceiptService,
      eetShop: this.eetShopService,
      export: this.exportService,
      file: this.fileService,
      inventoryExport: this.inventoryExportService,
      orderExport: this.orderExportService,
      order: this.orderService,
      pack: this.packageService,
      parameter: this.parameterService,
      payment: this.paymentService,
      priceLevel: this.priceLevelService,
      producer: this.producerService,
      productBundle: this.productBundleService,
      productMovePack: this.productMovePackService,
      productPack: this.productPackService,
      productMove: this.productMoveService,
      productParameter: this.productParameterService,
      productPriceLevel: this.productPriceLevelService,
      productPrice: this.productPriceService,
      product: this.productService,
      purchaseExport: this.purchaseExportService,
      saleExport: this.saleExportService,
      soldItem: this.soldItemService,
      stats: this.statsService,
      stockExport: this.stockExportService,
      supplier: this.supplierService,
      tag: this.tagService,
      userGroup: this.userGroupService,
      userRole: this.userRoleService,
      user: this.userService,
      vat: this.vatService,
      voucher: this.voucherService
    }
  }

  get endpoints(): any {
    return ENDPOINT
  }

  // todo proper tests
  // test(skipDownloads: boolean, stopOnError: boolean = false, showTestResponses: boolean = false) {
  //   const t = new TestingUtil(this.services)
  //   t.setStopOnError(stopOnError)
  //   t.setShowTestResponses(showTestResponses)
  //   t.clearTestCounts()
  //   return t.testAddress().then(() => {
  //     return t.testCarrier()
  //   }).then(() => {
  //     return t.testCheckout()
  //   }).then(() => {
  //     return t.testConsent()
  //   }).then(() => {
  //     return t.testCustomer()
  //   }).then(() => {
  //     return t.testDepot()
  //   }).then(() => {
  //     if (skipDownloads) {
  //       return Promise.resolve()
  //     } else {
  //       return t.testDownload()
  //     }
  //   }).then(() => {
  //     return t.testEETCert()
  //   }).then(() => {
  //     return t.testEETReceipt()
  //   }).then(() => {
  //     return t.testEETShop()
  //   }).then(() => {
  //     return t.testFile()
  //   }).then(() => {
  //     return t.testOrder()
  //   }).then(() => {
  //     return t.testPayment()
  //   }).then(() => {
  //     return t.testProducer()
  //   }).then(() => {
  //     return t.testProduct()
  //   }).then(() => {
  //     return t.testProductBundle()
  //   }).then(() => {
  //     return t.testStats()
  //   }).then(() => {
  //     return t.testSupplier()
  //   }).then(() => {
  //     return t.testUserRole()
  //   }).then(() => {
  //     return t.testUser()
  //   }).then(() => {
  //     return t.testUserGroup()
  //   }).then(() => {
  //     return t.getTestCounts()
  //   })
  // }
}

export default DepotoCore
